# e d -> p n pi0 Experiment model for "Deuteron" group at VEPP-3 storage

## The Problem Overview

This application represents a bunch of utilities that provide some statistical
models for experiment, planned on VEPP-3 storage ring (NPI, Novosibirsk, 2015).

The main task of these utilities is to estimate several practical-meaning
criteria for event separation from two concurrent channels that are selected
from all possible photonuclear processes by technical implementation:

 1)  e d -> p n pi0  (pi0-meson photoproduction)
 2)  e d -> p n      (deuteron photodisintegration)

We're curently interested only in pi0-photoproduction mechanism. However,
technical implementation of experimental setup needs to be corrected
according to relative contribution of these two processes.

The pi0-meson has extremely short life-time after that it decays on two photons
that are unregistered, so we're detecting only p,n-pair by two-armed detector.

Obvious and stright-forward way is to separate this reactions by angular
correlations. Some preliminary estimations provided before, however,
demonstrates that this approach is insufficient due to large contribution
of photodisintegration process.

## Equivalent Photon Approximation

Speaking of electronuclear reactions one can approximate all processes using
so-called *Equivalent Photon Approximation* full-fledged method (EPA) that was
firstly introduced by Weizsäcker-Williams. The basic idea is to approximate
electromagnetic interaction with equivalent field-photons with predictable
spectra. We should call this spectrum a *Dalitz-Yennie spectrum* by authors
of work where we obtain an operable form for these spectrum. Only one-photon
events are considered here.

This form, the (e,d)-interaction turns into (\gamma, d) form with interim
parameter E\_{\gamma}.

## Numerical Procedures

Though the particular detector system modelling is provided by Geant4 model,
we should generate primary events in some way, possibly avoiding a direct
beam-target interaction modelling.

Thereby, in order to estimate relative contributions, we should provide
two primary event generators for those processes.

### (p,n) Generator

Deuteron photodisintegration is a reaction with extremely low threshold that's
approximately equal 2 MeVs. There are numerous works devoted to (p,n)
cross-section investigations above the giant resonance experimentally and
theoretically. We used here experimental data measured by J-Lab.

### (p,n,pi0) Generator

... TODO: further explaination will be added as we move. Feel free to mail
me by any connected questions. Currently we're developing appropriate
wrapper for Arenhovel-Fix model.

## Build

Only out-of-source builds are allowed in order to keep repository clear from
any temporary data.

... TODO: detailes about building options (see CmakeLists.txt's, `option()`).

From current source directory:

    $ mkdir -p ../pn_low.build/debug/
    $ cd ../pn_low.build/debug/
    $ cmake ../../pn_low  # You can append any build options here
    $ make

## Usage

The particular routine inside the whole application can be called by pointing
out one or more `-t \<task\>` parameters. List of available task for current
build with breaf descriptions can be obtained by `--show-tasks` long option.

E.g., for parsing (p,n) data from files, located in `contrib/data/`, approximating
it and to store result in `pngrid.root`:

    $ ./pn_low -t pn-parse --pnCS.infile ../../pn_low/contrib/data/pn_cs.dat \
        -t pn-store-approximated --pnCS.noApproxGraphCtr=no \
        --pnCS.regularApproxNE=150 --pnCS.regularApproxNA=35 \
        --pnCS.gridFile="pngrid.root"

Tasks will be called sequentially, in order of appearance.

One can always obtain help for current build by:

    $ ./pn_low -h

# License

All source codes here, excluding content of `./contrib` directory,
should be redistributed under MIT license terms. For licensing of
contributed content see appropriate licensing informations.

