# include "app_config.h"
# include "app.hpp"

# include <cstring>
# include <cstdlib>
# include <iostream>
# include <string>
# include <locale>
# include <map>
# include <boost/regex.hpp>
# include <forward_list>
# include <fstream>
# include <iomanip>

# include <root/TGraph2DErrors.h>
# include <root/TCanvas.h>
# include <root/TApplication.h>
# include <root/TFile.h>
# include <boost/filesystem.hpp>

# include "pn_cross_section_parse.hpp"

# ifdef PN_LOW_EXPDAT_GEN

namespace pnLow {

// A physical units table.
// One can add units here according to needs of .dat-files.
// Units should be listed at the beginning of each .dat file (mandatory),
// before any actual data. Fixed order for them is:
// <value (cross-section) units>, <row-header (energetic) units>, <column (angular) units>
// Avoid the whitespace and comma symbols in units names.

static const struct
UnitsDictionaryEntry {
    const char name[16];
    double factor;
} unitsDictionary[] = {
    // Cross-section
    { "nb/sterad",  1           },
    { "mb/sterad",  1e3         },
    // Energy
    { "MeV",        1           },
    { "GeV",        1e3         },
    // Angle
    { "degrees",    1           },
    { "rad",        180./M_PI   },
    // Sentinel:
    { "",           NAN         },
};

// 0 -- ok, all units found, -1 -- error occured
int set_units( const std::string & tail );

// A primitive .dat file parser for cross section tables. Acquires data
// enlisted in planar table form filling the plain std::forward_set with
// instances of CrossSectionDataPoint.

static struct ParserState {
    CrossSectionDataPoint           current;  // operational instance
    std::set<CrossSectionDataPoint*> parsed;  // parsed entries storaging set
    std::set<CrossSectionDataPoint*> reconstructed;  // points to be reconstructed
    std::map<double, std::map<double, CrossSectionDataPoint*> >  _byEnergies;  // points sorted by energies
    std::map<double, std::map<double, CrossSectionDataPoint*> >  _byAngles;    // points sorted by angles

    std::forward_list<double> columns;
    std::forward_list<double>::iterator currentColumn;

    double currentRow;
    unsigned short nLine;

    union {
        struct {
            const UnitsDictionaryEntry * crossSection;
            const UnitsDictionaryEntry * eGamma;
            const UnitsDictionaryEntry * thetaP;
        } units;
        const UnitsDictionaryEntry * uptr[3];
    };

    int (*node_parser)( const std::string & );

    void init( bool keepParsedData=false ) {
        if( !keepParsedData ) {
            clear_parsed();
            parsed.clear();
            columns.clear();
        }
        currentColumn = columns.end();
        bzero( &current, sizeof(CrossSectionDataPoint) );
        currentRow = NAN;
        nLine = 0;
        units = { nullptr, nullptr, nullptr };
        node_parser = nullptr;
    }

    void clear_parsed() {
        _byEnergies.clear();
        _byAngles.clear();
        for( auto it = parsed.begin();
                  it != parsed.end(); ++it ) {
            free( *it );
        }
        for( auto it = reconstructed.begin();
                  it != reconstructed.end(); ++it ) {
            free( *it );
        }
    }

    WangApproximant<2> wangApproximant;
} pState;

void clear_database() {
    pState.init();
}

const std::map<double, std::map<double, CrossSectionDataPoint*> > &
get_energies_subsets() {
    return pState._byEnergies;
}

const std::map<double, std::map<double, CrossSectionDataPoint*> > &
get_angular_subsets() {
    return pState._byAngles;
}

//
//
//

int  // acquires data point in form: cross section, statistical error, systematic error
parse_triple_data_point( const std::string & triplet ) {
    const double valFactor = pState.units.crossSection->factor;
    boost::regex re(",");
    boost::sregex_token_iterator it(triplet.begin(), triplet.end(), re, -1);

    if( "--" == it->str() ) {
        std::cerr << "[W]"
                  << "No data for point E="
                  << pState.current.energy
                  << ", \\theta="
                  << *(pState.currentColumn)
                  << std::endl;
        //
        CrossSectionDataPoint * newEntry = (CrossSectionDataPoint *)
            malloc( sizeof(CrossSectionDataPoint) );
        bzero( newEntry, sizeof(CrossSectionDataPoint) );
        newEntry->energy = pState.current.energy;
        newEntry->angle  = *(pState.currentColumn);
        pState.reconstructed.insert(newEntry);
        //
        ++ pState.currentColumn;
        return 0;
    }

    CrossSectionDataPoint & point = pState.current;

    point.value = atof((*it++).str().c_str()) * valFactor;
    point.errors.statistical = atof((*it++).str().c_str()) * valFactor;
    point.errors.systematic  = atof((*it++).str().c_str()) * valFactor;
    point.angle = *(pState.currentColumn);
    ++ pState.currentColumn;
    //std::cout << "Parsed point: " << point.value << std::endl;  // XXX
    CrossSectionDataPoint * newEntry = (CrossSectionDataPoint *)
        malloc( sizeof(CrossSectionDataPoint) );
    memcpy( newEntry, &(pState.current), sizeof(CrossSectionDataPoint) );
    pState.parsed.insert( newEntry );
    if( pState._byEnergies.end() == pState._byEnergies.find(pState.current.energy) ) {
        pState._byEnergies[pState.current.energy] =
                std::map<double, CrossSectionDataPoint *>();
    }
    if( pState._byAngles.end() == pState._byAngles.find(pState.current.angle) ) {
        pState._byAngles[pState.current.angle] =
                std::map<double, CrossSectionDataPoint *>();
    }
    pState._byEnergies[pState.current.energy][pState.current.angle] = newEntry;
    pState._byAngles[pState.current.angle][pState.current.energy] = newEntry;
    return 1;
}

int  // acquires data point in form: cross section, statistical error, systematic error
parse_double_data_point( const std::string & dublet ) {
    const double valFactor = pState.units.crossSection->factor;
    boost::regex re(",");
    boost::sregex_token_iterator it(dublet.begin(), dublet.end(), re, -1);

    if( "--" == it->str() ) {
        std::cerr << "[W]"
                  << "No data for point E="
                  << pState.current.energy
                  << ", \\theta="
                  << *(pState.currentColumn)
                  << std::endl;
        //
        CrossSectionDataPoint * newEntry = (CrossSectionDataPoint *)
            malloc( sizeof(CrossSectionDataPoint) );
        bzero( newEntry, sizeof(CrossSectionDataPoint) );
        newEntry->energy = pState.current.energy;
        newEntry->angle  = *(pState.currentColumn);
        pState.reconstructed.insert(newEntry);
        //
        ++ pState.currentColumn;
        return 0;
    }

    CrossSectionDataPoint & point = pState.current;

    point.value = atof((*it++).str().c_str()) * valFactor;
    point.errors.statistical = atof((*it++).str().c_str()) * valFactor;
    point.errors.systematic  = 0.;
    point.angle = *(pState.currentColumn);
    ++ pState.currentColumn;
    //std::cout << "Parsed point: " << point.value << std::endl;  // XXX
    CrossSectionDataPoint * newEntry = (CrossSectionDataPoint *)
        malloc( sizeof(CrossSectionDataPoint) );
    memcpy( newEntry, &(pState.current), sizeof(CrossSectionDataPoint) );
    pState.parsed.insert( newEntry );
    if( pState._byEnergies.end() == pState._byEnergies.find(pState.current.energy) ) {
        pState._byEnergies[pState.current.energy] =
                std::map<double, CrossSectionDataPoint *>();
    }
    if( pState._byAngles.end() == pState._byAngles.find(pState.current.angle) ) {
        pState._byAngles[pState.current.angle] =
                std::map<double, CrossSectionDataPoint *>();
    }
    pState._byEnergies[pState.current.energy][pState.current.angle] = newEntry;
    pState._byAngles[pState.current.angle][pState.current.energy] = newEntry;
    return 1;
}

int
parse_special( const std::string & which,
               const std::string & tail ) {
    if( "@columns" == which ) {
        pState.columns.clear();
        boost::regex re(",");
        boost::sregex_token_iterator it(tail.begin(), tail.end(), re, -1), jt;
        while( it != jt ) {
            pState.columns.push_front( atof((*it++).str().c_str())
                            *pState.units.thetaP->factor );
        }
        pState.columns.reverse();
        pState.currentColumn = pState.columns.begin();
    } else if( "@nodeParser" == which ) {
        if( "errTriplet" == tail ) {
            if( Application::object().verbosity() > 2 ) {
                std::cout << "Interpreting table as a <value>, <stat err.>, <syst. err>;"
                          << std::endl;
            }
            pState.node_parser = parse_triple_data_point;
        } else if( "errDublet" == tail ) {
            pState.node_parser = parse_double_data_point;
            if( Application::object().verbosity() > 2 ) {
                std::cout << "Interpreting table as a <value>, <stat err.>;"
                          << std::endl;
            }
        } else {
            std::cerr << "[E] "
                      << "Unknown node point parser: \""
                      << tail << "\"."
                      << std::endl;
            return 1;
        }
    } else if( "@units" == which ) {
        if( set_units( tail ) < 0 ) {
            std::cerr << "[E] "
                      << "Directive \""
                      << which
                      << "\" caused an error during setting measurement units. "
                      << "Possible errors ahead."
                      << std::endl;
            return 1;
        }
        return 0;
    } else {
        std::cerr << "[E] "
                  << "Directive \""
                  << which
                  << "\" is unknown or contains a typo."
                  << std::endl;
        return 1;
    }
    return 0;
}

int  // 1 --ok, 0 -- error
parse_data_line( const std::string line ) {
    boost::regex re(";");
    boost::sregex_token_iterator it(line.begin(), line.end(), re, -1);
    boost::sregex_token_iterator jt;

    pState.currentColumn = pState.columns.begin();

    if( !pState.node_parser ) {
        std::cerr << "[E] Node parser callback is unset."
                  << "Use a special nodeParser directive to specify it."
                  << std::endl;
        return 0;
    }

    while(it != jt) {
        if( pState.currentColumn != pState.columns.end() ) {
            //std::cout << "TREATING: " << *it++ << std::endl; // XXX
            if( 0 > pState.node_parser( *it++ ) ) {
                // fatal parsing error
                return 0;
            }
        } else {
            std::cerr << "[E] Columns order mismatch. Check your last @columns directive.";
            return 0;
        }
    }
    return 1;
}

int
parse_cross_section_data(
    std::istream & is ) {

    // A floating point number notation regex match expression.
    // http://www.regular-expressions.info/floatingpoint.html
    const std::string doubleREToken = "[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?";

    // TODO: remove spaces from these regex:
    boost::regex emptyLineRE("^\\s*(#.*)?"),
                 dataLineRE("^\\s*" +
                     doubleREToken +
                     "\\s*:\\s*(("+doubleREToken+")|[\\s,;-])+$"),
                 specialRE("^@\\w+:.+$");

    for( std::string line; !is.eof(); std::getline(is, line), ++pState.nLine ) {
        std::string originalLine = line;
        boost::cmatch isEmptyM,
                      isDataLineM,
                      isSpecialLineM;
        line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
        if( line.empty() || boost::regex_match(line.c_str(), isEmptyM, emptyLineRE) ) {
            // comment or empty string
            continue;
        } else if( boost::regex_match(line.c_str(), isDataLineM, dataLineRE) ) {
            auto colPos = line.find(':');
            pState.current.energy = atof( line.substr(0, colPos).c_str() ) * pState.units.eGamma->factor;
            //std::cout << "Parsing: " << line << std::endl;  // XXX
            if( ! parse_data_line(line.substr(colPos+1)) ) {
                std::cerr << "[E]"
                          << "Error parsing data line "
                          << pState.nLine
                          << ". Aborting."
                          << std::endl;
            }
        } else if( boost::regex_match(line.c_str(), isSpecialLineM, specialRE) ) {
            auto colPos = line.find(':');
            std::string which = line.substr( 0, colPos ),
                        tail  = line.substr( colPos + 1 );
            //std::cout << "SEPC" << which << " --- " << tail << std::endl;  // XXX
            if( parse_special( which, tail ) ) {
                std::cerr << "[W]"
                          << "Parsing special directive at line " << pState.nLine
                          << " failed. Possible errors ahead." << std::endl;
            }
        } else {
            std::cerr << "[W]"
                      << "Omitting malformed line "
                      << pState.nLine
                      << ": \""
                      << originalLine
                      << "\"" << std::endl;
        }
    }

    return 0;  // ok
}

int  // 0 -- ok, all units found, -1 -- error occured
set_units( const std::string & tail ) {
    boost::regex re(",");
    boost::sregex_token_iterator it(tail.begin(), tail.end(), re, -1), jt;
    const UnitsDictionaryEntry ** uptrc = pState.uptr;
    while( it != jt ) {
        std::string name = (*it++).str();
        name.erase(std::remove(name.begin(), name.end(), ';'), name.end());
        if( name.empty() ) continue;

        if( Application::object().verbosity() > 2 ) {
            std::cout << "Looking for units \"" << name
                      << "\"" << std::endl;
        }

        const UnitsDictionaryEntry * found = nullptr;
        for(auto dictEntry = unitsDictionary;
                 !isnan( dictEntry->factor ); ++dictEntry ) {
            if( name == dictEntry->name ) {
                found = dictEntry;
                break;
            }
        }
        if( !found ) {
            std::cerr << "[E] Couldn't understand a measurement units name \""
                      << name
                      << "\"." << std::endl;
            return -1;
        } else {
            *uptrc++ = found;
        }
    }
    return 0;
}

void
print_out_scatter_data( std::ostream & os ) {
    os << "# E, MeV"
       << std::setw(16) << "\\Theta^{p}_{c.m.}, deg"
       << std::setw(16) << "d\\sigma/d\\Omega, nb/sterad"
       << std::setw(16) << "stat. error"
       << std::setw(16) << "syst. error" << std::endl;
    os << std::scientific;
    for( auto it = pState.parsed.begin(); it != pState.parsed.end(); ++it ) {
        os << std::setw(16) << (*it)->energy                 << " "
           << std::setw(16) << (*it)->angle                  << " "
           << std::setw(16) << (*it)->value                  << " "
           << std::setw(16) << (*it)->errors.statistical     << " "
           << std::setw(16) << (*it)->errors.systematic      << std::endl;
    }
}

WangApproximant<2> &
get_pn_approximant_set( ) {
    size_t nDataPoints;
    assert( nDataPoints = pState.parsed.size() );
    (void) nDataPoints;  // suppress unused variable warning on release
    pState.wangApproximant.reserve_data_set( pState.parsed.size() );
    WangApproximant<2>::DataPoint datP;
    for( auto it = pState.parsed.begin();
         pState.parsed.end() != it; ++it ) {
        datP.x[0] = (*it)->energy;
        datP.x[1] = (*it)->angle;
        datP.v = (*it)->value;
        datP.error = (*it)->errors.statistical +
                     (*it)->errors.systematic; // todo: unsure
        pState.wangApproximant.push_data_point(datP);
    }
    return pState.wangApproximant;
}

TGraph2DErrors *
get_graph_repr( size_t nEnergy,
                size_t nAngle,
                bool omitApproximation,
                double eMin, double eMax,
                double aMin, double aMax,
                uint8_t taylorOrder,
                uint8_t polynomialExactness,
                double safety,
                bool logarithmic ) {
    alog3( "Points will be stored asDelaunay interpolation data at TGraph2DErrors ROOT's class.\n" );

    if( Application::object().verbosity() > 1 ) {
        if( nEnergy && nAngle ) {
            std::cout << "# Will generate a grid of "
                      << (int) nEnergy << "×" << (int) nAngle
                      << " points.";
        } else {
            std::cout << "# No grid generation will be provided.";
        }
        std::cout << " Wang's method approximation "
                  << ( omitApproximation ? "disabled" : "enabled" )
                  << "." << std::endl;
    }

    auto S = pnLow::get_pn_approximant_set( );
    TGraph2DErrors * res = nullptr;
    size_t nPoints = 0;

    double stepX1=NAN,
           stepX2=NAN;
    bool regenerateGrid = false;

    // Form Grid:
    if( nEnergy > 0 && nAngle > 0 ) {   // use generated grid
        regenerateGrid = true;
        nPoints = nEnergy*nAngle;

        if( !eMin ) {   eMin = S.x_min()[0]; }
        if( !eMax ) {   eMax = S.x_max()[0]; }

        if( !aMin ) {   aMin = S.x_min()[1]; }
        if( !aMax ) {   aMax = S.x_max()[1]; }

        if( eMax <= eMin ||
            aMax <= aMin ) {
            std::cerr << "Bad ranges is deduced for grid generation." << std::endl
                      << "  energy: " << eMin << " - " << eMax << std::endl
                      << "   angle: " << aMin << " - " << aMax << std::endl
                      << std::endl;
            return nullptr;
        }

        stepX1 = (eMax - eMin)/(nEnergy-1);
        stepX2 = (aMax - aMin)/(nAngle-1);

        if(Application::object().verbosity() > 0 ) {
            std::cout << "Approximation will be invoked for user-specified grid: "
                      << nEnergy << "×" << nAngle 
                      << " within ranges ["
                      << eMin << "..(" << stepX1 << ").." << eMax
                      << "], ["
                      << aMin << "..(" << stepX2 << ").." << aMax
                      << "." << std::endl;
        }

        pnLow::pState.wangApproximant.reserve_poi_set( nPoints );

        for( double x1 =  eMin;
                    x1 <= eMax + stepX1/2; x1 += stepX1 ) {
            for( double x2 =  aMin;
                        x2 <= aMax + stepX2/2; x2 += stepX2 ) {
                S.push_poi( {x1, x2} );
            }
        }
    } else if(  nEnergy == 0 
              && nAngle == 0  ) {  // use grid, defined by original data
        if( eMin || eMax || aMin || aMax ) {
            std::cerr << "Warning: energy or angular ranges specified but will be ignored "
                      << "since original grid have to be used due to nodes numbers = 0."
                      << std::endl;
        }
        if( !pState.reconstructed.empty() && !omitApproximation ) {
            alog1( "Approximation will be invoked on omitted points only.\n" );
            regenerateGrid = false;
            nPoints = pState.reconstructed.size();
            pnLow::pState.wangApproximant.reserve_poi_set( nPoints );
            for( auto it  = pState.reconstructed.begin();
                      it != pState.reconstructed.end(); ++it ) {
                S.push_poi( { (*it)->energy, (*it)->angle } );
            }
        }
        nPoints = pState.parsed.size() +
                  ( omitApproximation ? 0 : pState.reconstructed.size());
    } else {
        aelog( "Invalid condition state for get_graph_repr().\n" );
        return nullptr;
    }

    {
        std::ofstream ofle("/tmp/one.dat", std::ofstream::out);
        S.dump_poi_ascii( ofle );
        ofle.close();
    }

    // Approximate
    if( !S.POIs().empty() && 0 != S.do_approx(
                   taylorOrder,
                   polynomialExactness,
                   safety,
                   logarithmic
        ) ) {
        std::cerr << "POI set is empty or approximation failed." << std::endl;
        return nullptr;
    }

    {
        std::ofstream ofle("/tmp/two.dat", std::ofstream::out);
        S.dump_poi_ascii( ofle );
        ofle.close();
    }

    // Dump approximated
    // fill TGraph2DErrors with POIs
    res = new TGraph2DErrors( nPoints );
    size_t nPoint = 0;
    if( !omitApproximation ) {
        for( auto it  = S.POIs().begin();
                  it != S.POIs().end(); ++it, ++nPoint ) {
            //std::cout << "#" << nPoint << ": "
            //          << "{" << it->x[0] << ", "
            //                 << it->x[1] << "}" << std::endl;  // XXX
            res->SetPoint( nPoint, it->x[0], it->x[1], it->v );
            res->SetPointError( nPoint, stepX1/2, stepX2/2, it->error );
        }
    }
    if( !regenerateGrid ) {
        // use parsed point
        for( auto it  = S.data_points().begin();
                  it != S.data_points().end(); ++it, ++nPoint ) {
            res->SetPoint( nPoint, it->x[0], it->x[1], it->v );
            res->SetPointError( nPoint, 0., 0., it->error );
        }
    }
    if( nEnergy > 0 && nAngle > 0 ) {
        res->SetNpx( nEnergy );
        res->SetNpy( nAngle );
    }

    return res;
}

TGraph2DErrors *
get_graph_repr_vm( const po::variables_map & vm ) {
    return get_graph_repr( vm["pnCS.regularApproxNE"].as<uint16_t>(),
                           vm["pnCS.regularApproxNA"].as<uint16_t>(),
                           vm["pnCS.noApproxGraphCtr"].as<bool>(),
                           vm["Dalitz-Yennie-min"].as<double>(),
                           vm["beamEnergy"].as<double>(),
                           vm["pnCS.regularARangeMin"].as<double>(),
                           vm["pnCS.regularARangeMax"].as<double>(),
                           vm["pnCS.mir-taylorOrder"        ].as<int>(),
                           vm["pnCS.mir-polynomialExactness"].as<int>(),
                           vm["pnCS.mir-safety"             ].as<double>(),
                           vm["pnCS.mir-logarithmic"        ].as<bool>());
}

}  // namespace pnLow

//
// Ctrs/dtrs
//

int
pn_cross_section_parse( const po::variables_map & vm,
                        int, char *[] ) {
    int rc = 0;
    if( !vm.count("pnCS.infile") ) {
        std::cerr << "Option(s) \"--pnCS.infile\" is required for parsing task."
                  << std::endl;
        return 1;
    }
    const std::vector<std::string>
            fileNames = vm["pnCS.infile"].as<std::vector<std::string>>();
    pnLow::pState.init();
    for( auto it = fileNames.begin(); it != fileNames.end(); ++it ) {
        std::ifstream ifle( *it,
                            std::ifstream::in);
        if(ifle) {
            pnLow::pState.init(true);
            if( Application::object().verbosity() > 1 ) {
                std::cout << "Parsing \""
                          << (*it) << "\"..."
                          << std::endl;
            }
            rc |= pnLow::parse_cross_section_data( ifle );
            ifle.close();
            if( Application::object().verbosity() > 1 ) {
                std::cout << "Parsing done." << std::endl;
            }
        } else {
            aelog( "Couldn't open file:\n" );
            aelog( vm["pnCS.infile"].as<std::string>() );
            rc = 1;
        }
        if( vm.count("pnCS.dump-parsed") ) {
            pnLow::print_out_scatter_data( std::cout );
        }
    }
    // TODO:
    // pnLow::clear_database();  // erases parsed entries and frees internal parser.
    return rc;
}

int  // XXX
pn_cross_section_approximate( const po::variables_map & vm,
                              int, char *[] ) {
    auto S = pnLow::get_pn_approximant_set( );
    uint16_t nEnergy = vm["pnCS.regularApproxNE"].as<uint16_t>(),
             nAngle  = vm["pnCS.regularApproxNA"].as<uint16_t>();
    double stepX1 = (S.x_max()[0] -
                     S.x_min()[0])/nEnergy,
           stepX2 = (S.x_max()[1] -
                     S.x_min()[1])/nAngle;
    pnLow::pState.wangApproximant.reserve_poi_set( nEnergy*nAngle );

    for( double x1 =  S.x_min()[0] + stepX1/2;
                x1 <= S.x_max()[0]; x1 += stepX1 ) {
        for( double x2 =  S.x_min()[1] + stepX2/2;
                    x2 <= S.x_max()[1]; x2 += stepX2 ) {
            S.push_poi( {x1, x2} );
        }
    }
    int rc;
    if( 0 != (rc = S.do_approx(
                vm["pnCS.mir-taylorOrder"        ].as<int>(),
                vm["pnCS.mir-polynomialExactness"].as<int>(),
                vm["pnCS.mir-safety"             ].as<double>(),
                vm["pnCS.mir-logarithmic"        ].as<bool>()
            )) ) {
        return rc;
    }
    S.dump_poi_ascii( std::cout );
    return rc;
}

int
pn_cross_section_build_delaunay(
        const po::variables_map & vm,
        int argc, char * argv[]) {
    auto rootApp = new TApplication("pn-CS Delaunay interpolation view",
                                    &argc, argv);
    rootApp->SetReturnFromRun(true);

    TGraph2DErrors * tgraph = pnLow::get_graph_repr_vm( vm );
    auto canvas = new TCanvas("CsIstats","CsI Graphics", 1200, 600);
    //TH2D * vis = tgraph->GetHistogram();
    //vis->Draw("COLZ");
    tgraph->Draw( "TRI1" );
    canvas->Update();
    rootApp->Run();
    return 0;
}

int
pn_cross_section_store_grid(
        const po::variables_map & vm,
        int, char *[]) {
    const std::string outFileName = vm["pnCS.gridFile"].as<std::string>();
    if( outFileName.empty() /* or inaccessible */ ) {
        std::cerr << "Can't store to \"" << outFileName << "\". "
                  << "Please, specify a valid path." << std::endl;
    }

    if( vm.count("pnCS.render-to") ) {
        std::ofstream ofle( (boost::filesystem::path(
                    vm["pnCS.render-to"].as<std::string>()) /= "pn-source.dat").string(),
                std::ofstream::out );
        pnLow::print_out_scatter_data( ofle );
        ofle.close();
    }

    TGraph2DErrors * tgraph = pnLow::get_graph_repr_vm( vm );

    if( !tgraph ) {
        std::cerr << "Exit grid saving task routine due to previous errors."
                  << std::endl;
        return -1;
    }

    if( vm.count("pnCS.render-to") ) {
        std::ofstream ofle( (boost::filesystem::path(
                    vm["pnCS.render-to"].as<std::string>()) /= "pn-approxd.dat").string(),
                std::ofstream::out );
        auto S = pnLow::get_pn_approximant_set( );
        S.dump_poi_ascii( ofle );
        ofle.close();
    }

    TH2D * hst = tgraph->GetHistogram();

    TFile * outFile = new TFile( outFileName.c_str(), "recreate" );
    hst->Write();
    outFile->Close();

    return 0;
}

void pn_cs_parser_task_dispatcher() __attribute((__constructor__));
void pn_cs_parser_task_dispatcher() {
    Application::object().add_task_callback(
        "pn-parse",
        pn_cross_section_parse,
        "Parses the ASCII tabular, finite-state markup file containing\n\
    cross-section data using --pnCS.infile argument and stores it\n\
    in static object for further treatment.");
    Application::object().add_task_callback(
        "pn-approximate",
        pn_cross_section_approximate,
        "(DEPR) Applies Wang's approximation to parsed data on regular grid.\n\
    This task is supposed for future development and to be as a Wang's \"mir\"\n\
    approximant utility set testing routine. Ignores min/max specification as\n\
    among all given data set.");
    Application::object().add_task_callback(
        "pn-delaunay",
        pn_cross_section_build_delaunay,
        "(DEV) Provides Delaunay interpolation among parsed data (optionally,\n\
    approximated with Wang's \"mir\". \"pn-parse\" task is required to be\n\
    precedent.)");
    Application::object().add_task_callback(
        "pn-store-approximated",
        pn_cross_section_store_grid,
        "Stores approximation grid-function for further operations.");
}

# endif  // PN_LOW_EXPDAT_GEN

