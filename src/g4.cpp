# include "app_config.h"
# include "app.hpp"

# ifdef WITH_GEANT4_MODEL

# include <G4RunManager.hh>
# include <G4UImanager.hh>
# include <G4SDManager.hh>
# include <G4HCofThisEvent.hh>
# include <G4VisExecutive.hh>
# include <G4UIExecutive.hh>

# include "g4_RunManager.hh"
# ifdef PN_LOW_EXPDAT_GEN
#   include "pn_cross_section_parse.hpp"
# endif  // PN_LOW_EXPDAT_GEN

int
run_g4( const po::variables_map & vm, int argc, char * argv[] ) {
    // Choose the Random engine
    CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);

    // User Verbose output class
    //G4VSteppingVerbose::SetInstance(new SteppingVerbose);

    // Construct the default run manager
    auto runManager = new G4I::RunManager( vm );

    // Initialize G4 kernel
    //
    runManager->Initialize();

    G4VisManager * visManager = nullptr;
    if( vm.count("g4mdl.useVis") && vm["g4mdl.useVis"].as<bool>() ) {
        // Initialize visualization.
        visManager = new G4VisExecutive;
        // G4VisExecutive can take a verbosity argument - see /vis/verbose guidance.
        // G4VisManager* visManager = new G4VisExecutive("Quiet");
        visManager->Initialize();
    }
    // Get the pointer to the User Interface manager
    G4UImanager * UImanager = G4UImanager::GetUIpointer();
    G4UIExecutive * ui = nullptr;
    if( vm.count("g4mdl.useUI") && vm["g4mdl.useUI"].as<bool>() ) {
        ui = new G4UIExecutive(argc, argv, "qt");
    }
    if( vm.count("g4mdl.useVis") && vm["g4mdl.useVis"].as<bool>() ) {
        if( vm.count("g4mdl.visMacroFile") ) {
            std::cout << "G4: applying \""
                      << vm["g4mdl.visMacroFile"].as<std::string>()
                      << "\"...\n"
                      << std::endl;
            UImanager->ApplyCommand("/control/execute " + vm["g4mdl.visMacroFile"].as<std::string>());
        }
    }
    if( ui ) {
        if( vm.count("g4mdl.guiMacroFile") ) {
            std::cout << "G4: applying \""
                      << vm["g4mdl.guiMacroFile"].as<std::string>()
                      << "\"...\n"
                      << std::endl;
            UImanager->ApplyCommand("/control/execute " + vm["g4mdl.guiMacroFile"].as<std::string>() );
        }
        if(ui && ui->IsGUI() ) {
            ui->SessionStart();
        }
        delete ui;
    }

    // Job termination
    // Free the store: user actions, physics_list and detector_description are
    //                 owned and deleted by the run manager, so they should not
    //                 be deleted in the main() program !
    if( visManager ) {
        delete visManager;
    }
    delete runManager;
    //UImanager = NULL;

    return 0;
}

//
// Push callback to application's dictionary.
//

void g4_task_dispatcher() __attribute((__constructor__));
void g4_task_dispatcher() {
    Application::object().add_task_callback(
        "modelling",
        run_g4,
        "Delegates execution to Geant4 run manager (whatsoever it will do further\n\
    according to it's runtime scripts).");
}

# endif  // WITH_GEANT4_MODEL

