# include "pn_generate.hpp"

# ifdef PN_LOW_EXPDAT_GEN

# include <fstream>

# include <root/TFile.h>
# include <root/TTree.h>

namespace pnLow {

static iPNGenerator * pnGenerator = nullptr;

int
iPNGenerator::new_generator_instance( const po::variables_map & vm ) {
    if( pnGenerator ) {
        std::cerr << "(p,n)-generator repitative construction ignored. "
                  << "Previous instance will be kept."
                  << std::endl;
        return 1;
    }
    const std::string name = vm["pnCS.generatorImplementation"].as<std::string>();
    if( "naive" == name ) {
        pnGenerator = new NaivePN( vm );
    } else if( "contracted" == name ) {
        // TODO
        std::cerr << "Unimplemented." << std::endl;
    } else {
        std::cerr << "Has no (p,n)-generator implementation named \""
                  << name << "\"." << std::endl;
        return 1;
    }
    return 0;
}

iPNGenerator &
iPNGenerator::get_generator_instance() {
    if( !pnGenerator ) {
        std::cerr << "Warning: (p,n)-generator is not constructed."
                  << std::endl;
    }
    return *pnGenerator;
}

void
iPNGenerator::free_generator_instance() {
    if( pnGenerator ) {
        delete pnGenerator;
        pnGenerator = nullptr;
    }
}

//
// Naive implementation
//

NaivePN::FixedEGammaGenerator::FixedEGammaGenerator(
        double eGamma,
        std::map<double, double> & piecewiseData,
        bool noSimulate
    ) : PDEBasedGenerator<1>(
                *(new SmoothSpectrum(piecewiseData)),
                "FoamEGamma" + boost::lexical_cast<std::string>(eGamma)
            ),
        _rndG( new TRandom3() ),
        _eGamma(eGamma) {
    if( !noSimulate && dynamic_cast<SmoothSpectrum&>(this->pdf()).is_good() ) {
        simulator().SetChat(Application::object().verbosity());
        simulator().SetPseRan(_rndG);
        simulator().Initialize();
    }
}

NaivePN::FixedEGammaGenerator::~FixedEGammaGenerator() {
    SmoothSpectrum & ss = dynamic_cast<SmoothSpectrum&>(this->pdf());
    delete &ss;
    delete _rndG;
}


void
NaivePN::FixedEGammaGenerator::dump_grid_f_to( std::ostream & os ) const {
    const SmoothSpectrum & pdf = 
        dynamic_cast<const SmoothSpectrum&>(this->pdf());
    const unsigned short N = 64;  // magic number
    double rMin, rMax,
           range = pdf.get_range( rMin, rMax ),
           step = range/(N-1),
           stepn = 1./(N-1);
    ((void) rMax);
    double val = rMin;
    for( size_t n = 0; n < N; ++n, val += step ) {
        os  << val
            << " "
            << pdf.density( stepn*n ) << std::endl;
    }
}

//
// Naive version of (p,n) reaction events generator.
//

static void
readout_energies_subsets( std::map<double, std::map<double, CrossSectionDataPoint*> > & dest,
                           const std::string & filename ) {
    TFile * inFile = new TFile( filename.c_str(), "READ" );
    TH2D * hst = (TH2D *) inFile->Get("Graph2D");

    uint16_t nE = hst->GetNbinsX(),
             nA = hst->GetNbinsY();

    if( Application::object().verbosity() > 1 ) {
        std::cout << "Found a histogram in file \"" << filename << "\" of "
                  << nE << "×" << nA << " bins."
                  << std::endl;
    }

    for( uint16_t i = 0; i < nE; ++i ) {
        for( uint16_t j = 0; j < nA; ++j ) {
            Int_t nBin = hst->GetBin( i, j );
            double sigma  = hst->GetBinContent( nBin ),
                   eGamma = hst->GetXaxis()->GetBinCenter(i),
                   thetaP = hst->GetYaxis()->GetBinCenter(j);
            //std::cout << "XXX: #"
            //          << nBin << ", "
            //          << eGamma << ", "
            //          << thetaP << ", "
            //          << sigma
            //          << std::endl;  // XXX
            if( (!i || !j) && sigma <= 0 ) continue;
            if( dest.end() == dest.find(eGamma) ) {
                dest[eGamma] = std::map<double, CrossSectionDataPoint*>();
            }
            dest[eGamma][thetaP] = new CrossSectionDataPoint{eGamma, thetaP, sigma, {0, 0}};
        }
    }

    inFile->Close();
    delete inFile;
}

NaivePN::NaivePN( const po::variables_map & vm ) :
        _eGammaGen( vm ) {

    bool noSimulate = true;
    {
        auto taskVector = vm["task"].as<std::vector<std::string> >();
        for( auto it = taskVector.begin(); it != taskVector.end(); ++it ) {
            if( *it == "pn-generate" ) {
                noSimulate = false;
            }
        }
        if( noSimulate && Application::object().verbosity() > 1 ) {
            std::cout << "WARNING: pn-generator will be constructed, but TFoam simulators "
                      << "will be disabled as there no pn-generate task provided."
                      << std::endl;
        }
    }

    std::map<double, std::map<double, CrossSectionDataPoint*> > energies;
    if( vm.count("pnCS.gridFile") ){
        readout_energies_subsets( energies, vm["pnCS.gridFile"].as<std::string>() );
    } else {
        energies = get_energies_subsets();
    }
    if( energies.empty() ) {
        std::cerr << "NaivePN instance can not be constructed since here are no available data sources."
                  << std::endl;
    }
    if( !vm["pnCS.override-gridFile-cut-ranges"].as<bool>() ) {
        for( auto it = energies.begin(); energies.end() != it; ++it ) {
            // Data points subset to be formed with paits {angle, probability}:
            std::map<double, double> subs;
            for( auto iit  = it->second.begin();
                      iit != it->second.end(); ++iit) {
                // Fill data point:
                subs[iit->first] = iit->second->value;
            }
            if( !it->second.empty() ) {
                if( Application::object().verbosity() > 2 ) {
                    std::cout << "(" << _thetaPGen.size() << ") "
                              << "CS planar sigma/theta profile adding for energy "
                              << it->first << "MeV of " << subs.size() << " points."
                              << std::endl;
                }
                auto nfeg = new NaivePN::FixedEGammaGenerator( it->first, subs, noSimulate );
                const NaivePN::FixedEGammaGenerator * cnfeg = nfeg;
                if( dynamic_cast<const SmoothSpectrum&>(cnfeg->pdf()).is_good() ) {
                    _thetaPGen.insert(nfeg);
                } else {
                    delete nfeg;
                }
            }
        }
    } else {  // override
        double eMin = vm["Dalitz-Yennie-min"].as<double>(),
               eMax = vm["beamEnergy"].as<double>(),
               aMin = vm["pnCS.regularARangeMin"].as<double>(),
               aMax = vm["pnCS.regularARangeMax"].as<double>();
        size_t nAdded = 0;
        for( auto it = energies.begin(); energies.end() != it; ++it ) {
            if( it->first > eMax ||
                it->first < eMin ) {
                if( Application::object().verbosity() > 2 ) {
                    std::cout << "Ignore angle " << it->first
                              << " as it lies outside overriden cut range."
                              << std::endl;
                }
                continue;
            }
            // Data points subset to be formed with paits {angle, probability}:
            std::map<double, double> subs;
            for( auto iit  = it->second.begin();
                      iit != it->second.end(); ++iit) {
                // Fill data point:
                if( iit->first > aMax ||
                    iit->first < aMin ) { continue; }
                subs[iit->first] = iit->second->value;
                ++nAdded;
            }
            if( subs.empty() ) {
                if( Application::object().verbosity() > 0 ) {
                    std::cout << "WARNING: Empty angular set for energy " << it->first
                              << ". Application state is undefined."
                              << std::endl;
                }
                continue;
            }
            if( !it->second.empty() ) {
                if( Application::object().verbosity() > 2 ) {
                    std::cout << "(" << _thetaPGen.size() << ") "
                              << "CS planar sigma/theta profile adding for energy "
                              << it->first << "MeV of " << subs.size() << " points."
                              << std::endl;
                }
                auto nfeg = new NaivePN::FixedEGammaGenerator( it->first, subs, noSimulate );
                const NaivePN::FixedEGammaGenerator * cnfeg = nfeg;
                if( dynamic_cast<const SmoothSpectrum&>(cnfeg->pdf()).is_good() ) {
                    _thetaPGen.insert(nfeg);
                } else {
                    delete nfeg;
                }
            }
        }
    }
}

NaivePN::~NaivePN() {
    for( auto it = _thetaPGen.begin(); _thetaPGen.end() != it; ++it ) {
        delete *it;
    }
}

NaivePN::FixedEGammaGenerator *
NaivePN::_get_nearest_thetaGen( double eGamma ) const {
    double lastDistance = NAN;
    FixedEGammaGenerator * lastGenerator = nullptr;

    assert( !_thetaPGen.empty() );  // no data known
    for( auto it  = _thetaPGen.begin();
              it != _thetaPGen.end(); ++it ) {
        double currentDistance = (*it)->distance( eGamma );
        if( currentDistance < lastDistance || !lastGenerator ) {
            lastDistance = currentDistance;
            lastGenerator = *it;
        }
    }
    assert( lastGenerator );  // No generators found.
    return lastGenerator;
}

double
NaivePN::_V_generate( Event & event ) const {
    // Phase 1: generate E_Gamma
    DalitzYennieSpectrum::Event equivalentPhotonEvent;
    double weight = _eGammaGen.generate( equivalentPhotonEvent );
    //std::cout << "XXX, E_gamma = " << equivalentPhotonEvent.v[0] << std::endl; //XXX
    // Phase 2: generate Theta_cm
    FixedEGammaGenerator::Event angularPartEvent;
    //const SmoothSpectrum & ss = dynamic_cast<const SmoothSpectrum *>(this->pdf());
    const FixedEGammaGenerator * gen = _get_nearest_thetaGen(
        equivalentPhotonEvent.v[0] );
    weight *= gen->generate( angularPartEvent );

    event.omega  = equivalentPhotonEvent.v[0];
    event.thetaP = angularPartEvent.v[0];

    event.thetaP = dynamic_cast<const SmoothSpectrum &>(gen->pdf()).denormalize(event.thetaP);

    return weight;
}

int
pn_generate(const po::variables_map & vm,
            int, char *[]) {
    size_t N = pow( 10.,
                    ((int) vm["pnCS.nEGenerate"].as<uint16_t>()) );

    iPNGenerator::new_generator_instance( vm );
    iPNGenerator::Event eve;
    ReactionPN k;
    TTree * outt = new TTree("pnEvents", "Photodisintegration events");
    /*TBranch * brEg = */outt->Branch("E_gamma", &(eve.omega),  "E_gamma/D"),
    /*        * brTh = */outt->Branch("Theta_p", &(eve.thetaP), "Theta_p/D");
    for( size_t n = 0; n < N; ++n ) {
        iPNGenerator::get_generator_instance().generate( eve );
        outt->Fill();
        # if 0
        reveal_pn_kinematics( eve.omega,
                              eve.thetaP,
                              k );
        if( Application::object().verbosity() > 2 ) {
            std::cout << "Event #" << n << ", "
                      << "s=" << k.s << ", t=" << k.t  << ", "
                      << "E_gamma=" << eve.omega << ", "
                      << "theta_p_cm=" << eve.thetaP
                      << "(theta_p=" << k.after.inLabFrame.p.Theta()*180./M_PI << ")"
                      << std::endl;
        }
        # endif
    }
    iPNGenerator::free_generator_instance();

    if( vm.count("pnCS.evfile") ) {
        TFile * outf = new TFile(vm["pnCS.evfile"].as<std::string>().c_str(), "RECREATE" );
        outf->cd();
        outt->Write();
        outf->Write();
        outf->Close();
        delete outt;
        //delete outf;
    }
    return 0;
}

int
pn_naive_dump_angular_sections(const po::variables_map & vm,
            int, char *[]) {

    iPNGenerator::new_generator_instance( vm );

    pnLow::NaivePN & pnG = dynamic_cast<pnLow::NaivePN &>(
                pnLow::iPNGenerator::get_generator_instance());

    struct _local_AngularDumpWrapper {
        void operator()( const pnLow::NaivePN::FixedEGammaGenerator & section ) {
            char bf[64];
            snprintf( bf, 64, "/tmp/angular-check-%f.dat", section.e_gamma() );
            std::ofstream os;
            os.open( bf, std::ofstream::out );
            section.dump_grid_f_to( os );
            os.close();
        }
    } dumper;

    pnG.for_all_theta_P_spectrums( dumper );

    iPNGenerator::free_generator_instance();
    return 1;  // TODO
}

void
reveal_pn_kinematics(
        double  E_gamma,
        double  thetaPcm,
        ReactionPN & K
    ) {
    thetaPcm *= M_PI/180.;
    const double        m_d = PN_LOW_DEUTERON_MASS_MEV,     // MeV/c^2
                      //e_m = 0.510998910,     // MeV/c^2
                        m_p = PN_LOW_PROTON_MASS_MEV,       // MeV/c^2
                        m_n = PN_LOW_NEUTERON_MASS_MEV,     // MeV/c^2
                       m_p2 = m_p*m_p,
                       m_n2 = m_n*m_n,
                          s = m_d*m_d + 2*E_gamma*m_d,
                        s12 = 1./(2*sqrt(s)),
                     p_p_cm = sqrt(pow(s - m_p2 - m_n2, 2) - 4*m_p2*m_n2)*s12,  // checked
                     E_p_cm = (s + m_p2 - m_n2)*s12,                            // checked
                     E_n_cm = (s - m_p2 + m_n2)*s12,                            // checked
                 E_gamma_cm = (s     - m_d*m_d)*s12,                            // checked
                 p_gamma_cm = E_gamma_cm,
                          t = m_p2 - 2*(E_gamma_cm*E_p_cm - p_gamma_cm*p_p_cm*cos(thetaPcm)),
                    beta_cm = E_gamma/(E_gamma + m_d);
    if( s <= 0 ||
        isnan(p_p_cm) ||
        isnan(E_p_cm) ||
        isnan(t) ) {
        std::cerr << "Variables:" << std::endl
                  << " - in: " << std::endl
                  << " -- E_{\\gamma}=" << E_gamma << std::endl
                  << " -- \\Theta_{p, c.m.}=" << thetaPcm << "(rad)" << std::endl
                  << " - out: " << std::endl
                  << " -- s: " << s << std::endl
                  //<< "XXX: " << pow(s - m_p2 - m_n2, 2) << "\n"
                  //<< "XXX: " << 4*m_p2*m_n2 << "\n"
                  //<< "XXX: " << pow(s - m_p2 - m_n2, 2) - 4*m_p2*m_n2 << "\n"
                  << " -- p_p_cm: " << p_p_cm << std::endl
                  << " -- E_p_cm: " << E_p_cm << std::endl
                  << " -- E_n_cm: " << E_n_cm << std::endl;
        throw(std::runtime_error("Error met during kinematics reveal for pn-reaction."));
    }
    K.s = s;
    K.t = t;
    K.Lambda = TLorentzRotation( 0, 0, -beta_cm );
    K.Lambda_1 = K.Lambda.Inverse();
    // Before interaction
    // - Lab frame
    { TLorentzVector & cv = K.before.inLabFrame.gamma;
        cv.SetVect( {0, 0, E_gamma} );
        cv.SetE( E_gamma );
    }
    { TLorentzVector & cv = K.before.inLabFrame.deu;
        cv.SetVect( {0, 0, 0} );
        cv.SetE( m_d );
    }
    // - C.M. frame
    { TLorentzVector & cv = K.before.inCMFrame.gamma;
        cv = K.Lambda * K.before.inLabFrame.gamma;
    }
    { TLorentzVector & cv = K.before.inCMFrame.deu;
        cv = K.Lambda * K.before.inLabFrame.deu;
    }
    // After interaction
    // - C.M. frame
    { TLorentzVector & cv = K.after.inCMFrame.p;
        cv.SetVect( {0, 0, 1.} );
        cv.SetTheta( thetaPcm );
        cv.SetRho( p_p_cm );
        cv.SetE( E_p_cm );
    }
    { TLorentzVector & cv = K.after.inCMFrame.n;
        cv = -K.after.inCMFrame.p;
        cv.SetE( E_n_cm );
    }
    // - Lab frame
    { TLorentzVector & cv = K.after.inLabFrame.p;
        cv = K.Lambda_1 * K.after.inCMFrame.p;
    }
    { TLorentzVector & cv = K.after.inLabFrame.n;
        cv = K.Lambda_1 * K.after.inCMFrame.n;
    }
}

}  // namespace pnLow

void pn_generator_task_dispatcher() __attribute((__constructor__));
void pn_generator_task_dispatcher() {
    Application::object().add_task_callback(
        "pn-generate",
        pnLow::pn_generate,
        "Generates (p,n) events according to presets configuration. Requires,\n\
    that all necessary stages where provided.");
    Application::object().add_task_callback(
        "pn-naive-dump-sections",
        pnLow::pn_naive_dump_angular_sections,
        "(DEV) Generates a set of cross-section data that are correspond to different\n\
    E_gamma. Stores it as a files in /tmp/ directory."
    );
}

# endif  // PN_LOW_EXPDAT_GEN

