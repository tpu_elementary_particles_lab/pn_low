# include "g4_Hit.hh"

# ifdef WITH_GEANT4_MODEL

namespace G4I {

static G4double DENSITY = 1.032;  // scintill. density - for light output calculation

G4Allocator<Hit> HitAllocator;

Hit::Hit() {
    _Edep = _A1 = _A2 = _LO = 0.; 
    _ToF = -1.*ns;
    _Pos = G4ThreeVector();
    _pde=0.;
    _Trig = false;
    _blkN = -1; 
    _Vpos = G4ThreeVector();
    _Vrot=G4RotationMatrix();
    _Nprim=-1;
    //std::cout << "XXX XXX: Hit ctrd #1" << std::endl; // XXX
}

Hit::Hit( G4double l,
          G4double a,
          G4double thr,
          G4ThreeVector vp,
          const G4RotationMatrix *rr ) :
            _halflength(l),
            _absorbtion(a),
            _threshold(thr) {
   _Edep = _A1 = _A2 = _LO = 0.; 
   _ToF = -1.*ns;
   _Pos = G4ThreeVector();
   _pde = 0.;
   _Trig = false;
   _blkN = -1; 
   _Nprim = -1;
   _Vpos = vp;
   if(rr) {
       _Vrot = *rr;
   } else {
       _Vrot = G4RotationMatrix();
   }
   //std::cout << "XXX XXX: Hit ctrd #2" << std::endl; // XXX
}

void
Hit::add_Pos(G4double de,
             G4ThreeVector pos,
             G4int prtn) {
    _Pos *= _pde;
    _pde += de;
    _Pos += pos*de;
    if(_pde>0.) {
        _Pos/=_pde;
    }
    _Nprim=prtn;
    //std::cout << "XXX XXX: Hit #1" << std::endl; // XXX: unused
}

void
Hit::add_Pos(G4double de,
             G4ThreeVector pos,
             G4int prtn,
             G4double t) {
    _pde += de;
    _Nprim = prtn;

    if(!_Trig && (_pde > _threshold)){
        _Pos=pos;
        _ToF = t;
        _Trig = true;
    }
    //std::cout << "XXX XXX: Hit #2" << std::endl; // XXX
}

void
Hit::add_T(G4double t) {
    if(!_Trig && (_Edep > _threshold)) {
        _ToF = t; _Trig = true;
    }
    //std::cout << "XXX XXX: Hit #3" << std::endl; // XXX: unused
}

void
Hit::add_LO(G4double de,
            G4ThreeVector pos,
            G4ThreeVector delta) {
    G4double lo;
    G4double dx = delta.mag();
    G4ThreeVector posit = pos - _Vpos; posit.transform(_Vrot);
    G4double xpos = posit.getX();

    if( dx > .0 ){
        G4double a = (de/MeV)/(dx/cm * DENSITY);
        G4double f = 1.0 + 0.011*a + 0.000009*a*a;
        lo = de/f;
        _LO += lo;
        _A1 += lo*exp(-(_halflength - xpos)/_absorbtion);
        _A2 += lo*exp(-(_halflength + xpos)/_absorbtion);
    }
    //std::cout << "XXX XXX: Hit #4" << std::endl; // XXX: unused
}

}  // namespace G4I

# endif  // WITH_GEANT4_MODEL

