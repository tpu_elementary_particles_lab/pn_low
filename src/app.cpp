# include "app_config.h"
# include "app.hpp"

# include <cstdlib>
# include <cstring>
# include <cstdarg>

# include <string>
# include <iostream>
# include <fstream>
# include <vector>

# ifdef WITH_GEANT4_MODEL
# include <G4UImanager.hh>
# endif

static const char * defaultCfgFile = "../../pn_low/defaults.cfg";
Application * Application::_self = nullptr;

Application &
Application::object() {
    if( !_self ) {
        _self = new Application();
    }
    return *_self;
}

int
Application::quench( int runRC ) {
    for( auto it = _self->_tskCleaners.begin();
                   _self->_tskCleaners.end() != it; ++it ) {
        runRC |= (*it)();
    }
    delete _self;
    return 0;
}

void
Application::g4_abort( ) {
    # ifdef WITH_GEANT4_MODEL
    if( G4UImanager::GetUIpointer() ) {
        G4UImanager::GetUIpointer()->ApplyCommand("/run/abort");
    }
    # endif
}

int
main(int argc, char * argv[]) {
    auto vm = Application::object().configure_command_line( argc, argv );
    //app.configure_file( "config.xml" );
    int rc = Application::object().run( vm, argc, argv );
    rc |= Application::quench( rc );
    return rc;
}

//
//
//

Application::Application() :
    _logstream(std::cout),
    _errstream(std::cerr) {
}

po::variables_map
Application::configure_command_line( int argc, char * argv[] ) {
    po::options_description generic("Generic options");
    generic.add_options()
        ("help,h",
            "Produces help message.")
        ("config,c",
            "Sets the config file to command line args be interfereded with.")
        ("show-tasks",
            "Shows the available tasks for current build.")
        ("verbosity,v",
            po::value<int>()->default_value(1),
            "Sets verbosity level (from 0 -- silent, to 3 -- loquacious).")
        // ... todo: logstream/errstream configurable.
        ;

    po::options_description config("Configuration");
    config.add_options()
        ("task,t",
            po::value< std::vector<std::string> >(),
            "tasks to do")
        ("beamEnergy",
            po::value< double >()->default_value(750 /*MeV*/),
            "Beam energy used in computations.")
        ("Dalitz-Yennie-min",
            po::value<double>()->default_value(2),
            "Minimal cut-off for Dalitz-Yennie spectrum.")
        ;

    # ifdef PN_LOW_EXPDAT_GEN
    po::options_description pnCSCfg("(pn) cross-section event generator config");
    pnCSCfg.add_options()
        ("pnCS.infile",
            po::value< std::vector<std::string> >(),
            ".dat-file with basic cross section data." )
        ("pnCS.dump-parsed",
            "Prints parsed data back to ASCII (uses stdout).")
        ("pnCS.render-to",
            po::value<std::string>(),
            "When pn-store-approximated task is choosen, this path determines, where \
to save plottable data to inspect. There two files will be generated: parsed surface \
points (pn-source.dat) and obtained approximated surface (with error) (pn-approxd.dat).")
        ("pnCS.regularApproxNE",
            po::value<uint16_t>()->default_value(0),
            "Number of regular grid points for CS approximation along energy axis.")
        ("pnCS.regularApproxNA",
            po::value<uint16_t>()->default_value(0),
            "Number of regular grid points for CS approximation along angular axis")
        ("pnCS.regularARangeMin",
            po::value<double>()->default_value(0),
            "Angular range minimum (in c.m. system, 0 -- is minimal available.)")
        ("pnCS.regularARangeMax",
            po::value<double>()->default_value(0),
            "Angular range maximum (in c.m. system, 0 -- is maximum available.)")
        ("pnCS.noApproxGraphCtr",
            po::value<bool>()->default_value(false),
            "If set, on \"pn-delaunay\" task Wang's approximation won't be provided.")
        ("pnCS.generatorImplementation",
            po::value<std::string>()->default_value("naive"),
            "(p,n) generator implementation version to use ('naive' and 'contraction' are available).")
        ("pnCS.nEGenerate",
            po::value<uint16_t>()->default_value(2),
            "N for 1e+N events to be generated using current settings for 'pn-generate' task.")
        ("pnCS.gridFile",
            po::value<std::string>(),
            "A grid file with cross section to be used in (p,n) generator.")
        ("pnCS.override-gridFile-cut-ranges",
            po::value<bool>()->default_value(false),
            "When set, app will override gridFile's ranges when constructing a generator.\
 Current Dalitz-Yennie-min/beamEnergy and pnCS.regularARange(Min/Max) will be used\
 instead. This behaviour is desired since MIR approximation algorithm sometimes\
 gives a significant error near the outern boundaries of POI set.")
        ("pnCS.evfile",
            po::value<std::string>(),
            "A root file where generated pn-events be stored.")
        ("pnCS.mir-taylorOrder",
            po::value<int>()->default_value(5),
            "MIR: The Taylor order parameter (>0).")
        ("pnCS.mir-polynomialExactness",
            po::value<int>()->default_value(2),
            "MIR: The polynomial exactness parameter (>=0).")
        ("pnCS.mir-safety",
            po::value<double>()->default_value(1.),
            "MIR: The safety factor, must be greater than 0.\
 Higher than 1 will produce a conservative (larger) gamma,\
 lower than 1 will produce a aggressive (smaller) gamma.")
        ("pnCS.mir-logarithmic",
            po::value<bool>()->default_value(false),
            "MIR: apply logarithmic scale transformation to approximated function.")
        ;
    # endif  // PN_LOW_EXPDAT_GEN

    # ifdef ARENHOVEL_FIX_GEN
    po::options_description pnPi0CSCfg("(pnpi0) cross-section event generator");
    pnPi0CSCfg.add_options()
        ("pnPi0.infile",
            po::value<std::string>(),
            ".dat-file with basic cross section data." )
        ;
    # endif // ARENHOVEL_FIX_GEN

    po::options_description g4cfg("Geant4 model options");
    g4cfg.add_options()
        ("g4mdl.outFile",
            po::value<std::string>()->default_value("/tmp/pn_low_last.root"),
            "output filename" )
        ("g4mdl.verbose",
            po::value<bool>()->default_value(false),
            "G4 model verbosity")
        ("g4mdl.useVis",
            po::value<bool>()->default_value(true),
            "Enable G4 'vis' run-time opts")
        ("g4mdl.visMacroFile",
            po::value<std::string>()->default_value("vis.mac"),
            "'vis' run-time script")
        ("g4mdl.useUI",
            po::value<bool>()->default_value(false),
            "Use G4 graphical user interface")
        ("g4mdl.guiMacroFile",
            po::value<std::string>()->default_value("ui.mac"),
            "run-time script called after vis (useful for HGUI additionals)")
        ("g4mdl.phiAngle",
            po::value<double>()->default_value(0.5),
            "asimutal angle interval for uniform event generation, fraction of M_PI")
        ;

    po::options_description opts;
    opts.add(generic)
        .add(config)
        # ifdef PN_LOW_EXPDAT_GEN
        .add(pnCSCfg)
        # endif // PN_LOW_EXPDAT_GEN
        # ifdef ARENHOVEL_FIX_GEN
        .add(pnPi0CSCfg)
        # endif // ARENHOVEL_FIX_GEN
        .add(g4cfg);

    po::positional_options_description popts;
    popts.add("input-source", -1);

    po::variables_map vm;

    po::store(po::command_line_parser(argc, argv).
        options(opts).
        positional(popts).
        run(), vm);

    std::string confFilePath = defaultCfgFile;
    if( vm.count("config") ) {
        confFilePath = vm["config"].as<std::string>();
        std::cerr << "Using user's config file: " << confFilePath << std::endl;
    } else {
        std::cerr << "Using default config file: " << confFilePath << std::endl;
    }
    std::ifstream configFile( confFilePath, std::ifstream::in );
    if( ! configFile ) {
        std::cerr << "Couldn't open config file \"" << confFilePath
                  << "\". Aborting execution." << std::endl;
    }
    po::store( po::parse_config_file( configFile, opts ), vm );
    configFile.close();

    po::notify( vm );

    if(vm.count("help")) {
        std::cout << opts << std::endl;
        exit(1);
    }
    if(vm.count("show-tasks")) {
        std::cout << "List of available tasks with names and descriptions:"
                  << std::endl;
        this->list_callbacks( std::cout );
        exit(1);
    }

    _verbosity = vm["verbosity"].as<int>();
    if( _verbosity > 3 ) {
        std::cerr << "Invalid verbosity level set: " << _verbosity;
        _verbosity = 3;
        std::cerr << ". Set to " << _verbosity << "." << std::endl;
    } else if( _verbosity < 0 ) {
        std::cerr << "Invalid verbosity level set: " << _verbosity;
        _verbosity = 0;
        std::cerr << ". Set to " << _verbosity << "." << std::endl;
    }

    return vm;
}

void
Application::log1( const std::string & msg, bool prefix ) {
    if( _verbosity > 0 ) {
        if( prefix ) _logstream << "[L1] ";
        _logstream << msg;
    }
}

void
Application::log2( const std::string & msg, bool prefix ) {
    if( _verbosity > 1 ) {
        if( prefix ) _logstream << "[L2] ";
        _logstream << msg;
    }
}

void
Application::log3( const std::string & msg, bool prefix ) {
    if( _verbosity > 2 ) {
        if( prefix ) _logstream << "[L3] ";
        _logstream << msg;
    }
}

void
Application::elog( const std::string & msg, bool prefix ) {
    if( prefix ) _logstream << "[EE] ";
    _errstream << msg;
}

int
Application::run( const po::variables_map & vm,
                  int argc, char * argv[] ) {
    # if 1
    auto tasksList = vm["task"].as< std::vector<std::string> >();
    for( auto taskIt = tasksList.begin(); tasksList.end() != taskIt; ++taskIt ) {
        _tasks[*taskIt].second( vm, argc, argv );
    }
    # else
    if( "modelling" == vm["task"].as<std::string>() ) {
        run_g4( vm, argc, argv );
    } else if( "pn-generate" == vm["task"].as<std::string>() ) {
        # ifdef PN_LOW_EXPDAT_GEN
        std::ifstream ifle(vm["pnCS.infile"].as<std::string>(), std::ifstream::in);
        /* check result */ pnLow::parse_cross_section_data( ifle );
        ifle.close();
        // ... do something with parsed stuff.
        pnLow::clear_database();  // erases parsed entries and frees internal parser.
        # else  // PN_LOW_EXPDAT_GEN
        std::cerr << "Feature PN_LOW_EXPDAT_GEN is disabled for this build. "
                  << "(p,n) event generator unsupported. Execution aborted"
                  << std::endl;
        return EXIT_FAILURE;
        # endif  // PN_LOW_EXPDAT_GEN
    } else if( "pnpi0-generate" == vm["task"].as<std::string>() ) {
        # ifdef ARENHOVEL_FIX_GEN
        // TODO ...
        # else  // ARENHOVEL_FIX_GEN
        std::cerr << "Feature ARENHOVEL_FIX_GEN is disabled for this build. "
                  << "(p,n,pi0) event generator unsupported. Execution aborted."
                  << std::endl;
        return EXIT_FAILURE;
        # endif  // ARENHOVEL_FIX_GEN
    }
    # endif
    return EXIT_SUCCESS;
}

void
Application::list_callbacks( std::ostream & os ) {
    for( auto it = _tasks.begin(); it != _tasks.end(); ++it ) {
        os << " " << it->first << std::endl
           << "    " << it->second.first << std::endl;
    }
    if( _tasks.empty() ) {
        os << "<no tasks available>" << std::endl;
    }
}

void
Application::add_task_callback( const std::string & name,
                                TaskCallback cllb,
                                const std::string & description ) {
    _tasks[name] = std::pair<const std::string, TaskCallback>( description, cllb );
}

void
Application::add_task_cleaner( TaskCleaner clrInst ) {
    _tskCleaners.insert( clrInst );
}

//
// See http://stackoverflow.com/questions/69738/c-how-to-get-fprintf-results-as-a-stdstring-w-o-sprintf#69911
// for original snippet.
//

static std::string
vformat(const char *fmt, va_list ap) {
    // Allocate a buffer on the stack that's big enough for us almost
    // all the time.
    size_t size = 1024;
    char buf[1024];

    // Try to vsnprintf into our buffer.
    va_list apcopy;
    va_copy (apcopy, ap);
    int32_t needed = vsnprintf(&buf[0], size, fmt, ap);
    // NB. On Windows, vsnprintf returns -1 if the string didn't fit the
    // buffer.  On Linux & OSX, it returns the length it would have needed.

    if( needed <= (int32_t) size && needed >= 0 ) {
        // It fit fine the first time, we're done.
        return std::string (&buf[0]);
    } else {
        // vsnprintf reported that it wanted to write more characters
        // than we allotted.  So do a malloc of the right size and try again.
        // This doesn't happen very often if we chose our initial size
        // well.
        std::vector <char> buf;
        size = needed;
        buf.resize (size);
        needed = vsnprintf (&buf[0], size, fmt, apcopy);
        return std::string (&buf[0]);
    }
}

std::string
fpr_format(const char *fmt, ...) {
    va_list ap;
    va_start (ap, fmt);
    std::string buf = vformat (fmt, ap);
    va_end (ap);
    return buf;
}

