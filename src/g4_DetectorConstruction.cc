# include "app_config.h"

# ifdef WITH_GEANT4_MODEL

# include <G4Element.hh>
# include <G4Material.hh>
# include <G4Colour.hh>
# include <G4Box.hh>
# include <G4Tubs.hh>
# include <G4LogicalVolume.hh>
# include <G4SubtractionSolid.hh>
# include <G4PVPlacement.hh>
# include <G4VisAttributes.hh>
# include <G4UnionSolid.hh>
# include <G4PVReplica.hh>
# include <G4Trap.hh>
# include <G4SDManager.hh>

# include "g4_DetectorConstruction.hh"
# include "g4_SensitiveDetector.hh"

namespace G4I {

DetectorConstructor * DetectorConstructor::_self = nullptr;

const int DetectorConstructor::armOneIndex = ARM1_IND;
const int DetectorConstructor::armTwoIndex = ARM2_IND;

DetectorConstructor::DetectorConstructor() {
    if( _self ) {
        throw std::runtime_error( "Repititative call of detector construction." );
    }
    _self = this;

    _init_rotation_matrices();

    for(G4int i=0;i<NSENS;i++){
        attLength[i]=1000.*cm;
        discrThreshold[i]=0.;
    }
}

DetectorConstructor::~DetectorConstructor() {
    _delete_world();
    _delete_materials();
    _delete_colours();
}

void
DetectorConstructor::_define_materials() {
    # define initialize_element( fieldName, strName, strSym, Am, molarMass ) \
        fieldName = new G4Element( strName, strSym, Am, molarMass );
    for_each_element( initialize_element )
    # undef initialize_element

    # define initialize_simple_material( fieldName, strName, Am, molarMass, density ) \
        fieldName = new G4Material( strName, Am, molarMass, density );
        for_each_simple_material( initialize_simple_material );
    # undef initialize_simple_material
    # define initialize_composite_material( fieldName, strName, density, num ) \
        fieldName = new G4Material( strName, density, num );
        for_each_composite_material( initialize_composite_material );
    # undef initialize_composite_material

    _air->AddElement( _elN, .7);
    _air->AddElement( _elO, .3);

    _brass->AddElement( _elCu, .96);
    _brass->AddElement( _elZn, .04);

    _gasArCO2->AddMaterial(_gasAr, .2);
    _gasArCO2->AddMaterial(_gasCO2, .8);

    _gasCO2->AddElement( _elC, 1);
    _gasCO2->AddElement( _elO, 2);

    _iron->AddElement( _elFe, 1.0);

    _mylar->AddElement( _elC, 5);
    _mylar->AddElement( _elH, 4);
    _mylar->AddElement( _elO, 2);

    _scintil->AddElement( _elC, 9);
    _scintil->AddElement( _elH, 10);

    _SiO2->AddElement( _elO, 2);
    _SiO2->AddElement( _elSi, 1);

    _steel->AddElement( _elCr, .197);
    _steel->AddElement( _elFe, .704);
    _steel->AddElement( _elNi, .099);
}

void
DetectorConstructor::_define_colours( ) {
    # define init_colour(fieldName, R, G, B) \
        fieldName = new G4Colour(R, G, B);
    for_each_colour( init_colour )
    # undef init_colour

    # define init_visual_attributes( fieldName, clr ) \
        fieldName = new G4VisAttributes( clr );
    for_each_visual_attr( init_visual_attributes )
    # undef init_visual_attributes
}

void
DetectorConstructor::_construct_world( ) {
   _world.solid = new G4Box("world", 5.*m/2., 5.*m/2., 5.*m/2.);

   _world.logv = new G4LogicalVolume(_world.solid, _air, "world", 0, 0, 0);
   _world.phys = new G4PVPlacement(0, G4ThreeVector(), 
                   _world.logv, "world", NULL, false, -1);

   _world.logv->SetVisAttributes(G4VisAttributes::Invisible);
}

void
DetectorConstructor::_delete_colours( ) {
    # define delete_colour(fieldName, R, G, B) \
        delete fieldName;
    for_each_colour( delete_colour )
    # undef delete_colour

    # define init_visual_attributes( fieldName, clr ) \
        delete fieldName;
    for_each_visual_attr( init_visual_attributes )
    # undef init_visual_attributes
}

void
DetectorConstructor::_delete_materials() {
    # define delete_element( fieldName, strName, strSym, Am, molarMass ) \
        fieldName = new G4Element( strName, strSym, Am, molarMass );
    for_each_element( delete_element )
    # undef delete_element

    # define delete_simple_material( fieldName, strName, Am, molarMass, density ) \
        fieldName = new G4Material( strName, Am, molarMass, density );
        for_each_simple_material( delete_simple_material );
    # undef delete_simple_material
    # define delete_composite_material( fieldName, strName, density, num ) \
        fieldName = new G4Material( strName, density, num );
        for_each_composite_material( delete_composite_material );
    # undef delete_composite_material
}

void
DetectorConstructor::_delete_world( ) {
    _world.clear();
}

G4VPhysicalVolume *
DetectorConstructor::Construct() {
    _define_materials();
    _define_colours();

    _construct_world();
    _task_construct_target();
    _task_construct_cell();
    _task_construct_lowq();
    G4LogicalVolume * VCBox_log = nullptr;
    _task_construct_vertex_chamber( VCBox_log );
    _task_construct_tracking_chambers( VCBox_log );
    _task_construct_hadron_sandwich();
    _task_construct_target_magnet();
    //_task_construct_

    _bind_sd();

    return _world.phys;
}

void
DetectorConstructor::_init_rotation_matrices() {
   _rot10X.       rotateX(10*deg);
   _rot180X.      rotateX(180*deg);
   _rot180Y.      rotateY(180*deg);
   _rot180Z.      rotateZ(180*deg);
   _rot25X.       rotateX(25*deg);
   _rot270X.      rotateX(-90*deg);
   _rot270Y.      rotateY(-90*deg);
   _rot270Z.      rotateZ(-90*deg);
   _rot45X.       rotateX(45*deg);
   _rot65X.       rotateX(65*deg);
   _rot90X.       rotateX(90*deg);
   _rot90Y.       rotateY(90*deg);
   _rot90Z.       rotateZ(90*deg);
   _rot99X.       rotateX(99*deg);
   _rot9X.        rotateX(9*deg);


   _rot90Y180X.   rotateY(90*deg);  _rot90Y180X.    rotateX(180*deg);
   _rot90Y180Z.   rotateY(90*deg);  _rot90Y180Z.    rotateZ(180*deg);
   _rot90X180Z.   rotateX(90*deg);  _rot90X180Z.    rotateZ(180*deg);
   _rot45X180Z.   rotateX(45*deg);  _rot45X180Z.    rotateZ(180*deg);
   _rot270Y180X.  rotateY(-90*deg); _rot270Y180X.   rotateX(180*deg);
}

//
// Detectors itself
//

void
DetectorConstructor::_task_construct_target() {
    G4double d_y1 = 1.*cm,          // inlet tube outer R
             d_y2 = d_y1-0.2*cm;    // inlet tube inner R
    # if 0  // RIA's old (?)
    G4double lvac=52.8*cm;

    G4Box *VolumeOut_box = 
        new G4Box("VolumeOut_box", lvac/2, 5.0/2*cm, 6.3/2*cm);
    G4Box *VolumeIn_box = 
        new G4Box("VolumeIn_box", (lvac+0.2*mm)/2, (5.0-0.6)/2*cm, (6.3-0.6)/2*cm);
    G4Box *Groove_box = 
        new G4Box("Groove_box",     9.0/2*cm, 0.21/2*cm, 3.8/2*cm);
    G4SubtractionSolid *Volume_0 = 
        new G4SubtractionSolid("Volume_0", VolumeOut_box, VolumeIn_box);
    G4SubtractionSolid *Volume_1 = Volume_0;
    G4SubtractionSolid *Volume_2 = 
    new G4SubtractionSolid("Volume_2", Volume_1, Groove_box, 
        G4Transform3D(RotateNull, G4ThreeVector(21.4*cm, 2.4*cm, 0.0*cm)));
    G4SubtractionSolid *Volume_3 = 
    new G4SubtractionSolid("Volume_3", Volume_2, Groove_box, 
        G4Transform3D(RotateNull, G4ThreeVector(21.4*cm, -2.4*cm, 0.*cm)));

    G4double z_pos = -75*mm + 21.6/2*cm; // to set an upstream edge of titanium window
    auto b_box = new G4Box("Window_box", 21.6/2*cm, 3.1/2*mm, 4.0/2*cm);

    G4SubtractionSolid *Volume_4 = 
    new G4SubtractionSolid("Volume_4", Volume_3, b_box, 
        G4Transform3D(RotateNull, G4ThreeVector(-z_pos, 5.0*cm/2.-3.*mm/2, 0.)));
    _target.hull.solid = 
    new G4SubtractionSolid("Volume_5", Volume_4, b_box, 
        G4Transform3D(RotateNull, G4ThreeVector(-z_pos,-(5.0*cm/2.-3.*mm/2),0.)));

    _target.hull.logv =  new G4LogicalVolume(_target.hull.solid, _steel, "Volume_log", 0, 0, 0);
    _target.hull.phys = new G4PVPlacement(0, G4ThreeVector(0.,0.,0.), 
        _target.hull.logv, "centr_box", _world.logv, false, 0);

    //
    // from RIA: titan foils
    //
    G4RotationMatrix Rotate180Z; Rotate180Z.rotateZ(180*deg);

    G4double y_pos = 2*mm + 1*mm + 0.07*mm;
    b_box = new G4Box("WIN_box", 23.0/2*cm, y_pos/2., 5.2/2*cm);
    G4LogicalVolume* WIN_log = new G4LogicalVolume(b_box, _vacuum, "WIN_log", 0, 0, 0);
    //WIN_log->SetVisAttributes(G4VisAttributes::Invisible);
    /*auto Volume_phys =*/ new G4PVPlacement(G4Transform3D(RotateNull, 
        G4ThreeVector(-z_pos, 5.*cm/2 + y_pos/2.,  0.)), 
        WIN_log, "Ti_win_vac", _world.logv, false, 0);
    /*Volume_phys =*/ new G4PVPlacement(G4Transform3D(Rotate180Z,
            G4ThreeVector(-z_pos,-(5.*cm/2 + y_pos/2.), 0.)), 
            WIN_log, "Ti_win_vac", _world.logv, false, 0);
    
    y_pos=-y_pos/2.;  //lower edge

    b_box = new G4Box("W1_box", 22.2/2*cm, 2.*mm/2., 4.4/2*cm);
    G4Box* s_box = new G4Box("W1_box", 21.6/2*cm, 2.02*mm/2., 4.0/2*cm);
    G4SubtractionSolid *vs = new G4SubtractionSolid("v_0", b_box, s_box);
    auto Volume_log = new G4LogicalVolume(vs, _steel, "W1_log", 0, 0, 0);
    //Volume_log->SetVisAttributes(Volume_logVisAtt);
    y_pos += 2.*mm/2.;
    /*Volume_phys =*/ new G4PVPlacement(0, G4ThreeVector(0., y_pos ,0.), 
        Volume_log, "window_fl1", WIN_log, false, 0);
    y_pos += 2.*mm/2.;

    b_box = new G4Box("W2_box", 23.0/2*cm, 1.*mm/2, 5.2/2*cm);
    s_box = new G4Box("W1_box", 21.6/2*cm, 1.02*mm/2., 4.0/2*cm);
   vs = new G4SubtractionSolid("v_1", b_box, s_box);
   Volume_log = new G4LogicalVolume(vs, _steel, "W2_log", 0, 0, 0);
   //Volume_log->SetVisAttributes(Volume_logVisAtt);
   y_pos += 1.*mm/2.;
   /*Volume_phys =*/ new G4PVPlacement(0, G4ThreeVector(0., y_pos ,0.), 
        Volume_log, "window_fl2", WIN_log, false, 0);
   y_pos += 1.*mm/2.;

   b_box =  new G4Box("TitanFoil_box", 23.0/2*cm, 0.070/2*mm, 5.2/2*cm);
   Volume_log = new G4LogicalVolume(b_box, _titan, "TitanFoil_log", 0, 0, 0);
   //Volume_log->SetVisAttributes(new G4VisAttributes(titanfoil_col));
   y_pos += 0.070*mm/2.;
   /*Volume_phys =*/ new G4PVPlacement(0, G4ThreeVector(0., y_pos ,0.), 
    Volume_log, "TitanFoil", WIN_log, false, 0);
    # else  // RIA's new
    G4double w_lng = 24.0*cm,       // titan window length
             w_shft = -10.*cm,
             w_pos = w_lng/2 + w_shft;

    auto VolumeOut_box = new G4Box("VolumeOut_box1", 6.3/2*cm, 5.0/2*cm, 52.8/2*cm);
    auto VolumeIn_box = new G4Box("VolumeIn_box", (6.3-0.6)/2*cm, (5.0-0.6)/2*cm, 52.81/2*cm);
    auto InletHole_tub = new G4Tubs("InletHole_tub", 0.0*cm, d_y1, 0.31/2*cm, 0*deg, 360*deg);
    auto Volume_0 = new G4SubtractionSolid("Volume_0", VolumeOut_box, VolumeIn_box);
    auto Volume_1 = new G4SubtractionSolid("Volume_1", Volume_0, InletHole_tub, 
                                           G4Transform3D(_rot90Y,
                                           G4ThreeVector(3.0*cm, 0.0*cm, 0.0*cm)));
    G4SubtractionSolid * Volume_3 = Volume_1;
    auto Cavity_box = new G4Box("Cavity_box", 4.0/2*cm, 5.05/2*cm, w_lng/2);
    _target.hull.solid = new G4SubtractionSolid("Volume", Volume_3, Cavity_box,
                                          G4Transform3D(RotateNull,
                                                        G4ThreeVector(0.0, 0.0, w_pos)));
    _target.hull.logv = new G4LogicalVolume(_target.hull.solid, _steel, "Volume_log", 0, 0, 0);
    _target.hull.phys = new G4PVPlacement(0, G4ThreeVector(), 
                                      _target.hull.logv,
                                      "Volume_phys",
                                      _world.logv, false, -1);
    _target.hull.logv->SetVisAttributes(_vaSteel);
    // windows
    G4Box *Window_box = new G4Box("Window_box", 5.2/2*cm, 3.0/2*mm, (w_lng+1.4*cm)/2);
    G4LogicalVolume* Window_log = new G4LogicalVolume(Window_box, _vacuum, "Window_log", 0, 0, 0);
    Window_log->SetVisAttributes(G4VisAttributes::Invisible);
   
    G4Box *W1b = new G4Box("W1b", 2.0/2*mm, 2.0/2*mm, w_lng/2);
    G4LogicalVolume* W1b_log = new G4LogicalVolume(W1b, _steel, "W1b_log", 0, 0, 0);
    W1b_log->SetVisAttributes(_vaSteel);
    // todo: keep vol_phys ptrs somewhere to possible cleaning
    /*vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(2.1*cm, -0.5*mm ,0.0), 
              W1b_log, "W1b", Window_log, false, -1);
    /*vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(-2.1*cm, -0.5*mm ,0.0), 
              W1b_log, "W1b", Window_log, false, -1);

    G4Box *W2b = new G4Box("W2b", 4.4/2*cm, 2.0/2*mm, 3.0/2*mm);
    G4LogicalVolume* W2b_log = new G4LogicalVolume(W2b, _steel, "W2b_log", 0, 0, 0);
    W2b_log->SetVisAttributes(_vaSteel);
    
    /*vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(0.0, -0.5*mm ,(w_lng+3*mm)/2.), 
              W2b_log, "W2b", Window_log, false, -1);
    /*vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(0.0, -0.5*mm ,-(w_lng+.3*mm)/2.), 
              W2b_log, "W2b", Window_log, false, -1);

    G4Box *W3b = new G4Box("W3b", 6.0/2*mm, 1.0/2*mm, w_lng/2);
    G4LogicalVolume* W3b_log = new G4LogicalVolume(W3b, _steel, "W3b_log", 0, 0, 0);
    W3b_log->SetVisAttributes(_vaSteel);
    
    /*vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(2.3*cm, 1.0*mm ,0.0), 
              W3b_log, "W3b", Window_log, false, -1);
    /*vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(-2.3*cm, 1.0*mm ,0.0), 
              W3b_log, "W3b", Window_log, false, -1);

    G4Box *W4b = new G4Box("W4b", 5.2/2*cm, 1.0/2*mm, 7.0/2*mm);
    G4LogicalVolume* W4b_log = new G4LogicalVolume(W4b, _steel, "W4b_log", 0, 0, 0);
    W4b_log->SetVisAttributes(_vaSteel);
    
    /*vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(0.0, 1.0*mm ,(w_lng+7.*mm)/2.), 
              W4b_log, "W4b", Window_log, false, -1);
    /*vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(0.0, 1.0*mm ,-(w_lng+7.*mm)/2.), 
              W4b_log, "W4b", Window_log, false, -1);

    // TITAN FOILS
    G4Box *TitanFoil_box = 
    new G4Box("TitanFoil_box", 5.2/2*cm, 0.007/2*cm, (w_lng+1.4*cm)/2.);
 
    G4LogicalVolume*
    TitanFoil_log = new G4LogicalVolume(TitanFoil_box, _titan, "TitanFoil_log", 0, 0, 0);    
    /*vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(0.0*cm, 2.81*cm, w_pos), 
              TitanFoil_log, "TitanFoil_phys", _world.logv, false, -1);
    /*vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(0.0*cm, -2.81*cm, w_pos), 
              TitanFoil_log, "TitanFoil_phys", _world.logv, false, -1);

    TitanFoil_log->SetVisAttributes(_vaTitanFoil);
    # endif
    // tubes
    _target.inTube.solid = new G4Tubs("Inlet_tub", d_y2, d_y1, 20./2.*cm, 0*deg, 360*deg);
    _target.inTube.logv = new G4LogicalVolume(_target.inTube.solid, _alumin,
                                         "InTub_log", 0, 0, 0);
    _target.inTube.phys = new G4PVPlacement(
                    G4Transform3D(_rot90Y, G4ThreeVector((1.2+10.)*cm, 0.0*cm, 0.0*cm)),
                    _target.inTube.logv, "InTub_phys", _world.logv, false, -1);
    _target.inTube.logv->SetVisAttributes(_vaAlum);
}

void
DetectorConstructor::_task_construct_cell() {
    G4double cellThickness = 0.05;
    auto Cell_tube = 
        new G4Tubs("Cell", 13./2.*mm, (13.+cellThickness)/2.*mm, 40./2.*cm, 90.*deg, 180.*deg);
    G4LogicalVolume*
    Cell_log = new G4LogicalVolume(Cell_tube, _alumin, "Cell_log", 0, 0, 0);
    Cell_log->SetVisAttributes(_vaAlum);
    /*vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(-(24.-13.)/2.*mm, 0., 0.), 
             Cell_log, "Cell", _world.logv, false, -1);    
    /*vol_phys =*/ new G4PVPlacement(&_rot180Z, G4ThreeVector((24.-13.)/2.*mm, 0., 0.), 
             Cell_log, "Cell", _world.logv, false, -1);
    G4Box *Cell_plate = new G4Box("Cplate",(24.-13.)/2*mm,cellThickness/2*mm,40./2.*cm);    
    Cell_log = new G4LogicalVolume(Cell_plate, _alumin, "Cell2_log", 0, 0, 0);
    Cell_log->SetVisAttributes(_vaAlum);
    /*vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(0.,(13.+cellThickness)/2., 0.), 
             Cell_log, "Cell", _world.logv, false, -1);    
    /*vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(0.,-(13.+cellThickness)/2., 0.), 
             Cell_log, "Cell", _world.logv, false, -1);    
}

void
DetectorConstructor::_task_construct_lowq() {
    G4Box *LQBox_box = new G4Box("LQBox_box", 10.0/2*cm, 4.25/2*cm, 2.8/2*cm);
    
    auto LQBox_log = new G4LogicalVolume(LQBox_box, _air, "LQBox_log", 0, 0, 0);
    /*auto vol_phys =*/ new G4PVPlacement(G4Transform3D(_rot270X, 
                   G4ThreeVector(0.0, +3.95*cm, 23.4*cm)), 
                LQBox_log, "LQBox_phys", _world.logv, false, -1);
    
    LQBox_log->SetVisAttributes(G4VisAttributes::Invisible);

    G4Box *LQArmBox_box = 
        new G4Box("LQArmBox_box", 6.95/2*cm, 1.95/2*cm, 2.8/2*cm);
    
    auto LQArmBox1_log = new G4LogicalVolume(LQArmBox_box, _air, "LQArmBox1_log", 0, 0, 0);
    auto LQArmBox2_log = new G4LogicalVolume(LQArmBox_box, _air, "LQArmBox2_log", 0, 0, 0);
    /*vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(-1.025*cm, 0.275*cm, 0.0*cm), 
                LQArmBox1_log, "LQArmBox1_phys", LQBox_log, false, armOneIndex);
    /*vol_phys =*/ new G4PVPlacement(G4Transform3D(_rot180Y, 
                   G4ThreeVector(1.025*cm, -0.275*cm, 0.0*cm)), 
                LQArmBox2_log, "LQArmBox2_phys", LQBox_log, false, armTwoIndex);

    LQArmBox1_log->SetVisAttributes(G4VisAttributes::Invisible);
    LQArmBox2_log->SetVisAttributes(G4VisAttributes::Invisible);


    G4Box *LQConv_box = 
        new G4Box("LQConv_box", 6.0/2*cm, 1.6/2*cm, 2.4/2*cm);

    auto LQConv_log = new G4LogicalVolume(LQConv_box, _tungsten, "LQConv_log", 0, 0, 0);
    /* vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(0.0*cm, 1.375*cm, -0.2*cm), 
                    LQConv_log, "LQConv_phys", LQBox_log, false, -1);

    G4VisAttributes *LQConv_logVisAtt = new G4VisAttributes(_clrConvertor);
    LQConv_log->SetVisAttributes(LQConv_logVisAtt);

    
    G4Tubs *LQHolder_tub =
        new G4Tubs("LQHolder_tub", 0.0*cm, 3.2/2*cm, 2.8/2*cm, 90*deg, 90*deg);

    G4Box *LQHolder_box1 =
        new G4Box("LQHolder_box1", 0.4/2*cm, 1.69/2*cm, 2.8/2*cm);

    G4Box *LQHolder_box2 =
        new G4Box("LQHolder_box2", 1.01/2*cm, 0.51/2*cm, 2.81/2*cm);

    G4UnionSolid *LQHolder_0 = 
        new G4UnionSolid("LQHolder_0", LQHolder_tub, LQHolder_box1, 
                     G4Transform3D(RotateNull, G4ThreeVector(0.2*cm, 0.855*cm, 0.0*cm)));

    G4SubtractionSolid *LQHolder_1 = 
        new G4SubtractionSolid("LQHolder_1", LQHolder_0, LQHolder_box2, 
                           G4Transform3D(RotateNull, G4ThreeVector(-0.1*cm, 0.22*cm, 0.0*cm)));

    G4Box *LQCavity_box1 = 
        new G4Box("LQCavity_box1", 4.9/2*cm, 0.3/2*cm, 2.8/2*cm);
    
    G4Box *LQCavity_box2 = 
        new G4Box("LQCavity_box2", 0.05/2*cm, 0.3/2*cm, 2.8/2*cm);

    G4Box *LQCavity_box3 = 
        new G4Box("LQCavity_box3", 5.3/2*cm, 0.2/2*cm, 2.8/2*cm);
    
    G4UnionSolid *LQCavity_0 = 
        new G4UnionSolid("LQCavity_0", LQCavity_box1, LQCavity_box2, 
            G4Transform3D(RotateNull, G4ThreeVector(2.475*cm, 0.0*cm, 0.0*cm)));

    G4UnionSolid *LQCavity_1 = 
        new G4UnionSolid("LQCavity_1", LQCavity_0, LQCavity_box3, 
            G4Transform3D(RotateNull, G4ThreeVector(-0.2*cm, 0.25*cm, 0.0*cm)));

    G4Box *LQGrooveArm1_box = 
        new G4Box("LQGrooveArm1_box", 4.9/2*cm, 0.21/2*cm, 2.0/2*cm);
    
    G4Box *LQWindowArm1_box = 
        new G4Box("LQWindowArm1_box", 0.91/2*cm, 0.11/2*cm, 2.0/2*cm);

    G4SubtractionSolid *LQCavityArm1_0 = 
        new G4SubtractionSolid("LQCavityArm1_0", LQCavity_1, LQGrooveArm1_box, 
            G4Transform3D(RotateNull, G4ThreeVector(0.0*cm, 0.05*cm, 0.0*cm)));

    G4SubtractionSolid *LQCavityArm1_1 = 
        new G4SubtractionSolid("LQCavityArm1_1", LQCavityArm1_0, LQWindowArm1_box, 
            G4Transform3D(RotateNull, G4ThreeVector(-2.0*cm, -0.1*cm, 0.0*cm)));

    G4Box *LQGrooveArm2_box = 
        new G4Box("LQGrooveArm2_box", 4.9/2*cm, 0.201/2*cm, 2.2/2*cm);

    G4Box *LQWindowArm2_box = 
        new G4Box("LQWindowArm2_box", 0.91/2*cm, 0.11/2*cm, 2.2/2*cm);

    G4SubtractionSolid *LQCavityArm2_0 = 
        new G4SubtractionSolid("LQCavityArm2_0", LQCavity_1, LQGrooveArm2_box, 
            G4Transform3D(RotateNull, G4ThreeVector(0.*cm, 0.05*cm, 0.0*cm)));
    
    G4SubtractionSolid *LQCavityArm2_1 = 
        new G4SubtractionSolid("LQCavityArm2_1", LQCavityArm2_0, LQWindowArm2_box, 
            G4Transform3D(RotateNull, G4ThreeVector(-2.0*cm, -0.1*cm, 0.0*cm)));

    G4UnionSolid *LQFrame1 = 
        new G4UnionSolid("LQFrame1", LQCavityArm1_1, LQHolder_1, 
            G4Transform3D(RotateNull, G4ThreeVector(-2.84*cm, -1.6*cm, 0.0*cm)));
    
    /*G4UnionSolid *LQFrame2 = */
        new G4UnionSolid("LQFrame2", LQCavityArm2_1, LQHolder_1, 
            G4Transform3D(RotateNull, G4ThreeVector(-2.84*cm, -1.6*cm, 0.0*cm)));

    auto LQFrame_log = new G4LogicalVolume(LQFrame1, _brass, "LQFrame_log", 0, 0, 0);
    /*vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(0.975*cm, -0.1*cm, 0.0*cm), 
                    LQFrame_log, "LQFrame_phys", LQArmBox1_log, false, -1);
    /*vol_phys =*/ new G4PVPlacement(0, G4ThreeVector(0.975*cm, -0.1*cm, 0.0*cm), 
                    LQFrame_log, "LQFrame_phys", LQArmBox2_log, false, -1);

    G4VisAttributes *LQFrame_logVisAtt = new G4VisAttributes(_clrBrass);
    LQFrame_log->SetVisAttributes(LQFrame_logVisAtt);

    
    G4Tubs *LQFibr_tub = 
        new G4Tubs("LQFibr_tub", 0.0*cm, 0.1*cm, 4.8/2*cm, 0*deg, 360*deg);

    auto LQFibr_log = new G4LogicalVolume(LQFibr_tub, _scintil, "LQFibr_log", 0, 0, 0);

    for(unsigned char i=0; i<=9; i++) {
        /*auto LQFibr_phys = */
        new G4PVPlacement(G4Transform3D(_rot90Y, G4ThreeVector(0.975*cm, -0.05*cm, (i*2.0/10-1.0+0.1)*cm)), 
                LQFibr_log, "LQFibr_phys", LQArmBox1_log, false, LQ_IND+i);
    }
    for(unsigned char i=0; i<=10; i++) {    
        /*auto LQFibr_phys = */
        new G4PVPlacement(G4Transform3D(_rot90Y, G4ThreeVector(0.975*cm, -0.05*cm, (i*2.2/11-1.1+0.1)*cm)), 
            LQFibr_log, "LQFibr_phys", LQArmBox2_log, false, LQ_IND+i+10);
    }

    LQFibr_log->SetVisAttributes(_vaPlastic);
}

void
DetectorConstructor::_task_construct_vertex_chamber(G4LogicalVolume *& VCBox_log) {
    G4Box *vbox = 
        new G4Box("VCBox", 12.0/2.*cm, 3.0/2.*cm, 53.0/2.*cm);
    VCBox_log = new G4LogicalVolume(vbox, _air, "VCBox_log", 0, 0, 0);
    VCBox_log->SetVisAttributes(G4VisAttributes::Invisible);

    vbox = new G4Box("frame", 11.4/2.*cm, 0.4/2.*cm, 52.0/2.*cm); 

    G4Box *vcwin = new G4Box("framew", 9.4/2.*cm, 0.4025/2.*cm, 43.6/2.*cm);

    G4SubtractionSolid *VCframe = 
        new G4SubtractionSolid("VCframe_0", vbox, vcwin, 
            G4Transform3D(RotateNull, G4ThreeVector(0., 0., 0.)));

    vbox = new G4Box("stef", 114./2.*mm, 20.0/2.*mm, 48.0/2.*cm); 

    vcwin = new G4Box("stefw", 94./2.*mm,20.02/2.*mm, 43.6/2.*cm);

    G4SubtractionSolid *VCStef = 
        new G4SubtractionSolid("VCframe_0", vbox, vcwin, 
            G4Transform3D(RotateNull, G4ThreeVector(0., 0., 0.)));

    G4LogicalVolume *VC_log = new G4LogicalVolume(VCframe, _steel, "VCFrame_log", 0, 0, 0);
        new G4PVPlacement(0, G4ThreeVector(0., 12.0*mm, 0.), 
            VC_log, "VCFrame_phys", VCBox_log, false, -1);

    VC_log->SetVisAttributes(_vaSteel);

    VC_log = new G4LogicalVolume(VCStef, _SiO2, "VCStef_log", 0, 0, 0);
    new G4PVPlacement(0, G4ThreeVector(0., 0.0*cm, 0.), 
            VC_log, "VCStef_phys", VCBox_log, false, -1);

    VC_log->SetVisAttributes(_vaStef);

    vbox =  new G4Box("VCGas_trap", 2.*NVC_WRS/2.*mm,10.0/2.*mm,43.6/2.*cm);

    VC_log = new G4LogicalVolume(vbox, _gasArCO2, "VCGas_log", 0, 0, 0);
    new G4PVPlacement(0, G4ThreeVector(0.0*cm, 0.0*cm, 0.0*cm), 
        VC_log, "VCtrap_phys", VCBox_log, false, VC_IND);
   
    vbox =  new G4Box("VCGas_cell", 2./2.*mm,10.0/2.*mm,43.6/2.*cm);

    VCGas_log = new G4LogicalVolume(vbox, _gasArCO2, "VCGas_log", 0, 0, 0);
    new G4PVReplica("VCcells",VCGas_log,VC_log, kXAxis, NVC_WRS, 2.*mm);
   

//   VCVisAtt = new G4VisAttributes(gas_col);
    VCGas_log->SetVisAttributes(_vaGas);

    vbox = new G4Box("VCMylar_box1", 94.0/2.*mm, 0.02/2.*mm, 43.6/2.*cm);

    VC_log = new G4LogicalVolume(vbox, _mylar, "VCMylar1_log", 0, 0, 0);
        new G4PVPlacement(0, G4ThreeVector(0.0, -5.01*mm, 0.0), 
             VC_log, "VCMylar1_phys", VCBox_log, false, -1);
    new G4PVPlacement(0, G4ThreeVector(0.0,  5.01*mm, 0.0), 
             VC_log, "VCMylar1_phys", VCBox_log, false, -1);

//   VCVisAtt = new G4VisAttributes(mylar_col);
//   VCVisAtt->SetForceWireframe(true);
    VC_log->SetVisAttributes(_vaMylar);

    vbox = new G4Box("VCMylar_box1", 94.0/2.*mm, 0.07/2.*mm, 43.6/2.*cm);

    VC_log = new G4LogicalVolume(vbox, _mylar, "VCMylar1_log", 0, 0, 0);
        new G4PVPlacement(0, G4ThreeVector(0.0, -10.0*mm, 0.0), 
             VC_log, "VCMylar1_phys", VCBox_log, false, -1);
    new G4PVPlacement(0, G4ThreeVector(0.0,  10.0*mm, 0.0), 
             VC_log, "VCMylar1_phys", VCBox_log, false, -1);

    VC_log->SetVisAttributes(_vaMylar);
}

void
DetectorConstructor::_task_construct_tracking_chambers(G4LogicalVolume *& VCBox_log) {
    G4LogicalVolume* WCTheta1 = _task_construct_single_tracking_chamber(
            NW1_WRS*2.*cm, 20.*cm, WC1_IND, _WC.Theta1_gas); 
    G4LogicalVolume* WCPhi1 = _task_construct_single_tracking_chamber(
            NW2_WRS*2.*cm, 45.*cm, WC2_IND, _WC.Phi1_gas);
    G4LogicalVolume* WCTheta2 = _task_construct_single_tracking_chamber(
            NW3_WRS*2.*cm, 43.*cm, WC3_IND, _WC.Theta2_gas); 

    new G4PVPlacement(G4Transform3D(_rot180Z, G4ThreeVector(0.0*cm,-7.3*cm, 10.*cm)), 
             VCBox_log, "VCBox_phys", _world.logv, false, ARM1_IND);
    new G4PVPlacement(G4Transform3D(_rot180Z,    
               G4ThreeVector(0.0*cm, -15.0*cm, 9.0*cm)), 
            WCTheta1, "WCTheta1a", _world.logv, false, ARM1_IND);
    new G4PVPlacement(G4Transform3D(_rot180Z,    
               G4ThreeVector(0.0*cm, -35.0*cm, 20.0*cm)), 
            WCTheta2, "WCTheta2a", _world.logv, false, ARM1_IND);
    new G4PVPlacement(G4Transform3D(_rot90Y180Z,    
               G4ThreeVector(0.0*cm, -25.0*cm, 14.5*cm)), 
            WCPhi1, "WCPhi1a", _world.logv, false, ARM1_IND);

    new G4PVPlacement(G4Transform3D(RotateNull, G4ThreeVector(0.0*cm, 7.3*cm, 10.*cm)), 
             VCBox_log, "VCBox_phys", _world.logv, false, ARM2_IND);
    new G4PVPlacement(G4Transform3D(RotateNull,    
               G4ThreeVector(0.0*cm,  15.0*cm, 9.0*cm)), 
            WCTheta1, "WCTheta1b", _world.logv, false, ARM2_IND);
    new G4PVPlacement(G4Transform3D(RotateNull,    
               G4ThreeVector(0.0*cm,  35.0*cm, 20.0*cm)), 
            WCTheta2, "WCTheta2b", _world.logv, false, ARM2_IND);
    new G4PVPlacement(G4Transform3D(_rot90Y,    
               G4ThreeVector(0.0*cm,  25.0*cm, 14.5*cm)), 
            WCPhi1, "WCPhi1b", _world.logv, false, ARM2_IND);
}


////////////////////////////////////////////////////////////////////////////////
// VOLUME : Tracking chamber
////////////////////////////////////////////////////////////////////////////////

G4LogicalVolume *
DetectorConstructor::_task_construct_single_tracking_chamber(
            G4double Lwin,
            G4double Wwin,
            G4int ind,
            G4LogicalVolume*& forSD) {
    //         
    //        Y^
    //         |
    //         +---->Z
    //       /        
    //    X|_


    G4double Lframe,    Wframe, Gap,    Swid,
             x_pos,     y_pos,  z_pos;

    //Lwin=40.*cm; Wwin=15.*cm; 
    Swid=4.*cm; Gap=6.*mm;
    Lframe=Lwin+Swid*2;
    Wframe=Wwin+Swid*2;

    G4double thk=Gap*8.;
    G4double foil_thk=0.03*mm;
    G4double mylar_thk=0.05*mm;

    G4Box *WCBox_box = 
    new G4Box("WCBox_box", Wframe/2., thk/2., Lframe/2.);

    G4LogicalVolume * 
    WCBox_log = new G4LogicalVolume(WCBox_box, _gasArCO2, "WCBox_log", 0, 0, 0);
    WCBox_log->SetVisAttributes(G4VisAttributes::Invisible);
    
    //-------------------------------------------------------------------------

    G4Box*
    WCBox = new G4Box("WC_S1", Swid/2., thk/2., Lwin/2.);
    G4LogicalVolume* 
    WC_log = new G4LogicalVolume(WCBox, _SiO2, "S1", 0,0,0);
    WC_log->SetVisAttributes(_vaStef);

    x_pos=Wwin/2.+Swid/2.;
    new G4PVPlacement(0,G4ThreeVector(x_pos,0.,0.0),WC_log,"WCS1a",
                      WCBox_log,false,-1);
    new G4PVPlacement(0,G4ThreeVector(-x_pos,0.,0.0),WC_log,"WCS1b",
                      WCBox_log,false,-1);

    x_pos=Wwin+2*Swid;
    WCBox = new G4Box("WC_S2", x_pos/2., thk/2., Swid/2.);
    WC_log = new G4LogicalVolume(WCBox, _SiO2, "S2", 0,0,0);
    WC_log->SetVisAttributes(_vaStef);
    z_pos=Lwin/2.+Swid/2.;
    new G4PVPlacement(0,G4ThreeVector(0.0,0.0,z_pos),WC_log,"WCS2a",
                      WCBox_log,false,-1);
    new G4PVPlacement(0,G4ThreeVector(0.0,0.0,-z_pos),WC_log,"WCS2b",
                      WCBox_log,false,-1);

    //----------------------------------------
    G4Box*
    WCFoil_box = new G4Box("WCFoil_box", Wwin/2., foil_thk, Lwin/2.);

    G4Box* 
    WCMylar_box = new G4Box("WCMylar_box", Wwin/2., mylar_thk, Lwin/2.);

    G4LogicalVolume*
    WCFoil = new G4LogicalVolume(WCFoil_box, _mylar, "WCfoil",0,0,0);
    WCFoil->SetVisAttributes(_vaFoil);
    G4LogicalVolume*
    WCMylar = new G4LogicalVolume(WCMylar_box, _mylar, "WCmylar",0,0,0);
    WCMylar->SetVisAttributes(_vaMylar);

    y_pos=Gap*4;
    new G4PVPlacement(0,G4ThreeVector(0.0,y_pos,0.0),WCMylar,"WCM1a",
                     WCBox_log,false,-1);
    new G4PVPlacement(0,G4ThreeVector(0.0,-y_pos,0.0),WCMylar,"WCM1b",
                     WCBox_log,false,-1);
    y_pos=Gap*1;
    new G4PVPlacement(0,G4ThreeVector(0.0,y_pos,0.0),WCFoil,"WCF1a",
                     WCBox_log,false,-1);
    new G4PVPlacement(0,G4ThreeVector(0.0,-y_pos,0.0),WCFoil,"WCF1b",
                     WCBox_log,false,-1);
    y_pos=Gap*3;
    new G4PVPlacement(0,G4ThreeVector(0.0,y_pos,0.0),WCFoil,"WCF2a",
                     WCBox_log,false,-1);
    new G4PVPlacement(0,G4ThreeVector(0.0,-y_pos,0.0),WCFoil,"WCF2b",
                     WCBox_log,false,-1);
    //------------------------------------------------------------------------------
    // for sensitive detector -- wire planes
    x_pos=Wwin;//-4.*mm; 
    y_pos=2.*Gap*0.9; 
    z_pos=Lwin;//-4.*mm;
    G4Box*
    WCplane_box = new G4Box("WCplane_box", x_pos/2., y_pos/2., z_pos/2.);
    G4Box*
    WCcell_box = new G4Box("WCcell_box", x_pos/2., y_pos/2., 2.*cm/2.);

    G4LogicalVolume*
    WCplane_log = new G4LogicalVolume(WCplane_box, _gasArCO2, "WCplane",0,0,0);
    WCplane_log->SetVisAttributes(G4VisAttributes::Invisible);

    G4LogicalVolume*
    WCcell_log = new G4LogicalVolume(WCcell_box, _gasArCO2, "WCcell",0,0,0);
    //   WCcell_log->SetVisAttributes(Gas_VisAtt);
    WCcell_log->SetVisAttributes(_vaSteel);
    G4int nr=Lwin/(2.*cm)+0.1;
    new G4PVReplica("WCcells",WCcell_log,WCplane_log, kZAxis, nr, 2.*cm);
    y_pos=-2.*Gap;
    new G4PVPlacement(0,G4ThreeVector(0.0,y_pos,0.0),WCplane_log,"WCpl1",
                     WCBox_log,false,ind);
    y_pos=0.;
    new G4PVPlacement(0,G4ThreeVector(0.0,y_pos,0.0),WCplane_log,"WCpl2",
                     WCBox_log,false,ind);
    y_pos=2.*Gap;
    new G4PVPlacement(0,G4ThreeVector(0.0,y_pos,0.0),WCplane_log,"WCpl3",
                     WCBox_log,false,ind);            
    forSD = WCcell_log;
    return WCBox_log;
}

void
DetectorConstructor::_task_construct_hadron_sandwich() {
    G4LogicalVolume *lFEU;

    G4Tubs *FEU30_box = new G4Tubs("FEU30",0.0*cm,4.2*cm,25.2/2.0*cm,0.,2.0*M_PI);
    G4LogicalVolume *FEU30_log = 
           new G4LogicalVolume(FEU30_box, _air,"FEU30_log",0,0,0);
    FEU30_log->SetVisAttributes(G4VisAttributes::Invisible);

    G4Tubs *FEU30_tub = new G4Tubs("FEU30tub",3.0*cm,4.0*cm,25.0/2.0*cm,0.,2.0*M_PI);
    lFEU=new G4LogicalVolume(FEU30_tub, _steel,"FEU30t_log",0,0,0);
    new G4PVPlacement(0, G4ThreeVector(0.,0.,0.1*cm), 
             lFEU, "t", FEU30_log, false, -1);
    G4Tubs *FEU30_lid = new G4Tubs("FEU30lid",0.0,4.0*cm,0.1/2.0*cm,0.,2.0*M_PI);
    lFEU=new G4LogicalVolume(FEU30_lid, _steel,"FEU30l_log",0,0,0);
    new G4PVPlacement(0, G4ThreeVector(0.,0.,+(25.0/2+0.1)*cm), 
             lFEU, "l", FEU30_log, false, -1);

   

    G4Tubs *FEU63_box = new G4Tubs("FEU63",0.0*cm,8.2*cm,35.2/2.0*cm,0.,2.0*M_PI);
    G4LogicalVolume *FEU63_log = 
           new G4LogicalVolume(FEU63_box, _air,"FEU63_log",0,0,0);
    FEU63_log->SetVisAttributes(G4VisAttributes::Invisible);

    G4Tubs *FEU63_tub = new G4Tubs("FEU63tub",7.0*cm,8.0*cm,35.0/2.0*cm,0.,2.0*M_PI);
    lFEU=new G4LogicalVolume(FEU63_tub, _steel,"FEU63t_log",0,0,0);
    lFEU->SetVisAttributes( _vaIron);
    new G4PVPlacement(0, G4ThreeVector(0.,0.,0.1*cm), 
             lFEU, "t", FEU63_log, false, -1);
    G4Tubs *FEU63_lid = new G4Tubs("FEU63lid",0.0,8.0*cm,0.1/2.0*cm,0.,2.0*M_PI);
    lFEU=new G4LogicalVolume(FEU63_lid, _steel,"FEU63l_log",0,0,0);
    lFEU->SetVisAttributes(_vaIron);
    new G4PVPlacement(0, G4ThreeVector(0.,0.,+(35.0/2+0.1)*cm), 
             lFEU, "l", FEU63_log, false, -1);

    // SANDWICH

    G4double AbsorberThickness    = 20.*mm;
    G4double ScintThickness    =  5.*mm,
       ScintSizeX        = 10.0*cm,    // 10 !!!
       ScintSizeZ        = 10.0*cm;    // 10 !!!
    G4double GapSize        = 0.1*mm;
    G4int NbOfLayers        = 10,       // Hadron Calo layers */
    //  G4int NbOfELayers        = 2;        // EM preshower layers */
        NbOfXBars        = NX_BARS,    // number of bars for phi
        NbOfZBars        = NZ_BARS;    // number of bars for theta

    G4double VertPos        = 150.0*cm;    // 150.*cm; !!
    G4double HorPos = VertPos*(tan((90.-45.)*deg)+tan((90.-85.)*deg))/2.;

    // Compute sizes
    _sandwhichSizes.layerX = ScintSizeX*NbOfXBars;
    _sandwhichSizes.layerY = (AbsorberThickness+GapSize+2.*(ScintThickness+GapSize));
    _sandwhichSizes.layerZ = ScintSizeZ*NbOfZBars;

    _sandwhichSizes.sizeX = _sandwhichSizes.layerX;
    _sandwhichSizes.sizeY = _sandwhichSizes.layerY*NbOfLayers;
    _sandwhichSizes.sizeZ = _sandwhichSizes.layerZ;


    //G4VisAttributes *ProCover_VisAtt = new G4VisAttributes(blackpaper_col); //todo

    G4Box* ubox=new G4Box( "Sand",
                            _sandwhichSizes.sizeX/2.,
                            _sandwhichSizes.sizeY/2.,
                            _sandwhichSizes.sizeZ/2.);
    G4LogicalVolume* sand_vol = new G4LogicalVolume(ubox, _air, "Sand", 0,0,0);
    sand_vol->SetVisAttributes(G4VisAttributes::Invisible);

    ubox = new G4Box("ScintZ", _sandwhichSizes.layerX/2.,
                               ScintThickness/2.,
                               ScintSizeZ/2.);
    _scintillators.zscint_log = new G4LogicalVolume(ubox, _scintil, "ScintZ", 0,0,0);
    _scintillators.zscint_log->SetVisAttributes(_vaPlastic);

    ubox = new G4Box("LayerZ", _sandwhichSizes.layerX/2.,
                               ScintThickness/2.,
                               _sandwhichSizes.layerZ/2.);
    G4LogicalVolume* zlayer_log = new G4LogicalVolume(ubox, _air, "LayerZ", 0,0,0);
    zlayer_log->SetVisAttributes(_vaPlastic);
  
    new G4PVReplica("BarsZ",_scintillators.zscint_log,zlayer_log, kZAxis, NbOfZBars, ScintSizeZ);

  
    ubox = new G4Box("ScintX", ScintSizeX/2.,
                               ScintThickness/2.,
                               _sandwhichSizes.sizeZ/2.);
    _scintillators.xscint_log = new G4LogicalVolume(ubox, _scintil, "ScintX", 0,0,0);
    _scintillators.xscint_log->SetVisAttributes(_vaPlastic);

    ubox = new G4Box("LayerX",  _sandwhichSizes.layerX/2.,
                                ScintThickness/2.,
                                _sandwhichSizes.sizeZ/2.);
    G4LogicalVolume* xlayer_log = new G4LogicalVolume(ubox, _air, "LayerX", 0,0,0);
    xlayer_log->SetVisAttributes(_vaPlastic);
  
    new G4PVReplica("BarsX", _scintillators.xscint_log,xlayer_log, kXAxis, NbOfXBars, ScintSizeX);

  
    ubox = new G4Box("Absor", _sandwhichSizes.sizeX/2.,
                              AbsorberThickness/2.,
                              _sandwhichSizes.sizeZ/2.);
    G4LogicalVolume* absor_vol = new G4LogicalVolume(ubox, _steel, "Absor", 0,0,0);
    absor_vol->SetVisAttributes(_vaConvertor);

    G4double y_pos=-(NbOfLayers/2.)*_sandwhichSizes.layerY;
    for(int i=0; i < NbOfLayers; i++){
        y_pos +=AbsorberThickness/2.;
        new G4PVPlacement(0, G4ThreeVector(0.,y_pos,0.), 
                 absor_vol, "Absorbs", sand_vol, false, -1);
        y_pos+=AbsorberThickness/2.+GapSize+ScintThickness/2.;
        new G4PVPlacement(0, G4ThreeVector(0.,y_pos,0.), 
                 zlayer_log, "ScintsZ", sand_vol, false, HCZ_IND);
        y_pos+=ScintThickness/2.+GapSize+ScintThickness/2.;
        new G4PVPlacement(0, G4ThreeVector(0.,y_pos,0.), 
                 xlayer_log, "ScintsX", sand_vol, false, HCX_IND);
        y_pos+=ScintThickness/2.+GapSize;
    }

    // Place sandwitches   
    new G4PVPlacement(G4Transform3D(_rot180Z, G4ThreeVector(0.0*cm,-VertPos, HorPos)), 
             sand_vol, "Sand_phys", _world.logv, false, ARM1_IND);
    new G4PVPlacement(G4Transform3D(RotateNull, G4ThreeVector(0.0*cm, VertPos, HorPos)), 
             sand_vol, "Sand_phys", _world.logv, false, ARM2_IND);

    G4double pl_thick =  1.0*cm,
             pl_width =  100.0*cm,    // z-axis
             pl_length = 60.0*cm;    // x-axis

    ubox = new G4Box("AC_box", 300.0/2.*cm, (pl_thick+0.5*cm)/2.,(pl_width+1*cm)/2.);
    G4LogicalVolume *ACBox_log = new G4LogicalVolume(ubox, _air, "ACBox_log", 0, 0, 0);
    ACBox_log->SetVisAttributes(G4VisAttributes::Invisible);

    ubox = new G4Box("AC", pl_length/2., pl_thick/2., pl_width/2.);
    _scintillators.AC_log = new G4LogicalVolume(ubox, _scintil, "AC_log", 0, 0, 0);
    _scintillators.AC_log->SetVisAttributes(_vaPlastic);
    new G4PVPlacement(0, G4ThreeVector(0.0, 0.0, 0.0), 
        _scintillators.AC_log, "AC", ACBox_log, false, AC_IND);

    ubox = new G4Box("ACCover", pl_length/2., 0.15/2.*mm, pl_width/2.);
    G4LogicalVolume *ACCover_log = new G4LogicalVolume(ubox, _mylar, "ACCover_log", 0, 0, 0);
    //   ACCover_log->SetVisAttributes(ProCover_VisAtt);
    ACCover_log->SetVisAttributes(new G4VisAttributes(G4Color(0.1,0.1,0.5)));
    new G4PVPlacement(0,G4ThreeVector(0.0, +pl_thick/2.+0.1*mm, 0.0), 
        ACCover_log, "ACCover_phys", ACBox_log, false, -1);
    new G4PVPlacement(0,G4ThreeVector(0.0, -pl_thick/2.-0.1*mm, 0.0), 
        ACCover_log, "ACCover_phys", ACBox_log, false, -1);

    G4double d_z  = 15.0*cm,
             d_y1 = pl_thick,
             d_y2 = 10.0*cm;        
    G4Trap *actrap = new G4Trap("actrap",d_z/2.0, 0., 0., 
                        d_y1/2.0, pl_width/2.0, pl_width/2.0, 0.,
                        d_y2/2.0, d_y2/2.0, d_y2/2.0, 0.);
    new G4LogicalVolume(actrap, _scintil, "aclg", 0, 0, 0);
    // PLACE AC
    // Position -- close to 3rd Drift Chamber
    G4double x_pos = 0.0;
             y_pos = 45.*cm;
    G4double z_pos = y_pos*(tan((90.-45.)*deg)+tan((90.-85.)*deg))/2.;
  
    new G4PVPlacement(G4Transform3D(RotateNull, 
               G4ThreeVector(x_pos, -y_pos, z_pos)), 
            ACBox_log, "AC_phys", _world.logv, false, ARM1_IND);
    new G4PVPlacement(G4Transform3D(_rot180Z, 
               G4ThreeVector(x_pos,  y_pos, z_pos)), 
            ACBox_log, "AC_phys", _world.logv, false, ARM2_IND);

    // TODO: do we need them?
    attLength[AC_IND+ARM1_IND] = attLength[AC_IND+ARM2_IND] = 200.*cm;
    discrThreshold[AC_IND+ARM1_IND] = discrThreshold[AC_IND+ARM2_IND] = 1.0*MeV;

    // RPC

    pl_thick=1.0*cm;    // y-axis
    pl_width=_sandwhichSizes.sizeZ;    // z-axis
    pl_length=_sandwhichSizes.sizeX;    // x-axis
    ubox = new G4Box("RPC", pl_length/2., pl_thick/2., pl_width/2.);
    _scintillators.RPC_log = new G4LogicalVolume(ubox, _scintil, "RPC_log", 0, 0, 0);
    _scintillators.RPC_log->SetVisAttributes(_vaPlastic);

    // PLACE RPC    
    // Position -- close to HS
    x_pos=0.0; y_pos = VertPos - _sandwhichSizes.sizeY*0.75; 
    z_pos= y_pos*(tan((90.-45.)*deg)+tan((90.-85.)*deg))/2.; 
    //G4cerr << "========> AC : y_pos="<<y_pos/cm<<" cm  z_pos="<<z_pos/cm<<" cm"<<G4endl;

    new G4PVPlacement(G4Transform3D(RotateNull, 
                        G4ThreeVector(x_pos, -y_pos, z_pos)), 
                        _scintillators.RPC_log, "RPC1_phys", _world.logv, false, AC_IND+1  + ARM1_IND);
    //attLength[i] = 1000.*cm; discrThreshold[i] =  0.1*MeV;
    new G4PVPlacement(G4Transform3D(_rot180Z, 
            G4ThreeVector(x_pos,  y_pos, z_pos)), 
            _scintillators.RPC_log, "RPC2_phys", _world.logv, false, AC_IND+1  + ARM2_IND);
    //attLength[i] = 1000.*cm; discrThreshold[i] =  0.1*MeV;
}

void
DetectorConstructor::_task_construct_target_magnet() {
      G4double z_size=50.*cm,
               y_size=10.*cm,
               x_size= 6.*cm,
               d_z = 2.*cm,
               x_pos = 8.5*cm,  // was 10.5 cm
               z_pos = 0.*cm,   // was 5 cm
               d_y1 = 1.*cm;
      
      G4Box* solidMag1 = new G4Box("Mag1",x_size/2.,y_size/2.,z_size/2.);
      G4LogicalVolume* logicMag1 = new G4LogicalVolume(solidMag1, _copper,"Mag1");
      new G4PVPlacement(0,G4ThreeVector(x_pos,0.,z_pos),
                      "Mag1",logicMag1,_world.phys,false,-1);
      new G4PVPlacement(0,G4ThreeVector(-x_pos,0.,z_pos),
                      "Mag1",logicMag1,_world.phys,false,-1);

      G4Box* solidMag1a = new G4Box("Mag1a",x_size/2.+d_z,(y_size/2.-d_y1)/2.,z_size/2.+d_z);
      G4LogicalVolume* logicMag1a = new G4LogicalVolume(solidMag1a,_copper,"Mag1a");
      G4double y_pos = (d_y1 + y_size/2.)/2.;
      new G4PVPlacement(0,G4ThreeVector(x_pos,y_pos,z_pos),
                      "Mag1a",logicMag1a,_world.phys,false,-1);
      new G4PVPlacement(0,G4ThreeVector(x_pos,-y_pos,z_pos),
                      "Mag1a",logicMag1a,_world.phys,false,-1);
      new G4PVPlacement(0,G4ThreeVector(-x_pos,y_pos,z_pos),
                      "Mag1a",logicMag1a,_world.phys,false,-1);
      new G4PVPlacement(0,G4ThreeVector(-x_pos,-y_pos,z_pos),
                      "Mag1a",logicMag1a,_world.phys,false,-1);
                    
      G4Tubs* solidMag2 = new G4Tubs("Mag2",0.,x_size/2.,z_size/2.,0.,M_PI);
      G4LogicalVolume* logicMag2 = new G4LogicalVolume(solidMag2,_copper,"Mag2");
      new G4PVPlacement(0,G4ThreeVector(x_pos,y_size/2.,z_pos),
                "Mag2",logicMag2,_world.phys,false,-1);
      new G4PVPlacement(0,G4ThreeVector(-x_pos,y_size/2.,z_pos),
                "Mag2",logicMag2,_world.phys,false,-1);
      new G4PVPlacement(G4Transform3D(_rot180Z,G4ThreeVector(x_pos,-y_size/2.,z_pos)),
                "Mag2",logicMag2,_world.phys,false,-1);
      new G4PVPlacement(G4Transform3D(_rot180Z,G4ThreeVector(-x_pos,-y_size/2.,z_pos)),
                "Mag2",logicMag2,_world.phys,false,-1);
                      
   G4VisAttributes *Mag_VisAtt = new G4VisAttributes(_clrCopper);
   logicMag1->SetVisAttributes(_vaSteel);
   logicMag1a->SetVisAttributes(Mag_VisAtt);
   logicMag2->SetVisAttributes(_vaSteel);
}

void
DetectorConstructor::_bind_sd() {

    SensitiveDetector * hcSD = nullptr;
    G4SDManager * SDman = G4SDManager::GetSDMpointer();

    if( !hcSD ) {
        hcSD = new SensitiveDetector(/*"hcSD", this*/);
        SDman->AddNewDetector( hcSD );
    }

    // scintillators
    _scintillators.AC_log         ->SetSensitiveDetector(hcSD);
    _scintillators.RPC_log        ->SetSensitiveDetector(hcSD);
    _scintillators.xscint_log     ->SetSensitiveDetector(hcSD);
    _scintillators.zscint_log     ->SetSensitiveDetector(hcSD);
    // wire chambers
    _WC.Theta1_gas   ->SetSensitiveDetector(hcSD);
    _WC.Phi1_gas     ->SetSensitiveDetector(hcSD);
    _WC.Theta2_gas   ->SetSensitiveDetector(hcSD);
    VCGas_log        ->SetSensitiveDetector(hcSD);

    # ifdef LOWQ_FINAL
    if(!lowqSD) {
        lowqSD = new PNT_PLowqSD("LowqSD",this);
        SDman->AddNewDetector( lowqSD );
    }
    LQPlScint_log    ->SetSensitiveDetector(lowqSD);
    # endif 
}

}  // namespace G4I

# endif  // WITH_GEANT4_MODEL

