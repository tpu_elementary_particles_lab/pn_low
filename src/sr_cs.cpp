# include "app_config.h"
# include "app.hpp"
# include "sr_cs.hpp"
# include <gsl/gsl_sys.h>
# include <CLHEP/Units/GlobalPhysicalConstants.h>

# if 0

namespace sr {

void
InertialFrameUndirected::_V_invalidate_caches() {
    _beta  = tanh(r());
    _gamma = cosh(r());
    _eta   = sinh(r());
}

void
InertialFrameUndirected::beta(  double b ) {
    r( atanh(b) );
}

void
InertialFrameUndirected::gamma( double g ) {
    r( acosh(g) );
}

void
InertialFrameUndirected::eta(   double et ) {
    r( asinh(et) );
}

double
InertialFrameUndirected::velocity_CLHEP() const {
    return _beta*CLHEP::c_light;
}

void
InertialFrameUndirected::velocity_CLHEP( double v ) {
    beta( v/CLHEP::c_light );
}

InertialFrameUndirected::InertialFrameUndirected() :
    _beta(NAN),
    _gamma(NAN),
    _eta(NAN) {}


//
// Real Vector
//

RealVector::RealVector() : _r{0,0,0} {
}

RealVector::RealVector(double x_,
                       double y_,
                       double z_) : _r{x_, y_, z_} {}

double
RealVector::norm() const {
    return sqrt( _r[0]*_r[0] +
                 _r[1]*_r[1] +
                 _r[2]*_r[2] );
}

void
RealVector::x( double nx ) {
    // ...
    _c.x = nx;
}

void
RealVector::y( double ny ) {
    // ...
    _c.y = ny;
}

void
RealVector::z( double nz ) {
    // ...
    _c.z = nz;
}

//
// InertialFrame
//

void base_frame_destructor() __attribute__(( __destructor__ ));

static InertialFrame * ifBase = nullptr;  // TODO: constructor!

void
base_frame_constructor() {
    ifBase = new InertialFrame( true );
}

void
base_frame_destructor() {
    delete ifBase;
}

const InertialFrame &
InertialFrame::get_base() {
    return *ifBase;
}

InertialFrame::InertialFrame( bool ) :
            _betaV{0, 0, 0},
            _base( nullptr) {
}

InertialFrame::InertialFrame() :
        _betaV{0, 0, 0},
        _base( &(InertialFrame::get_base()) ) {
}

InertialFrame::InertialFrame(
            const InertialFrame & bif,
            const RealVector & betaV ) : _betaV(betaV), _base(&bif) {
}

bool
InertialFrame::is_base() const {
    return !_base;
}

void
InertialFrame::beta_vector( const RealVector & newBeta ) {
    _betaV = newBeta;
    InertialFrameUndirected::beta( newBeta.norm() );
}

void
InertialFrame::get_Lorentz_matrix( TMatrixD & L ) const {
    const Double_t
        gamma = this->gamma(),
        eta   = this->eta(),
        beta  = this->beta(),
        eta2  = eta*eta,
        beta2 = beta*beta,
        etab2 = eta2/beta2,
        betaX = beta_vector().x(),
        betaY = beta_vector().y(),
        betaZ = beta_vector().z(),
        lorentzMatrix[][4] = {
            { gamma,        -gamma*betaX,               -gamma*betaY,           -gamma*betaZ            },
            {-gamma*betaX,  1 + etab2*betaX*betaX,      etab2*betaX*betaY,      etab2*betaX*betaZ       },
            {-gamma*betaY,  etab2*betaY*betaX,          1 + etab2*betaY*betaY,  etab2*betaY*betaZ       },
            {-gamma*betaZ,  etab2*betaZ*betaX,          etab2*betaZ*betaY,      1 + etab2*betaZ*betaZ   },
        };
    L.SetMatrixArray( (const Double_t *) lorentzMatrix, "" );
}

}  // namespace sr
# endif

