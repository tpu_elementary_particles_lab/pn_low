# include "app.hpp"
# include "app_config.h"
# include "wang_approx.tcc"

int
wang_run_tests_Cpp( const po::variables_map &, int, char *[] ){
    wang_test_C();
    return 0;
}

void wang_approx_run_tests_task_dispatcher() __attribute((__constructor__));
void wang_approx_run_tests_task_dispatcher() {
    Application::object().add_task_callback(
        "wang-apprx-run-tests",
        wang_run_tests_Cpp,
        "Run mir's standard test suite.");
}

