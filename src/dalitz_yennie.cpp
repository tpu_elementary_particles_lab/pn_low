# include <cmath>
# include <cassert>
# include <iomanip>
# include <iostream>

# include <root/TH1F.h>
# include <root/TApplication.h>
# include <root/TCanvas.h>

# include "dalitz_yennie.hpp"
# include "CLHEP/Units/PhysicalConstants.h"
# include "app.hpp"

namespace pnLow {

static const double m_e = CLHEP::electron_mass_c2;

EPA::EPA() : _Ebase(NAN), _eGammaMin(NAN), _scale(NAN) {}

EPA::EPA( double newE ) : _Ebase(newE), _eGammaMin(NAN), _scale(NAN) {}

void
EPA::set_E_gamma_limits( double mn, double mx ) {
    _scale = 1./(mx - mn);
    _eGammaMin = mn;
}

double
EPA::E_base() const {
    assert( !isnan(_Ebase) );
    return _Ebase;
}

void
EPA::E_base( double newEbase ) {
    _Ebase = newEbase;
}

double
EPA::_V_density( const EPA::Vector & omegaN ) const {
    # ifndef NDEBUG
    if( omegaN < 0 && omegaN > 1 ) {
        aelog( "Density method for EPA invoked for incorrect argument.\n" );
    }
    # endif  // !NDEBUG
    double omega = omegaN/_scale + _eGammaMin;
    double 
    // Note: budnev's (6.19d) equation is given for colliding
    // particles in notation that sqrt(s) = 2*E, where E is a
    // beam energy. In order to obtain spectrum for single-beam
    // accelerator, one should use E' -> E/2 (E' -- Budnev's E)
    // substitution. So, E becomes a full colliding system's
    // energy (a beam energy for ordinary accelerator) (??? -- TODO)
           // E = E_base()*2 + omega,  // corrected (???)
           E = E_base() + omega,  // not-corrected (???)
           om2   = omega*omega,
           E2    = E*E,
           fact1 = 2*E - omega;
    //std::cout << "Asked for: " 
    //          << omegaN 
    //          << " => "
    //          << omega << std::endl; //XXX XXX XXX
    return (CLHEP::fine_structure_const / M_PI) *
           (2*(1-omega/E + om2/(2*E2) * log( fabs(2*E*(E - omega)) / ( m_e * fact1 ) ) ) +
           (om2/(2*E2)) * log( fabs(fact1/omega) ) - 1 + omega/E
           ) / omega;
}

void
EPA::denorm_E_gamma( EPA::Vector & ev ) const {
    ev = ev/_scale + _eGammaMin;
}

//
// Dalitz and Yennie spectrum event generator
//

DalitzYennieSpectrum::DalitzYennieSpectrum(
        const po::variables_map & vm ) :
    PDEBasedGenerator<1>( *(new EPA(vm["beamEnergy"].as<double>())),
                          "DalitzYennie",
                          Application::object().verbosity() ),
    _rndG(new TRandom3()) {
    dynamic_cast<EPA&>(pdf()).set_E_gamma_limits(
            vm["Dalitz-Yennie-min"].as<double>(),
            vm["beamEnergy"].as<double>()
        );
    // todo: simulator().SetnCells(vm["...nCells"].as<int>()), etc.
    simulator().SetPseRan(_rndG);
    //simulator().SetChat(2); // XXX
    simulator().Initialize();
}

DalitzYennieSpectrum::~DalitzYennieSpectrum() {
    EPA & epa = get_EPA_density_function();
    delete &epa;
    delete _rndG;
}

}  // namespace pnLow

int
dalitz_yennie_spectrum_test_surf(
        const po::variables_map &,
        int, char *[]) {
    pnLow::EPA epa( 100 );
    for( double E = 100; E < 3100; E += 100 ) {
        for( double eGamma = 100; eGamma < 2000; eGamma += 100 ) {
            std::cout << std::scientific
                      << std::setw(16) << E
                      << std::setw(16) << eGamma
                      << std::setw(16) << epa.density( eGamma )
                      << std::endl;
        }
        epa.E_base(E);
    }
    return 0;
}

int
dalitz_yennie_spectrum_test_generator(
        const po::variables_map & vm,
        int argc, char * argv[]) {
    pnLow::DalitzYennieSpectrum spc( vm );

    auto rootApp = new TApplication("pn-CS Delaunay interpolation view",
                                    &argc, argv);
    rootApp->SetReturnFromRun(true);

    TH1F * hst = new TH1F(  "EPA-DY",
                            "EPA spectrum test run",
                            35,
                            spc.get_EPA_density_function().E_gamma_min(),
                            spc.get_EPA_density_function().E_gamma_max() );
    //double hst[20],
    //       upperB = vm["beamEnergy"].as<double>();
    pnLow::DalitzYennieSpectrum::Event event;
    for( uint16_t loopN = 0; loopN < 1e4; ++loopN ) {
        spc.generate(event);
        hst->Fill( event.v[0] );
    }

    auto canvas = new TCanvas("CsIstats","CsI Graphics", 1200, 600);
    hst->Draw( "COLZ" );
    canvas->Update();
    rootApp->Run();

    return 0;
}

void dalitz_yennie_spectrum_test_task_dispatcher() __attribute((__constructor__));
void dalitz_yennie_spectrum_test_task_dispatcher() {
    Application::object().add_task_callback(
        "Dalitz-Yennie-test-surface",
        dalitz_yennie_spectrum_test_surf,
        "Dumps ASCII three-column table (E, \\omega, dn/d_{\\omega}) for\n\
    Dalitz-Yennie EPA spectrum built according to Budnev's (6.19d)\n\
    equation.");
    Application::object().add_task_callback(
        "Dalitz-Yennie-test-generate",
        dalitz_yennie_spectrum_test_generator,
        "Generates 10k events according to given beam energy into\n\
    histogram with 20 bins and dumps it to stdout as ASCII table (N, E_{\\omega}).");
}


