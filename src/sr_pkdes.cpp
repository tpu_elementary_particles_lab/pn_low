# include "sr_pkdes.hpp"

# if 0

namespace sr {

MovingParticle::MovingParticle( double mass,
                                const RealVector & velocity,
                                const InertialFrame * frame ) :
        _m(mass),
        _frame( frame ? *frame : InertialFrame::get_base() ),
        _ownFrame( velocity ) {
}

double
MovingParticle::E() const {
    return _ownFrame.gamma()*_m;
}

double
MovingParticle::T() const {
    return (_ownFrame.gamma() - 1)*_m;
}


MovingParticle
MovingParticle::operator()( const InertialFrame & newFrame ) {
    # if 0
    const Double_t v4_arr[] = {
        _ownFrame.beta_vector().x(),
        _ownFrame.beta_vector().y(),
        _ownFrame.beta_vector().z(),
        // ...
    };
    TMatrixD L, v, nv;
    _ownFrame.get_Lorentz_matrix( L );
    # endif
    throw "TODO";
}

}  // namespace sr

# endif

