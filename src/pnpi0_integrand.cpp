# include "app_config.h"
# include "pnpi0_integrand.hpp"

# ifdef ARENHOVEL_FIX_GEN

namespace pnpi0 {

static const double me   = 0.51099906;
static const double mp   = 938.27231;
static const double mn   = 939.56563;
static const double mpi  = 139.56563;
static const double mpi0 = 134.9764;
static const double md   = 1875.6134;
static const double mpiC = 139.57018;

void
angle(Double_t& cost, Double_t& fi) {
    cost = 1.0;
    fi = 0.0;
}

FixIntegrand::FixIntegrand( double ee,
                      double e_min,
                      double e_max) {
    double eg = e_min;

    //sk = new StarKop;  //?

    P0 = new TLorentzVector;
    target = new TLorentzVector(0.0, 0.0, 0.0, md);
    beam = new TLorentzVector;
    beam->SetXYZM(0.0, 0.0, eg, 0.0);
    eg_min=e_min; eg_max=e_max;

    *P0 = *beam + *target;

    e0=ee;
    //lm=PT_MAX;
    //lm2=lm*lm; 
    //me2=me*me;
}

FixIntegrand::~FixIntegrand() {
}

void FixIntegrand::Init( int jmi,
                         int lWWi,
                         int iNDi) {
    jm = jmi;
    lWW = lWWi;
    iND = iNDi;

    if(jm<0){
        mpi = mpiC;
        mN1 = mp;
        mN2 = mp;
    } else if(jm>0){
        mpi = mpiC;
        mN1 = mn;
        mN2 = mn;
    } else {
        mpi = mpi0;
        mN1 = mp;
        mN2 = mn;
    }
    m3[0]= mN1; m3[1]=mN2; m3[2] = mpi; //pion should be last
  
    //sk->SetDecay(*P0, 3, m3);

    init_(jm, lWW, iND);
  
    eg=1000.;
    Check(TMath::Pi()/4.,0.);
    Check(TMath::Pi()/4.,TMath::Pi());
}

double FixIntegrand::Check(double theta,double phi){
    # if 0
    beam->SetXYZM(0.0, 0.0, eg, 0.0);
    *P0 = *beam + *target;

    TVector3 b=P0->BoostVector();

    TLorentzVector C0 = *P0; C0.Boost(-b);
    TLorentzVector C1,C2,PP;

    double T = C0.E()-mp-mn-mpi0;
    double T3=C0.M()-mp-mn-mpi0;
    double r1=0.05;
    double t1 = T*r1, t2=(T-t1)/2., t3=t2;
    double p1=sqrt(t1*t1+2.*mpi0*t1);
    double p2=sqrt(t2*t2+2.*mp*t2);
    double p3=sqrt(t3*t3+2.*mn*t3);
    TLorentzVector P1,P2,P3;
    P1.SetXYZM(0.,0.,p1,mpi0); P1.SetTheta(theta); 
    C1=C0-P1; double T2 = C1.M()-mp-mn;  
    TVector3 v1(0.,0.,1.); v1.SetTheta(theta);
    double psi = acos((p3*p3-p2*p2-p1*p1)/(2.*p1*p2));
    P2.SetXYZM(0.,0.,p2,mp);  P2.SetTheta(theta+psi); P2.Rotate(phi,v1);

    P3=C0-P1-P2;
    P1.Boost(b); P2.Boost(b); P3.Boost(b);  

    C2=P2+P3; b=C2.BoostVector(); PP=P3; PP.Boost(-b); double pp=PP.P(); 

    ampl = ampl2(*beam, P2, P3, P1, fk);
  
    double ww = pp/sqrt(T2)*p1/sqrt(T3-T2)*T3*T3;

    printf(" T=%6.2f pi=%6.1f %6.1f %6.1f  ampl*fk=%8g pp=%6.1f ww=%8g\n",
           T,P1.P(),P2.P(),P3.P(), ampl*fk, pp, ww);
  
    return ampl*fk;
    # endif
    return -NAN;
}

# if 0
TLorentzVector* FixIntegrand::GetDecay(Int_t i) { 
    return sk->GetDecay(i);
}
# endif

Double_t FixIntegrand::Density(Int_t ndim, Double_t* xarray) {
    TLorentzVector  p1,p2,q;

    eg=eg_min+(eg_max-eg_min)*xarray[0];    // photon energy
    //w0 = n_virt_phot(eg);                   // weight due to VirtPhoton spectrum

    beam->SetXYZM(0.0, 0.0, eg, 0.0);
    *P0 = *beam + *target;

    //w1 = sk->Generate(*P0, xarray+1);    // weight due to Kopylov's star

    //p1 = *(sk->GetDecay(0));
    //p2 = *(sk->GetDecay(1));
    //q  = *(sk->GetDecay(2));

    ampl =  ampl2(*beam, p1, p2, q, fk);

    dens = ampl*w0*w1*fk;///2.5681478e-9;
    if(dens<=0.0){
        //cerr << "==> dens=" << dens << " w1="<<w1<<" ampl="<<ampl<<endl;
    }
    return dens;
}

Double_t FixIntegrand::ampl2(TLorentzVector& k,
                             TLorentzVector& p1,
                             TLorentzVector& p2,
                             TLorentzVector& q,
                             double& fk
                            ) {
    double kk[4], pp1[4], pp2[4], qq[4];
    double tkb;
    std::complex<double> TM[3][2][3][3];

    kk[0] = k.E();
    kk[1] = k.Px();
    kk[2] = k.Py();
    kk[3] = k.Pz();

    pp1[0] = p1.E();
    pp1[1] = p1.Px();
    pp1[2] = p1.Py();
    pp1[3] = p1.Pz();

    pp2[0] = p2.E();
    pp2[1] = p2.Px();
    pp2[2] = p2.Py();
    pp2[3] = p2.Pz();

    qq[0] = q.E();
    qq[1] = q.Px();
    qq[2] = q.Py();
    qq[3] = q.Pz();
    //
    wed_(jm, kk, qq, pp1, pp2, tkb, TM, fk);
    //
    //  cout << "tkb = " << tkb << endl;
    //

    return tkb;    ///2.5681478e-9;
}

double FixIntegrand::amplitude( const TLorentzVector & P,       // Overall boost.
                                const TLorentzVector & P_1,     // Nucl. #1 momentum
                                const TLorentzVector & P_2,     // Nucl. #2 momentum
                                double * fk_,                   // (?) factor
                                int8_t jm_
                           ){
    double ffk = 0;  // dummy (can be unused)
    double  kk[4],
            pp1[4],
            pp2[4],
            qq[4];
    double tkb;

    if( !fk_ ) { fk_ = &ffk; }  // set dummy variable, if unused.

    kk[0] = P.E();
    kk[1] = P.Px();
    kk[2] = P.Py();
    kk[3] = P.Pz();

    pp1[0] = P_1.E();
    pp1[1] = P_1.Px();
    pp1[2] = P_1.Py();
    pp1[3] = P_1.Pz();

    pp2[0] = P_2.E();
    pp2[1] = P_2.Px();
    pp2[2] = P_2.Py();
    pp2[3] = P_2.Pz();

    // TODO Obtain 4-momentum for pi0 from kinematics:
    //TLorentzVector q = (-(P_1.Boost(-P) + P_2.Boost(-P))).boost(P);

    //qq[0] = q.E();
    //qq[1] = q.Px();
    //qq[2] = q.Py();
    //qq[3] = q.Pz();

    {   int jm__ = jm_;
        std::complex<double> TM[3][2][3][3];
        wed_(jm__, kk, qq, pp1, pp2, tkb, TM, *fk_);
    }

    return tkb;    ///2.5681478e-9;
}

# if 0
Double_t FixIntegrand::n_virt_phot(Double_t w ){
    // number of equivalent photons per 1 MeV
    Double_t  nf=0.;
    Double_t  x=w/e0;
    Double_t  x2=x*x;
    //    lm,     // maximum transverse momentum, MeV/c

    Double_t qma=(lm2+me2*x2)/(1.0-x);    /* q^2 -max*/

    //  Double_t qma=e0*(e0-w)*thm2;
    //    Double_t qma=e0*lm;

    Double_t qmi=me2*x2/(1.0-x);        /* q^2 -min*/
      nf=1.0/w*Al/M_PI*((1-x+x2/2.)*Log(qma/qmi)-me2*x2*(1./qmi-1./qma)); 
                    /*n equivalent photons*/
    return nf;
}
# endif

}  // namespace pnpi0

# endif  // ARENHOVEL_FIX_GEN

