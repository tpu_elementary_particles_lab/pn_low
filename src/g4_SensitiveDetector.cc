# include <g4_SensitiveDetector.hh>

# ifdef WITH_GEANT4_MODEL

# include <G4TouchableHistory.hh>
# include <G4HCofThisEvent.hh>
# include <G4Box.hh>
# include <G4Step.hh>
# include <G4SDManager.hh>

# include "g4_DetectorConstruction.hh"

namespace G4I {

SensitiveDetector::SensitiveDetector() :
        G4VSensitiveDetector("pnLowDetectors"),
        _collection(nullptr),
        _ids(nullptr) {
    collectionName.insert("hcCollection");
    _ids = new G4int[NSENS];
}

SensitiveDetector::~SensitiveDetector() {
    delete [] _ids;
}

void
SensitiveDetector::Initialize(G4HCofThisEvent *) {
    G4int j;
    _collection = new Collection(
            SensitiveDetectorName,
            collectionName[0]); 
    for(j=0; j<NSENS; j++) {
        _ids[j] = -1;
    }
}

G4bool
SensitiveDetector::ProcessHits(G4Step * aStep,
                               G4TouchableHistory *) {
    G4double edep;
    if((edep = aStep->GetTotalEnergyDeposit()) < 0.001*keV) {
        return false;  // ignore low energy events.
    }

    G4Track * aTrack = aStep->GetTrack();
    G4bool isForerunner = (aTrack->GetParentID() <= 2);

    G4TouchableHistory * theTouchable =
        (G4TouchableHistory*)(aStep->GetPreStepPoint()->GetTouchable());

    G4ThreeVector spatialOffset = aStep->GetDeltaPosition();
    G4double tof = aTrack->GetGlobalTime();
  
    //tof=aStep->GetPostStepPoint()->GetGlobalTime();

    G4int CB=0;
    G4int ni = theTouchable->GetHistoryDepth();
    for(G4int i = 0; i < ni; i++){        // determines element label
        G4int k = theTouchable->GetReplicaNumber(i);
        if( k > 0 ) {
            CB += k;
        }
    }
 

    G4int prtn = (isForerunner) ? aTrack->GetTrackID()
                                : -1;

    G4I::Hit * hcHit;
    G4ThreeVector position = aTrack->GetPosition();
    if( _ids[CB] == -1 ) {
        hcHit = new G4I::Hit(static_cast<G4Box*>(theTouchable->GetSolid())
                                ->GetXHalfLength(),
                             G4I::DetectorConstructor::get()->att_length(CB),
                             G4I::DetectorConstructor::get()->discr_threshold(CB),
                             theTouchable->GetTranslation(),
                             theTouchable->GetRotation());

        hcHit->blkN(CB);
        hcHit->add_Edep(edep);
        // if(isForerunner)  // only for primary particles or direct daughters ?
        {
            hcHit->add_Pos( edep, position, prtn, tof);
            // hcHit->AddT(tof);
        }
        // hcHit->AddLO(edep, position, spatialOffset);
        _ids[CB] = _collection->insert(hcHit) - 1;
        if( Application::object().verbosity() > 2 ) {
            if(CB==257 || CB==1)
                if(position.y()<1000.)/* && aTrack->GetTrackID()==3)*/ {
                    G4cout << "    <<< hy=" << hcHit->Pos().y()
                           <<" de="<<edep<<" y="<<position.y()
                           <<" prtn="<<aTrack->GetTrackID()
                           << G4endl;
                }
        }
    } else {
        // Append
        hcHit = (*_collection)[_ids[CB]];
        hcHit->add_Edep(edep);
        // if(isForerunner)  // only for primary particles ?
        {
            hcHit->add_Pos(edep,position,prtn,tof);
            // hcHit->AddT(tof);
        }
        // hcHit->AddLO(edep, position, spatialOffset);
        if( Application::object().verbosity() > 1 ){
            if(CB==257)
                if(tof<1.5) /* && aTrack->GetTrackID()==3) */ {
                    G4cout << "    >>> tof="<<tof<<" dt="<<aStep->GetDeltaTime()
                           << " de="<<edep<<" y="<<position.y()
                           << " prtn="<<aTrack->GetTrackID()
                           << G4endl;
                }
        }
    }
    return true;
}

void
SensitiveDetector::EndOfEvent(G4HCofThisEvent * HCE) {
    static G4int HCID = -1;
    if(HCID<0) {
        HCID = G4SDManager::GetSDMpointer()->GetCollectionID(
                    collectionName[0]);
    }
    HCE->AddHitsCollection(HCID, _collection);
}

}  // namespace G4I

# endif  // WITH_GEANT4_MODEL

