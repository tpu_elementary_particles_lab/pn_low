# include "g4_PrimaryGeneratorAction.hh"

# ifdef WITH_GEANT4_MODEL

# include <G4Event.hh>
# include <G4ParticleGun.hh>
# include <G4ThreeVector.hh>
# include <G4Geantino.hh>
# include <G4UImanager.hh>
# include <G4ParticleTable.hh>
# include <Randomize.hh>
# include <globals.hh>

# include <root/TFile.h>
# include <root/TTree.h>

# include "g4_EventInfo.hh"

namespace G4I {

PrimaryGeneratorAction::PrimaryGeneratorAction(
        const std::string & evFileName,
        double phiRange ) :
        _pGun(nullptr),
        _evFile(nullptr),
        _evTree(nullptr),
        _nEntry(0),
        _phiRange(phiRange) {
    _evFile = new TFile( evFileName.c_str(), "READ" );
    _pGun = new G4ParticleGun( 1 /* number of identical particles to be shoot*/ );
    if(         (_evTree = reinterpret_cast<TTree*>( _evFile->Get("Events") )) ) {
        alog1("Found mixed event tree.");
        // ... TODO
    } else if(  (_evTree = reinterpret_cast<TTree*>( _evFile->Get("pnpi0Events") )) ) {
        alog1("Found pnpi0 reaction events tree.");
        // ... TODO
    } else if(  (_evTree = reinterpret_cast<TTree*>( _evFile->Get("pnEvents") )) ) {
        alog1("Found pn reaction events tree.");
        _evTree->SetBranchAddress( "E_gamma", &(_pnEvent.omega) );
        _evTree->SetBranchAddress( "Theta_p", &(_pnEvent.thetaP) );
    } else {
        G4cerr << "Has no known \"...Events\" tree in file \""
               << evFileName << "\"."
               << std::endl;
        aelog( "Trying to abort G4 application.\n" );
        Application::g4_abort();
    }

    G4ParticleTable * pTable = G4ParticleTable::GetParticleTable();
    _pdN   = pTable->FindParticle("proton");
    _pdP   = pTable->FindParticle("neutron");
    _pdPi0 = pTable->FindParticle("pi0");

    assert( _pdN && _pdP && _pdPi0 );

    _primaryRotation.RotateY( 180*deg);
    _primaryRotation.RotateX( 180*deg);
}

PrimaryGeneratorAction::~PrimaryGeneratorAction() {
    if( _pGun ) {
        delete _pGun;
    }
    if( _evTree ) {
        delete _evTree;
    }
    if( _evFile ) {
        _evFile->Close();
        //delete _evFile;
    }
}

void
PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent) {
    if( !_evTree ) {
        aelog( "No events tree associated with PrimaryGeneratorAction.\n" );
        return;
    }
    _evTree->GetEntry( _nEntry++ );
    // TODO: Route concrete pnpi0/pn production routine.
    // TODO: Act out the generation vertex.
    generate_pn_event( anEvent, {0, 0, 0} );
}

void
PrimaryGeneratorAction::generate_pn_event(
        G4Event* anEvent,
        const TVector3 & vxSpatialPoint ) {
    G4ThreeVector position = G4ThreeVector(
            vxSpatialPoint(0),
            vxSpatialPoint(1),
            vxSpatialPoint(2)
        );
    pnLow::ReactionPN k;
    double phi = 0.5 - G4UniformRand();
    if( phi > 0 ) {
        phi = M_PI*( .5 - _phiRange*(.5 - 2.*phi))*rad;
    } else {
        phi = M_PI*(-.5 + _phiRange*(.5 + 2.*phi))*rad;
    }
    if( Application::object().verbosity() > 2 ) {
        G4cout << "pn-event: E_{\\gamma}=" << _pnEvent.omega << ", theta_{p, c.m}="
               << _pnEvent.thetaP << ", phi=" << phi << "." << std::endl;
    }
    pnLow::reveal_pn_kinematics( _pnEvent.omega,
                                 _pnEvent.thetaP,
                                 k );
    _pGun->SetParticlePosition(position);

    // Rotation
    //  - task differences
    k.after.inLabFrame.p.Transform(_primaryRotation);
    k.after.inLabFrame.n.Transform(_primaryRotation);
    //  - random asimutal
    TRotation asimutal;

    if( Application::object().verbosity() > 2 ) {
        G4cout << " - revealed (no asimuth rotation): " << std::endl
               << " -- proton, P = {"
               << k.after.inLabFrame.p.Px() << ", "
               << k.after.inLabFrame.p.Py() << ", "
               << k.after.inLabFrame.p.Pz() << "}"
               << std::endl
               << " -- neutron, P = {"
               << k.after.inLabFrame.n.Px() << ", "
               << k.after.inLabFrame.n.Py() << ", "
               << k.after.inLabFrame.n.Pz() << "}"
               << std::endl;
    }

    asimutal.RotateZ(phi);
    k.after.inLabFrame.p.Transform(asimutal);
    k.after.inLabFrame.n.Transform(asimutal);

    {  // proton
        _pGun->SetParticleDefinition(_pdP);
        _pGun->SetParticleEnergy(k.after.inLabFrame.p.E());
        _pGun->SetParticleMomentumDirection( {
                    k.after.inLabFrame.p.Px(),
                    k.after.inLabFrame.p.Py(),
                    k.after.inLabFrame.p.Pz()
                } );
        //_pGun->GeneratePrimaryVertex(anEvent);
    } _pGun->GeneratePrimaryVertex(anEvent);

    {  // neutron
        _pGun->SetParticleDefinition(_pdN);
        _pGun->SetParticleEnergy(k.after.inLabFrame.n.E());
        _pGun->SetParticleMomentumDirection( {
                    k.after.inLabFrame.n.Px(),
                    k.after.inLabFrame.n.Py(),
                    k.after.inLabFrame.n.Pz()
                } );
        //_pGun->GeneratePrimaryVertex(anEvent);
    } _pGun->GeneratePrimaryVertex(anEvent);
    //anEvent->GetPrimaryVertex( 0 )->SetUserInformation( GP );
    EventInfo * info = dynamic_cast<EventInfo*>(anEvent->GetUserInformation());
    if( info ) {
        info->SetEgamma(_pnEvent.omega);
        info->SetNreac(1);  // TODO: Reaction type -- what were the RIA's codes?
        info->SetNp(2);     // TODO: # of primaries
    } //else {
    //    throw std::runtime_error( "Invalid EventInfo class." );
    //}
}

}  // namespace G4I

# endif  // WITH_GEANT4_MODEL

