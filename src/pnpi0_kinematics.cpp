# include "app_config.h"
# include "app.hpp"
# include "pnpi0_kinematics.hpp"
# include "pnpi0_integrand.hpp"

# ifdef ARENHOVEL_FIX_GEN

# include <root/TGenPhaseSpace.h>

namespace pnLow {

//
//  Моделирование фазовых распределений
//          gamma + d -> p + p + pi-
//
//   Продукты реакции летят в полный телесный угол, если в аргументе
//   команды не укзано "включить триггер"
//   Опции командной строки:
//    -n hints -- установить число отобранных событий (default: 10)
//    -e energy  -- установить энергию фотонов равную energy (default: 600 МэВ)
//    -l logic  -- установить логику триггера: 1 - регистрация в первом детекторе(fi = 0)
//                                             2 -  --""--     во втором (fi = 180)
//                                             3 - совпадение в первом и втором детекторах
//                  нумерация протонов устанавливается по номеру сработавшего детектора
//    -t -- включить триггер отбора событий (default: false);
//    -с -- использовать генератор случайных чисел из CLHEP (default: false -
//                                                          генератор TRandom3 из ROOT'a)
//    -r -- использовать генератор событий TGenPhaseSpace (default: генератор по Копылову)
//    -a -- вычислять амплитуду (default: false - фазовые распределения)
//
//   Параметры амплитуды:
//***********************************************************************
//      jm  ! -1 = >pi^-pp;     0 = >pi^0 np;    1 = >pi^+ nn;     Kanal
//      lWW !  0 = >IA;         1 = >IA+NN;      2 = >IA+NN+PiN;   FSI
//***********************************************************************
//
//

static const double _static_Masses[] = {
    0.,
    PN_LOW_PROTON_MASS_MEV,
    PN_LOW_NEUTERON_MASS_MEV,
    PN_LOW_PI0_MASS_MEV,
},
_static_deuteronMass = PN_LOW_DEUTERON_MASS_MEV;

PNPi0Kinematics::PNPi0Kinematics() { }

PNPi0Kinematics::PNPi0Kinematics(
                    double E_gamma,
                    double T_p, double theta_P, double phi_P,
                    double T_n, double theta_N ) {
    // in Lab frame
    double  m_p  = _static_Masses[forProton],
            m_n  = _static_Masses[forNeutron],
            m_pi = _static_Masses[forPion],
            E_pi = E_gamma + _static_deuteronMass - (
                       T_p + m_p +
                       T_n + m_n
                  ),
            p_pi = sqrt( E_pi*E_pi - m_pi*m_pi ),
            p_p = sqrt( T_p*(T_p + 2*_static_Masses[forProton]) ),
            p_n = sqrt( T_n*(T_n + 2*_static_Masses[forNeutron]) ),
            beta_cm = E_gamma/(E_gamma + _static_deuteronMass)
            ;

    // TODO: if( E_pi > 0 && isnan(p_pi) ) { throw }

    TVector3 vp_p( p_p*sin(theta_P)*cos(phi_P),
                   p_p*sin(theta_P)*sin(phi_P),
                   p_p*cos(theta_P) ),
             vp_cm( 0, 0, E_gamma ),
             vp_rest = vp_cm - vp_p;

    double  p_rest      = vp_rest.Mag(),
            theta_rest  = vp_rest.Theta(),
            phi_rest    = vp_rest.Phi(),
            theta_pi    = acos(
                    (p_rest/p_pi)*cos(theta_rest)
                  - (p_n   /p_pi)*cos(theta_N)
                ),
            phi_pi = phi_rest - acos(
                    ( pow(p_rest*sin(theta_rest), 2) + pow(p_pi*sin(theta_pi), 2)
                        - pow(p_n*sin(theta_N), 2) ) /
                    (2*p_rest*p_pi*sin(theta_rest)*sin(theta_pi))
                ),
            phi_N = phi_pi + acos(
                    ( pow(p_rest*sin(theta_rest), 2) - pow(p_n*sin(theta_N), 2)
                        - pow(p_pi*sin(theta_pi), 2) ) /
                    (2*p_n*p_pi*sin(theta_N)*sin(theta_pi))
                );

    // TODO: if(p_pi > p_rest) { throw }

    // vectors in lab frame:
    _ps[forProton].SetPxPyPzE(
            p_p*sin(theta_P)*cos(phi_P),
            p_p*sin(theta_P)*sin(phi_P),
            p_p*cos(theta_P),
            T_p + _static_Masses[forProton]
        );
    _ps[forNeutron].SetPxPyPzE(
            p_n*sin(theta_N)*cos(phi_N),
            p_n*sin(theta_N)*sin(phi_N),
            p_n*cos(theta_N),
            T_n + _static_Masses[forNeutron]
        );
    _ps[forPion].SetPxPyPzE(
            p_pi*sin(theta_pi)*cos(phi_pi),
            p_pi*sin(theta_pi)*sin(phi_pi),
            p_pi*cos(theta_pi),
            E_pi
        );
    // boost them to translate into CM frame
    _ps[forProton]  .Boost(0, 0, -beta_cm);
    _ps[forNeutron] .Boost(0, 0, -beta_cm);
    _ps[forPion]    .Boost(0, 0, -beta_cm);

    TVector3 scmCheck =
            _ps[forProton].Vect() +
            _ps[forNeutron].Vect() +
            _ps[forPion].Vect();

    printf( "CHECK: |p*| = %e ≈ 0;\n", scmCheck.Mag());
}

PNPi0Kinematics::~PNPi0Kinematics() { }

}  // namespace pnLow

double
integrate_amplitude( std::complex<double> (& TM)[3][2][3][3] ) {
    const double 
        tau20[3][3] = {{1.0/sqrt(2.0), 0, 0}, {0, -sqrt(2.0), 0}, {0, 0, 1.0/sqrt(2.0)}},
        tau21[3][3] = {{0, 0, 0}, {sqrt(3.0/2.0), 0, 0}, {0, -sqrt(3.0/2.0), 0}},
        tau22[3][3] = {{0, 0, 0}, {0, 0, 0}, {sqrt(3.0), 0, 0}}; // tau[md][md']; md = -1, 0, 1

    double m2 = 0.0;

    for(int il=0; il<3; il+=2) {  // спиральность фотона = -1, +1
        for(int id=0; id<3; id++) { // проекция спина дейтрона = -1, 0, +1
            for(int s=0; s<2; s++) {  // спин системы нуклонов = 0, 1
	            for(int is=-s+1; is<=s+1; is++) {  // его проекции на ось z, s = 0 -> is = 0; s = 1 -> is = -1, 0, 1
	                m2 += abs(TM[is][s][id][il])*abs(TM[is][s][id][il]); 
	                //cerr << "T = " << TM[is][s][id][il] << " T+ =" <<  conj(TM[is][s][id][il]) << endl;
	            }
            }
        }
    }
    //  cout << "m2 = " << m2/6.0 << endl;
    std::complex<double> t20(.0, .0), t21(.0, .0), t22(.0, .0);
    for(int il=0; il<3; il+=2) {  // спиральность фотона = -1, +1
        for(int id=0; id<3; id++) { // проекция спина дейтрона = -1, 0, +1
            for(int idd=0; idd<3; idd++) { // проекция спина дейтрона = -1, 0, +1
	            for(int s=0; s<2; s++) {  // спин системы нуклонов = 0, 1
	                for(int is=-s+1; is<=s+1; is++) {  // его проекции на ось z, s = 0 -> is = 0; s = 1 -> is = -1, 0, 1
	                    t20 += ((TM[is][s][id][il]*tau20[id][idd]*conj(TM[is][s][idd][il]))/m2); 
	                    t21 += ((TM[is][s][id][il]*tau21[id][idd]*conj(TM[is][s][idd][il]))/m2); 
	                    t22 += ((TM[is][s][id][il]*tau22[id][idd]*conj(TM[is][s][idd][il]))/m2); 
	                }
	            }
            }
        }
    }
    //  cerr << "t20 = " << t20 << " t21 = " << t21 << " t22 = " << t22 << endl;
    return m2/6.0;
}

/* It is required for 4-momenta provided here to be given
 * in CM-frame.
 */
double
obtain_amplitude_for( const TLorentzVector & P_gamma,
                      const TLorentzVector & P_p,
                      const TLorentzVector & P_n,
                      const TLorentzVector & P_pi ) {
    double ffk = 0;  // dummy (can be unused)
    double  kk[4] = {
                P_gamma.E(),
                P_gamma.Px(),
                P_gamma.Py(),
                P_gamma.Pz(),
            },
            pp1[4] = {
                P_p.E(),
                P_p.Px(),
                P_p.Py(),
                P_p.Pz(),
            },
            pp2[4] = {
                P_n.E(),
                P_n.Px(),
                P_n.Py(),
                P_n.Pz(),
            },
            qq[4] = {
                P_pi.E(),
                P_pi.Px(),
                P_pi.Py(),
                P_pi.Pz(),
            };
    double tkb;

    std::complex<double> TM[3][2][3][3];
    {   int jm__ = 0;  // Determines type of the lepton.
        wed_(jm__, kk, qq, pp1, pp2, tkb, TM, ffk);
        //printf( "ampl = %e\n", ampl );
    }
    return integrate_amplitude( TM );
}

int
pnpi0_kinematics_sample(
        const po::variables_map &,
        int, char *[] ) {
    using pnLow::PNPi0Kinematics;

    const double
        E_gamma = 700,  // MeV
        m_p  = pnLow::_static_Masses[PNPi0Kinematics::forProton],
        m_n  = pnLow::_static_Masses[PNPi0Kinematics::forNeutron],
        m_pi = pnLow::_static_Masses[PNPi0Kinematics::forPion],
        m_d  = pnLow::_static_deuteronMass,
        prodMasses[] = { m_p, m_n, m_pi };

    TLorentzVector P_d(
            .0, .0, .0, m_d
        ),
        P_gamma(
            .0, .0, E_gamma, .0
        ),
        P = P_d + P_d,
        * P_p, * P_n, * P_pi;

    TGenPhaseSpace phSpace;
    phSpace.SetDecay( P, 3, prodMasses ); {
        P_p  = phSpace.GetDecay(0);
        P_n  = phSpace.GetDecay(1);
        P_pi = phSpace.GetDecay(2);
    }

    {
        int jm  = 0,
            fsi = 0,
            iND = 0;
        init_(jm, fsi, iND);
    }

    for( uint8_t i = 0; i < 10; ++i ) {
        phSpace.Generate();
        double ampl = obtain_amplitude_for( P_gamma, *P_p, *P_n, *P_pi );
        std::cout << "Done #" << (int) i << " ("
                  << ampl
                  << ")..." << std::endl;
    }

    # if 0
    const double
        // Set this variables to describe some meaningful case:
        cmP = 200, // MeV/c
        _thetaP = M_PI/12., _phiP = M_PI/8.,
        _thetaN = M_PI/6,   _phiN = -M_PI/11.,
        //
        T_p  = sqrt( cmP*cmP + m_p*m_p ) - m_p,
        T_n  = sqrt( cmP*cmP + m_n*m_n ) - m_n;
    TLorentzVector P_p(
            cmP*sin(_thetaP)*cos(_phiP),
            cmP*sin(_thetaP)*sin(_phiP),
            cmP*cos(_thetaP),
            T_p + m_p
        ), P_n(
            cmP*sin(_thetaN)*cos(_phiN),
            cmP*sin(_thetaN)*sin(_phiN),
            cmP*cos(_thetaN),
            T_n + m_n
        );
    TVector3 vp_pi_cm = -(P_p.Vect() + P_n.Vect());
    const double T_pi = sqrt( vp_pi_cm.Mag2() + m_pi*m_pi ) - m_pi,
                 E_sum = T_p + T_n + T_pi + m_p + m_n + m_pi,
                 s = E_sum*E_sum,
                 E_gamma = ( s - m_d*m_d )/(2*m_d),
                 beta_cm = E_gamma/(E_gamma + m_d)
                 ;
    
    TLorentzVector
        P_pi(
            vp_pi_cm.X(),
            vp_pi_cm.Y(),
            vp_pi_cm.Z(),
            T_pi + m_pi );

    P_p.Boost(0,  0, beta_cm);
    P_n.Boost(0,  0, beta_cm);
    P_pi.Boost(0, 0, beta_cm);

    TLorentzVector P_rest = P_pi + P_n;

    delete new pnLow::PNPi0Kinematics( E_gamma,
            P_p.E() - m_p, P_p.Theta(), P_p.Phi(),
            P_n.E() - m_n, P_n.Theta() );

    { // call Fix-Arenhovel generator now.
        TLorentzVector P = P_n + P_p + P_pi;
        double ffk = 0;  // dummy (can be unused)
        double  kk[4],
                pp1[4],
                pp2[4],
                qq[4];
        double tkb;

        kk[0] = P.E();
        kk[1] = P.Px();
        kk[2] = P.Py();
        kk[3] = P.Pz();

        pp1[0] = P_p.E();
        pp1[1] = P_p.Px();
        pp1[2] = P_p.Py();
        pp1[3] = P_p.Pz();

        pp2[0] = P_n.E();
        pp2[1] = P_n.Px();
        pp2[2] = P_n.Py();
        pp2[3] = P_n.Pz();

        // TODO Obtain 4-momentum for pi0 from kinematics:
        //TLorentzVector q = (-(P_1.Boost(-P) + P_2.Boost(-P))).boost(P);

        //qq[0] = q.E();
        //qq[1] = q.Px();
        //qq[2] = q.Py();
        //qq[3] = q.Pz();

        {   int jm__ = 0;
            std::complex<double> TM[3][2][3][3];
            wed_(jm__, kk, qq, pp1, pp2, tkb, TM, ffk);
            //printf( "ampl = %e\n", ampl );
        }
    }
    # endif
    return EXIT_SUCCESS;
}

void pnpi0_kinematics_task_dispatcher() __attribute((__constructor__));
void pnpi0_kinematics_task_dispatcher() {
    Application::object().add_task_callback(
        "pnpi0-sampling",
        pnpi0_kinematics_sample,
        "(DEV) Generates several valid points in order to control\n\
    kinematics revealing for pnpi0 reaction.");
}

# endif  // ARENHOVEL_FIX_GEN

