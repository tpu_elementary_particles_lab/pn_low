# include "g4_RunManager.hh"
# include "g4_EventInfo.hh"
# include "g4_DetectorConstruction.hh"
//# include "g4_PhysicsList.hh"
# include "FTFP_BERT.hh"
# include "g4_PrimaryGeneratorAction.hh"
# include "g4_RunAction.hh"
# include "g4_EventAction.hh"

# ifdef WITH_GEANT4_MODEL

namespace G4I {

RunManager::RunManager( const po::variables_map & vm ) {
    // Set mandatory initialization classes
    auto detector = new G4I::DetectorConstructor();
    this->SetUserInitialization(detector);

    //auto physics = new G4I::PhysicsList();
    //this->SetUserInitialization(physics);
    this->SetUserInitialization(new FTFP_BERT);

    if( vm.count("pnCS.evfile") ) {
        // Set user action classes
        auto genAction = new G4I::PrimaryGeneratorAction(
            vm["pnCS.evfile"].as<std::string>(),
            vm["g4mdl.phiAngle"].as<double>()
            );
        this->SetUserAction(genAction);
        //
        auto runAction = new G4I::RunAction(vm["g4mdl.outFile"].as<std::string>());
        this->SetUserAction(runAction);
        //
        auto eventAction = new G4I::EventAction( runAction );
        this->SetUserAction(eventAction);
        //
        //auto tracking_action = new G4I::TrackingAction(
        //    /*storeTrajectories .... */ true);  // draws exp-specific trajectories in different colors
        //this->SetUserAction(tracking_action);
        //
        //auto stepping_action =
        //    new G4I::StepAct();
        //this->SetUserAction(stepping_action);
    } else {
        G4cerr << "Event file isn't provided. Starting void session."
               << std::endl;
    }
}

RunManager::~RunManager() {
    // ...
}

void
RunManager::RunInitialization() {
    Parent::RunInitialization();
    // ...
}

void
RunManager::DoEventLoop(G4int n_event,
                        const char* macroFile,
                        G4int n_select) {
    Parent::DoEventLoop(n_event, macroFile, n_select);
    // ...
}

G4Event*
RunManager::GenerateEvent(G4int i_event) {
    auto event = Parent::GenerateEvent(i_event);
    event->SetUserInformation( new EventInfo() );
    // ...
    return event;
}

void
RunManager::RunTermination() {
    // ...
    Parent::RunTermination();
}

# if 0

/*
 * DoEventLoop is the most important piece in RunManager class to control the program runs.
 */
void
ParRunManager::DoEventLoop( G4int n_event, const char* macroFile, G4int n_select ) {
    TOPC_OPT_trace_input = trace_event_input;

    G4StateManager* stateManager = G4StateManager::GetStateManager();

    #ifdef G4_ORIGINAL
    cout << "ParRunManager::DoEventLoop" << endl;
    G4RunManager::DoEventLoop(n_event, macroFile, n_select);
    return;
    # endif

    if( verboseLevel > 0 ) {
        timer->Start();
    }

    G4String msg;
    if(macroFile!=0) {
        if(n_select<0) n_select = n_event;
        msg = "/control/execute ";
        msg += macroFile;
    } else {
        n_select = -1;
    }


    // BeginOfEventAction() and EndOfEventAction() would normally be
    // called inside G4EventManager::ProcessOneEvent() in ParRunManager::DoEvent
    // on slave.  Since this is often where hits are collected and where
    // histogram data is first stored, must do it
    // on master instead, to process hits.  So, set user event action to NULL.
    // Keep private copy, and execute it in CheckTaskResult().
    // If user later does:  SetUserAction( (G4UserEventAction *)NULL );
    // we won't notice and copy it to origUserEventAction.
    if ( eventManager->GetUserEventAction() ) {
        origUserEventAction = eventManager->GetUserEventAction();
        SetUserAction( (G4UserEventAction *)0 );
    }

    // Make these variables accessible to TOP-C callback functions
    ImportDoEventLoopLocals( stateManager, n_event, n_select, msg );

    #ifdef FIXEDRND
    // Setup random seeds for each event
    g_Seeds = (long*)calloc(n_event, sizeof(long));

    G4int i_event;
    for( i_event=0; i_event<n_event; i_event++ ) {
        g_Seeds[i_event] = (long) (100000000L * HepRandom::getTheGenerator()->flat());
    }
    #endif

    // This is where all the parallelism occurs
    TOPC_raw_begin_master_slave(MyDoEvent, MyCheckEventResult, NULL);

    if(TOPC_is_master()){
        G4int i_event;
        // RIA: master will send parameters of generated primaries to slaves:
	    char kbuf[512];
        // RIA: obtain pointer to actual Prim.Act. object from RunManager:
	    PNT_PPrimaryAction* uP =  (PNT_PPrimaryAction*) userPrimaryGeneratorAction;
            G4bool countFlag=uP->GetCountFlag();
            G4int cstep = uP->GetCStep();
        for( i_event=0; i_event<n_event; i_event++ ) {
            #ifdef FIXEDRND
            // RIA: random generator for Master should be initialized here
            HepRandom::setTheSeed(g_Seeds[i_event]);
            #endif
            // RIA: use method from PrimAction to generate primaries, 
            // 	  however, put it not in event, but in output buffer,
            // 	  together with some additional data (Egamma, Theta_CM)
            G4int sz=0; memcpy(kbuf,&i_event,sizeof(G4int)); sz+=sizeof(G4int);
            uP->GeneratePrimaries(NULL);
            sz += uP->GetBuf(kbuf+sz);
            // RIA: submit this staff to slave -- it will insert primaries into the event
            TOPC_raw_submit_task_input(TOPC_MSG( kbuf, sz));
            if(runAborted) break;
            if(countFlag){
                G4int nev=i_event+1;
                if(nev%cstep==0) {
                    fprintf(stderr,"\r event #%05d             ", nev);
                }
            }
        }	
    }	
    TOPC_raw_end_master_slave();

    # ifdef FIXEDRND
    free(g_Seeds);
    # endif

    if( verboseLevel > 0 ) {
        timer->Stop();
        G4cout << "Run terminated." << G4endl;
        G4cout << "Run Summary" << G4endl;
        if( runAborted ) { 
	        G4cout << "  Run Aborted." << G4endl; 
	    } else { 
	        G4cout << "  Number of events processed : " << n_event << G4endl; 
	    }
        G4cout << "  "  << *timer << G4endl;
    }
}

static MarshaledG4HCofThisEvent *aMarshaledObj = NULL;

TOPC_BUF ParRunManager::DoEvent( void *input_buf ) {
    static G4int i_event;
    memcpy(&i_event, input_buf, sizeof(G4int));
    # ifdef FIXEDRND
    HepRandom::setTheSeed(g_Seeds[i_event]);
    # endif

    // was : currentEvent = GenerateEvent(i_event);
    // RIA : on the slave replace standard GenerateEvent method with my version, 
    // 	 which uses InsertPrimaries() method from PNT_PPrimaryAction class
    currentEvent = MyGenerateEvent(input_buf);

    eventManager->ProcessOneEvent(currentEvent);

    G4HCofThisEvent* HCE = currentEvent->GetHCofThisEvent();

    if(aMarshaledObj) delete aMarshaledObj;
    aMarshaledObj = new MarshaledG4HCofThisEvent(HCE);

    StackPreviousEvent( currentEvent );
    currentEvent = 0;
    return TOPC_MSG( aMarshaledObj->getBuffer(), aMarshaledObj->getBufferSize());
}


TOPC_ACTION ParRunManager::CheckEventResult( void * input_buf, void *output_buf )
{
  G4int i_event;
  memcpy(&i_event, input_buf, sizeof(G4int));

//  if ( !userPrimaryGeneratorAction )
//    G4Exception("ParRunManager::BeamOn - G4VUserPrimaryGeneratorAction is not defined.");

  //This creates a trivial event in lieu of GenerateEvent(i_event);
  currentEvent = new G4Event( i_event );
  // 
  SetUserAction( (G4UserEventAction *)0 );

  // When Geant4 4.0 sees empty event, it still calls userStackingAction.
  // On master, only trivial events exist, so we delete userStackingAction
  SetUserAction( (G4UserStackingAction*)0 );
  eventManager->ProcessOneEvent( currentEvent ); // Processing the trivial event

  // Called with output_buf and no size, creates object for unmarshaling
  // using marshalgen
  MarshaledG4HCofThisEvent marshaledObj( output_buf );
  G4HCofThisEvent* oldCE = currentEvent->GetHCofThisEvent();
  G4HCofThisEvent* HCE = new G4HCofThisEvent(oldCE->GetNumberOfCollections());

  marshaledObj.unmarshalTo(HCE);
  if(oldCE) delete(oldCE);

  currentEvent->SetHCofThisEvent(HCE);

  // RIA: before building an output event insert vertexes and user info 
  currentEvent->SetUserInformation(new PNT_PEventInfo);
  PNT_PPrimaryAction* uP =  (PNT_PPrimaryAction*) userPrimaryGeneratorAction;
  uP->InsertPrimaries(currentEvent, input_buf);

  // Original UserEventAction was saved and set to NULL.  Do it now on master.
  if ( origUserEventAction )
    origUserEventAction->BeginOfEventAction( currentEvent );



  if ( origUserEventAction )
    origUserEventAction->EndOfEventAction( currentEvent );

  AnalyzeEvent(currentEvent);

  if (i_event<n_select) G4UImanager::GetUIpointer()->ApplyCommand(msg);
  // Geant4 6.0 requires the state to be G4State_GeomClosed
  stateManager->SetNewState( G4State_GeomClosed );
  StackPreviousEvent(currentEvent);
  currentEvent = 0;
  return NO_ACTION;
}

// RIA: this method replaces original one, forcing a slave to use InsertPrimaries()
G4Event* ParRunManager::MyGenerateEvent(void *ibuf)
{
  G4int i_event;
  memcpy(&i_event, ibuf, sizeof(G4int));

  G4Event* anEvent = new G4Event(i_event);

  if(storeRandomNumberStatusToG4Event==1 || storeRandomNumberStatusToG4Event==3)
  {
    std::ostringstream oss;
    HepRandom::saveFullState(oss);
    randomNumberStatusForThisEvent = oss.str();
    anEvent->SetRandomNumberStatus(randomNumberStatusForThisEvent);
  }

  if(storeRandomNumberStatus) {
    G4String fileN = randomNumberStatusDir + "currentEvent.rndm"; 
    HepRandom::saveEngineStatus(fileN);
  }  
    
  PNT_PPrimaryAction* uP =  (PNT_PPrimaryAction*) userPrimaryGeneratorAction;
  uP->InsertPrimaries(anEvent, ibuf);


  return anEvent;
}

# endif

}  // namespace G4I

# endif // WITH_GEANT4_MODEL

