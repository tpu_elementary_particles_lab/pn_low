# include "g4_RunAction.hh"

# include <G4Timer.hh>
# include <G4Run.hh>
# include <G4UImanager.hh>
# include <G4ios.hh>
# include <G4UnitsTable.hh>

namespace G4I {

RunAction::RunAction(G4String Fname) : _outfile(nullptr) {
    _timer = new G4Timer;
    //if(TOPC_is_master()){ 
    _outfile = new TFile(Fname,"recreate");
    //    G4cout << "============== OUTPUT FILE : "<< Fname<<G4endl;
    //} else {
    //    outfile = NULL;
    //}
}

RunAction::~RunAction() {
    delete _timer;
}

void
RunAction::BeginOfRunAction(const G4Run* aRun) {
    //G4UImanager* UI = G4UImanager::GetUIpointer();
    // UI->ApplyCommand("/event/verbose 1");
    // UI->ApplyCommand("/tracking/verbose 1");
    if( Application::object().verbosity() > 2 ) {
        G4cout << "Run " << aRun->GetRunID() << " start." << G4endl;
    }
    _timer->Start();
}

void
RunAction::EndOfRunAction(const G4Run* aRun) {
    _timer->Stop();
    if(_outfile){
        _outfile->Write();
        _outfile->Close();
    }
    if( Application::object().verbosity() > 1 ) {
        G4cout << "Events proceed: " << aRun->GetNumberOfEvent()
               << ", time: " << * _timer
               << ", RunID: " << aRun->GetRunID()
               << G4endl;
    }
}

}  // namespace G4I

