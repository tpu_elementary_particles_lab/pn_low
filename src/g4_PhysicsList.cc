# include "app_config.h"

# ifdef WITH_GEANT4_MODEL

# include "g4_PhysicsList.hh"
# include <G4ParticleTypes.hh>
# include <G4ProcessManager.hh>
# include <G4BosonConstructor.hh>
# include <G4LeptonConstructor.hh>
# include <G4MesonConstructor.hh>
# include <G4BosonConstructor.hh>
# include <G4BaryonConstructor.hh>
# include <G4IonConstructor.hh>
# include <G4ComptonScattering.hh>
# include <G4GammaConversion.hh>
# include <G4PhotoElectricEffect.hh>
# include <G4eMultipleScattering.hh>
# include <G4eIonisation.hh>
# include <G4eBremsstrahlung.hh>
# include <G4eplusAnnihilation.hh>
# include <G4MuMultipleScattering.hh>
# include <G4MuIonisation.hh>
# include <G4MuBremsstrahlung.hh>
# include <G4MuPairProduction.hh>
# include <G4hMultipleScattering.hh>
# include <G4hIonisation.hh>
# include <G4hBremsstrahlung.hh>
# include <G4hPairProduction.hh>
# include <G4ionIonisation.hh>
# include <G4Decay.hh>

namespace G4I {

PhysicsList::PhysicsList(){
}

PhysicsList::~PhysicsList(){
}

void
PhysicsList::cut_value( const G4double & v) {
    //dprintf( "Default cut value set to %d from now\n" );
    defaultCutValue = v;
}

G4double
PhysicsList::cut_value() const {
    return defaultCutValue;
}

void
PhysicsList::_construct_EM() {
    // Note for AddProcess(...): signature is following:
    // pointer to (new) the process instance
    // others -- ordering params: at rest, along step, post step
    // There are shrtcuts for this. In order:
    // [AddRest/AddDiscrete/AddContinious]Process()
    // Negative number mens that process will not be applied.
    theParticleIterator->reset();
    while( (*theParticleIterator)() ){
        G4ParticleDefinition* particle = theParticleIterator->value();
        G4ProcessManager* pmanager = particle->GetProcessManager();
        G4String particleName = particle->GetParticleName();
    if(particleName == "gamma") {
            pmanager->AddDiscreteProcess(new G4PhotoElectricEffect);
            pmanager->AddDiscreteProcess(new G4ComptonScattering);
            pmanager->AddDiscreteProcess(new G4GammaConversion);
    } else if (particleName == "e-") {
            pmanager->AddProcess(new G4eMultipleScattering,-1, 1, 1);
            pmanager->AddProcess(new G4eIonisation,        -1, 2, 2);
            pmanager->AddProcess(new G4eBremsstrahlung,    -1, 3, 3);
    } else if (particleName == "e+") {
            pmanager->AddProcess(new G4eMultipleScattering,-1, 1, 1);
            pmanager->AddProcess(new G4eIonisation,        -1, 2, 2);
            pmanager->AddProcess(new G4eBremsstrahlung,    -1, 3, 3);
            pmanager->AddProcess(new G4eplusAnnihilation,   0,-1, 4);
        } else if( particleName == "mu+" || 
                   particleName == "mu-"    ) {
            pmanager->AddProcess(new G4MuMultipleScattering,-1, 1, 1);
            pmanager->AddProcess(new G4MuIonisation,       -1, 2, 2);
            pmanager->AddProcess(new G4MuBremsstrahlung,   -1, 3, 3);
            pmanager->AddProcess(new G4MuPairProduction,   -1, 4, 4);
        } else if( particleName == "proton" ||
                   particleName == "pi-" ||
                   particleName == "pi+"    ) {
            pmanager->AddProcess(new G4hMultipleScattering, -1, 1, 1);
            pmanager->AddProcess(new G4hIonisation,         -1, 2, 2);
            pmanager->AddProcess(new G4hBremsstrahlung,     -1, 3, 3);
            pmanager->AddProcess(new G4hPairProduction,     -1, 4, 4);
        } else if( particleName == "alpha" ||
	        particleName == "He3" ) {
            pmanager->AddProcess(new G4hMultipleScattering, -1, 1, 1);
            pmanager->AddProcess(new G4ionIonisation,       -1, 2, 2);
        } else if( particleName == "GenericIon" ) { 
            pmanager->AddProcess(new G4hMultipleScattering, -1, 1, 1);
            pmanager->AddProcess(new G4ionIonisation,       -1, 2, 2);
        } else if ((!particle->IsShortLived()) &&
	            (particle->GetPDGCharge() != 0.0) && 
	            (particle->GetParticleName() != "chargedgeantino")) {
            // all others charged particles except geantino
            pmanager->AddProcess(new G4hMultipleScattering,-1, 1, 1);
            pmanager->AddProcess(new G4hIonisation,        -1, 2, 2);
        }
    }
}

void
PhysicsList::_construct_decay() {
  G4Decay* theDecayProcess = new G4Decay();
  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
        G4ParticleDefinition* particle = theParticleIterator->value();
        G4ProcessManager* pmanager = particle->GetProcessManager();
        if (theDecayProcess->IsApplicable(*particle)) { 
            pmanager ->AddProcess(theDecayProcess);
            pmanager ->SetProcessOrdering(theDecayProcess, idxPostStep);
            pmanager ->SetProcessOrdering(theDecayProcess, idxAtRest);
        }
    }
}

void PhysicsList::ConstructParticle() {
    G4BosonConstructor  pBosonConstructor;
    pBosonConstructor.ConstructParticle();
    //
    G4LeptonConstructor pLeptonConstructor;
    pLeptonConstructor.ConstructParticle();
    //
    G4MesonConstructor pMesonConstructor;
    pMesonConstructor.ConstructParticle();
    //
    G4BaryonConstructor pBaryonConstructor;
    pBaryonConstructor.ConstructParticle();
    //
    G4IonConstructor pIonConstructor;
    pIonConstructor.ConstructParticle();
}

void PhysicsList::ConstructProcess() {
    AddTransportation();
    _construct_EM();
    _construct_decay();
}

void PhysicsList::SetCuts() {
    // set cut values for gamma at first and for e- second and next for e+,
    // because some processes for e+/e- need cut values for gamma
    SetCutValue(defaultCutValue, "gamma");
    SetCutValue(defaultCutValue, "e-");
    SetCutValue(defaultCutValue, "e+");
    SetCutValue(defaultCutValue, "proton");
}

}  // namespace G4I

# endif  // WITH_GEANT4_MODEL

