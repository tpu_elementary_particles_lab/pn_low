# include <iostream>

# include "spectrum.hpp"

namespace pnLow {

SmoothSpectrum::SmoothSpectrum() :
        _acc(nullptr),
        _spline(nullptr),
        _min(NAN),
        _max(NAN),
        _scale(NAN) {
}

SmoothSpectrum::~SmoothSpectrum() {
    if( _spline ) {
        gsl_spline_free(_spline);
    }
    if( _acc ) {
        gsl_interp_accel_free(_acc);
    }
}

SmoothSpectrum::SmoothSpectrum(
        const std::map<double, double> & origSet ) : _originalData(origSet),
        _acc(nullptr),
        _spline(nullptr),
        _min(NAN),
        _max(NAN),
        _scale(NAN) {
    for( auto it = _originalData.begin(); _originalData.end() != it; ++it ) {
        _correct_range( it->first );
    }
    approximate();
}

void
SmoothSpectrum::_correct_range( double x ) {
    if( isnan(_min) ) { _min = _max = x; }
    else if( x < _min ) { _min = x; }
    else if( x > _max ) { _max = x; }
}

void
SmoothSpectrum::insert_point( double x, double f ) {
    _correct_range(x);
    _originalData[x] = f;
}

int
SmoothSpectrum::approximate() {
    if( _spline ) {
        gsl_spline_free(_spline);
    }
    if( _acc ) {
        gsl_interp_accel_free(_acc);
    }
    _scale = 1./(_max - _min);

    const size_t n = _originalData.size();
    double * x = new double[ n ],
           * y = new double[ n ],
           sum = 0.;

    size_t nP = 0;
    for( auto it  = _originalData.begin();
              it != _originalData.end(); ++it, ++nP) {
        if( y[nP] < 0 ) {
            std::cerr << "WARNING: negative density " << y[nP]
                      << " ignored at x=" << x[nP]
                      << "." << std::endl;
            continue;
        }
        x[nP] = (it->first - _min)*_scale;
        y[nP] = it->second;
        sum += y[nP]*y[nP];
        //std::cout << "spc + : " << x[nP] << ", " << y[nP] << std::endl;  // XXX
    }

    if( _max <= _min || _originalData.empty() || !sum ) {
        std::cerr << "Malformed or empty data set for object "
                  << this << " range: [" << _min << ", " << _max << "], #="
                  << _originalData.size()
                  << ", sum_sq = "
                  << sum << "."
                  << std::endl;
        return -1;
    } else if( Application::object().verbosity() > 2 ) {
        std::cout << "Spline-smoothed spectrum object (" << this << ") approximation constructed: "
                  << "range: [" << _min << ", " << _max << "], #="
                  << _originalData.size()
                  << ", sum_sq = "
                  << sum << "."
                  << std::endl;
    }

    _acc  = gsl_interp_accel_alloc();
    _spline = gsl_spline_alloc(gsl_interp_cspline, n);

    gsl_spline_init(_spline, x, y, n);
    delete [] x;
    delete [] y;
    return 0;
}

double
SmoothSpectrum::_V_density( const Vector & x ) const {
    assert( _spline && _acc );
    double res = (gsl_spline_eval(_spline, x, _acc));
    if( res < 0 ) {
        std::cerr << "WARNING: preventing from returning of negative density " << res
                      << " at (normed) x =" << x
                      << "." << std::endl;
        return 0;
    }
    return res;
}

}  // namespace pnLow
