# include "g4_EventAction.hh"

# ifdef WITH_GEANT4_MODEL
# include <iomanip>
# include <G4Event.hh>
# include <G4EventManager.hh>
# include <G4HCofThisEvent.hh>
# include <G4VHitsCollection.hh>
# include <G4TrajectoryContainer.hh>
# include <G4Trajectory.hh>
# include <G4VVisManager.hh>
# include <G4SDManager.hh>
# include <G4UImanager.hh>
# include <G4ios.hh>
# include <G4UnitsTable.hh>
# include <G4LorentzVector.hh>
# include <G4ParticleTable.hh>
# include <G4ParticleDefinition.hh>

# include "g4_EventAction.hh"
# include "g4_PrimaryGeneratorAction.hh"
# include "g4_Hit.hh"
# include "g4_DetectorConstruction.hh"
# include "g4_SensitiveDetector.hh"
# include "g4_EventInfo.hh"

namespace G4I {

EventAction::EventAction(RunAction* PNT_Run) : 
        hcCollID(-1),
        drawFlag("all"),
        _run( PNT_Run ) {
    //  eventMessenger = new PNT_PEventActionMessenger(this);
    //  outfile = new TFile("outfile.root","recreate");
    outfile = _run->GetFile();
    if(outfile){
        outfile->cd();
        TO=new TTree("TO","PNT Out");
        /*
        G4int nev;    // event number
        G4int nr;    // reaction type
        G4float Eg;    // gamma energy
        G4float vx,vy,vz;    // vertex
        G4float ep,tp,fp;    // proton energy, theta, phi (all zeroes if no proton)
        G4int nhx;    // number of HC x-bars hits
        G4float hxx[NMAX],hxy[NMAX],hxz[NMAX],hxa[NMAX];    // x,y,z and amplitude in each x-bar 
        G4int nhz;    // number of HC z-bars hits
        G4float hzx[NMAX],hzy[NMAX],hzz[NMAX],hza[NMAX];    // x,y,z and amplitude in each y-bar 
        G4int nvc;    // number of vx chamber hits
        G4float vcx[NMAX],vcy[NMAX];    // hit position in VC
        G4int nwa;    // number of WA (theta-1) chamber hits
        G4float waz[NMAX],way[NMAX];    // hit position in WA
        G4int nwb;    // number of WB (phi-1) chamber hits
        G4float wbx[NMAX],wby[NMAX];    // hit position in WB
        G4int nwc;    // number of WC (theta-2) chamber hits
        G4float wcz[NMAX],wcy[NMAX];    // hit position in WC
        G4float ac1,ac2;    // AntiCoin upper,lower
        G4float t1,t2;    // TOF upper,lower
        G4float L1,L2;    // path length upper,lower    
        */
        TO->Branch("nev",&nev,"nev/I");
        TO->Branch("nr",&nr,"nr/I");
        TO->Branch("eg",&Eg,"eg/F");
        TO->Branch("vx",&vx,"vx/F");
        TO->Branch("vy",&vy,"vy/F");
        TO->Branch("vz",&vz,"vz/F");
      
        TO->Branch("ep",&ep,"ep/F");
        TO->Branch("tp",&tp,"tp/F");
        TO->Branch("fp",&fp,"fp/F");

        TO->Branch("nhx",&nhx,"nhx/I");
        TO->Branch("hxx",hxx,"hxx[nhx]/F");
        TO->Branch("hxy",hxy,"hxy[nhx]/F");
        //  TO->Branch("hxz",hxz,"hxz[nhx]/F");
        TO->Branch("hxa",hxa,"hxa[nhx]/F");

        TO->Branch("nhz",&nhz,"nhz/I");
        //  TO->Branch("hzx",hzx,"hzx[nhz]/F");
        TO->Branch("hzy",hzy,"hzy[nhz]/F");
        TO->Branch("hzz",hzz,"hzz[nhz]/F");
        TO->Branch("hza",hza,"hza[nhz]/F");

        TO->Branch("nvc",&nvc,"nvc/I");
        TO->Branch("vcx",vcx,"vcx[nvc]/F");
        TO->Branch("vcy",vcy,"vcy[nvc]/F");

        TO->Branch("nwa",&nwa,"nwa/I");
        TO->Branch("waz",waz,"waz[nwa]/F");
        TO->Branch("way",way,"way[nwa]/F");

        TO->Branch("nwb",&nwb,"nwb/I");
        TO->Branch("wbx",wbx,"wbx[nwb]/F");
        TO->Branch("wby",wby,"wby[nwb]/F");

        TO->Branch("nwc",&nwc,"nwc/I");
        TO->Branch("wcz",wcz,"wcz[nwc]/F");
        TO->Branch("wcy",wcy,"wcy[nwc]/F");
        
        TO->Branch("ac",ac,"ac[2]/F");
        TO->Branch("tof",tof,"tof[2]/F");
        TO->Branch("L",&L,"L[2]/F");
    } else {
        TO=NULL;
    }
  
    G4ParticleTable *particleTable = G4ParticleTable::GetParticleTable();
    G4ParticleDefinition *particle = particleTable->FindParticle("proton");
    base_code=particle->GetPDGEncoding();
}

EventAction::~EventAction() {
    //  delete eventMessenger;
}

void
EventAction::BeginOfEventAction(const G4Event*) {
    // G4int evtNb = evt->GetEventID();
    if(hcCollID==-1) {
        G4SDManager * SDmanp = G4SDManager::GetSDMpointer();
        hcCollID = SDmanp->GetCollectionID("hcCollection");
        if( hcCollID <= 0 ) {
            std::cerr << "hcColID is null. Possible errors ahead."
                      << std::endl;
        }
    }
}

void
EventAction::EndOfEventAction(const G4Event* evt) {
    G4bool verb = Application::object().verbosity() > 2;
    G4int nvx = evt->GetNumberOfPrimaryVertex();
    if( nvx < 2 ){
        G4cerr << "Event with wrong number of primary verteces avoided."
               << std::endl;  // ?
        return;
    }

    G4HCofThisEvent * HCE = evt->GetHCofThisEvent();
    SensitiveDetector::Collection * DHC = 0;
    G4int n_hit = 0;
    //  G4double blkE,blkT,blkL,totE=0;
    G4int i,n;
  
    DHC = 0;
    if(HCE){
        auto DHC_ = HCE->GetHC(hcCollID);
        DHC = dynamic_cast<SensitiveDetector::Collection *>(DHC_);
        if( DHC_ && !DHC ) {
            throw std::runtime_error(
                "Application architecture malformed: class mismatch." );// XXX
        }
    } else {
        if(verb){
            G4cout << " HCE!" << G4endl;
        } else {
            return;
        }
    }
    if(DHC==NULL) {
        return;
    }
    n_hit = DHC->entries();
    if(verb) {
        G4cout << "n_hit=" << n_hit << G4endl;
    }

    ///-----------------------------------------------------------------
    nev = evt->GetEventID();
  
    EventInfo* info = dynamic_cast<EventInfo*>(evt->GetUserInformation());
    assert( info );
    Eg = info->GetEgamma();
    nr = info->GetNreac();
  
    G4PrimaryVertex * pv;
  
    for(i=0;i<nvx;i++){     
        pv = evt->GetPrimaryVertex(i);
        if(pv->GetPrimary()->GetPDGcode() == base_code) {
            break; // proton ?
        }
    }
    if(i==nvx) {
        pv = evt->GetPrimaryVertex(0);
    }
  
    G4ThreeVector vertex=pv->GetPosition();
    vz=pv->GetZ0();
    vy=pv->GetY0();
    vx=pv->GetX0();

    if(i<nvx){    // proton found
        G4ThreeVector v=pv->GetPrimary()->GetMomentum();
        ep=pv->GetPrimary()->GetKineticEnergy();
        tp=v.theta();
        fp=v.phi();
    } else {
        ep=tp=fp=0.0;
    }

    nhx = nhz = nvc = nwa = nwb = nwc = 0;
    ac[0]=ac[1]=tof[0]=tof[1]=L[0]=L[1]=0.0;
    Hit *hch;
  
    // some stuff for "on-line" trigger
    # define HC_THRES    0.5*MeV
    # define WC_THRES    0.5*keV
    G4int hcdep[2]={0,0};
    G4int wcdep[2]={0,0};
    G4float ahx[2]={0.,0.},
            ahz[2]={0.,0.};
  
    for(i=0;i<n_hit;i++) {
        hch = (*DHC)[i];
        n = hch->blkN();
        G4int arm = (n >= ARM2_IND);
        if(arm) {
            n -= ARM2_IND;
        }
        G4float e = hch->Edep();
        G4float t = hch->ToF();
        //std::cout << "Hit's e = " << e << ", t = " << t
        //          << ", arm = " << arm << std::endl;  // XXX
        G4ThreeVector pos=hch->Pos();
        G4ThreeVector vpos=hch->Vpos();
        if(n==0){ // AC
            ac[arm]=e;
            //std::cout << "Triggered (" << i << "/" << n << "): AC." << std::endl;  // XXX
            continue;
        }
     
        if(n==1) { // RPC
            //ac[arm]=e;
            if(abs(pos.y())>1.) {
                G4ThreeVector path=pos-vertex; L[arm]=path.mag();
            }
            if(e>0.5*MeV) {
                tof[arm]=t;
                //if(L[arm]<1000. && L[arm]>0)G4cout<<" >>> nev="<<nev<<" pos="<<pos.x()<<","<<pos.y()<<","<<pos.z()<<G4endl;       
            } else { 
                tof[arm]=0;
            }
            //std::cout << "Triggered (" << i << "/" << n << "): RPC." << std::endl;  // XXX
            continue;
        }
        if(n >= HCX_IND&&n<HCX_IND+NX_BARS) {
            hxa[nhx]=e;
            hxx[nhx]=vpos.x();
            hxy[nhx]=vpos.y();
            hxz[nhx]=vpos.z();
            if(e>HC_THRES) {
                hcdep[arm]|=1;
            }
            ahx[arm]+=e;
            nhx++;
            //std::cout << "Triggered (" << i << "/" << n << "): #1." << std::endl;  // XXX
            continue;
        }
         
        if(n>=HCZ_IND&&n<HCZ_IND+NZ_BARS){
            hza[nhz]=e;
            hzx[nhz]=vpos.x();
            hzy[nhz]=vpos.y();
            hzz[nhz]=vpos.z();
            if(e > HC_THRES) {
                hcdep[arm]|=2;
            }
            ahz[arm]+=e;
            nhz++;
            //std::cout << "Triggered (" << i << "/" << n << "): #2." << std::endl;  // XXX
            continue;
        }
         
        if(n>=VC_IND&&n<VC_IND+NVC_WRS){
            vcx[nvc]=vpos.x();
            vcy[nvc]=vpos.y();
            if(e>WC_THRES) {
                wcdep[arm]|=1;
            }
            nvc++;
            //std::cout << "Triggered (" << i << "/" << n << "): #3." << std::endl;  // XXX
            continue;
        }

        if(n>=WC1_IND&&n<WC1_IND+NW1_WRS){
            waz[nwa]=pos.z();
            way[nwa]=pos.y();//vpos.y();
            if(e>WC_THRES) {
                wcdep[arm]|=2;
            }
            nwa++;
            //std::cout << "Triggered (" << i << "/" << n << "): #4." << std::endl;  // XXX
            continue;
        }
         
        if(n>=WC2_IND&&n<WC2_IND+NW2_WRS){
            wbx[nwb]=pos.x();
            wby[nwb]=pos.y();//vpos.y();
            if( e > WC_THRES ) {
                wcdep[arm]|=4;
            }
            nwb++;
            //std::cout << "Triggered (" << i << "/" << n << "): #5." << std::endl;  // XXX
            continue;
        }

        if(n>=WC3_IND&&n<WC3_IND+NW3_WRS) {
            wcz[nwc]=pos.z();
            wcy[nwc]=pos.y();//vpos.y();
            if(e>WC_THRES) wcdep[arm]|=8;
                nwc++;
            //std::cout << "Triggered (" << i << "/" << n << "): #6." << std::endl;  // XXX
            continue;
        }
    }

    /// Trigger
    if(TO){ 
        if( 1 
            // && (hcdep[0]==3  && hcdep[1]==3)    // hit in both HC arms
            && (     (ahx[0] > HC_THRES && ahz[0] > HC_THRES)    // HC hit in arm 1 
                  && (ahx[1] > HC_THRES && ahz[1] > HC_THRES))   // HC hit in arm 2
            && (wcdep[0]>=14 || wcdep[1]>=14)    // track at least in one arm (VX not mandatory)
           ){
            if( Application::object().verbosity() > 2 ) {
                G4cout << "Taken!" << G4endl;
            }
            TO->Fill();
        } else {
            if( Application::object().verbosity() > 2 ) {
                G4cout << "Rejected: hcdep = " << hcdep[0] << ", " << hcdep[1]
                       << "; wcdep = " << wcdep[0] << ", " << wcdep[1] << "; "
                       << "ahx[0] = " << ahx[0] << ", "
                       << "ahx[1] = " << ahx[1] << ", "
                       << "ahz[0] = " << ahz[0] << ", "
                       << "ahz[1] = " << ahz[1] << "."
                       << G4endl;
            }
        }
    }

    // extract the trajectories and draw them
    if(G4VVisManager::GetConcreteInstance()) {
        G4TrajectoryContainer* trajectoryContainer = evt->GetTrajectoryContainer();
        G4int n_trajectories = 0;
        if(trajectoryContainer) {
            n_trajectories = trajectoryContainer->entries();
        }

        for(G4int i=0; i<n_trajectories; i++) {
            G4Trajectory* trj = (G4Trajectory*)
                        ((*(evt->GetTrajectoryContainer()))[i]);
            if(drawFlag == "all") {
                trj->DrawTrajectory(50);
            } else if ((drawFlag == "charged")&&(trj->GetCharge() != 0.)) {
                trj->DrawTrajectory(50);
            } else if ((drawFlag == "neutral")&&(trj->GetCharge() == 0.)) {
                trj->DrawTrajectory(50);
            }
        }
    }
}


}  // namespace G4I

# endif  // WITH_GEANT4_MODEL

