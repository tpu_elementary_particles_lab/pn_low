c ----------------------------------------------------------------------
c     gamma+d --> pi NN 
c     (Pion Winkelverteilung im Lab-System)
c ----------------------------------------------------------------------

      program main
      implicit double precision (a-h,k,m,o-z)
      integer*4 jjm, llWW
      dimension xa(100),wa(100),pNf(0:3),Fg(12,4),h(2),pe2(3)
     . ,pa(80),wpa(80),pNN(3),ds2(5),ds12(5),ds22(5)
     . ,x1(100),x2(100),wx1(100),wx2(100)
     . ,ya(100),wya(100),y1(100),y2(100),wy1(100),wy2(100)
      dimension kgamma_(0:3),qP_(0:3),pN1_(0:3),pN2_(0:3)

      complex*16 um,Am(20,-1:1,100),KNt(-1:1,-1:1),LNt(-1:1,-1:1,-1:1)
     .,FNN(-1:1,-1:1,-1:1,-2:2,10,4),ANN,Gd(9,4,4),Tfi,GpN
     .,f2(5),TM(-1:1,-1:1,0:1,-1:1),f(5),tt20,tt21,tt22

      common /pi/pi,/unt/unt,/um/um
     . ,/MN/MN,/Mpi/Mpi,/Md/Md,/W/W,/Eb/Eb
     . ,/qP/qP(0:3),/k/k(0:3),/pf1/pf1(0:3),/pf2/pf2(0:3)
     . ,/q/q(100),/wg/wg(100),/Nq/Nq
     . ,/q1/q1(100),/wg1/wg1(100),/Nq1/Nq1
     . ,/x/x(30),wx(30),/Nx/Nx,/y/y(30),wy(30),/Ny/Ny
     . ,/ANN/ANN(-1:1,-1:1,-1:1,-2:2,12)
     . ,/wN/wN(100),/Am/Am,/Nm/Nm
     . ,/cbNN/cNN(12,4,4),bNN(12,4,4),/gNN/gNN(12,4,4)
     . ,/NLA/nLA(12),/NLN/nLN(12),/NdN/Nd(12),/NsN/nS(12),/NFN/NFN(12)
     . ,/LnP/LnP(10),/JnP/JnP(10),/iPT/iPT(10) 
     . ,/c_PiN/c_PiN(10,10),/GpN/GpN(10,2,2)
     . ,/Dd/Dd(11),Sd(11),al(11),/dN/dN,/lWW/lWW,/iND/iND 
     . ,/xy12/x12,y12

     . ,/TCG/TCG(-1:1,-1:1,0:1,-1:1,-2:2,-1:1)
     . ,/Di/Mdi,Gdi,gM1,fND,Gdig,MDl,fpND,Ldi,iS

      common /rs/ rsig

      data    !Bonn
     . (Sd(i),i=1,11)/0.90457337d0,-0.35058661d0
     . ,-0.17635927d0,-0.10418261d2,0.45089439d2,-0.14861947d3
     . ,0.31779642d3,-0.37496518d3,0.22560032d3
     . ,-0.54858290d2,-2.59399414d-3 /
     . ,(Dd(i),i=1,11)/0.24133026d-1,-0.64430531d0
     . ,0.51093352d0,-0.54419065d1,0.15872034d2,-0.14742981d2
     . ,0.44956539d1,-0.71152863d-1,0.43469850d0
     . ,-0.10267993d1,0.58969198d0 /

c     ------------------------------------------------------------  NN-Waves
c      1     2     3     4     5     6     7     8     9    10    11    12
c     ----------------------------------------------------------------------
c     1S0 ! 1P1 ! 3P0 ! 3P1 ! 1D2 ! 3D2 ! 3S1 ! 3P2 ! 3D3 ! 3D1 ! 3F2 ! 3G3

      data (Nd(i),i=1,12)/3,2,2, 2,2,2, 4,3,4, 4,3,4/
     . ,(nLA(i),i=1,12)/0,1,1, 1,2,2, 0,1,2, 2,2,2/ 
     . ,(nLN(i),i=1,12)/0,1,1, 1,2,2, 0,1,2, 2,3,4/ 
     . ,(NFN(i),i=1,12)/0,0,0, 0,1,1, 0,0,1, 1,1,2/ 
     . ,(nS(i),i=1,12)/0,0,1, 1,0,1, 1,1,1, 1,1,1/ 

      data !Paris
     . ((bNN(1,i,j),j=1,4),i=1,3)/1.1115753d0,2.021232d0,2.6434278d0
     .,4.0877911d0,1.0297944d0,1.5435994d0
     .,2.6129194d0,4.0837929d0,1.0416637d0,1.8741794d0,3.0550142d0
     ,,3.8479250d0 /
     .,((cNN(1,i,j),j=1,4),i=1,3)/-773.8d0,-424.05244d0
     .,17326.082d0,-38504.839d0,-18.494719d0,-53.628348d0 
     .,925.42408d0,-2040.002d0,1.2361845d0
     .,9.4601176d0,646.74738d0,-1031.2936d0 /
     .,((gNN(1,i,j),j=1,3),i=1,3)/-0.0004644316d0,0.0098132185d0
     .,0.0012041892d0,0.0098132185d0,-0.24043231d0,-0.1366375d0  
     .,0.0012041892d0,-0.1366375d0,0.67334726d0 /  

     .,((bNN(2,i,j),j=1,4),i=1,2)/1.5652079d0,2.0058818d0,2.4100051d0
     .,4.2981726d0,1.5463024d0,1.6196926d0,2.0004143d0
     .,2.1796197d0 /
     .,((cNN(2,i,j),j=1,4),i=1,2)/89.398777d0,-208.13553d0
     .,425.34413d0,-630.35239d0,43.477263d0,166.46466d0 
     .,-404.03968d0,528.13575d0 /
     .,((gNN(2,i,j),j=1,2),i=1,2)/0.44514549d0,-0.3406788d0
     .,-0.3406788d0,0.44501514d0/  

     .,((bNN(3,i,j),j=1,4),i=1,2)/1.8691379d0,3.0499866d0,3.5585752d0
     .,1.1596292d0,2.5512467d0,2.6651826d0,2.9537139d0
     .,3.3827130d0 /
     .,((cNN(3,i,j),j=1,4),i=1,2)/-189.74407d0,2096.7281d0
     .,-3066.2694d0,102.75384d0,59.86155d0,-681.32014d0 
     .,5265.5015d0,-6825.757d0 /
     .,((gNN(3,i,j),j=1,2),i=1,2)/-0.14665888d0,0.40513783d0
     .,0.40513783d0,-5.478932d0 /  

     .,((bNN(4,i,j),j=1,4),i=1,2)/1.6225099d0,2.4712881d0,2.7120059d0
     .,3.1642803d0,1.7697160d0,2.40497229d0,1.8631357d0
     .,4.1084782d0 /
     .,((cNN(4,i,j),j=1,4),i=1,2)/ 76.181266d0,-381.78599d0
     .,1471.7803d0,-1646.4168d0,61.335643d0,1348.5020d0 
     .,-842.10204d0,-1604.2632d0 /
     .,((gNN(4,i,j),j=1,2),i=1,2)/0.66973064d0,-0.52203105d0
     .,-0.52203105d0,0.64075503d0/  

     .,((bNN(5,i,j),j=1,4),i=1,2)/1.1690501d0,1.3354235d0
     .,3.6311131d0,2.9623562d0,0.87157918d0,1.2853211d0,3.2457085d0
     .,3.8839614d0 /
     .,((cNN(5,i,j),j=1,4),i=1,2)/-7.6541157d0,3.8397789d0
     .,-348.08899d0,330.93549d0,-1.0867868d0,-22.731322d0 
     .,265.30215d0,-303.60967d0 /
     .,((gNN(5,i,j),j=1,2),i=1,2)/-1.9086253d0,1.0085124d0
     .,1.0085124d0,-0.97543827d0/  

     .,((bNN(6,i,j),j=1,4),i=1,2)/1.2055706d0,1.2960008d0
     .,1.6239553d0,1.4452618d0,1.5953203d0,0.36269907d0,1.7370869d0
     .,0.58735827d0 /
     .,((cNN(6,i,j),j=1,4),i=1,2)/-40.322168d0,-21.021097d0
     .,533.48026d0,-436.55162d0,-245.06021d0,3.0950339d0 
     .,299.77237d0,42.826073d0 /
     .,((gNN(6,i,j),j=1,2),i=1,2)/-0.26674102d0,0.14305117d0
     .,0.14305117d0,-0.15817248d0/  

     .,((bNN(7,i,j),j=1,4),i=1,4)/1.6855291d0,3.9205339d0,5.763684d0
     .,6.0419695d0,1.78779d0,2.175921d0,2.4705717d0,2.7303931d0 
     .,1.5971102d0,9.9678931d0,4.5948011d0,2.1206347d0
     .,4.0268195d0,5.0466356d0,2.5795951d0,2.3953665d0 /
     .,((cNN(7,i,j),j=1,4),i=1,4)/-51.009539d0,765.2439d0,-3363.4996d0
     .,2783.6926d0,-68.04132d0,488.86119d0,-1199.164d0,871.79604d0
     .,-89.517302d0,2582.2164d0,-3042.3325d0,1235.3473d0,-382.75856d0
     .,1005.8091d0,2298.7534d0,-2718.2533d0 /
     .,((gNN(7,i,j),j=1,4),i=1,4)/-0.25422242d0,-0.17178772d0
     .,0.071723814d0,0.0051050109d0,-0.17178772d0,0.08772439d0
     .,0.094518781d0,-0.079244845d0,0.071723814d0,0.094518781d0
     .,-0.030645992d0,-0.034997609d0,0.0051050109d0,-0.079244845d0
     .,-0.034997609d0,0.16955343d0/

     .,((bNN(8,i,j),j=1,4),i=1,3)/1.4452936d0,2.0173835d0
     .,5.4463467d0,2.8490316d0,1.8993563d0,2.4547294d0,1.5499793d0
     .,5.9773387d0,2.9659955d0,4.9324835d0,2.9051889d0,2.0463803d0 /
     .,((cNN(8,i,j),j=1,4),i=1,3)/-28.609391d0,-186.15068d0
     .,-1094.0574d0,1022.0712d0,65.794074d0,173.01447d0 
     .,-190.40569d0,-217.74704d0,-327.4079d0,-2950.6615d0
     .,4127.4473d0,-1298.0249 /
     .,((gNN(8,i,j),j=1,3),i=1,3)/0.046725027d0,0.41710484d0
     .,-0.060922483d0,0.41710484d0,0.25492496d0,-0.41481703d0
     .,-0.060922483d0,-0.41481703d0,-0.13593596d0 /  
     .,((bNN(11,i,j),j=1,4),i=1,3)/1.1667491d0,1.5406619d0
     .,2.3802869d0,4.8335309d0,1.0955323d0,1.6020596d0,4.4585234d0
     .,1.8254989d0,2.1084981d0,3.1977404d0,3.9573349d0,4.5120514d0 /
     .,((cNN(11,i,j),j=1,4),i=1,3)/0.96744407d0,166.30912d0
     .,-119.42176d0,36.96981d0,-0.63868415d0,-37.538146d0 
     .,-316.97599d0,179.59323d0,-47.364933d0,2035.5781d0
     .,-5925.4169d0,5407.1036 /

     .,((bNN(9,i,j),j=1,4),i=1,4)/1.7275071d0,2.5493316d0,1.9111954d0
     .,4.0669172d0,1.9189644d0,2.738169d0,1.539602d0,1.9159334d0
     .,3.227718d0,4.8060158d0,3.100509d0,3.7214195d0
     .,2.8426813d0,3.3549103d0,3.2121201d0,2.7766588d0 /
     .,((cNN(9,i,j),j=1,4),i=1,4)/-8.6379311d0,178.84657d0,-114.21507d0
     .,-86.040765d0,-116.07858d0,354.48169d0,196.1262d0,-419.69957d0
     .,-110.31148d0,483.29454d0,442.80069d0,-903.63957d0,-364.8871d0
     .,1848.1193d0,2550.6547d0,-3985.0257d0 /
     .,((gNN(9,i,j),j=1,4),i=1,4)/-0.098992244d0,-0.39484562d0
     .,0.040135281d0,0.11343039d0,-0.39484562d0,0.28242764d0
     .,0.40048398d0,-0.34677241d0,0.040135281d0,0.40048398d0
     .,-0.14224121d0,-0.3800097d0,0.11343039d0,-0.34677241d0
     .,-0.3800097d0,0.50560372/
     .,((bNN(12,i,j),j=1,4),i=1,4)/2.4000943d0,1.2830437d0
     .,1.6627122d0,1.5581999d0,3.9194456d0,1.5719585d0,1.655341d0
     .,2.0963683d0,4.8032746d0,3.6976328d0,2.7214754d0,1.9739909d0
     .,1.8399157d0,2.0748484d0,2.283176d0,2.6095142d0 /
     .,((cNN(12,i,j),j=1,4),i=1,4)/10.076418d0,-90.394797d0,283.81707d0
     .,-204.52799d0,10.924992d0,82.607174d0,-132.30266d0,47.770331d0
     .,-204.05105d0,-751.6137d0,1886.8354d0,-954.9154d0,-0.24299133d0
     .,197.5883d0,-411.75699d0,229.35025d0 /

     .,((bNN(10,i,j),j=1,4),i=1,4)/2.6228398d0,1.8815276d0
     .,3.834678d0,4.9959386d0,1.9098545d0,1.1170776d0,1.4259853d0
     .,3.0734784d0,2.6431889d0,2.7483825d0,1.9283993d0,2.2891433d0
     .,4.2598369d0,2.1463834d0,2.4905282d0,2.2930841d0 /
     .,((cNN(10,i,j),j=1,4),i=1,4)/-410.15751d0,156.64127d0,888.03985d0
     .,-792.11994d0,87.065827d0,20.554605d0,-65.702538d0,-91.839262d0
     .,143.6215d0,6.4744841d0,-1384.0981d0,1550.1289d0,366.50118d0
     .,263.87202d0,-611.66811d0,-12.606207d0 /

     .,dN/98.1822994d0/,Eb/-2.225099467609525d0/

c     -----------------------------------------------  piN-Waves
c      1     2     3     4     5     6     7     8     9    10    
c     ----------------------------------------------------------
c     S11 ! S31 ! P11 ! P13 ! P31 ! P33 ! D13 ! D15 ! D33 ! D35 

      data  !PiN-Parameter
     . ((c_PiN(nP,j),j=1,10),nP=1,10)/ 
     . 0,0,3, 1.d2,    2.598d0,2,2,4.952d0, 2.877d0,-1 
     .,0,0,2, 3.085d0, 1.806d0,2,2,1.925d0, 1.275d0, 1 
     .,1,2,3,31.623d0, 2.665d0,0,2,0.5793d0,1.185d0,-1 
     .,1,0,2, 0.4269d0,1.181d0,2,3,3.970d0, 1.721d0, 1 
     .,1,0,2, 1.473d0, 1.542d0,2,3,8.053d0, 1.861d0, 1 
     .,1,0,2, 2.770d0, 1.415d0,0,2,1.778d0, 1.218d0, 1 
     .,2,0,2, 1.639d0, 2.165d0,2,3,9.312d0, 3.263d0,-1 
     .,2,0,2, 0.2172d0,1.175d0,2,3,1.011d0, 1.461d0,-1 
     .,2,0,2, 0.1306d0,1.128d0,2,3,1.081d0, 1.972d0,-1 
     .,2,0,2, 0.2270d0,1.168d0,2,3,1.151d0, 1.780d0, 1/ 
     .,(LnP(nP),nP=1,10) /0,0,1,1,1,1,2,2,2,2/ !Bahndrehinpuls
     .,(JnP(nP),nP=1,10) /1,1,1,2,1,2,1,2,1,2/ !Spin 
     .,(iPT(nP),nP=1,10) /1,2,1,1,2,2,1,1,2,2/ !Isospin 

c     -------------------------------------------------------------
      do i=1,11
      al(i)=(0.231609d0+(i-1)*9d-1)
      end do
c     -------------------------------------------------------------

      open(unit=10,file='E0pl.dat')
      open(unit=11,file='E1pl.dat')
      open(unit=12,file='M1pl.dat')
      open(unit=13,file='M1mi.dat')
      open(unit=14,file='E2pl.dat')
      open(unit=15,file='M2pl.dat')
      open(unit=16,file='E2mi.dat')
      open(unit=17,file='M2mi.dat')
      open(unit=18,file='E3pl.dat')
      open(unit=19,file='M3pl.dat')
      open(unit=20,file='E3mi.dat')
      open(unit=21,file='M3mi.dat')
      open(unit=22,file='E4pl.dat')
      open(unit=23,file='M4pl.dat')
      open(unit=24,file='E4mi.dat')
      open(unit=25,file='M4mi.dat')
      open(unit=26,file='E5pl.dat')
      open(unit=27,file='M5pl.dat')
      open(unit=28,file='E5mi.dat')
      open(unit=29,file='M5mi.dat')

      Nm=97
      read(10,*) (wN(i),Am(1,0,i),Am(1,1,i),Am(1,-1,i),i=1,Nm)    !E0pl
      read(11,*) (wN(i),Am(2,0,i),Am(2,1,i),Am(2,-1,i),i=1,Nm)    !E1pl
      read(12,*) (wN(i),Am(3,0,i),Am(3,1,i),Am(3,-1,i),i=1,Nm)    !M1pl
      read(13,*) (wN(i),Am(4,0,i),Am(4,1,i),Am(4,-1,i),i=1,Nm)    !M1mi
      read(14,*) (wN(i),Am(5,0,i),Am(5,1,i),Am(5,-1,i),i=1,Nm)    !E2pl
      read(15,*) (wN(i),Am(6,0,i),Am(6,1,i),Am(6,-1,i),i=1,Nm)    !M2pl
      read(16,*) (wN(i),Am(7,0,i),Am(7,1,i),Am(7,-1,i),i=1,Nm)    !E2mi
      read(17,*) (wN(i),Am(8,0,i),Am(8,1,i),Am(8,-1,i),i=1,Nm)    !M2mi
      read(18,*) (wN(i),Am(9,0,i),Am(9,1,i),Am(9,-1,i),i=1,Nm)    !E3pl
      read(19,*) (wN(i),Am(10,0,i),Am(10,1,i),Am(10,-1,i),i=1,Nm) !M3pl
      read(20,*) (wN(i),Am(11,0,i),Am(11,1,i),Am(11,-1,i),i=1,Nm) !E3mi
      read(21,*) (wN(i),Am(12,0,i),Am(12,1,i),Am(12,-1,i),i=1,Nm) !M3mi
      read(22,*) (wN(i),Am(13,0,i),Am(13,1,i),Am(13,-1,i),i=1,Nm) !E4pl
      read(23,*) (wN(i),Am(14,0,i),Am(14,1,i),Am(14,-1,i),i=1,Nm) !M4pl
      read(24,*) (wN(i),Am(15,0,i),Am(15,1,i),Am(15,-1,i),i=1,Nm) !E4mi
      read(25,*) (wN(i),Am(16,0,i),Am(16,1,i),Am(16,-1,i),i=1,Nm) !M4mi
      read(26,*) (wN(i),Am(17,0,i),Am(17,1,i),Am(17,-1,i),i=1,Nm) !E5pl
      read(27,*) (wN(i),Am(18,0,i),Am(18,1,i),Am(18,-1,i),i=1,Nm) !M5pl
      read(28,*) (wN(i),Am(19,0,i),Am(19,1,i),Am(19,-1,i),i=1,Nm) !E5mi
      read(29,*) (wN(i),Am(20,0,i),Am(20,1,i),Am(20,-1,i),i=1,Nm) !M5mi

c     ***********************************************************
c      jm=-1  ! -1 = >pi^-pp;     0 = >pi^0 np;    1 = >pi^+ nn;     Kanal
c      lWW=2  !  0 = >IA;         1 = >IA+NN;      2 = >IA+NN+PiN;   FSI
c     ***********************************************************
c      print *,'jjm = ', jjm, ' llWW = ', llWW

      jjm=0
      llWW=2     
      iiND=0  

      jm = jjm
      lWW = llWW
      iND = iiND

c     print *,'jm = ', jm, ' lWW = ', lWW, 'iND = ',iND

      um=(0d0,1d0)
      pi=3.141592653589793238462643d0
      unt=197.32705359d0
c     -------------------------------------------------------------

      do 1 nN=1,12
      do 1 i=1,Nd(nN)
      do 1 n=1,4
      bNN(nN,i,n)=bNN(nN,i,n)*unt
      gNN(nN,i,n)=gNN(nN,i,n)*2d0*pi**2*unt
      if(nLN(nN).eq.1)gNN(nN,i,n)=gNN(nN,i,n)*unt**2
 1    continue

      do 2 nP=1,10
      c_PiN(nP,5)=c_PiN(nP,5)*unt
      c_PiN(nP,9)=c_PiN(nP,9)*unt
      c_PiN(nP,4)=c_PiN(nP,4)
     . /unt**(-2d0*c_PiN(nP,3)+c_PiN(nP,2)+c_PiN(nP,1)+1)
 2    c_PiN(nP,8)=c_PiN(nP,8)
     . /unt**(-2d0*c_PiN(nP,7)+c_PiN(nP,6)+c_PiN(nP,1)+1)
      c_PiN(3,8)=c_PiN(3,8)*dsqrt(unt)
      c_PiN(6,8)=c_PiN(6,8)*dsqrt(unt)
c     -----------------------------------------
      call CG                   !Clebsch-Gordan 
c     -----------------------------------------
      MN=(938.271988d0+939.56533d0)/2d0
      if(jm.eq.1)MN=939.56533d0
      if(jm.eq.-1)MN=938.271988d0
      Mpi=139.57d0
      if(jm.eq.0)Mpi=134.95d0
      Md=1875.612762d0

      MDl=1232d0
      GDl=120d0
      qDl=Flmb(MDl,Mpi,MN)
      fpND=4d0*pi*dsqrt(Mpi**2*MDl*GDl/(qDl**3*2d0*MN))

      Mdi=2500d0
      Gdi=50d0
      Gdig=0.0005d0
      kdi=Flmb(Mdi,MN,0d0)
      Ed=dsqrt(kdi**2+Md**2)
      gM1=dsqrt(24d0*pi*Md**2*Mdi*Gdig/(kdi**3*Ed))
      qdi=Flmb(Mdi,MDl,MN)
c     ---------
      Nx=19
      call gaussp (Nx,x,wx)
      Ny=19
      call gaussp (Ny,y,wy)
      do i=1,Ny
      y(i)=pi*(y(i)+1d0)
      wy(i)=pi*wy(i)
      end do
c     -----------------------------------------
      Nq=12
      call gaussp0inf(Nq,q,wg,150d0)
c     -----------------------------------------
      Nq1=12
      call gaussp0inf(Nq1,q1,wg1,150d0)
c     -----------------------------------------

      if(iND.eq.0) goto 21
      iND=4
      goto (301,302,303,304,305,306,307) iND   !Vibor Dibariona
 301  iJ=0
      Ldi=2
      iS=2
      goto 308
c     -----------------
 302  iJ=1
      Ldi=0
      iS=1
      goto 308
c     -----------------
 303  iJ=1
      Ldi=2
      iS=1
      goto 308
c     -----------------
 304  iJ=1
      Ldi=2
      iS=2
      goto 308
c     -----------------
 305  iJ=2
      Ldi=0
      iS=2
      goto 308
c     -----------------
 306  iJ=2
      Ldi=2
      iS=1
      goto 308
c     -----------------
 307  iJ=2
      Ldi=2
      iS=2
c     -----------------
 308  continue

      fND=4d0*pi*dsqrt(4d0*MN**(2*Ldi)*Mdi*Gdi/(qdi**(2*Ldi+1)*2d0*MDl))

      do 101 id=-1,1
      do 101 il=-1,1,2
      imJ=id+il
      do 101 isNN=0,1
      do 101 imL=max(-Ldi,imJ-iS),min(Ldi,imJ+iS)
      imS=2*(imJ-imL)
      do 101 imsNN=max(-isNN,imS/2-1),min(isNN,imS/2+1)
      im=imS/2-imsNN
      do 101 im1=max(-1,2*imsNN-1,imS-3),min(1,2*imsNN+1,imS+3),2
      imD=imS-im1
      TCG(il,id,isNN,imsNN,imL,im)=0d0
      imD=imS-im1
      im2=2*imsNN-im1
 101  TCG(il,id,isNN,imsNN,imL,im)=TCG(il,id,isNN,imsNN,imL,im)+
     .  clgo(1d0,id*1d0,1d0,il*1d0,iJ*1d0,imJ*1d0)
     . *clgo(5d-1,im1/2d0,1.5d0,imD/2d0,iS*1d0,imS/2d0)
     . *clgo(iS*1d0,imS/2d0,Ldi*1d0,imL*1d0,iJ*1d0,imJ*1d0)
     . *clgo(5d-1,im2/2d0,1d0,im*1d0,1.5d0,imD/2d0)
     . *clgo(5d-1,im1/2d0,5d-1,im2/2d0,isNN*1d0,imsNN*1d0)

 21   continue

c     ============================================================================
      open (unit=151,file='/home/alex/store/aren2')
      open (unit=152,file='/home/alex/store/ssig')

      RGR=3.141592653589/180.
c     MAJORANTA BLOCK *******************
      mjweight_pimi_tot=0.15E-010
      mjweight_pi0_tot=0.30E-010
c     ***********************************
      LXZ=0

 991  CONTINUE     
      xxzz=rand()
      CALL cin(kgamma_,PN1_,PN2_,qP_,ROS,z)     
      CALL wed(jm,lWW,kgamma_,qP_,pN1_,pN2_,TKB,TKR,TM,tt20,tt21,tt22)
      WEIGHT=TKR*ROS*DALIZ(kgamma_(0))
      YM=mjweight_pi0_tot*rand()     
      IF (YM.LT.WEIGHT) THEN  
      CONTINUE  
      ELSE 
      GOTO 991
      END IF 
      LXZ=LXZ+1

      

      PP1=DSQRT(PN1_(1)**2+PN1_(2)**2+PN1_(3)**2)
      PP2=DSQRT(PN2_(1)**2+PN2_(2)**2+PN2_(3)**2)
      PQQ=DSQRT(qP_(1)**2+qP_(2)**2+qP_(3)**2)      
      M1Q=DSQRT((PN1_(0)+qP_(0))**2-(PN1_(1)+qP_(1))**2
     *   -(PN1_(2)+qP_(2))**2-(PN1_(3)+qP_(3))**2)
      M2Q=DSQRT((PN2_(0)+qP_(0))**2-(PN2_(1)+qP_(1))**2
     *   -(PN2_(2)+qP_(2))**2-(PN2_(3)+qP_(3))**2)
      M12=DSQRT((PN1_(0)+PN2_(0))**2-(PN1_(1)+PN2_(1))**2
     *         -(PN1_(2)+PN2_(2))**2-(PN1_(3)+PN2_(3))**2)
      CALL AXIAL(PN1_,T1,FI1)
      CALL AXIAL(PN2_,T2,FI2)
      CALL AXIAL(qP_,TQ,FQ)
      EGL=kgamma_(0)
      M123=DSQRT(MD**2+2*MD*EGL)

c      print*,'WEIGHT = ',WEIGHT,'LXZ = ',LXZ
c      print*,'Z  RSIG ',z,'  ',rsig
c      print*,'TKB   TKR ',TKB,' ',TKR,' ',TKR/TKB
c      print*,tt20,' ',tt21,' ',tt22
c      print*,'P1  = ',PP1_,'T1  = ',T1/RGR,'FI1 = ',FI1/RGR
c      print*,'P2  = ',PP2_,'T2  = ',T2/RGR,'FI2 = ',FI2/RGR
c      print*,'Q   = ',PQQ_,'TQ  = ',TQ/RGR,'FQ  = ',FQ/RGR
c      print*,'M1Q = ',M1Q_,'M2Q = ',M2Q_,'M12 = ',M12_
c      print*,'EGL = ',EGL,'M123= ',M123
c      print*,' '

      if (pp1.ge.pp2) then
      write(151,111)  pp1,t1/rgr,fi1/rgr,
     *              pp2,t2/rgr,fi2/rgr,
     *              pqq,tq/rgr,fq/rgr,
     *              m1q,m2q,m12,m123,
     *              egl,pp1,t1/rgr,fi1/rgr,
     *              pp2,t2/rgr,fi2/rgr,z,
     *              dreal(tt20),dreal(tt21),dimag(tt21),
     *              dreal(tt22),dimag(tt22),rsig,
     *              weight,weight
      else
      write(151,111)  pp2,t2/rgr,fi2/rgr,
     *              pp1,t1/rgr,fi1/rgr,
     *              pqq,tq/rgr,fq/rgr,
     *              m2q,m1q,m12,m123,
     *              egl,pp1,t1/rgr,fi1/rgr,
     *              pp2,t2/rgr,fi2/rgr,z,
     *              dreal(tt20),dreal(tt21),dimag(tt21),
     *              dreal(tt22),dimag(tt22),rsig,
     *              weight,weight
     
      end if
 111  format(29E14.7)

      write(152,112) weight
 112  format(E14.7)

      print*,'L = ',LXZ

      IF (LXZ.LT.500000) GOTO 991

c     ===========================================================================

      stop 'The  programm  main is stopped'
      end

c -------------------------------------------------------------------

      subroutine 
     .wed(jm,llWW,kgamma,qP,pN1,pN2,TKB,TKR,TM,tt20,tt21,tt22)
      implicit double precision (a-h,k,m,o-z)
      integer s,Sdi
      complex*16 um,TM(-1:1,-1:1,0:1,-1:1)
     . ,Y1(-2:2),Y2(-2:2),Ym(-2:2),YND1(-2:2),YND2(-2:2)
     . ,YD1(-2:2),YD2(-2:2)
     . ,TIA_1(-1:1,-1:1,0:1,-1:1,0:1),TIA_2(-1:1,-1:1,0:1,-1:1,0:1)
     . ,TNN(-1:1,-1:1,0:1,-1:1,0:1)
     . ,TPN_1(-1:1,-1:1,0:1,-1:1,0:1),TPN_2(-1:1,-1:1,0:1,-1:1,0:1)
     . ,TDi(-1:1,-1:1,0:1,-1:1,0:1)
     . ,sig(0:1,-1:1,-1:1),tau(0:1,-1:1,-1:1),f(5)
     . ,st20,st21,st22,rho(-1:1,-1:1),tt20,tt21,tt22
     . ,ANN,FNN(-1:1,-1:1,-1:1,-2:2,10,4),Gd(9,4,4)

      dimension Qr(3),qPN1(3),qPN2(3),Fg(12,4)
     . ,kgamma(0:3),qP(0:3),pN1(0:3),pN2(0:3),pNN(3)

      dimension T20(-1:1,-1:1),T21(-1:1,-1:1),T22(-1:1,-1:1)

      common /pi/pi,/um/um,/W/W,/MN/MN,/Mpi/Mpi,/Md/Md
     . ,/KEN/pf(0:3),/lWW/lWW,/iND/iND
     . ,/k/k(0:3),/qP/q(0:3),/pf1/pf1(0:3),/pf2/pf2(0:3)
     . ,/xy12/x12,y12,/dN/dN,/Eb/Eb

     . ,/TCG/TCG(-1:1,-1:1,0:1,-1:1,-2:2,-1:1)
     . ,/Di/Mdi,Gdi,gM1,fND,Gdig,MDl,fpND,Ldi,Sdi

     . ,/NLA/nLA(12),/NdN/Nd(12),/NsN/nS(12)
     . ,/ANN/ANN(-1:1,-1:1,-1:1,-2:2,12)

      common /rs/ rsig

      lWW = llWW
c      print *,'jm = ',jm,' lWW = ',lWW

      T20(1,1)=1./dsqrt(2.d0)
      T20(1,0)=0.
      T20(1,-1)=0.
      T20(0,1)=0.
      T20(0,0)=-dsqrt(2.d0)
      T20(0,-1)=0.
      T20(-1,1)=0.
      T20(-1,0)=0.
      T20(-1,-1)=1./dsqrt(2.d0)

      T21(1,1)=(0.,0.)
      T21(1,0)=-(1.,0.)*dsqrt(3.d0/2.d0)
      T21(1,-1)=(0.,0.)
      T21(0,1)=(0.,0.)
      T21(0,0)=(0.,0.)
      T21(0,-1)=(1.,0.)*dsqrt(3.d0/2.d0)
      T21(-1,1)=(0.,0.)
      T21(-1,0)=(0.,0.)
      T21(-1,-1)=(0.,0.)  

      T22(1,1)=(0.,0.)
      T22(1,0)=(0.,0.)
      T22(1,-1)=(1.,0.)*dsqrt(3.d0)
      T22(0,1)=(0.,0.)
      T22(0,0)=(0.,0.)
      T22(0,-1)=(0.,0.)
      T22(-1,1)=(0.,0.)
      T22(-1,0)=(0.,0.)
      T22(-1,-1)=(0.,0.)   

      do i=0,3
      k(i)=kgamma(i)
      q(i)=qP(i)
      pf1(i)=pN1(i)
      pf2(i)=pN2(i)
c     print *,k(i),q(i),pf1(i),pf2(i)
      end do

      W = dsqrt(Md**2 + 2d0*k(0)*Md)

      do i=1,3
      pNN(i)=5d-1*(pf1(i)-pf2(i))
      end do
c
      pNNa=dsqrt(pNN(1)**2+pNN(2)**2+pNN(3)**2)
c
      x12=pNN(3)/pNNa
      y12=datan(pNN(2)/pNN(1))
      wNN=dsqrt((pf1(0)+pf2(0))**2-(pf1(1)+pf2(1))**2
     . -(pf1(2)+pf2(2))**2-(pf1(3)+pf2(3))**2)
      zd=wNN-2d0*MN

c     -----------------------
      if (lWW.eq.0) goto 190
c     ----------------------- TNN --------------
      do 10 il=-1,1,2
      do 10 id=-1,1
      do 10 nN=1,12
      do 10 im=-nLA(nN),nLA(nN)
      do 10 is=-nS(nN),nS(nN)
      ANN(il,id,is,im,nN)=0d0  
 10   continue
      call Gdr_L(zd,Gd)
      call Ff_NN(pNNa,Fg)
c     ----------------------
      call StNN(jm,pNNa,FNN)
c     ----------------------
      do 48 il=-1,1,2
      do 48 id=-1,1
      do 47 nN=1,9
      do 47 im=-nLA(nN),nLA(nN)
      do 47 is=-nS(nN),nS(nN)
      do 47 ni=1,Nd(nN)
      do 47 nj=1,Nd(nN)
 47   ANN(il,id,is,im,nN)=ANN(il,id,is,im,nN)
     . +FNN(il,id,is,im,nN,ni)*Gd(nN,ni,nj)*Fg(nN,nj)
      do 48 is=-1,1          !3SD1,3DS1,3D1
      do 48 ni=1,4
      do 48 nj=1,4
      ANN(il,id,is,0,11)=ANN(il,id,is,0,11)
     . +FNN(il,id,is,0,7,ni)*Gd(7,ni,nj)*Fg(10,nj)
      do 48 im=-2,2
      ANN(il,id,is,im,12)=ANN(il,id,is,im,12)
     . +FNN(il,id,is,im,10,ni)*Gd(7,ni,nj)*Fg(7,nj)
 48   ANN(il,id,is,im,10)=ANN(il,id,is,im,10)
     . +FNN(il,id,is,im,10,ni)*Gd(7,ni,nj)*Fg(10,nj)
 190  continue
c     ------------------------------------------- Kinematik
      p1=dsqrt(pf1(1)**2+pf1(2)**2+pf1(3)**2)
      p2=dsqrt(pf2(1)**2+pf2(2)**2+pf2(3)**2)
      do 1 il=-1,1,2
      do 1 id=-1,1
      do 1 s=0,1
      do 1 is=-s,s
      TM(il,id,s,is)=0d0
      do 1 it=0,1
      TIA_1(il,id,s,is,it)=0d0
      TIA_2(il,id,s,is,it)=0d0
      TNN(il,id,s,is,it)=0d0
      TPN_1(il,id,s,is,it)=0d0
      TPN_2(il,id,s,is,it)=0d0
 1    TDi(il,id,s,is,it)=0d0
c     -----------------------------------
c     IA
      call KIN_IA(q,pf1,pf2)
      call IA(jm,TIA_1)
c     --------
      call KIN_IA(q,pf2,pf1)
      call IA(jm,TIA_2)
c     print *,'IA'
      goto (4,3,2) lWW+1
c     --------------------------------
c     PiN-Wechselwirkung
 2    continue
      do i=1,3
      Qr(i)=(MN*q(i)-Mpi*pf2(i))/(MN+Mpi)
      pf(i)=pf1(i)
      end do
      pf(0)=pf1(0)
      q0=dsqrt(Qr(1)**2+Qr(2)**2+Qr(3)**2)
      call YY(1,Qr(3)/q0,(Qr(1)+um*Qr(2))/dsqrt(q0**2-Qr(3)**2),Y1)
      call YY(2,Qr(3)/q0,(Qr(1)+um*Qr(2))/dsqrt(q0**2-Qr(3)**2),Y2)
      call G_PiN(dsqrt((W-pf1(0))**2-p1**2))
      call PiN(jm,Y1,Y2,q0,TPN_1)
c     --------
      do i=1,3
      Qr(i)=(MN*q(i)-Mpi*pf1(i))/(MN+Mpi)
      pf(i)=pf2(i)
      end do
      pf(0)=pf2(0)
      q0=dsqrt(Qr(1)**2+Qr(2)**2+Qr(3)**2)
      call YY(1,Qr(3)/q0,(Qr(1)+um*Qr(2))/dsqrt(q0**2-Qr(3)**2),Y1)
      call YY(2,Qr(3)/q0,(Qr(1)+um*Qr(2))/dsqrt(q0**2-Qr(3)**2),Y2)
      call G_PiN(dsqrt((W-pf2(0))**2-p2**2))
      call PiN(jm,Y1,Y2,q0,TPN_2)
c     --------------------------------
c     NN-Wechselwirkung
 3    call NNW(x12,y12,TNN)
 4    continue

c     -------------------------------------------
      if(iND.eq.0) goto 1000
      p1c=((k(0)+Md)*pf1(3)-k(0)*pf1(0))/W
      p2c=((k(0)+Md)*pf2(3)-k(0)*pf2(0))/W
      p01=dsqrt(pf1(1)**2+pf1(2)**2+p1c**2)
      p02=dsqrt(pf2(1)**2+pf2(2)**2+p2c**2)
      call YY(Ldi,p1c/p01,(pf1(1)+um*pf1(2))/dsqrt(p01**2-p1c**2),YND1)
      call YY(Ldi,p2c/p02,(pf2(1)+um*pf2(2))/dsqrt(p02**2-p2c**2),YND2)
 
      wN1=dsqrt((q(0)+pf1(0))**2-(q(1)+pf1(1))**2
     . -(q(2)+pf1(2))**2-(q(3)+pf1(3))**2)
      wN2=dsqrt((q(0)+pf2(0))**2-(q(1)+pf2(1))**2
     . -(q(2)+pf2(2))**2-(q(3)+pf2(3))**2)
      aP1=(q(0)-(-pf2(1)*q(1)-pf2(2)*q(2)+(k(3)-pf2(3))*q(3))
     . /(wN1+q(0)+pf1(0)))/wN1
      aP2=(q(0)-(-pf1(1)*q(1)-pf1(2)*q(2)+(k(3)-pf1(3))*q(3))
     . /(wN2+q(0)+pf2(0)))/wN2

      do i=1,3
      qPN1(i)=q(i)-(k(i)-pf2(i))*aP1
      qPN2(i)=q(i)-(k(i)-pf1(i))*aP2
      end do
      q01=dsqrt(qPN1(1)**2+qPN1(2)**2+qPN1(3)**2)
      q02=dsqrt(qPN2(1)**2+qPN2(2)**2+qPN2(3)**2)

      call YY(1,qPN1(3)/q01
     . ,(qPN1(1)+um*qPN1(2))/dsqrt(q01**2-qPN1(3)**2),YD1)
      call YY(1,qPN2(3)/q02
     . ,(qPN2(1)+um*qPN2(2))/dsqrt(q02**2-qPN2(3)**2),YD2)

      GDl1=fpND**2/(4d0*pi)**2d0*MN/(wN1*Mpi**2)*q01**3
      GDl2=fpND**2/(4d0*pi)**2d0*MN/(wN2*Mpi**2)*q02**3

      do 101 il=-1,1,2
      do 101 id=-1,1
      imJ=id+il
      do 101 s=0,1
      do 101 imL=max(-Ldi,imJ-Sdi),min(Ldi,imJ+Sdi)
      imS=imJ-imL
      do 101 is=max(-s,imS-1),min(s,imS+1)
      im=imS-is
 101  TDi(il,id,s,is,1)=TDi(il,id,s,is,1)
     . +gM1/(2d0*Md)*il/dsqrt(2d0)*Flmb(W,MN,0d0)
     . /(W-Mdi+um/2d0*Gdi)
     . *fND/(2d0*MN**Ldi)*fpND/Mpi*(
     .  p01**Ldi*YND1(imL)*q02*YD2(im)/(wN2-MDl+um/2d0*GDl2)
     . -(-1)**s*p02**Ldi*YND2(imL)*q01*YD1(im)/(wN1-MDl+um/2d0*GDl1)
     . )*TCG(il,id,s,is,imL,im)
 1000 continue
c     -------------------------------------------

      do 112 il=-1,1,2
      do 112 id=-1,1
      do 112 s=0,1
      do 112 is=-s,s
      do 112 it=0,1
      TM(il,id,s,is)=TM(il,id,s,is)
     . +(TIA_1(il,id,s,is,it)-(-1)**(s+it)*TIA_2(il,id,s,is,it)) 
     . +TNN(il,id,s,is,it)                                        
     . +TPN_1(il,id,s,is,it)-(-1)**(s+it)*TPN_2(il,id,s,is,it)    
     . -TDi(il,id,s,is,it)
c      print *,'TM = ', TM(il,id,s,is)                                    
 112  continue

c     The  deuteron density matrix block ============
      rgr=3.141592653589/180.
      thx=(120.,0.)*rgr
      fhx=(30.,0.)*rgr
      pzx=(0.,0.)

      c1x=rand()

      if (c1x.ge.0.and.c1x.lt.1./2.) then
      pzzx=0.4
      rsig=1.
      else if (c1x.ge.1./2.and.c1x.lt.1.) then
      pzzx=-0.8
      rsig=2.
      end if
      call rhod(pzx,pzzx,thx,fhx,rho) 
 
c      do id=-1,1
c      do idd=-1,1
c      print*,id,'   ',idd,'  ',rho(id,idd)
c      end do
c      end do
c      pause
c     =============================================    

c      TKB=0d0
c      do id=-1,1
c      TKB=TKB+cdabs(TM(-1,id,0,0))**2+cdabs(TM(1,id,0,0))**2
c      end do
c      do id=-1,1
c      do is=-1,1
c      TKB=TKB+cdabs(TM(-1,id,1,is))**2+cdabs(TM(1,id,1,is))**2
c      end do
c      end do

      TKB=0d0
      do il=-1,1,2
      do id=-1,1
      do s=0,1
      do is=-s,s
      TKB=TKB+cdabs(TM(il,id,s,is))**2
      end do
      end do
      end do
      end do

      TKB=TKB/6d0


      TKR=0d0
      do il=-1,1,2
      do id=-1,1
      do idd=-1,1
      do s=0,1
      do is=-s,s
      TKR=TKR+TM(il,id,s,is)*rho(id,idd)*dconjg(TM(il,idd,s,is))
      end do
      end do
      end do
      end do
      end do

      TKR=TKR/2d0

c      print*,'TKB  TKR',TKB,'  ',TKR
c      pause
     

c      do s=0,1
c      do id=-1,1
c      do id1=-1,1 
c      sig(s,id1,id)=0d0
c      end do
c      end do
c      end do

c      do s=0,1
c      do id=-1,1
c      do id1=-1,1
c      do is=-s,s
c      sig(s,id1,id)=sig(s,id1,id)+dconjg(TM(1,id1,s,is))*TM(-1,id,s,is)
c      end do
c      end do
c      end do
c      end do

c      do s=0,1
c      do id=-1,1
c      do id1=-1,1  
c      tau(s,id1,id)=0d0
c      end do
c      end do
c      end do

c      do s=0,1
c      do id=-1,1
c      do id1=-1,1
c      do is=-s,s
c      tau(s,id1,id)=tau(s,id1,id)+dconjg(TM(1,id1,s,is))*TM(1,id,s,is)
c      end do
c      end do
c      end do
c      end do

c      print*,sig
c      print*,tau

c      do i=1,5
c      f(i)=0d0
c      end do

c      do s=0,1
c     ----------------------------- Sigma
c      f(1)=f(1)+(sig(s,1,1)+sig(s,-1,-1)+sig(s,0,0))/3d0
c     ----------------------------- T11
c      f(2)=f(2)-dsqrt(2d0/3d0)*dimag(tau(s,-1,0)+tau(s,0,1))
c     ----------------------------- T22
c      f(3)=f(3)+2d0/dsqrt(3d0)*dreal(tau(s,-1,1))
c     ----------------------------- T21
c      f(4)=f(4)+dsqrt(2d0/3d0)*dreal(tau(s,-1,0)-tau(s,0,1))
c     ----------------------------- T20
c      f(5)=f(5)+1d0/dsqrt(18d0)*dreal(tau(s,1,1)+tau(s,-1,-1)
c     . -2d0*tau(s,0,0))
c     ----------------------------- 
c      end do

c     print*,f
c     print*,tkb

      st20 = (0.,0.)
      st21 = (0.,0.)
      st22 = (0.,0.)
      do 130 id=-1,1
      do 130 idd=-1,1
      do 130 il=-1,1,2
      do 130 s=0,1
      do 130 is=-s,s
      st20=st20+TM(il,id,s,is)*T20(id,idd)*dconjg(TM(il,idd,s,is))
      st21=st21+TM(il,id,s,is)*T21(id,idd)*dconjg(TM(il,idd,s,is))
      st22=st22+TM(il,id,s,is)*T22(id,idd)*dconjg(TM(il,idd,s,is))
 130  continue

      tt20=st20/(TKB*6d0)
      tt21=st21/(TKB*6d0)
      tt22=st22/(TKB*6d0)

c      print*,'T20  T21  T22 ',st20/(TKB*6d0),st21/(TKB*6d0),
c     *                        st22/(TKB*6d0)

c      st20 = (0.,0.)
c      st21 = (0.,0.)
c      st22 = (0.,0.)
c      do 131 id=-1,1
c      do 131 idd=-1,1
c      do 131 il=-1,1,2
c      do 131 s=0,1
c      do 131 is=-s,s
c      st20=st20+TM(il,id,s,is)*T20(id,idd)*dconjg(TM(il,idd,s,is))*
c     *     (-1)**(id+idd) 
c      st21=st21+TM(il,id,s,is)*T21(id,idd)*dconjg(TM(il,idd,s,is))*
c     *     (-1)**(id+idd)
c      st22=st22+TM(il,id,s,is)*T22(id,idd)*dconjg(TM(il,idd,s,is))*
c     *     (-1)**(id+idd)
c 131  continue
c      print*,'T20  T21  T22 ',st20/(TKB*6d0),st21/(TKB*6d0),
c     *                        st22/(TKB*6d0)
c      pause

c      ================================================================
c            original kinematic factor
c
c      FK=1d0/(2d0*pi)**5*MN**2/(4d0*kl*pf1(0))*p1**2*p2**3/dabs(
c     . qP(0)*p2**2-pf2(0)*(pf2(1)*qP(1)+pf2(2)*qP(2)+pf2(3)*qP(3)))
c     =================================================================

c      FK=1d0/(2d0*pi)**5*MN**2/(4d0*k(0)*pf1(0))*p1**2*p2**3/dabs(
c     . qP(0)*p2**2-pf2(0)*(pf2(1)*qP(1)+pf2(2)*qP(2)+pf2(3)*qP(3)))

c
c        cross section
c
c      ds = FK*TKB/2.5681478d-9
c      print *,'FK =', FK

      return
      end

c -------------------------------------------------------------------

      subroutine NNW(x,y,T)
      implicit double precision (a-h,k,m,o-z)
      complex*16 um,T(-1:1,-1:1,0:1,-1:1,0:1),TNN,Ym(-2:2)
      common /pi/pi,/um/um,/TNN/TNN(-1:1,-1:1,-1:1,12)

      call ANN_1S0
      call ANN_3S1
      call ANN_3DS1

      call YY(1,x,dcos(y)+um*dsin(y),Ym)
      call ANN_1P1(Ym)
      call ANN_3P0(Ym)
      call ANN_3P1(Ym)
      call ANN_3P2(Ym)

      call YY(2,x,dcos(y)+um*dsin(y),Ym)
      call ANN_1D2(Ym)
      call ANN_3SD1(Ym)
      call ANN_3D1(Ym)
      call ANN_3D2(Ym)
      call ANN_3D3(Ym)

      do 1 il=-1,1,2
      do 1 id=-1,1
      do 1 it=0,1
      T(il,id,0,0,it)=4d0*pi*(
     . it*TNN(il,id,0,1) +(1-it)*TNN(il,id,0,2)
     . +it*TNN(il,id,0,5))
      do 1 is=-1,1
      T(il,id,1,is,it)=4d0*pi*(
     .  (1-it)*TNN(il,id,is,7)
     . +it*(TNN(il,id,is,3)+TNN(il,id,is,4)+TNN(il,id,is,8))
     . +(1-it)*(TNN(il,id,is,12)+TNN(il,id,is,11)
     . +TNN(il,id,is,10)+TNN(il,id,is,6)+TNN(il,id,is,9))
     . )
 1    continue

      return
      end

c -------------------------------------------------------------------

      subroutine gaussp(n,e,w)
      implicit double precision (a-h,o-z)
      dimension e(100),w(100)
      data pi,eps/3.141592653589793238462643d0,1.d-16/

      m=(n+1)/2
      dn=n
      do 1000 i=1,m
      di=i
      x=pi*(4.d0*(dn-di)+3.d0)/(4.d0*dn+2.d0)
      xn=(1.d0-(dn-1.d0)/(8.d0*dn*dn*dn))*dcos(x)
      if(i.gt.n/2) xn=0
      do 100  iter=1,10
      x=xn
      y1=1.d0
      y=x
      if(n.lt.2) goto 250
      do 200 j=2,n
      dj=j
      y2=y1
      y1=y
200   y=((2.d0*dj-1.d0)*x*y1-(dj-1.d0)*y2)/dj
250   continue
      ys=dn*(x*y-y1)/(x*x-1.d0)
      h=-y/ys
      xn=x+h
      if (dabs(h) .lt. eps) goto 110
100   continue
110   e(i)=x
      e(n-i+1)=-x
      gew=2.d0/((1.d0-x*x)*ys*ys)
      w(i)=gew
      w(n-i+1)=gew
1000  continue
      return
      end

c ----------------------------------------------------------------

      subroutine gaussp0inf (ngrid,grid,weight,scal)
      implicit double precision (a-h,o-z)
      dimension grid(ngrid),weight(ngrid)

      pi4=datan(1.d0)
      call gaussp(ngrid,grid,weight)
      do 1 i=1,ngrid
      x=pi4*(grid(i)+1.d0)
      grid(i)=scal*dtan(x)
    1 weight(i)=weight(i)*pi4*scal/dcos(x)**2
      return
      end

c -------------------------------------------------------------------

      subroutine CG
      implicit double precision (a-h,o-z)
      common /C111/C1(-1:1,-1:1),/C212/C2(-2:2,-2:2)

      C1(-1,-1)=-dsqrt(5d-1)
      C1(-1, 0)=-dsqrt(5d-1)
      C1(-1, 1)=0d0
      C1( 0,-1)=dsqrt(5d-1)
      C1( 0, 0)=0d0
      C1( 0, 1)=-dsqrt(5d-1)
      C1( 1,-1)=0d0
      C1( 1, 0)=dsqrt(5d-1)
      C1( 1, 1)=dsqrt(5d-1)

      C2(-2,-2)=-dsqrt(2d0/3d0)
      C2(-2,-1)=-dsqrt(1d0/3d0)
      C2(-2, 0)=0d0
      C2(-2, 1)=0d0
      C2(-2, 2)=0d0

      C2(-1,-2)=dsqrt(1d0/3d0)
      C2(-1,-1)=-dsqrt(1d0/6d0)
      C2(-1, 0)=-dsqrt(5d-1)
      C2(-1, 1)=0d0
      C2(-1, 2)=0d0

      C2( 0,-2)=0d0
      C2( 0,-1)=dsqrt(5d-1)
      C2( 0, 0)=0d0
      C2( 0, 1)=-dsqrt(5d-1)
      C2( 0, 2)=0d0

      C2( 1,-2)=0d0
      C2( 1,-1)=0d0
      C2( 1, 0)=dsqrt(5d-1)
      C2( 1, 1)=dsqrt(1d0/6d0)
      C2( 1, 2)=-dsqrt(1d0/3d0)

      C2( 2,-2)=0d0
      C2( 2,-1)=0d0
      C2( 2, 0)=0d0
      C2( 2, 1)=dsqrt(1d0/3d0)
      C2( 2, 2)=dsqrt(2d0/3d0)

      return
      end

c -------------------------------------------------------------------

      subroutine ANN_1D2(Y)
      implicit double precision (a-h,m,o-z)
      complex*16 A,Y(-2:2),T
      common /ANN/A(-1:1,-1:1,-1:1,-2:2,12)
     . ,/TNN/T(-1:1,-1:1,-1:1,12)

      N=5
      do 1 il=-1,1,2
      do 1 id=-1,1
      T(il,id,0,N)=0d0
      do 1 im=-2,2
 1    T(il,id,0,N)=T(il,id,0,N)+A(il,id,0,im,N)*Y(im)
      return
      end

c ----------------------------------------------------------------------

      subroutine ANN_3DS1
      implicit double precision (a-h,m,o-z)
      complex*16 A,T
      common /pi/pi
     .,/ANN/A(-1:1,-1:1,-1:1,-2:2,12),/TNN/T(-1:1,-1:1,-1:1,12)

      Y=1d0/dsqrt(4d0*pi)
      N=12
      do 1 il=-1,1,2
      do 1 id=-1,1
      T(il,id,-1,N)=(dsqrt(6d0)*A(il,id,1,-2,N)
     . -dsqrt(3d0)*A(il,id,0,-1,N)+A(il,id,-1,0,N))/dsqrt(10d0)*Y
      T(il,id,0,N)=(A(il,id,1,-1,N)-2d0/dsqrt(3d0)*A(il,id,0,0,N)
     . +A(il,id,-1,1,N))*dsqrt(3d-1)*Y
      T(il,id,1,N)=(dsqrt(6d0)*A(il,id,-1,2,N)
     . -dsqrt(3d0)*A(il,id,0,1,N)+A(il,id,1,0,N))/dsqrt(10d0)*Y
 1    continue

      return
      end

c ----------------------------------------------------------------------

      subroutine ANN_3SD1(Y)
      implicit double precision (a-h,m,o-z)
      complex*16 A,Y(-2:2),T
      common /ANN/A(-1:1,-1:1,-1:1,-2:2,12),/TNN/T(-1:1,-1:1,-1:1,12)

      N=11
      do 1 il=-1,1,2
      do 1 id=-1,1
      T(il,id,-1,N)=(dsqrt(6d0)*A(il,id,1,0,N)*Y(2)
     . +dsqrt(3d0)*A(il,id,0,0,N)*Y(1)
     . +A(il,id,-1,0,N)*Y(0))/dsqrt(10d0)
      T(il,id,0,N)=-(A(il,id,1,0,N)*Y(1)
     . +2d0/dsqrt(3d0)*A(il,id,0,0,N)*Y(0)
     . +A(il,id,-1,0,N)*Y(-1))*dsqrt(3d-1)
      T(il,id,1,N)=(dsqrt(6d0)*A(il,id,-1,0,N)*Y(-2)
     . +dsqrt(3d0)*A(il,id,0,0,N)*Y(-1)
     . +A(il,id,1,0,N)*Y(0))/dsqrt(10d0)
 1    continue
   
      return
      end

c ----------------------------------------------------------------------

      subroutine ANN_3P1(Y)
      implicit double precision (a-h,m,o-z)
      complex*16 A,Y(-2:2),T
      common /ANN/A(-1:1,-1:1,-1:1,-2:2,12),/TNN/T(-1:1,-1:1,-1:1,12)

      N=4
      do 1 il=-1,1,2
      do 1 id=-1,1
      T(il,id,-1,N)=((A(il,id,-1,0,N)-A(il,id,0,-1,N))*Y(0)
     . +(A(il,id,-1,1,N)-A(il,id,1,-1,N))*Y(1))/2d0
      T(il,id,0,N)=((A(il,id,0,-1,N)-A(il,id,-1,0,N))*Y(-1)
     . +(A(il,id,0,1,N)-A(il,id,1,0,N))*Y(1))/2d0
      T(il,id,1,N)=((A(il,id,1,0,N)-A(il,id,0,1,N))*Y(0)
     . +(A(il,id,1,-1,N)-A(il,id,-1,1,N))*Y(-1))/2d0
 1    continue
    
      return
      end

c ----------------------------------------------------------------------

      subroutine ANN_3D1(Y)
      implicit double precision (a-h,m,o-z)
      complex*16 A1,A2,A3,A,Y(-2:2),T
      common /ANN/A(-1:1,-1:1,-1:1,-2:2,12),/TNN/T(-1:1,-1:1,-1:1,12)

      N=10
      do 1 il=-1,1,2
      do 1 id=-1,1
      A1=dsqrt(6d0)*A(il,id,-1,2,N)
     . -dsqrt(3d0)*A(il,id,0,1,N)+A(il,id,1,0,N)
      A2=A(il,id,-1,1,N)
     . -2d0/dsqrt(3d0)*A(il,id,0,0,N)+A(il,id,1,-1,N)
      A3=dsqrt(6d0)*A(il,id,1,-2,N)
     . -dsqrt(3d0)*A(il,id,0,-1,N)+A(il,id,-1,0,N)

      T(il,id,-1,N)=
     . dsqrt(1.5d0)/5d0*Y(2)*A1+3d-1*Y(1)*A2+1d-1*Y(0)*A3
      T(il,id,0,N)=
     . -dsqrt(3d0)/10d0*(Y(1)*A1+2d0*Y(0)*A2+Y(-1)*A3)
      T(il,id,1,N)=
     . 1d-1*Y(0)*A1+3d-1*Y(-1)*A2+dsqrt(1.5d0)/5d0*Y(-2)*A3
 1    continue

      return
      end

c ----------------------------------------------------------------------

      subroutine ANN_3D2(Y)
      implicit double precision (a-h,m,o-z)
      complex*16 A,Y(-2:2),T
      common /ANN/A(-1:1,-1:1,-1:1,-2:2,12),/TNN/T(-1:1,-1:1,-1:1,12)

      N=6
      do 1 il=-1,1,2
      do 1 id=-1,1
      T(il,id,-1,N)=
     . (dsqrt(2d0)*A(il,id,-1,2,N)+A(il,id,0,1,N)
     .-dsqrt(3d0)*A(il,id,1,0,N))*Y(2)/(3d0*dsqrt(2d0))
     .+(A(il,id,-1,1,N)-A(il,id,1,-1,N))*Y(1)/2d0
     .+(dsqrt(3d0)*A(il,id,-1,0,N)-A(il,id,0,-1,N)
     .-dsqrt(2d0)*A(il,id,1,-2,N))*Y(0)/(2d0*dsqrt(3d0))
     .+(A(il,id,-1,-1,N)-dsqrt(2d0)*A(il,id,0,-2,N))*Y(-1)/3d0
      T(il,id,0,N)=
     .+(dsqrt(2d0)*A(il,id,0,2,N)-A(il,id,1,1,N))
     .*Y(2)*dsqrt(2d0)/3d0
     .+(dsqrt(2d0)*A(il,id,-1,2,N)+A(il,id,0,1,N)
     .-dsqrt(3d0)*A(il,id,1,0,N))*Y(1)/6d0
     .-(dsqrt(3d0)*A(il,id,-1,0,N)-A(il,id,0,-1,N)
     .-dsqrt(2d0)*A(il,id,1,-2,N))*Y(-1)/6d0
     .-(A(il,id,-1,-1,N)-dsqrt(2d0)*A(il,id,0,-2,N))
     .*Y(-2)*dsqrt(2d0)/3d0
      T(il,id,1,N)=
     .-(dsqrt(2d0)*A(il,id,0,2,N)-A(il,id,1,1,N))*Y(1)/3d0
     .-(dsqrt(2d0)*A(il,id,-1,2,N)+A(il,id,0,1,N)
     .-dsqrt(3d0)*A(il,id,1,0,N))*Y(0)/(2d0*dsqrt(3d0))
     .-(A(il,id,-1,1,N)-A(il,id,1,-1,N))*Y(-1)/2d0
     .-(dsqrt(3d0)*A(il,id,-1,0,N)-A(il,id,0,-1,N)
     .-dsqrt(2d0)*A(il,id,1,-2,N))*Y(-2)/(3d0*dsqrt(2d0))
 1    continue

      return
      end

c ----------------------------------------------------------------------

      subroutine ANN_3D3(Y)
      implicit double precision (a-h,m,o-z)
      complex*16 A,Y(-2:2),T
      common /ANN/A(-1:1,-1:1,-1:1,-2:2,12),/TNN/T(-1:1,-1:1,-1:1,12)

      N=9
      do 1 il=-1,1,2
      do 1 id=-1,1
      T(il,id,-1,N)=
     . (A(il,id,-1,2,N)+dsqrt(8d0)*A(il,id,0,1,N)
     . +dsqrt(6d0)*A(il,id,1,0,N))*Y(2)/15d0
     . +(A(il,id,-1,1,N)+dsqrt(3d0)*A(il,id,0,0,N)
     . +A(il,id,1,-1,N))*Y(1)/5d0+(dsqrt(6d0)*A(il,id,-1,0,N)
     . +dsqrt(8d0)*A(il,id,0,-1,N)+A(il,id,1,-2,N))
     . *Y(0)*dsqrt(6d0)/15d0
     . +(dsqrt(2d0)*A(il,id,-1,-1,N)+A(il,id,0,-2,N))
     . *Y(-1)*dsqrt(2d0)/3d0+A(il,id,-1,-2,N)*Y(-2)
      T(il,id,0,N)=
     . (A(il,id,0,2,N)+dsqrt(2d0)*A(il,id,1,1,N))*Y(2)/3d0
     . +(A(il,id,-1,2,N)+dsqrt(8d0)*A(il,id,0,1,N)
     . +dsqrt(6d0)*A(il,id,1,0,N))*Y(1)*dsqrt(8d0)/15d0
     . +(A(il,id,-1,1,N)+dsqrt(3d0)*A(il,id,0,0,N)
     . +A(il,id,1,-1,N))*Y(0)*dsqrt(3d0)/5d0
     . +(dsqrt(6d0)*A(il,id,-1,0,N)+dsqrt(8d0)*A(il,id,0,-1,N)
     . +A(il,id,1,-2,N))*Y(-1)*dsqrt(8d0)/15d0
     . +(dsqrt(2d0)*A(il,id,-1,-1,N)+A(il,id,0,-2,N))*Y(-2)/3d0
      T(il,id,1,N)=
     . A(il,id,1,2,N)*Y(2)+(dsqrt(2d0)*A(il,id,1,1,N)+A(il,id,0,2,N))
     . *Y(1)*dsqrt(2d0)/3d0+(dsqrt(6d0)*A(il,id,1,0,N)
     . +dsqrt(8d0)*A(il,id,0,1,N)+A(il,id,-1,2,N))
     . *Y(0)*dsqrt(6d0)/15d0
     . +(A(il,id,-1,1,N)+dsqrt(3d0)*A(il,id,0,0,N)
     . +A(il,id,1,-1,N))*Y(-1)/5d0+(dsqrt(6d0)*A(il,id,-1,0,N)
     . +dsqrt(8d0)*A(il,id,0,-1,N)+A(il,id,1,-2,N))*Y(-2)/15d0
 1    continue

      return
      end

c ----------------------------------------------------------------------

      subroutine ANN_3P0(Y)
      implicit double precision (a-h,m,o-z)
      complex*16 A,Y(-2:2),T,AN
      common /ANN/A(-1:1,-1:1,-1:1,-2:2,12),/TNN/T(-1:1,-1:1,-1:1,12)

      N=3 
      do 1 il=-1,1,2
      do 1 id=-1,1
      AN=-A(il,id,1,-1,N)+A(il,id,0,0,N)-A(il,id,-1,1,N)
      do 3 is=-1,1
 3    T(il,id,is,N)=(-1)**is/3d0*AN*Y(-is)
 1    continue

      return
      end

c ----------------------------------------------------------------------

      subroutine ANN_1P1(Y)
      implicit double precision (a-h,m,o-z)
      complex*16 A,Y(-2:2),T
      common /ANN/A(-1:1,-1:1,-1:1,-2:2,12),/TNN/T(-1:1,-1:1,-1:1,12)

      N=2
      do 1 il=-1,1,2
      do 1 id=-1,1
      T(il,id,0,N)=0d0
      do 1 im=-1,1
 1    T(il,id,0,N)=T(il,id,0,N)+A(il,id,0,im,N)*Y(im)
      return
      end

c ----------------------------------------------------------------------

      subroutine ANN_3P2(Y)
      implicit double precision (a-h,m,o-z)
      complex*16 A,Y(-2:2),T
      common /ANN/A(-1:1,-1:1,-1:1,-2:2,12),/TNN/T(-1:1,-1:1,-1:1,12)

      N=8
      do 1 il=-1,1,2
      do 1 id=-1,1
      T(il,id,-1,N)=((A(il,id,-1,1,N)
     .+A(il,id,1,-1,N)+2d0*A(il,id,0,0,N))*Y(1)
     .+3d0*(A(il,id,-1,0,N)+A(il,id,0,-1,N))*Y(0)
     .+6d0*A(il,id,-1,-1,N)*Y(-1))/6d0

      T(il,id,0,N)=(3d0*(A(il,id,0,1,N)+A(il,id,1,0,N))*Y(1)
     .+2d0*(A(il,id,-1,1,N)+A(il,id,1,-1,N)+2d0*A(il,id,0,0,N))*Y(0)
     .+3d0*(A(il,id,0,-1,N)+A(il,id,-1,0,N))*Y(-1))/6d0

      T(il,id,1,N)=((A(il,id,1,-1,N)+A(il,id,-1,1,N)
     .+2d0*A(il,id,0,0,N))*Y(-1)+3d0*(A(il,id,1,0,N)
     .+A(il,id,0,1,N))*Y(0)+6d0*A(il,id,1,1,N)*Y(1))/6d0
 1    continue

      return
      end

c ----------------------------------------------------------------------

      subroutine ANN_1S0
      implicit double precision (a-h,m,o-z)
      complex*16 A,T
      common /pi/pi
     . ,/ANN/A(-1:1,-1:1,-1:1,-2:2,12),/TNN/T(-1:1,-1:1,-1:1,12)

      N=1
      do 1 il=-1,1,2
      do 1 id=-1,1
 1    T(il,id,0,N)=A(il,id,0,0,N)/dsqrt(4d0*pi)
      return
      end

c ----------------------------------------------------------------------

      subroutine ANN_3S1
      implicit double precision (a-h,m,o-z)
      complex*16 A,T
      common /pi/pi
     . ,/ANN/A(-1:1,-1:1,-1:1,-2:2,12),/TNN/T(-1:1,-1:1,-1:1,12)

      N=7
      do 1 il=-1,1,2
      do 1 id=-1,1
      do 1 is=-1,1
 1    T(il,id,is,N)=A(il,id,is,0,N)/dsqrt(4d0*pi)
      return
      end

c ----------------------------------------------------------------------

      subroutine AInv(N,B,AI)
      implicit double precision (a-h,m,o-z)
      complex*16 A(4,10),B(4,4),AI(4,4),R,S

      do 10 i=1,N
      do 10 j=1,N
 10   A(i,j)=B(i,j)

      do 2 i=1,N
      do 1 j=N+1,2*N
 1    A(i,j)=0d0
 2    A(i,i+N)=1d0
      do 27 il=1,N
      S=A(il,il)
      j=il
      if(il.eq.N) goto 22
      do 21 i=il+1,N
      R=A(i,il)
      if(cdabs(R).le.cdabs(S)) goto 21
      S=R
      j=i
 21   continue
 22   if(cdabs(S).eq.0d0) stop 'det=0'
      if(j.eq.il) goto 24
      do 23 i=il,2*N
      R=A(il,i)
      A(il,i)=A(j,i)
 23   A(j,i)=R
 24   do 25 j=il+1,2*N
 25   A(il,j)=A(il,j)/S
      if(il.eq.N) goto 27
      do 26 i=il+1,N
      R=A(i,il)
      do 26 j=il+1,2*N
 26   A(i,j)=A(i,j)-A(il,j)*R
 27   continue
      do 29 j=N+1,2*N
      do 29 i=N-1,1,-1
      R=A(i,j)
      do 28 il=i+1,N
 28   R=R-A(il,j)*A(i,il)
 29   A(i,j)=R

c ..................................................
      do 30 i=1,N
      do 30 j=1,N
 30   AI(i,j)=A(i,j+N)

      return
      end

c --------------------------------------------------------
      
      subroutine Ff_NN(q,F)
      implicit double precision (a-h,o-z)
      dimension F(12,4)
      common /cbNN/c(12,4,4),b(12,4,4),/NLN/L(12),/NFN/nL(12)
     .,/NdN/Nd(12)

      do 1 nN=1,12
      do 1 i=1,Nd(nN)
      F(nN,i)=0d0 
      do 1 n=1,4
 1    F(nN,i)=F(nN,i)+c(nN,i,n)*q**(L(nN)+2*(n-1))
     . /(q**2+b(nN,i,n)**2)**(L(nN)+n-nL(nN))
      return
      end 

c --------------------------------------------------------

      subroutine Ff_de(q,Fg1,Fg2)
      implicit double precision (a-h,o-z)
      common /cbNN/c(12,4,4),b(12,4,4)

      Fg1=0d0
      Fg2=0d0
      do 1 n=1,4
      Fg1=Fg1+c(7,1,n)*q**(2*(n-1))/(q**2+b(7,1,n)**2)**n
 1    Fg2=Fg2+c(10,1,n)*q**(2*n)/(q**2+b(10,1,n)**2)**(1+n)
      return
      end 

c ----------------------------------------------------------------------
 
      subroutine Gdr_L(z,Gd)
      implicit double precision (a-h,m,o-z)
      dimension F0(12,4),F(12,4)
      complex*16 um,AA(4,4),A(9,4,4),As(12,4,4),D(4,4),Gd(9,4,4)
      common /pi/pi,/um/um,/MN/MN,/q/q(100),/wg/wg(100),/Nq/Nq
     . ,/gNN/g(12,4,4),/NdN/Nd(12)

      do 1 nN=1,9
      do 1 ni=1,Nd(nN)
      do 1 nj=1,Nd(nN)
 1    A(nN,ni,nj)=0d0

      q0=dsqrt(MN*z)
      call Ff_NN(q0,F0)
      do 3 nN=1,12
      do 3 ni=1,Nd(nN)
      do 3 nj=1,Nd(nN)
 3    As(nN,ni,nj)=-um*q0*MN/(4d0*pi)*F0(nN,ni)*F0(nN,nj)

      do 4 k=1,Nq
      call Ff_NN(q(k),F)
      do 4 nN=1,12
      do 4 ni=1,Nd(nN)
      do 4 nj=1,Nd(nN)
 4    As(nN,ni,nj)=As(nN,ni,nj)
     . -MN*(q0**2*F0(nN,ni)*F0(nN,nj)-q(k)**2*F(nN,ni)*F(nN,nj))
     . /(q0**2-q(k)**2)*wg(k)/(2d0*pi**2)

      do 8 nN=1,6
      do 8 ni=1,Nd(nN)
      do 8 nj=1,Nd(nN)
      do 8 nk=1,Nd(nN)
 8    A(nN,ni,nj)=A(nN,ni,nj)-g(nN,ni,nk)*As(nN,nk,nj)
      do 9 nN=7,9
      do 9 ni=1,Nd(nN)
      do 9 nj=1,Nd(nN)
      do 9 nk=1,Nd(nN)
 9    A(nN,ni,nj)=A(nN,ni,nj)-g(nN,ni,nk)
     .                    *(As(nN,nk,nj)+As(nN+3,nk,nj))
      do 10 nN=1,9
      do 10 ni=1,Nd(nN)
 10   A(nN,ni,ni)=1d0+A(nN,ni,ni)
      do 11 nN=1,9
      do 12 ni=1,Nd(nN)
      do 12 nj=1,Nd(nN)
 12   AA(ni,nj)=A(nN,ni,nj)
c     --------------------
      call AInv(Nd(nN),AA,D)
c     --------------------
      do 13 ni=1,Nd(nN)
      do 13 nj=1,Nd(nN)
      Gd(nN,ni,nj)=0d0
      do 13 nk=1,Nd(nN)
 13   Gd(nN,ni,nj)=Gd(nN,ni,nj)+D(ni,nk)*g(nN,nk,nj)
 11   continue

      return
      end

c ----------------------------------------------------------------------

      subroutine YY(L,x,ePh,Y)
      implicit double precision (a-h,o-z)
      complex*16 Y(-2:2),ePh
      common /pi/pi

      sinx=dsqrt(dabs(1d0-x**2))
      goto (1,2,3) L+1
 1    Y(0)=1d0/dsqrt(4d0*pi)
      return
 2    a=dsqrt(3d0/(8d0*pi))
      Y(-1)=a*sinx*dconjg(ePh)
      Y(0)=dsqrt(2d0)*a*x
      Y(1)=-a*sinx*ePh
      return 
 3    a=dsqrt(15d0/(2d0*pi))
      Y(-2)=a/4d0*sinx**2*dconjg(ePh**2)
      Y(-1)=a/2d0*sinx*x*dconjg(ePh)
      Y(0)=a/dsqrt(24d0)*(3d0*x**2-1d0)
      Y(1)=-a/2d0*sinx*x*ePh
      Y(2)=a/4d0*sinx**2*ePh**2

      return
      end

c ----------------------------------------------------------------------

      subroutine StNN(jm,q0,A)
      implicit double precision (a-h,m,k,o-z)
      integer s 
      dimension g0(12,4),gq(12,4)
      complex*16 um,f(-1:1,-1:1,0:1,-1:1,0:1)
     .,v(-1:1,-1:1,0:1,-1:1,0:2,-2:2),u(-1:1,-1:1,0:1,-1:1,0:2,-2:2)
     .,A(-1:1,-1:1,-1:1,-2:2,10,4),Ym(-2:2),wxy

      common /pi/pi,/um/um,/q1/q(100),/wg1/wg(100),/Nq1/Nq
     .,/x/x(30),wx(30),/Nx/Nx,/y/y(30),wy(30),/Ny/Ny
     .,/qi/qi,/MN/MN,/NLA/nL(12),/NdN/Nd(12),/NsN/nS(12)

      q0KB=q0**2
      FK=2d0*MN/(2d0*pi)**3
      
      qi=q0
c     -------------
      do 110 il=-1,1,2
      do 110 L=0,2
      do 110 im=-L,L
      do 110 id=-1,1
      do 110 s=0,1
      do 110 is=-s,s
 110  v(il,id,s,is,L,im)=0d0
c     -------------
      do 20 ix=1,Nx
      do 20 iy=1,Ny
c     ----------------------------------
      call FiNN(jm,x(ix),y(iy),f)
c     ----------------------------------
      do 20 L=0,2
      call YY(L,x(ix),cdexp(um*y(iy)),Ym)
      do 20 im=-L,L
      wxy=wx(ix)*wy(iy)*dconjg(Ym(im))
      do 20 il=-1,1,2
      do 20 id=-1,1
      do 20 s=0,1 
      do 20 is=-s,s 
      v(il,id,s,is,L,im)=v(il,id,s,is,L,im)+
     .                f(il,id,s,is,((-1)**(L+s)+1)/2)*wxy
 20   continue

      call Ff_NN(q0,g0)
      do 22 nN=1,10
      do 22 il=-1,1,2
      do 22 id=-1,1
      do 22 im=-nL(nN),nL(nN)
      do 22 is=-nS(nN),nS(nN)
      do 22 nj=1,Nd(nN)
 22   A(il,id,is,im,nN,nj)=
     .  -um*q0/2d0*pi*v(il,id,nS(nN),is,nL(nN),im)*g0(nN,nj)*FK

c     ------------------------

      do i=1,Nq
      qi=q(i)
      qKB=qi**2
c     -------------
      do 210 il=-1,1,2
      do 210 L=0,2
      do 210 im=-L,L
      do 210 id=-1,1
      do 210 s=0,1
      do 210 is=-s,s
 210  u(il,id,s,is,L,im)=0d0
c     -------------

      do 40 ix=1,Nx
      do 40 iy=1,Ny
c     --------------------------------
      call FiNN(jm,x(ix),y(iy),f)
c     --------------------------------
      do 40 L=0,2
      call YY(L,x(ix),cdexp(um*y(iy)),Ym)
      do 40 im=-L,L
      wxy=wx(ix)*wy(iy)*dconjg(Ym(im))
      do 40 il=-1,1,2
      do 40 id=-1,1
      do 40 s=0,1 
      do 40 is=-s,s 
      u(il,id,s,is,L,im)=u(il,id,s,is,L,im)
     .               +f(il,id,s,is,((-1)**(L+s)+1)/2)*wxy
 40   continue
c     --------------------------------
      call Ff_NN(qi,gq)
      do 42 nN=1,10
      do 42 il=-1,1,2
      do 42 id=-1,1
      do 42 im=-nL(nN),nL(nN)
      do 42 is=-nS(nN),nS(nN)
      do 42 nj=1,Nd(nN)
 42   A(il,id,is,im,nN,nj)=A(il,id,is,im,nN,nj)
     . -(q0KB*v(il,id,nS(nN),is,nL(nN),im)*g0(nN,nj)
     . -qKB*u(il,id,nS(nN),is,nL(nN),im)*gq(nN,nj))/(q0KB-qKB)*wg(i)*FK
      end do

      return
      end       

c -----------------------------------------------------------------------

      subroutine FiNN(jm,x,y,f)
      implicit double precision (a-h,k,m,o-z)
      dimension q(3),ps(0:3),pf(0:3)
      complex*16 f(-1:1,-1:1,0:1,-1:1,0:1)
      common /pi/ppi,/MN/MN,/Mpi/Mpi,/qi/qi,/qP/qP(0:3),/k/k(0:3)
     . ,/W/W,/Md/Md

      q(1)=qi*dsqrt(dabs(1d0-x**2))*dcos(y)
      q(2)=qi*dsqrt(dabs(1d0-x**2))*dsin(y)
      q(3)=qi*x
      do i=1,3
      ps(i)=(k(i)-qP(i))/2d0-q(i)
      pf(i)=(k(i)-qP(i))/2d0+q(i)
      end do
c     ps(0)=dsqrt(MN**2+ps(1)**2+ps(2)**2+ps(3)**2)
c     pf(0)=k(0)+Md-ps(0)-qP(0)
      pf(0)=dsqrt(MN**2+pf(1)**2+pf(2)**2+pf(3)**2)
      ps(0)=k(0)+Md-pf(0)-qP(0)

      if(pf(0).lt.MN)pf(0)=MN
c     ------------------------------
      call KIN_IA(qP,pf,ps)
      call IA(jm,f)
c     ------------------------------
      return
      end       

c -------------------------------------------------------------------

      subroutine Crush(W,q,pf,KN,LN)
      implicit double precision (a-h,k,m,n,o-z)
      complex*16 um,KN(-1:1,-1:1),LN(-1:1,-1:1,-1:1),U(4),L(3),A(4)
     . ,F1(-1:1),F2(-1:1),F3(-1:1),F4(-1:1),e(3),epi,epf
      dimension pi(0:3),pf(0:3),q(0:3),kc(3),qc(3),esp(-1:1)
      common /MN/MN,/Mpi/Mpi,/um/um,/k/k(0:3)

      do i=0,3
      pi(i)=q(i)+pf(i)-k(i)
      end do 
c ..............................................

      Ei=pi(0)+MN
      Ef=pf(0)+MN
      pipf=pi(1)*pf(1)+pi(2)*pf(2)+pi(3)*pf(3)
      qpi=q(1)*pi(1)+q(2)*pi(2)+q(3)*pi(3)
      kpi=k(3)*pi(3)
      kpf=k(3)*pf(3)
      kpi4=k(0)*pi(0)-kpi
      kpf4=k(0)*pf(0)-kpf
      Ni=dsqrt(Ei/(2d0*MN))
      Nf=dsqrt(Ef/(2d0*MN))

c     ---------------------------------- c.m. Wariablen

      aG=(k(0)**2+kpi)/(w*(w+k(0)+pi(0)))-k(0)/w      
      aP=(k(3)*q(3)+qpi)/(w*(w+k(0)+pi(0)))-q(0)/w
      do i=1,3
      kc(i)=k(i)+aG*(k(i)+pi(i))
      qc(i)=q(i)+aP*(k(i)+pi(i))
      end do
      ka=dsqrt(kc(1)**2+kc(2)**2+kc(3)**2)
      qa=dsqrt(qc(1)**2+qc(2)**2+qc(3)**2)
      qk=ka*qa
      xc=(kc(1)*qc(1)+kc(2)*qc(2)+kc(3)*qc(3))/qk 
      ts=Mpi**2-2d0*ka*(dsqrt(qa**2+Mpi**2)-qa*xc)

      Eic=dsqrt(ka**2+MN**2)+MN
      Efc=dsqrt(qa**2+MN**2)+MN
      Nic=dsqrt(Eic/(2d0*MN))
      Nfc=dsqrt(Efc/(2d0*MN))
      Hc=1d0/(2d0*w*(w-MN)*Nic*Nfc)
      qa=dsqrt((W**2-(Mpi+MN)**2)*(W**2-(Mpi-MN)**2))/(2d0*W)

c     ----------------------------------
      call CGLN(w,xc,F1,F2,F3,F4)
c     ----------------------------------
      do 1 il=-1,1,2
      do jl=-1,1,2
      esp(jl)=0d0
      end do
      esp(-il)=-1d0

      e(1)=-(esp(1)-esp(-1))/dsqrt(2d0)
      e(2)=um*(esp(1)+esp(-1))/dsqrt(2d0)
      e(3)=0d0

      epi=e(1)*pi(1)+e(2)*pi(2)
      epf=e(1)*pf(1)+e(2)*pf(2)

      do jt=-1,1
      A(1)=(-F1(jt)*(w-MN)+F2(jt)*Efc/qa*(w+MN))*Hc 
      A(2)=(F3(jt)-F4(jt)*Efc/qa)*Hc/qa
      A(3)=(2d0*F1(jt)+2d0*Efc/qa*F2(jt)
     .    -(F3(jt)/(w-MN)+Efc/qa*F4(jt)/(w+MN))
     .    *(MN**2+Mpi**2-ts-w**2)/qa)*Hc
      A(4)=(-F3(jt)*(w+MN)-F4(jt)*Efc/qa*(w-MN))*Hc/qa    

      KN(il,jt)=-Ni*Nf*(A(1)*(k(3)*(e(1)*pf(2)-e(2)*pf(1))/Ef-
     .  k(3)*(e(1)*pi(2)-e(2)*pi(1))/Ei)+
     .  ((A(3)*epi+A(4)*epf)*k(3)*(pi(1)*pf(2)-pi(2)*pf(1))+
     .  (A(1)*k(0)+A(3)*kpi4+A(4)*kpf4)*(e(1)*(pi(2)*pf(3)-pi(3)*
     .  pf(2))-e(2)*(pi(1)*pf(3)-pi(3)*pf(1))+
     .  e(3)*(pi(1)*pf(2)-pi(2)*pf(1))))/(Ei*Ef))

      U(1)=A(1)*(epi/Ei+epf/Ef)-(A(3)*epi+
     .     A(4)*epf)*(1d0-pipf/(Ei*Ef))
      U(2)=A(1)*(k(0)*(1d0+pipf/(Ei*Ef))-kpi/Ei-kpf/Ef)
     .    -(A(3)*kpi4+A(4)*kpf4)*(1d0-pipf/(Ei*Ef))
      U(3)=-A(1)*k(0)*epf/(Ei*Ef)-A(2)*2d0/Ei*(epf*kpi4-epi*kpf4)
     .    -(A(3)*epi+A(4)*epf)*(kpf/(Ef*Ei)-k(0)/Ei)
     .    -(A(3)*kpi4+A(4)*kpf4)*epf/(Ei*Ef)
      U(4)=-A(1)*k(0)*epi/(Ei*Ef)+A(2)*2d0/Ef*(epf*kpi4-epi*kpf4)+
     .   (k(0)/Ef-kpi/(Ef*Ei))*(A(3)*epi+A(4)*epf)-epi/(Ei*Ef)*
     .   (A(3)*kpi4+A(4)*kpf4)

      do i=1,3
      L(i)=-um*Ni*Nf*(k(i)*U(1)+e(i)*U(2)+pi(i)*U(3)+pf(i)*U(4)) 
      end do

      LN(il,jt, 1)=-(L(1)+um*L(2))/dsqrt(2d0) 
      LN(il,jt, 0)=L(3)
      LN(il,jt,-1)=(L(1)-um*L(2))/dsqrt(2d0)
      end do
 1    continue

      return 
      end

c -------------------------------------------------------------------

      subroutine CGLN(wx,x,F1,F2,F3,F4)
      implicit double precision (a-h,m,o-z)
      complex*16 Ax(-1:1,20),F1(-1:1),F2(-1:1),F3(-1:1),F4(-1:1)
      common /pi/pi,/MN/MN

      P2=3d0*x
      P2d=3d0
      P3=(15d0*x**2-3d0)/2d0
      P3d=15d0*x
      P4=(35d0*x**3-15d0*x)/2d0
      P4d=(105d0*x**2-15d0)/2d0
      P5=(315d0*x**4-210d0*x**2+15d0)/8d0
      P5d=(315d0*x**3-105d0*x)/2d0
      P6=(693d0*x**5-630d0*x**3+105d0*x)/8d0
      P6d=(3465d0*x**4-1890d0*x**2+105d0)/8d0
      Hc=4d0*pi*wx/MN/(1d3*139.57d0)
c     ---------------------------
      call Newt(wx,Ax)
c     ---------------------------

      do jt=-1,1
      F1(jt)=Ax(jt,1)+3d0*Ax(jt,8)+Ax(jt,7)
     . +(Ax(jt,3)+Ax(jt,2)+4d0*Ax(jt,12)+Ax(jt,11))*P2
     . +(2d0*Ax(jt,6)+Ax(jt,5)+5d0*Ax(jt,16)+Ax(jt,15))*P3
     . +(3d0*Ax(jt,10)+Ax(jt,9)+6d0*Ax(jt,20)+Ax(jt,19))*P4
     . +(4d0*Ax(jt,14)+Ax(jt,13))*P5+(5d0*Ax(jt,18)+Ax(jt,17))*P6
      F2(jt)=2d0*Ax(jt,3)+Ax(jt,4)+(3d0*Ax(jt,6)+2d0*Ax(jt,8))*P2
     . +(4d0*Ax(jt,10)+3d0*Ax(jt,12))*P3+(5d0*Ax(jt,14)
     . +4d0*Ax(jt,16))*P4+(6d0*Ax(jt,18)+5d0*Ax(jt,20))*P3
      F3(jt)=(Ax(jt,2)-Ax(jt,3)+Ax(jt,11)+Ax(jt,12))*P2d
     . +(Ax(jt,5)-Ax(jt,6)+Ax(jt,15)+Ax(jt,16))*P3d
     . +(Ax(jt,9)-Ax(jt,10)+Ax(jt,19)+Ax(jt,20))*P4d
     . +(Ax(jt,13)-Ax(jt,14))*P5d+(Ax(jt,17)-Ax(jt,18))*P6d
      F4(jt)=(Ax(jt,6)-Ax(jt,5)-Ax(jt,8)-Ax(jt,7))*P2d
     . +(Ax(jt,10)-Ax(jt,9)-Ax(jt,12)-Ax(jt,11))*P3d
     . +(Ax(jt,14)-Ax(jt,13)-Ax(jt,16)-Ax(jt,15))*P4d
     . +(Ax(jt,18)-Ax(jt,17)-Ax(jt,20)-Ax(jt,19))*P5d

      F1(jt)=F1(jt)*Hc            
      F2(jt)=F2(jt)*Hc
      F3(jt)=F3(jt)*Hc
      F4(jt)=F4(jt)*Hc
      end do 

      return
      end

c ------------------------------------------------------------------------

      subroutine Newt(wx,Ax)
      implicit double precision (a-h,m,o-z)
      complex*16 A(20,-1:1,100),Ax(-1:1,20)
      common /Am/A,/wN/w(100),/Nm/N,/MN/MN,/Mpi/Mpi

      do 2 jt=-1,1
      Ax(jt,1)=A(1,jt,1)
      do 2 l=2,20
 2    Ax(jt,l)=0d0 

c     if(wx.lt.MN+Mpi) return

      if(wx.lt.1080d0) return

      do ii=1,N
      if(wx.lt.w(ii))then
      i=ii
      if(i.eq.N)i=ii-1
      if(i.eq.2)i=ii+1
      wx0=wx-w(i-2)
      wx1=wx-w(i-1)
      wx2=wx-w(i)
      wx3=wx-w(i+1)

      do 1 jt=-1,1
      do 1 l=1,20
 1    Ax(jt,l)=A(l,jt,i-2)*wx1*wx2*wx3/(w(i-2)-w(i-1))/(w(i-2)-w(i))
     . /(w(i-2)-w(i+1))+A(l,jt,i-1)*wx0*wx2*wx3/(w(i-1)-w(i-2))
     . /(w(i-1)-w(i))/(w(i-1)-w(i+1))+A(l,jt,i)*wx0*wx1*wx3
     . /(w(i)-w(i-2))/(w(i)-w(i-1))/(w(i)-w(i+1))+A(l,jt,i+1)
     . *wx0*wx1*wx2/(w(i+1)-w(i-2))/(w(i+1)-w(i-1))/(w(i+1)-w(i))
      return
      endif

      enddo
      end

c ----------------------------------------------------------------------

      subroutine StEN(q0)
      implicit double precision (a-h,m,k,o-z)
      dimension g0(2),gq(2),gP(10,2)
      complex*16 um,GpN,Gl
     .,v00(-1:1,-1:1,0:2,-2:2),v10(-1:1,-1:1,-1:1,0:2,-2:2)
     .,v01(-1:1,-1:1,-1:1,0:2,-2:2),v11(-1:1,-1:1,-1:1,-1:1,0:2,-2:2)
     .,u00(-1:1,-1:1,0:2,-2:2),u10(-1:1,-1:1,-1:1,0:2,-2:2)
     .,u01(-1:1,-1:1,-1:1,0:2,-2:2),u11(-1:1,-1:1,-1:1,-1:1,0:2,-2:2)
     .,B00(-1:1,-1:1,-2:2,10,2),B10(-1:1,-1:1,-1:1,-2:2,10,2)
     .,B01(-1:1,-1:1,-1:1,-2:2,10,2),B11(-1:1,-1:1,-1:1,-1:1,-2:2,10,2)
     .,Ym(-2:2),wxy,A00,A10,A01,A11
     .,f00(-1:1,-1:1),f10(-1:1,-1:1,-1:1)
     .,f01(-1:1,-1:1,-1:1),f11(-1:1,-1:1,-1:1,-1:1)

      common /MN/MN,/Mpi/Mpi,/k/k(0:3),/W/W,/Md/Md
      common /pi/pi,/um/um,/q1/q(100),/wg1/wg(100),/Nq1/Nq
     .,/x/x(30),wx(30),/Nx/Nx,/y/y(30),wy(30),/Ny/Ny,/qi/qi
     .,/FEN/A00(-1:1,-1:1,-2:2,10),A10(-1:1,-1:1,-1:1,-2:2,10)
     .,A01(-1:1,-1:1,-1:1,-2:2,10),A11(-1:1,-1:1,-1:1,-1:1,-2:2,10)
     .,/nL/nL(10),/GpN/GpN(10,2,2)

      q0KB=q0**2

      qi=q0
      wq0=dsqrt(qi**2+Mpi**2)
      FP=MN*Mpi/(MN+Mpi)/(2d0*pi)**3
c     -------------
      do 110 il=-1,1,2
      do 110 L=0,2
      do 110 im=-L,L
      do 110 id=-1,1
      v00(il,id,L,im)=0d0
      do 111 jt=-1,1
 111  v10(il,id,jt,L,im)=0d0
      do 110 is=-1,1
      v01(il,id,is,L,im)=0d0
      do 112 jt=-1,1
 112  v11(il,id,is,jt,L,im)=0d0
 110  continue
c     -------------

      do 20 ix=1,Nx
      do 20 iy=1,Ny
c     ---------------------------------------- ! f_2T_2S
      call FiEN(x(ix),y(iy),f00,f01,f10,f11)      
c     ----------------------------------------
      do 20 L=0,2
      call YY(L,x(ix),cdexp(um*y(iy)),Ym)
      do 20 im=-L,L
      wxy=wx(ix)*wy(iy)*dconjg(Ym(im))
      do 20 il=-1,1,2
      do 20 id=-1,1 
      v00(il,id,L,im)=v00(il,id,L,im)+f00(il,id)*wxy
      do 101 jt=-1,1
 101  v10(il,id,jt,L,im)=v10(il,id,jt,L,im)+f10(il,id,jt)*wxy
      do 20 is=-1,1 
      v01(il,id,is,L,im)=v01(il,id,is,L,im)+f01(il,id,is)*wxy
      do 102 jt=-1,1
 102  v11(il,id,is,jt,L,im)=v11(il,id,is,jt,L,im)+f11(il,id,is,jt)*wxy
 20   continue

      do 22 nP=1,10
      call Ff_PiN(q0,nP,g0)
      do ni=1,2
      gP(nP,ni)=g0(ni)
      end do
      do 22 il=-1,1,2
      do 22 id=-1,1 
      do 22 im=-nL(nP),nL(nP)
      do 22 nj=1,2
      B00(il,id,im,nP,nj)=-um*q0/2d0*pi*v00(il,id,nL(nP),im)*g0(nj)*FP
      do 1 jt=-1,1
 1    B10(il,id,jt,im,nP,nj)=-um*q0/2d0*pi
     .                              *v10(il,id,jt,nL(nP),im)*g0(nj)*FP
      do 22 is=-1,1
      B01(il,id,is,im,nP,nj)=-um*q0/2d0*pi
     .                              *v01(il,id,is,nL(nP),im)*g0(nj)*FP
      do 2 jt=-1,1
 2    B11(il,id,is,jt,im,nP,nj)=-um*q0/2d0*pi
     .                           *v11(il,id,is,jt,nL(nP),im)*g0(nj)*FP
 22   continue
c     ------------------------

      do i=1,Nq
      qi=q(i)
      qKB=qi**2
      wq=dsqrt(qKB+Mpi**2)
      FP=MN*Mpi/(MN+Mpi)*dsqrt(wq0/wq)/(2d0*pi)**3

      do 210 L=0,2
      do 210 im=-L,L
      do 210 il=-1,1,2
      do 210 id=-1,1
      u00(il,id,L,im)=0d0
      do 211 jt=-1,1
 211  u10(il,id,jt,L,im)=0d0
      do 210 is=-1,1
c     -------------
      u01(il,id,is,L,im)=0d0
      do 212 jt=-1,1
 212  u11(il,id,is,jt,L,im)=0d0
 210  continue
c     -------------
      do 40 ix=1,Nx
      do 40 iy=1,Ny
      call FiEN(x(ix),y(iy),f00,f01,f10,f11)
c     --------------------------------------------

      do 40 L=0,2
      call YY(L,x(ix),cdexp(um*y(iy)),Ym)
      do 40 im=-L,L
      wxy=wx(ix)*wy(iy)*dconjg(Ym(im))
      do 40 il=-1,1,2
      do 40 id=-1,1 
      u00(il,id,L,im)=u00(il,id,L,im)+f00(il,id)*wxy
      do 201 jt=-1,1
 201  u10(il,id,jt,L,im)=u10(il,id,jt,L,im)+f10(il,id,jt)*wxy
      do 40 is=-1,1 
      u01(il,id,is,L,im)=u01(il,id,is,L,im)+f01(il,id,is)*wxy
      do 202 jt=-1,1
 202  u11(il,id,is,jt,L,im)=u11(il,id,is,jt,L,im)
     .                                   +f11(il,id,is,jt)*wxy
 40   continue

c     -------------------------------------------

      do 42 nP=1,10
      call Ff_PiN(qi,nP,gq)
      do 42 il=-1,1,2
      do 42 id=-1,1
      do 42 im=-nL(nP),nL(nP)
      do 42 nj=1,2
      B00(il,id,im,nP,nj)=B00(il,id,im,nP,nj)
     . -(q0KB*v00(il,id,nL(nP),im)*gP(nP,nj)
     . -qKB*u00(il,id,nL(nP),im)*gq(nj))
     . /(q0KB-qKB)*wg(i)*FP
      do 3 jt=-1,1
 3    B10(il,id,jt,im,nP,nj)=B10(il,id,jt,im,nP,nj)
     . -(q0KB*v10(il,id,jt,nL(nP),im)*gP(nP,nj)
     . -qKB*u10(il,id,jt,nL(nP),im)*gq(nj))
     . /(q0KB-qKB)*wg(i)*FP
      do 42 is=-1,1
      B01(il,id,is,im,nP,nj)=B01(il,id,is,im,nP,nj)
     . -(q0KB*v01(il,id,is,nL(nP),im)*gP(nP,nj)
     . -qKB*u01(il,id,is,nL(nP),im)*gq(nj))
     . /(q0KB-qKB)*wg(i)*FP
      do 4 jt=-1,1
 4    B11(il,id,is,jt,im,nP,nj)=B11(il,id,is,jt,im,nP,nj)
     . -(q0KB*v11(il,id,is,jt,nL(nP),im)*gP(nP,nj)
     . -qKB*u11(il,id,is,jt,nL(nP),im)*gq(nj))
     . /(q0KB-qKB)*wg(i)*FP
 42   continue
      end do

c     -------------------------

      do 50 il=-1,1,2
      do 50 id=-1,1
      do 50 nP=1,10
      do 50 im=-nL(nP),nL(nP)
      A00(il,id,im,nP)=0d0
      do jt=-1,1
      A10(il,id,jt,im,nP)=0d0
      end do
      do 50 is=-1,1
      A01(il,id,is,im,nP)=0d0
      do 50 jt=-1,1
 50   A11(il,id,is,jt,im,nP)=0d0

      do 43 nP=1,10
      do 43 ni=1,2
      Gl=0d0 
      do nj=1,2
      Gl=Gl+GpN(nP,ni,nj)*gP(nP,nj)
      end do
      Gl=Gl*4d0*pi
      do 43 il=-1,1,2
      do 43 id=-1,1
      do 43 im=-nL(nP),nL(nP)
      A00(il,id,im,nP)=A00(il,id,im,nP)+B00(il,id,im,nP,ni)*Gl
      do jt=-1,1
      A10(il,id,jt,im,nP)=A10(il,id,jt,im,nP)
     . +B10(il,id,jt,im,nP,ni)*Gl
      end do
      do 43 is=-1,1
      A01(il,id,is,im,nP)=A01(il,id,is,im,nP)
     . +B01(il,id,is,im,nP,ni)*Gl
      do 43 jt=-1,1
 43   A11(il,id,is,jt,im,nP)=A11(il,id,is,jt,im,nP)
     . +B11(il,id,is,jt,im,nP,ni)*Gl

      return
      end       

c -----------------------------------------------------------------------

      subroutine FiEN(x,y,f00,f01,f10,f11)
      implicit double precision (a-h,k,m,o-z)
      dimension q(3),qP(0:3),ps(0:3)
      complex*16 f(-1:1,-1:1,0:1,-1:1,0:1)
     .,f00(-1:1,-1:1),f10(-1:1,-1:1,-1:1)
     .,f01(-1:1,-1:1,-1:1),f11(-1:1,-1:1,-1:1,-1:1)
      common /pi/ppi,/MN/MN,/Mpi/Mpi,/qi/qi,/k/k(0:3),/W/W,/Md/Md
     .,/KEN/pf(0:3)

      q(1)=qi*dsqrt(dabs(1d0-x**2))*dcos(y)
      q(2)=qi*dsqrt(dabs(1d0-x**2))*dsin(y)
      q(3)=qi*x

      do i=1,3
      ps(i)=-q(i)+MN/(MN+Mpi)*(k(i)-pf(i))
      qP(i)=q(i)+Mpi/(MN+Mpi)*(k(i)-pf(i))
      end do
c     ps(0)=MN+(ps(1)**2+ps(2)**2+ps(3)**2)/(2d0*MN)
c     qP(0)=W-ps(0)-pf(0)

      qP(0)=dsqrt(Mpi**2+qP(1)**2+qP(2)**2+qP(3)**2)
      ps(0)=Md+k(0)-qP(0)-pf(0)

      if(qP(0).lt.Mpi)qP(0)=Mpi
      call KIN_IA(qP,pf,ps)
c     ---------------------------- 
      call IA(0,f)
c     ---------------------------- 
      do 1 il=-1,1,2
      do 1 id=-1,1
      f00(il,id)=f(il,id,0,0,0)
      f10(il,id,0)=f(il,id,0,0,1)
      do 1 is=-1,1
      f01(il,id,is)=f(il,id,1,is,0)
 1    f11(il,id,is,0)=f(il,id,1,is,1)

      do 2 jt=-1,1,2
c     -------------------------------- 
      call IA(jt,f)
c     --------------------------------
      do 2 il=-1,1,2
      do 2 id=-1,1
      f10(il,id,jt)=f(il,id,0,0,1)
      do 2 is=-1,1
 2    f11(il,id,is,jt)=f(il,id,1,is,1)

      return
      end       

c -------------------------------------------------------------------
  
      subroutine F_s
      implicit double precision (a-h,k,m,o-z)
      complex*16 A00,A10,A01,A11,F00,F10,F01,F11
      common /pi/pi
     .,/FTS/F00(-1:1,-1:1,10),F10(-1:1,-1:1,-1:1,10)
     .,F01(-1:1,-1:1,-1:1,10),F11(-1:1,-1:1,-1:1,-1:1,10)
     .,/FEN/A00(-1:1,-1:1,-2:2,10),A10(-1:1,-1:1,-1:1,-2:2,10)
     .,A01(-1:1,-1:1,-1:1,-2:2,10),A11(-1:1,-1:1,-1:1,-1:1,-2:2,10)

      Y=1d0/dsqrt(4d0*pi)
      do 1 nP=1,2
      do 1 il=-1,1,2
      do 1 id=-1,1
      F00(il,id,nP)=A00(il,id,0,nP)*Y
      do 2 jt=-1,1
 2    F10(il,id,jt,nP)=A10(il,id,jt,0,nP)*Y
      do 1 is=-1,1 
      F01(il,id,is,nP)=A01(il,id,is,0,nP)*Y
      do 1 jt=-1,1
 1    F11(il,id,is,jt,nP)=A11(il,id,is,jt,0,nP)*Y
      return
      end

c -------------------------------------------------------------------
  
      subroutine F_p(Y)
      implicit double precision (a-h,k,m,o-z)
      dimension c1(0:1,2),c2(0:1,2),c3(0:1,2)
      complex*16 Y(-2:2),A00,A10,A01,A11,F00,F10,F01,F11
      common /FTS/F00(-1:1,-1:1,10),F10(-1:1,-1:1,-1:1,10)
     .,F01(-1:1,-1:1,-1:1,10),F11(-1:1,-1:1,-1:1,-1:1,10)
     .,/FEN/A00(-1:1,-1:1,-2:2,10),A10(-1:1,-1:1,-1:1,-2:2,10)
     .,A01(-1:1,-1:1,-1:1,-2:2,10),A11(-1:1,-1:1,-1:1,-1:1,-2:2,10)
     .,/JnP/iJ(10),/C111/C11(-1:1,-1:1)

c     ----------------------------- !cN(S,J)
      c1(0,1)=1d0/3d0
      c2(0,1)=-dsqrt(2d0)*c1(0,1)
      c3(0,1)=0d0

      c1(0,2)=2d0/3d0
      c2(0,2)=c1(0,2)/dsqrt(2d0)
      c3(0,2)=0d0

      c1(1,1)=1d0/3d0
      c2(1,1)=-c1(1,1)*dsqrt(2d0)
      c3(1,1)=c2(1,1)*dsqrt(2d0)

      c1(1,2)=2d0/3d0
      c2(1,2)=c1(1,2)/dsqrt(2d0)
      c3(1,2)=c2(1,2)*dsqrt(2d0)

      do 1 nP=3,6
      do 1 il=-1,1,2
      do 1 id=-1,1

c     -------------------- S=0
      do 10 im=-1,1
      F00(il,id,nP)=F00(il,id,nP)
     .     +c1(0,iJ(nP))*A00(il,id,im,nP)*Y(im)
      do jt=-1,1
      F10(il,id,jt,nP)=F10(il,id,jt,nP)
     .     +c1(0,iJ(nP))*A10(il,id,jt,im,nP)*Y(im)
      end do
      do 10 im1=max(-1,im-1),min(1,im+1)
      F00(il,id,nP)=F00(il,id,nP)
     . +c2(0,iJ(nP))*C11(im1,im)*A01(il,id,im-im1,im1,nP)*Y(im)
      do 10 jt=-1,1
 10   F10(il,id,jt,nP)=F10(il,id,jt,nP)
     . +c2(0,iJ(nP))*C11(im1,im)*A11(il,id,im-im1,jt,im1,nP)*Y(im)

c     -------------------- S=1

      do 1 is=-1,1
      do 20 im=-1,1
      F01(il,id,is,nP)=F01(il,id,is,nP)
     . +c1(1,iJ(nP))*A01(il,id,is,im,nP)*Y(im)
      if(abs(im+is).le.1)
     .F01(il,id,is,nP)=F01(il,id,is,nP)+c2(1,iJ(nP))
     . *(-1)**is*C11(im+is,im)*A00(il,id,im+is,nP)*Y(im)
      do jt=-1,1
      F11(il,id,is,jt,nP)=F11(il,id,is,jt,nP)
     . +c1(1,iJ(nP))*A11(il,id,is,jt,im,nP)*Y(im)
      if(abs(im+is).le.1)
     .F11(il,id,is,jt,nP)=F11(il,id,is,jt,nP)+c2(1,iJ(nP))
     .*(-1)**is*C11(im+is,im)*A10(il,id,jt,im+is,nP)*Y(im)
      end do
      do 20 im1=max(-1,im-1),min(1,im+1)
      is1=is+im-im1
      if(abs(is1).le.1)
     .F01(il,id,is,nP)=F01(il,id,is,nP)+c3(1,iJ(nP))
     . *C11(is,is1)*C11(im1,im)*A01(il,id,is1,im1,nP)*Y(im)
      do 20 jt=-1,1
      if(abs(is1).le.1)
     .F11(il,id,is,jt,nP)=F11(il,id,is,jt,nP)+c3(1,iJ(nP))
     .*C11(is,is1)*C11(im1,im)*A11(il,id,is1,jt,im1,nP)*Y(im)
 20   continue
 1    continue

      return
      end 

c -------------------------------------------------------------------
  
      subroutine F_d(Y)
      implicit double precision (a-h,k,m,o-z)
      dimension c1(0:1,2),c2(0:1,2),c3(0:1,2)
      complex*16 Y(-2:2),A00,A10,A01,A11,F00,F10,F01,F11
      common /FTS/F00(-1:1,-1:1,10),F10(-1:1,-1:1,-1:1,10)
     .,F01(-1:1,-1:1,-1:1,10),F11(-1:1,-1:1,-1:1,-1:1,10)
     .,/FEN/A00(-1:1,-1:1,-2:2,10),A10(-1:1,-1:1,-1:1,-2:2,10)
     .,A01(-1:1,-1:1,-1:1,-2:2,10),A11(-1:1,-1:1,-1:1,-1:1,-2:2,10)
     .,/JnP/iJ(10),/C111/C11(-1:1,-1:1),/C212/C22(-2:2,-2:2) 

c     ----------------------------- !cN(S,J)
      c1(0,1)=4d-1
      c2(0,1)=-c1(0,1)*dsqrt(1.5d0)
      c3(0,1)=0d0

      c1(0,2)=6d-1
      c2(0,2)=c1(0,2)/dsqrt(1.5d0)
      c3(0,2)=0d0

      c1(1,1)=4d-1
      c2(1,1)=-dsqrt(6d0)/5d0
      c3(1,1)=c2(1,1)*dsqrt(2d0)

      c1(1,2)=6d-1
      c2(1,2)=dsqrt(6d0)/5d0
      c3(1,2)=c2(1,2)*dsqrt(2d0)

      do 1 nP=7,10
      do 1 il=-1,1,2
      do 1 id=-1,1
c     -------------------- S=0
      do 10 im=-2,2
      F00(il,id,nP)=F00(il,id,nP)
     .      +c1(0,iJ(nP))*A00(il,id,im,nP)*Y(im)
      do jt=-1,1
      F10(il,id,jt,nP)=F10(il,id,jt,nP)
     .      +c1(0,iJ(nP))*A10(il,id,jt,im,nP)*Y(im)
      end do
      do 10 im1=max(-2,im-1),min(2,im+1)
      F00(il,id,nP)=F00(il,id,nP)
     . +c2(0,iJ(nP))*C22(im1,im)*A01(il,id,im-im1,im1,nP)*Y(im)
      do 10 jt=-1,1
 10   F10(il,id,jt,nP)=F10(il,id,jt,nP)
     . +c2(0,iJ(nP))*C22(im1,im)*A11(il,id,im-im1,jt,im1,nP)*Y(im)
c     -------------------- S=1

      do 1 is=-1,1
      do 20 im=-2,2
      F01(il,id,is,nP)=F01(il,id,is,nP)
     . +c1(1,iJ(nP))*A01(il,id,is,im,nP)*Y(im)
      if(abs(im+is).le.2)
     .F01(il,id,is,nP)=F01(il,id,is,nP)+c2(1,iJ(nP))
     . *(-1)**is*C22(im+is,im)*A00(il,id,im+is,nP)*Y(im)
      do jt=-1,1
      F11(il,id,is,jt,nP)=F11(il,id,is,jt,nP)+c1(1,iJ(nP))
     . *A11(il,id,is,jt,im,nP)*Y(im)
      if(abs(im+is).le.2)
     .F11(il,id,is,jt,nP)=F11(il,id,is,jt,nP)+c2(1,iJ(nP))
     . *(-1)**is*C22(im+is,im)*A10(il,id,jt,im+is,nP)*Y(im)
      end do
      do 20 im1=max(-2,im-1),min(2,im+1)
      is1=is+im-im1
      if(abs(is1).le.1)
     .F01(il,id,is,nP)=F01(il,id,is,nP)+c3(1,iJ(nP))
     . *C11(is,is1)*C22(im1,im)*A01(il,id,is1,im1,nP)*Y(im)
      do 20 jt=-1,1
      if(abs(is1).le.1)
     .F11(il,id,is,jt,nP)=F11(il,id,is,jt,nP)+c3(1,iJ(nP))
     . *C11(is,is1)*C22(im1,im)*A11(il,id,is1,jt,im1,nP)*Y(im)
 20   continue
 1    continue

      return
      end 

c -------------------------------------------------------------------

      subroutine PiN(jm,Y1,Y2,q0,TPN)
      implicit double precision (a-h,k,m,o-z)
      integer s
      complex*16 um,Y1(-2:2),Y2(-2:2),F00,F01,F10,F11
     . ,TPN(-1:1,-1:1,0:1,-1:1,0:1)
      common /pi/pi,/um/um,/MN/MN,/Mpi/Mpi,/C111/C(-1:1,-1:1)
     . ,/iPT/iT(10)
     . ,/FTS/F00(-1:1,-1:1,10),F10(-1:1,-1:1,-1:1,10)
     . ,F01(-1:1,-1:1,-1:1,10),F11(-1:1,-1:1,-1:1,-1:1,10)

      call StEN(q0)

      do 110 il=-1,1,2
      do 110 id=-1,1
      do 110 s=0,1
      do 110 is=-s,s
      do 110 it1=0,1
 110  TPN(il,id,s,is,it1)=0d0

      do 120 il=-1,1,2
      do 120 id=-1,1
      do 120 nP=1,10
      F00(il,id,nP)=0d0
      do jt=-1,1
      F10(il,id,jt,nP)=0d0
      end do
      do 120 is=-1,1 
      F01(il,id,is,nP)=0d0
      do 120 jt=-1,1
 120  F11(il,id,is,jt,nP)=0d0

c     ------------------------------------
      call F_s
      call F_p(Y1)
      call F_d(Y2)
c     ------------------------------------

      do 2 nP=1,10      !Summierung "uber L_(2t2s)
      do 2 il=-1,1,2
      do 2 id=-1,1
      goto (30,40,30) jm+2
 40   TPN(il,id,0,0,0)=TPN(il,id,0,0,0)+iT(nP)/3d0*F00(il,id,nP)
      do 20 jt=-1,1
 20   TPN(il,id,0,0,0)=TPN(il,id,0,0,0)
     . -jt/3d0*(-1)**iT(nP)*F10(il,id,jt,nP)
      do 21 is=-1,1
      TPN(il,id,1,is,0)=TPN(il,id,1,is,0)+iT(nP)/3d0*F01(il,id,is,nP)
      do 21 jt=-1,1
 21   TPN(il,id,1,is,0)=TPN(il,id,1,is,0)
     . -jt/3d0*(-1)**iT(nP)*F11(il,id,is,jt,nP)

 30   TPN(il,id,0,0,1)=TPN(il,id,0,0,1)
     . +iT(nP)/3d0*F10(il,id,jm,nP)-jm/3d0*(-1)**iT(nP)*F00(il,id,nP)
      do 22 is=-1,1
 22   TPN(il,id,1,is,1)=TPN(il,id,1,is,1)
     . +iT(nP)/3d0*F11(il,id,is,jm,nP)
     . -jm/3d0*(-1)**iT(nP)*F01(il,id,is,nP)
      do 23 jt=max(-1,jm-1),min(1,jm+1)
      TPN(il,id,0,0,1)=TPN(il,id,0,0,1)
     . -2d0/3d0*(-1)**(iT(nP)+jm-jt)*C(jm,jt)**2*F10(il,id,jt,nP)
      do 23 is=-1,1
 23   TPN(il,id,1,is,1)=TPN(il,id,1,is,1)
     . -2d0/3d0*(-1)**(iT(nP)+jm-jt)*C(jm,jt)**2
     . *F11(il,id,is,jt,nP)
 2    continue

      return
      end 

c -------------------------------------------------------------------

      subroutine G_PiN(w)
      implicit double precision (a-h,k,m,o-z)
      dimension h0(2),h(2),gP(10)
      complex*16 um,F(2,2),GpN,Det
      common /pi/pi,/um/um,/MN/MN,/Mpi/Mpi
     . ,/c_PiN/c(10,10),/GpN/GpN(10,2,2)
     . ,/q/q(100),/wg/wg(100),/Nq/Nq

      q0KB=((w**2-(MN+Mpi)**2)*(w**2-(MN-Mpi)**2))/4d0/w**2

      do nP=1,10
      gP(nP)=c(nP,10)
      gP(3)=1d0/(W-1580d0)
      gP(6)=1d0/(W-1245d0)  

      do 1 i=1,2
      do 1 j=1,2
 1    F(i,j)=0d0

      if(q0KB.lt.0d0)goto 100
      q0=dsqrt(q0KB)
      wq=dsqrt(Mpi**2+q0KB)
      EN=dsqrt(MN**2+q0KB)
      EE0=2d0*wq*EN/w
      call Ff_PiN(q0,nP,h0)
      do 2 i=1,2
      do 2 j=1,2
 2    F(i,j)=-um*q0*pi/2d0*h0(i)*h0(j)*EE0

      do iq=1,Nq
      qi=q(iq)
      qKB=qi**2
      ENq=dsqrt(MN**2+qKB)
      wqq=dsqrt(Mpi**2+qKB)
      EE=(EN+ENq)*(wq+wqq)/(W+ENq+wqq)
      call Ff_PiN(qi,nP,h)
      do 3 i=1,2
      do 3 j=1,2
 3    F(i,j)=F(i,j)+(qKB*h(i)*h(j)*EE-q0KB*h0(i)*h0(j)*EE0)
     . /(q0KB-qKB)*wg(iq)
      end do
      goto 101

 100  do iq=1,Nq
      qKB=q(iq)**2
      ENq=dsqrt(MN**2+qKB)
      wqq=dsqrt(Mpi**2+qKB)
      call Ff_PiN(qi,nP,h)
      do 4 i=1,2
      do 4 j=1,2
 4    F(i,j)=F(i,j)+qKB*h(i)*h(j)/(w-ENq-wqq)*wg(iq)
      end do
 101  continue

      Det=((1d0-c(nP,10)*F(1,1))*(1d0-gP(nP)*F(2,2))
     . -c(nP,10)*gP(nP)*F(1,2)*F(2,1))/(2d0*pi**2)
      
      GpN(nP,1,1)=c(nP,10)*(1d0-gP(nP)*F(2,2))/Det
      GpN(nP,1,2)=c(nP,10)*gP(nP)*F(1,2)/Det
      GpN(nP,2,1)=GpN(nP,1,2)
      GpN(nP,2,2)=gP(nP)*(1d0-c(nP,10)*F(1,1))/Det
      end do

      return
      end

c -------------------------------------------------------------------

      subroutine Ff_PiN(q,N,h)
      implicit double precision (a-h,k,m,o-z)
      dimension h(2) 
      common /c_PiN/c(10,10)

      h(1)=c(N,4)*q**c(N,2)/(q**2+c(N,5)**2)**c(N,3)*q**c(N,1)
      h(2)=c(N,8)*q**c(N,6)/(q**2+c(N,9)**2)**c(N,7)*q**c(N,1)

      return
      end

c -------------------------------------------------------------------

      subroutine KIN_IA(q,pf,ps)
      implicit double precision (a-h,k,m,o-z)
      complex*16 um,KNt(-1:1,-1:1),LNt(-1:1,-1:1,-1:1),Y(-2:2)
      dimension q(0:3),pf(0:3),ps(0:3),up(3)
      common /pi/pi,/unt/unt,/um/um,/W/W,/MN/MN,/Mpi/Mpi
     .,/Md/Md,/Eb/Eb,/k/k(0:3),/Dd/Dd(11),Sd(11),al(11)
     .,/wPN/wN,/Gs/Gs,Gd,/Y2/Y,/KNt/KNt,LNt,/dN/dN

      do i=1,3
      up(i)=-ps(i)
      end do
      upa=dsqrt(up(1)**2+up(2)**2+up(3)**2)
      call YY(2,up(3)/upa,(up(1)+um*up(2))/dsqrt(upa**2-up(3)**2),Y)
      call Ff_de(upa,Fg1,Fg2)
      Gs=dN*Fg1/(Eb-upa**2/MN)
      Gd=-dN*dsqrt(4d0*pi)*Fg2/(Eb-upa**2/MN) 

      goto 167
      Gs=0d0
      Gd=0d0
      do i=1,11
      dm=1d0/((upa/unt)**2+al(i)**2)
      Gs=Gs+Sd(i)*dm
      Gd=Gd+Dd(i)*dm   
      end do
      Gs=Gs*dsqrt(4d0*pi)/unt**(1.5d0)      
      Gd=Gd*4d0*pi/unt**(1.5d0)            
 167  continue

c .............................................. Elementare Amplitude

c     wN=(W-ps(0))**2-ps(0)**2+MN**2      !Spectator on-shell 
c     if(wN.lt.(MN+Mpi+1d-3)**2)wN=(MN+Mpi+1d-3)**2
c     if(wN.gt.4d6)wN=4d6
c     wN=dsqrt(wN)

      wN=(pf(0)+q(0))**2                  !Final nucleon  on-shell 
     . -(pf(1)+q(1))**2-(pf(2)+q(2))**2-(pf(3)+q(3))**2
      if(wN.lt.(MN+Mpi+1d-3)**2)wN=(MN+Mpi+1d-3)**2
      if(wN.gt.4d6)wN=4d6
      wN=dsqrt(wN)
c     ---------------------------
      call Crush(wN,q,pf,KNt,LNt)
c     ---------------------------

      return
      end 

c ----------------------------------------------------------------------

      subroutine IA(jm,TM)
      implicit double precision (a-h,k,m,o-z)
      complex*16 KNt(-1:1,-1:1),LNt(-1:1,-1:1,-1:1),KN,LN(-1:1)
     . ,TM(-1:1,-1:1,0:1,-1:1,0:1),Y(-2:2)
      common /KNt/KNt,LNt,/Gs/Gs,Gd,/Y2/Y,/wPN/wN

      do 40 il=-1,1,2
      do 40 it=0,1

c *******************************************************************      
      goto (1,2,3) jm+2
c     ----------------------------- !pi^- pp
 1    KN=it*(-KNt(il,0)+KNt(il,-1))   
      do i=-1,1
      LN(i)=it*(-LNt(il,0,i)+LNt(il,-1,i))     
      end do
      goto 4
c     ----------------------------- !pi^0 np
 2    KN=(1-it)*KNt(il,1)+it*KNt(il,0)
      do i=-1,1
      LN(i)=(1-it)*LNt(il,1,i)+it*LNt(il,0,i)
      end do
      goto 4
c     ----------------------------- !pi^+ nn
 3    KN=-it*(KNt(il,0)+KNt(il,-1))             
      do i=-1,1
      LN(i)=-it*(LNt(il,0,i)+LNt(il,-1,i))
      end do
 4    continue

c    ----------------------------------------------------------- 
 100  continue

      TM(il,-1,0,0,it)=
     .  LN(-1)*(Gs+Gd*Y(0)*dsqrt(1d-1))+Gd*(LN(1)*Y(-2)*dsqrt(6d-1)
     . -LN(0)*Y(-1)*dsqrt(3d-1))
      TM(il,-1,1,1,it)=
     . -LN(-1)*Gd*Y(-1)*dsqrt(3d-1)+(KN+LN(0))*Gd*Y(-2)*dsqrt(6d-1)
      TM(il,-1,1,0,it)=
     . -KN*Gd*Y(-1)*dsqrt(3d-1)+LN(-1)*(Gs+Gd*Y(0)*dsqrt(1d-1))
     . -LN(1)*Gd*Y(-2)*dsqrt(6d-1)
      TM(il,-1,1,-1,it)=
     . (KN-LN(0))*(Gs+Gd*Y(0)*dsqrt(1d-1))+LN(1)*Gd*Y(-1)*dsqrt(3d-1)
 
      TM(il,0,0,0,it)=
     .  LN(0)*(Gs-Gd*Y(0)*dsqrt(4d-1))+Gd*(LN(-1)*Y(1)*dsqrt(3d-1)
     . +LN(1)*Y(-1)*dsqrt(3d-1))
      TM(il,0,1,1,it)=
     . LN(-1)*(Gs-Gd*Y(0)*dsqrt(4d-1))+(KN+LN(0))*Gd*Y(-1)*dsqrt(3d-1)
      TM(il,0,1,0,it)=
     . KN*(Gs-Gd*Y(0)*dsqrt(4d-1))
     . +Gd*(LN(-1)*Y(1)-LN(1)*Y(-1))*dsqrt(3d-1)
      TM(il,0,1,-1,it)=
     . -LN(1)*(Gs-Gd*Y(0)*dsqrt(4d-1))+(KN-LN(0))*Gd*Y(1)*dsqrt(3d-1)

      TM(il,1,0,0,it)=
     .  LN(1)*(Gs+Gd*Y(0)*dsqrt(1d-1))+Gd*(LN(-1)*Y(2)*dsqrt(6d-1)
     . -LN(0)*Y(1)*dsqrt(3d-1))
      TM(il,1,1,1,it)=
     . (KN+LN(0))*(Gs+Gd*Y(0)*dsqrt(1d-1))-LN(-1)*Gd*Y(1)*dsqrt(3d-1)
      TM(il,1,1,0,it)=
     . -KN*Gd*Y(1)*dsqrt(3d-1)-LN(1)*(Gs+Gd*Y(0)*dsqrt(1d-1))
     . +LN(-1)*Gd*Y(2)*dsqrt(6d-1)
      TM(il,1,1,-1,it)=
     .  LN(1)*Gd*Y(1)*dsqrt(3d-1)+(KN-LN(0))*Gd*Y(2)*dsqrt(6d-1)

c    ----------------------------------------------------------- 
 40   continue

      return
      end

c ------------------------------------------------- Lambda - Funktion

      function Flmb(w,m1,m2)
      double precision w,m1,m2,Flmb
      Flmb=dsqrt((w**2-(m1-m2)**2)*(w**2-(m1+m2)**2))/(2d0*w)
      return
      end

c -------------------------------------------------------------------
 
      function clgo (a,az,b,bz,ab,abz)
c     (a az, b bz | ab abz)
      implicit double precision (a-h,o-z)
      dimension g(101),sqr(101),g1(47),g2(54),ss1(47),ss2(54) 
      equivalence (g(1),g1(1)),(g(48),g2(1)),(sqr(1),ss1(1)), 
     .(sqr(48),ss2(1))
      data g1/0d0                 ,0d0                 ,
     .6.93147180559945d-01,1.79175946922806d 00,3.17805383034795d 00, 
     .4.78749174278205d 00,6.57925121201010d 00,8.52516136106541d 00, 
     .1.06046029027453d 01,1.28018274800815d 01,1.51044125730755d 01, 
     .1.75023078458739d 01,1.99872144956619d 01,2.25521638531234d 01, 
     .2.51912211827387d 01,2.78992713838409d 01,3.06718601060807d 01, 
     .3.35050734501369d 01,3.63954452080331d 01,3.93398841871995d 01, 
     .4.23356164607535d 01,4.53801388984769d 01,4.84711813518352d 01, 
     .5.16066755677644d 01,5.47847293981123d 01,5.80036052229805d 01, 
     .6.12617017610020d 01,6.45575386270063d 01,6.78897431371815d 01, 
     .7.12570389671680d 01,7.46582363488302d 01,7.80922235533153d 01, 
     .8.15579594561150d 01,8.50544670175815d 01,8.85808275421977d 01, 
     .9.21361756036871d 01,9.57196945421432d 01,9.93306124547874d 01, 
     .1.02968198614514d 02,1.06631760260643d 02,1.10320639714757d 02, 
     .1.14034211781462d 02,1.17771881399745d 02,1.21533081515439d 02, 
     .1.25317271149357d 02,1.29123933639127d 02,1.32952575035616d 02/ 
      data g2/
     .1.36802722637326d 02,1.40673923648234d 02,1.44565743946345d 02, 
     .1.48477766951773d 02,1.52409592584497d 02,1.56360836303079d 02, 
     .1.60331128216631d 02,1.64320112263195d 02,1.68327445448428d 02, 
     .1.72352797139163d 02,1.76395848406997d 02,1.80456291417544d 02, 
     .1.84533828861449d 02,1.88628173423672d 02,1.92739047287845d 02, 
     .1.96866181672890d 02,2.01009316399282d 02,2.05168199482641d 02, 
     .2.09342586752537d 02,2.13532241494563d 02,2.17736934113954d 02, 
     .2.21956441819130d 02,2.26190548323728d 02,2.30439043565777d 02, 
     .2.34701723442818d 02,2.38978389561834d 02,2.43268849002983d 02, 
     .2.47572914096187d 02,2.51890402209723d 02,2.56221135550010d 02, 
     .2.60564940971863d 02,2.64921649798553d 02,2.69291097651020d 02, 
     .2.73673124285694d 02,2.78067573440366d 02,2.82474292687630d 02, 
     .2.86893133295427d 02,2.91323950094270d 02,2.95766601350761d 02, 
     .3.00220948647014d 02,3.04686856765669d 02,3.09164193580147d 02, 
     .3.13652829949879d 02,3.18152639620209d 02,3.22663499126726d 02, 
     .3.27185287703775d 02,3.31717887196928d 02,3.36261181979198d 02, 
     .3.40815058870799d 02,3.45379407062267d 02,3.49954118040770d 02, 
     .3.54539085519441d 02,3.59134205369575d 02,3.63739375555563d 02/ 
      data ss1/1.00000000000000d 00,1.41421356237310d 00, 
     .1.73205080756888d 00,2.00000000000000d 00,2.23606797749979d 00, 
     .2.44948974278318d 00,2.64575131106459d 00,2.82842712474619d 00, 
     .3.00000000000000d 00,3.16227766016838d 00,3.31662479035540d 00, 
     .3.46410161513775d 00,3.60555127546399d 00,3.74165738677394d 00, 
     .3.87298334620742d 00,4.00000000000000d 00,4.12310562561766d 00, 
     .4.24264068711929d 00,4.35889894354067d 00,4.47213595499958d 00, 
     .4.58257569495584d 00,4.69041575982343d 00,4.79583152331272d 00, 
     .4.89897948556636d 00,5.00000000000000d 00,5.09901951359278d 00, 
     .5.19615242270663d 00,5.29150262212918d 00,5.38516480713450d 00, 
     .5.47722557505166d 00,5.56776436283002d 00,5.65685424949238d 00, 
     .5.74456264653803d 00,5.83095189484530d 00,5.91607978309962d 00, 
     .6.00000000000000d 00,6.08276253029822d 00,6.16441400296898d 00, 
     .6.24499799839840d 00,6.32455532033676d 00,6.40312423743285d 00, 
     .6.48074069840786d 00,6.55743852430200d 00,6.63324958071080d 00, 
     .6.70820393249937d 00,6.78232998312527d 00,6.85565460040104d 00/ 
      data ss2/ 
     .6.92820323027551d 00,7.00000000000000d 00,7.07106781186548d 00, 
     .7.14142842854285d 00,7.21110255092798d 00,7.28010988928052d 00, 
     .7.34846922834953d 00,7.41619848709566d 00,7.48331477354788d 00, 
     .7.54983443527075d 00,7.61577310586391d 00,7.68114574786861d 00, 
     .7.74596669241483d 00,7.81024967590665d 00,7.87400787401181d 00, 
     .7.93725393319377d 00,8.00000000000000d 00,8.06225774829855d 00, 
     .8.12403840463596d 00,8.18535277187245d 00,8.24621125123532d 00, 
     .8.30662386291807d 00,8.36660026534076d 00,8.42614977317636d 00, 
     .8.48528137423857d 00,8.54400374531753d 00,8.60232526704263d 00, 
     .8.66025403784439d 00,8.71779788708135d 00,8.77496438739212d 00, 
     .8.83176086632785d 00,8.88819441731559d 00,8.94427190999916d 00, 
     .9.00000000000000d 00,9.05538513813742d 00,9.11043357914430d 00, 
     .9.16515138991168d 00,9.21954445729289d 00,9.27361849549570d 00, 
     .9.32737905308882d 00,9.38083151964686d 00,9.43398113205660d 00, 
     .9.48683298050514d 00,9.53939201416946d 00,9.59166304662544d 00, 
     .9.64365076099295d 00,9.69535971483266d 00,9.74679434480896d 00, 
     .9.79795897113271d 00,9.84885780179610d 00,9.89949493661167d 00, 
     .9.94987437106620d 00,1.00000000000000d 01,1.00498756211209d 01/ 
      c=az
      d=bz
      e=ab
      u=dsqrt(2d0*e+1d0)
      if(c+d-abz) 23,3,23
      entry wigner(a,b,az,bz,ab,abz)
      e=az
      c=bz
      d=ab
      u=1d0-2d0*dabs(dmod(a-b-abz,2d0)) 
      if(c+d+abz) 23,3,23
   3  i2 = b+e-a
      i3 = e+a-b
      i4=a+b-e
      if(min0(i2,i3,i4))23,29,29 
 29   if(dmin1(a+c,a-c,b+d,b-d,e+abz,e-abz))23,31,31
 31   i1=a+b+e
      if(dmod(a+b+e,1d0)) 23,54,23
 54   if(dmod(a+c,1d0)) 23,55,23 
 55   if(dmod(b+d,1d0)) 23,56,23 
 56   i5=a+c
      i6=a-c
      i7=b+d
      i8=b-d
      i9=e+abz
      i10=e-abz 
      i11=a+d-e 
      i12=b-e-c 
      n0=max0(i11,i12,0)
      n9=min0(i4,i6,i7) 
      if(n0.gt.n9) goto 23 
      u=-u*(1d0-2d0*mod(n0,2))
      n1=i4-n0
      n2=i6-n0
      n3=i7-n0
      n4=n0+1 
      n5=n0-i12 +1
      n6=n0-i11 +1
      x=u*exp((g(i2+1)+g(i3+1)+g(i4+1)+g(i5+1)+g(i6+1)+g(i7+1)+g(i8+1)+ 
     .g(i9+1)+g(i10+1)-g(i1+2))*.5d0-g(n1+1)-g(n2+1)-g(n3+1)-g(n4)- 
     .g(n5)-g(n6))
      sum=-1. 
      if (n0.eq.n9) go to 25
      iw=n1*n2
      kt=n1+n2+1
      iy=n4*n5
      kr=n4+n5-1
      v=iw*n3 
      y=iy*n6 
      s=v/y
      sum=s-1d0
      if (n0+1.eq.n9) go to 25
      n9=n9-n0 -1 
      do 22 n=1,n9
      n3=n3-1 
      n6=n6+1 
      iw=kt-iw-n-n
      v=iw*n3 
      iy=iy+kr+n+n 
      y=iy*n6 
      s=s*v/y 
      sum=sum+s 
 22   iw=-iw
 25   clgo=x*sum
      if(dabs(clgo).gt.1d-10) return 
 23   clgo=0d0 

 44   return
      end

c -------------------------------------------------------------------

C     ������������ ��� ��������� ������������ �������������
C     ��������� ������ �����������  �����������������  ������
C     �������.

      SUBROUTINE TRIANGLE(Z)
      implicit double precision (a-h,k,m,o-z)
      REAL*4 RR1,RR2
  12  CONTINUE
      RR1=RAND()
      RR2=RAND()
      Z=-100.+230.*RR1
      IF (Z.GT.-100.AND.Z.LT.0) THEN
      YY=(1.,0.)/100.*(Z+100.)
      ELSE IF (Z.GE.0.AND.Z.LT.130) THEN
      YY=(1.,0.)/130.*(130.-Z)
      END IF
      IF (YY.LT.RR2) GOTO 12
      RETURN
      END


C     ������������  ���  �����������  ������������  �
C     �������������     ����    �������     ���������.

      SUBROUTINE TETBOARD(Z,TMIN,TMAX)
      implicit double precision (a-h,k,m,o-z)
      PI=3.141592653589
      RGR=PI/180.

      xmin=337.
      zmin=265.
      xmax=360.+235.*dsin(10.7*rgr)
      zmax=305.-235.*dcos(10.7*rgr)

      zzmax=zmax-z
      zzmin=zmin-z

      if (zzmax.gt.0) then
      tmin=datan(xmin/zzmin)
      tmax=datan(xmax/zzmax)
      else
      tmin=datan(xmin/zzmin)
      tmax=pi/2-datan(zzmax/xmax)
      end if

      RETURN
      END


C     ������������ ��� ��������� ������� ������� �����
C     �����������  �����������������  ������  �������.

      SUBROUTINE EGLL(EGL)
      implicit double precision (a-h,k,m,o-z)
      REAL*4 RR1,RR2
  11  CONTINUE
      RR1=RAND()
      RR2=RAND()
      OMM=(200.,0.)*DEXP(RR1*DLOG(2D3/2D2))
      Y=0.033*RR2
      IF (Y.LT.DALIZ(OMM)) THEN
      EGL=OMM
      ELSE
      GOTO 11
      END IF
      RETURN
      END

      FUNCTION DALIZ(OM)
      implicit double precision (a-h,k,m,o-z)
      ME=0.51099906
      ALF=1./137.035989561
      PI=3.141592653589
      E=2000.
      FL1=2.*E*(E-OM)/ME/(2.*E-OM)
      FL2=(2.*E-OM)/OM
      F1=2.*(1.-OM/E+OM**2/2/E**2)
      F2=OM**2/2./E**2
      SL=OM/E-1.
      DALIZ=ALF/PI*(F1*DLOG(DABS(FL1))+F2*DLOG(DABS(FL2))+SL)/OM
C     /OM !!!
      RETURN
      END


c     ������������  ���  ���������� ��������������  �������  �������
c     (���������   4-���������   ����   ��������   �  \{pi}-������).
c                            K  ---  procedure

      SUBROUTINE CIN(KK,PP1,PP2,QQ,ROS,Z)
      implicit double precision (a-h,k,m,o-z)
      REAL*8 LAMBDA_1,LAMBDA_2
      DIMENSION KK(0:3),PP1(0:3),PP2(0:3),QQ(0:3),P1(0:3),P2(0:3)
      DIMENSION P0cm(0:3),P0(0:3),P_3(0:3),P__2(0:3),P_2(0:3),
     *          P_12(0:3),R12(1:3,1:3)

      MP=938.27231
      MN=939.56563
      MPI=139.56563
C     MPI0=134.9764
      MD=1875.6134

      PI= (3.141592653589,0.)
      RGR=(3.141592653589,0.)/(180.,0.)

     
  1   CONTINUE    
 
C     CALL EGLL(EGL)
      EGL=200.+(2000.-200.)*RAND()
      S=MD**2+2.*EGL*MD
      M123=DSQRT(S)
      KK(0)=EGL
      KK(1)=(0.,0.)
      KK(2)=(0.,0.)
      KK(3)=EGL

      p0cm(0)=m123
      p0cm(1)=(0.,0.)
      p0cm(2)=(0.,0.)
      p0cm(3)=(0.,0.)

      mu3=2*mp+mpi
      t3=m123-mu3

      x=rand()

      xi_2=0.00799583 + 1.9198134*x - 
     *     5.25384581*x**2 + 11.65756065*x**3 - 
     *     12.23306942*x**4 +  4.89409263*x**5 - 
     *     0.000540466*x**6

      t2=t3*xi_2
      mu2=mu3-mpi
      m12=mu2+t2

      om_3=(m123**2+mpi**2-m12**2)/2/m123
      pp_3=dsqrt(om_3**2-mpi**2)
      cos_3=2*rand()-1
      phi_3=2*pi*rand()

      p_3(0)=om_3
      p_3(1)=pp_3*dsqrt(1-cos_3**2)*dcos(phi_3)
      p_3(2)=pp_3*dsqrt(1-cos_3**2)*dsin(phi_3)
      p_3(3)=pp_3*cos_3

      p_12(0)=p0cm(0)-p_3(0)
      p_12(1)=p0cm(1)-p_3(1)
      p_12(2)=p0cm(2)-p_3(2)
      p_12(3)=p0cm(3)-p_3(3)
      pp_12=dsqrt(p_12(1)**2+p_12(2)**2+p_12(3)**2)
      
      t1=(0.,0.)
      mu1=mu2-mp
      m1=mu1+t1

      om__2=m12**2/2/m12
      pp__2=dsqrt(om__2**2-mp**2)
      cos_2=2*rand()-1
      phi_2=2*pi*rand()

      p__2(0)=om__2
      p__2(1)=pp__2*dsqrt(1-cos_2**2)*dcos(phi_2)
      p__2(2)=pp__2*dsqrt(1-cos_2**2)*dsin(phi_2)
      p__2(3)=pp__2*cos_2

      CALL AXIAL(p_12,tet_12,phi_12)
      CALL RR(tet_12,phi_12,r12)

      b1=r12(1,1)*p__2(1)+r12(1,2)*p__2(2)+r12(1,3)*p__2(3)
      b2=r12(2,1)*p__2(1)+r12(2,2)*p__2(2)+r12(2,3)*p__2(3)
      b3=r12(3,1)*p__2(1)+r12(3,2)*p__2(2)+r12(3,3)*p__2(3)
      
      p__2(1)=b1
      p__2(2)=b2
      p__2(3)=b3

      p_2(0)=(p_12(0)*p__2(0)+p_12(1)*p__2(1)+p_12(2)*p__2(2)+
     *        p_12(3)*p__2(3))/m12
      lambda_2=(p_2(0)+p__2(0))/(p_12(0)+m12)
      p_2(1)=p__2(1)+p_12(1)*lambda_2
      p_2(2)=p__2(2)+p_12(2)*lambda_2
      p_2(3)=p__2(3)+p_12(3)*lambda_2 
      
      p0(0)=egl+md
      p0(1)=(0.,0.)
      p0(2)=(0.,0.)
      p0(3)=egl
      
      p2(0)=(p0(0)*p_2(0)+p0(1)*p_2(1)+p0(2)*p_2(2)+
     *       p0(3)*p_2(3))/m123
      lambda_1=(p2(0)+p_2(0))/(p0(0)+m123)
      p2(1)=p_2(1)+p0(1)*lambda_1
      p2(2)=p_2(2)+p0(2)*lambda_1
      p2(3)=p_2(3)+p0(3)*lambda_1 

      qq(0)=(p0(0)*p_3(0)+p0(1)*p_3(1)+p0(2)*p_3(2)+
     *       p0(3)*p_3(3))/m123
      lambda_1=(qq(0)+p_3(0))/(p0(0)+m123)
      qq(1)=p_3(1)+p0(1)*lambda_1
      qq(2)=p_3(2)+p0(2)*lambda_1
      qq(3)=p_3(3)+p0(3)*lambda_1

      p1(0)=md+egl-p2(0)-qq(0)
      p1(1)=-p2(1)-qq(1)
      p1(2)=-p2(2)-qq(2)
      p1(3)=egl-p2(3)-qq(3)

      p1_=dsqrt(p1(1)**2+p1(2)**2+p1(3)**2)
      p2_=dsqrt(p2(1)**2+p2(2)**2+p2(3)**2)
      q_=dsqrt(qq(1)**2+qq(2)**2+qq(3)**2)
      call axial(p1,tet1,phi1)
      call axial(p2,tet2,phi2)

      if (p1_.lt.340.or.p1_.gt.670) goto 1
      if (p2_.lt.340.or.p2_.gt.670) goto 1
      CALL TRIANGLE(Z)
      CALL TETBOARD(Z,TMIN,TMAX)    
      IF (TET1.LT.TMIN.OR.TET1.GT.TMAX) GOTO 1
      IF (TET2.LT.TMIN.OR.TET2.GT.TMAX) GOTO 1

      if (phi1/rgr.gt.0.and.phi1/rgr.lt.60.and.
     *    phi2/rgr.gt.180.and.phi2/rgr.lt.240) then

      do i1=0,3
      pp1(i1)=p1(i1)
      pp2(i1)=p2(i1)
      end do

      else if (phi1/rgr.gt.180.and.phi1/rgr.lt.240.and.
     *         phi2/rgr.gt.0.and.phi2/rgr.lt.60) then
      
      do i1=0,3
      pp2(i1)=p1(i1)
      pp1(i1)=p2(i1)
      end do

      phi1=phi2

      else 

      goto 1

      end if

      ph1=pi**3*t3**2/(2*m123*2)
      ph2=pp_3*pp__2/dsqrt(t3-t2)/dsqrt(t2)
      ros=ph1*ph2

      RETURN
      END


      SUBROUTINE RHOD(PZ,PZZ,TH,FH,RHO)
      implicit double precision (a-h,k,m,o-z)
      COMPLEX*16 RHO(-1:1,-1:1)

      rho(1,1)=(8 + Pzz + 12*Pz*dcos(tH) + 3*Pzz*dcos(2*tH))/24
      rho(1,0)=((Pz + Pzz*dcos(tH))*dsin(tH))/
     *       (2*Sqrt(2.)*cdexp((0.,1.)*fH))
      rho(1,-1)=Pzz*dsin(tH)**2/(4*cdexp(2*(0.,1.)*fH))
      rho(0,1)=(cdexp((0.,1.)*fH)*(Pz +
     *       Pzz*dcos(tH))*dsin(tH))/(2*Sqrt(2.))
      rho(0,0)=(4 - Pzz - 3*Pzz*dcos(2*tH))/12
      rho(0,-1)=((Pz - Pzz*dcos(tH))*dsin(tH))/
     *         (2*Sqrt(2.)*cdexp((0.,1.)*fH))
      rho(-1,1)=(cdexp(2*(0.,1.)*fH)*Pzz*dsin(tH)**2)/4
      rho(-1,0)=(cdexp((0.,1.)*fH)*(Pz - Pzz*dcos(tH))*
     * dsin(tH))/(2*Sqrt(2.))
      rho(-1,-1)=(8 + Pzz - 12*Pz*dcos(tH) +
     * 3*Pzz*dcos(2*tH))/24

      RETURN
      END

      SUBROUTINE RR(TH,PH,R)
      implicit double precision (a-h,k,m,o-z)
      DIMENSION R(1:3,1:3)
      PI=3.141592653589
      RGR=PI/180.

      R(1,1)=dcos(th)*dcos(ph)
      R(1,2)=-dsin(ph)
      R(1,3)=dsin(th)*dcos(ph)
      R(2,1)=dcos(th)*dsin(ph)
      R(2,2)=dcos(ph)
      R(2,3)=dsin(th)*dsin(ph)
      R(3,1)=-dsin(th)
      R(3,2)=(0.,0.)
      R(3,3)=dcos(th)

      RETURN
      END

      SUBROUTINE AXIAL(A,TET,FII)
      implicit double precision (a-h,k,m,o-z)
      DIMENSION A(0:3)
      PI=3.141592653589
      RGR=3.141592653589/180.
      AX=A(1)
      AY=A(2)
      AZ=A(3)
      AA=DSQRT(AX**2+AY**2+AZ**2)
      if (aa.eq.0) then
      tet=(0.,0.)
      fii=(0.,0.)
      goto 200
      end if  
 
      IF (ax.gt.0.and.ay.gt.0) then
      fi=atan(ay/ax)/rgr
      else if (ax.lt.0.and.ay.gt.0) then
      fi=90.-atan(ax/ay)/rgr
      else if (ax.lt.0.and.ay.lt.0) then
      fi=180.+atan(ay/ax)/rgr
      else if (ax.gt.0.and.ay.lt.0) then
      fi=270.-atan(ax/ay)/rgr  
      else if (ax.eq.0.and.ay.gt.0) then 
      fi=90.
      else if (ax.eq.0.and.ay.lt.0) then 
      fi=270.
      else if (ax.gt.0.and.ay.eq.0) then
      fi=0.
      else if (ax.lt.0.and.ay.eq.0) then 
      fi=180.
      else
      fi=180.
      END IF
      FII=FI*RGR
      TET=DACOS(AZ/AA)
  200 RETURN
      END



