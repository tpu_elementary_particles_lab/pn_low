C     test.f
C     
C     Copyright (C) 2009 Qiqi Wang
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 3 of the License, or (at
C     your option) any later version.
C     
C     This program is distributed in the hope that it will be useful, but
C     WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C     General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 51 Franklin Street, Fifth Floor,
C     Boston, MA 02110-1301, USA.

C     This is a demo for using MIR in Fortran.
C     Three subroutine are provided:
C     BINOMIAL_INV: refer to binomialInv in mir.h
C     MIR_EVALUATE: refer to function mirEvaluate in mir.h
C     MIR_BASIS: refer to function mirBasis in mir.h
C     MIR_BETA_GAMMA: refer to function mirBetaGamma in mir.h

      PROGRAM TEST

      IMPLICIT NONE

      INTEGER, PARAMETER :: WP = 8
      INTEGER, PARAMETER :: NV_MAX = 50, NX = 1000
      INTEGER :: I, NV, N, IERR
      REAL(WP), DIMENSION(NV_MAX) :: XV, FV, SIGV
      REAL(WP), DIMENSION(NX) :: X, FX, SIGMA, FX_EXACT
      REAL(WP) :: BETA, GAMM, ERROR
      REAL(WP), DIMENSION(0) :: NIL

      DO I = 1, NX
          X(I) = -5.0_WP + 10.0_WP * (I - 1) / (NX - 1)
          FX_EXACT(I) = 1.0_WP / (1.0_WP + X(I)**2)
      END DO

      DO NV = 3, 50, 4
          DO I = 1, NV
              XV(I) = -5.0_WP + 10.0_WP * (I - 1) / (NV - 1)
              FV(I) = 1.0_WP / (1.0_WP + XV(I)**2)
              SIGV(I) = 0.0_WP
          END DO

          ! INTERPOLATION
          WRITE (*, "('NV = ',I3)") NV
          N = NV
          IF (N > 100) N = 100
          CALL MIR_BETA_GAMMA(1, 1, NV, XV, FV, SIGV, 0, NIL, NIL, NIL,
     .                        N, 1, 1.0_WP, BETA, GAMM, IERR)
          CALL MIR_EVALUATE(1, 1, NX, X, NV, XV, FV, SIGV, 0, NIL, NIL,
     .                      NIL, BETA, GAMM, N, 1, FX, SIGMA, IERR)

          ! CALCULATE APPROXIMATION ERROR
          ERROR = 0.0_WP
          DO I = 1, NX
              ERROR = ERROR + (FX_EXACT(I) - FX(I)) ** 2
          END DO
          ERROR = SQRT(ERROR)
          WRITE (*,
     .      "('    BETA = ',F4.2,', GAMMA = 'F4.2,', ERROR = ',E8.2)")
     .      BETA, GAMM, ERROR
      END DO

      END PROGRAM TEST

