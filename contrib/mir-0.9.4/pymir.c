/* pymir.c
 * 
 * Copyright (C) 2009 Qiqi Wang
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <Python.h>
#include <numpy/arrayobject.h>

#include "mir.h"


static PyObject * _mirBasis(PyObject * self, PyObject * args)
{
    npy_intp ndim, N, P, nv, ng, nx;
    npy_intp dims[3];
    double beta, gamma;
    int ierr;

    PyObject * x;
    PyObject * xv, * sigv;
    PyObject * xg, * sigg;
    PyObject * av, *ag, * sigma, * ret;

    x = xv = sigv = xg = sigg = av = ag = sigma = NULL;

    if (! PyArg_ParseTuple(args, "OOOOOddii", &x, &xv,
                           &sigv, &xg, &sigg, &beta, &gamma, &N, &P))
    {
        return NULL;
    }

    x = PyArray_FROM_OTF(x, NPY_DOUBLE, NPY_IN_ARRAY);
    xv = PyArray_FROM_OTF(xv, NPY_DOUBLE, NPY_IN_ARRAY);
    sigv = PyArray_FROM_OTF(sigv, NPY_DOUBLE, NPY_IN_ARRAY);
    xg = PyArray_FROM_OTF(xg, NPY_DOUBLE, NPY_IN_ARRAY);
    sigg = PyArray_FROM_OTF(sigg, NPY_DOUBLE, NPY_IN_ARRAY);

    if (!(x && xv && sigv && xg && sigg)) {
        goto fail;
    }

    if (PyArray_NDIM(x) != 2 || PyArray_NDIM(xv) != 2 ||
        PyArray_NDIM(sigv) != 1 || PyArray_NDIM(xg) != 2 ||
        PyArray_NDIM(sigg) != 1)
    {
        PyErr_SetString(PyExc_ValueError,
                        "Arguments have wrong dimension");
        goto fail;
    }

    nx = PyArray_DIMS(x)[0];
    ndim = PyArray_DIMS(x)[1];
    nv = PyArray_DIMS(xv)[0];
    ng = PyArray_DIMS(xg)[0];

    if (PyArray_DIMS(xv)[1] != ndim || PyArray_DIMS(sigv)[0] != nv ||
        PyArray_DIMS(xg)[1] != ndim || PyArray_DIMS(sigg)[0] != ng)
    {
        PyErr_SetString(PyExc_ValueError,
                        "Arguments have wrong shape");
        goto fail;
    }

    dims[0] = nx;
    dims[1] = nv;
    av = PyArray_SimpleNew(2, dims, NPY_DOUBLE);
    dims[1] = ng;
    dims[2] = ndim;
    ag = PyArray_SimpleNew(3, dims, NPY_DOUBLE);
    sigma = PyArray_SimpleNew(1, &nx, NPY_DOUBLE);
    if (!(av && ag && sigma)) goto fail;

    ierr = mirBasis(ndim, nx, (double*) PyArray_DATA(x),
             nv, (double*) PyArray_DATA(xv), (double*) PyArray_DATA(sigv), 
             ng, (double*) PyArray_DATA(xg), (double*) PyArray_DATA(sigg),
             beta, gamma, N, P, (double*) PyArray_DATA(av),
             (double*) PyArray_DATA(ag), (double*) PyArray_DATA(sigma));
    if (ierr < 0) goto fail;

    ret = Py_BuildValue("OOO", av, ag, sigma);
    Py_XDECREF(x);
    Py_XDECREF(xv);
    Py_XDECREF(sigv);
    Py_XDECREF(xg);
    Py_XDECREF(sigg);
    Py_XDECREF(av);
    Py_XDECREF(ag);
    Py_XDECREF(sigma);
    return ret;

fail:
    Py_XDECREF(x);
    Py_XDECREF(xv);
    Py_XDECREF(sigv);
    Py_XDECREF(xg);
    Py_XDECREF(sigg);
    Py_XDECREF(av);
    Py_XDECREF(ag);
    Py_XDECREF(sigma);
    return NULL;
}


static PyObject * _mirBetaGamma(PyObject * self, PyObject * args)
{
    npy_intp nfunc, ndim, nv, ng, N, P;
    double safety, beta, gamma;
    int ierr;

    PyObject * xv, * fv, * sigv, * xg, * fg, * sigg, * ret;

    xv = fv = sigv = xg = fg = sigg = NULL;

    if (! PyArg_ParseTuple(args, "OOOOOOiid", &xv, &fv,
                           &sigv, &xg, &fg, &sigg, &N, &P, &safety))
    {
        return NULL;
    }

    xv = PyArray_FROM_OTF(xv, NPY_DOUBLE, NPY_IN_ARRAY);
    fv = PyArray_FROM_OTF(fv, NPY_DOUBLE, NPY_IN_ARRAY);
    sigv = PyArray_FROM_OTF(sigv, NPY_DOUBLE, NPY_IN_ARRAY);
    xg = PyArray_FROM_OTF(xg, NPY_DOUBLE, NPY_IN_ARRAY);
    fg = PyArray_FROM_OTF(fg, NPY_DOUBLE, NPY_IN_ARRAY);
    sigg = PyArray_FROM_OTF(sigg, NPY_DOUBLE, NPY_IN_ARRAY);

    if (!(xv && fv && sigv && xg && fg && sigg)) {
        goto fail;
    }

    if (PyArray_NDIM(xv) != 2 || PyArray_NDIM(fv) != 2 ||
        PyArray_NDIM(sigv) != 1 || PyArray_NDIM(xg) != 2 ||
        PyArray_NDIM(fg) != 3 || PyArray_NDIM(sigg) != 1)
    {
        PyErr_SetString(PyExc_ValueError, "Arguments have wrong dimension");
        goto fail;
    }

    nv = PyArray_DIMS(xv)[0];
    ng = PyArray_DIMS(xg)[0];
    ndim = PyArray_DIMS(xv)[1];
    nfunc = PyArray_DIMS(fv)[0];

    if (PyArray_DIMS(fv)[1] != nv || PyArray_DIMS(sigv)[0] != nv ||
        PyArray_DIMS(xg)[1] != ndim || PyArray_DIMS(fg)[0] != nfunc ||
        PyArray_DIMS(fg)[1] != ng || PyArray_DIMS(fg)[2] != ndim ||
        PyArray_DIMS(sigg)[0] != ng)
    {
        PyErr_SetString(PyExc_ValueError, "Arguments have wrong shape");
        goto fail;
    }

    ierr = mirBetaGamma(nfunc, ndim, nv, (double*) PyArray_DATA(xv),
                 (double*) PyArray_DATA(fv), (double*) PyArray_DATA(sigv),
                 ng, (double*) PyArray_DATA(xg), (double*) PyArray_DATA(fg),
                 (double*) PyArray_DATA(sigg), N, P, safety, &beta, &gamma);
    if (ierr < 0) {
        PyErr_SetString(PyExc_ValueError, "mirBetaGamma failed");
        goto fail;
    }

    ret = Py_BuildValue("dd", beta, gamma);
    Py_XDECREF(xv);
    Py_XDECREF(fv);
    Py_XDECREF(sigv);
    Py_XDECREF(xg);
    Py_XDECREF(fg);
    Py_XDECREF(sigg);
    return ret;

fail:
    Py_XDECREF(xv);
    Py_XDECREF(fv);
    Py_XDECREF(sigv);
    Py_XDECREF(xg);
    Py_XDECREF(fg);
    Py_XDECREF(sigg);
    return NULL;
}


static PyMethodDef _PY_MVINTERP_METHODS[] = {
    {"_mirBasis", _mirBasis, METH_VARARGS,
        "Bare minimum wrapper of the mirBasis C function in mir.c"},
    {"_mirBetaGamma", _mirBetaGamma, METH_VARARGS,
        "Bare minimum wrapper of the mirBetaGamma C function in mir.c"},
    {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC init_mir(void)
{
    Py_InitModule("_mir", _PY_MVINTERP_METHODS);
    import_array();
} 

