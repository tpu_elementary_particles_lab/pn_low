# test.py
# 
# Copyright (C) 2009 Qiqi Wang
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.

import pylab
import numpy
from numpy import zeros, arange, linspace, random, vstack, hstack, array

from mir import Mir

def test2d():
    SCALE = 1.0
    NX = 32

    # construct interpolation points
    x = zeros([NX**2, 2])
    for i in range(NX):
        x[i::NX, 0] = linspace(-SCALE, SCALE, NX)
        x[i*NX:(i+1)*NX, 1] = linspace(-SCALE, SCALE, NX)
        fxe = 1.0 / (1.0 + (x**2).sum(1))

    for nv in range(3, 18, 2):
        xv = zeros([nv**2, 2])
        for i in range(nv):
            xv[i::nv, 0] = linspace(-SCALE, SCALE, nv)
            xv[i*nv:(i+1)*nv, 1] = linspace(-SCALE, SCALE, nv)
        fv = 1.0 / (1.0 + (xv**2).sum(1))
        interp = Mir(xv, fv)
        fx = interp.evaluate(x)
        print interp.beta, interp.gamma, \
              numpy.sqrt(((fx - fxe)**2).sum() / fx.size), \
              numpy.abs(fx - fxe).max()

test2d()

