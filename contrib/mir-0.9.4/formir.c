/* formir.c
 * 
 * Copyright (C) 2009 Qiqi Wang
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "mir.h"

void binomial_inv(int* l, int* n, int* m)
{
    *m = binomialInv(*l, *n);
}

void binomial_inv_(int* l, int* n, int* m)
{
    *m = binomialInv(*l, *n);
}

void binomial_inv__(int* l, int* n, int* m)
{
    *m = binomialInv(*l, *n);
}


void mir_evaluate(int* nfunc, int* ndim, int* nx, double* x,
           int* nv, double* xv, double* fv, double* sigv,
           int* ng, double* xg, double* fg, double* sigg,
           double* beta, double* gamma, int* N, int* P, double* fx,
           double* sigma, int* ierr)
{
    *ierr = mirEvaluate(*nfunc, *ndim, *nx, x, *nv, xv, fv, sigv, *ng,
                        xg, fg, sigg, *beta, *gamma, *N, *P, fx, sigma);
}

void mir_evaluate_(int* nfunc, int* ndim, int* nx, double* x,
           int* nv, double* xv, double* fv, double* sigv,
           int* ng, double* xg, double* fg, double* sigg,
           double* beta, double* gamma, int* N, int* P, double* fx,
           double* sigma, int* ierr)
{
    *ierr = mirEvaluate(*nfunc, *ndim, *nx, x, *nv, xv, fv, sigv, *ng,
                        xg, fg, sigg, *beta, *gamma, *N, *P, fx, sigma);
}

void mir_evaluate__(int* nfunc, int* ndim, int* nx, double* x,
           int* nv, double* xv, double* fv, double* sigv,
           int* ng, double* xg, double* fg, double* sigg,
           double* beta, double* gamma, int* N, int* P, double* fx,
           double* sigma, int* ierr)
{
    *ierr = mirEvaluate(*nfunc, *ndim, *nx, x, *nv, xv, fv, sigv, *ng,
                        xg, fg, sigg, *beta, *gamma, *N, *P, fx, sigma);
}


void mir_basis(int* ndim, int* nx, double* x,
               int* nv, double* xv, double* sigv,
               int* ng, double* xg, double* sigg,
               double* beta, double* gamma, int* N, int* P,
               double* av, double* ag, double* sigma, int* ierr)
{
    *ierr = mirBasis(*ndim, *nx, x, *nv, xv, sigv, *ng, xg, sigg,
                     *beta, *gamma, *N, *P, av, ag, sigma);
}

void mir_basis_(int* ndim, int* nx, double* x,
               int* nv, double* xv, double* sigv,
               int* ng, double* xg, double* sigg,
               double* beta, double* gamma, int* N, int* P,
               double* av, double* ag, double* sigma, int* ierr)
{
    *ierr = mirBasis(*ndim, *nx, x, *nv, xv, sigv, *ng, xg, sigg,
                     *beta, *gamma, *N, *P, av, ag, sigma);
}

void mir_basis__(int* ndim, int* nx, double* x,
               int* nv, double* xv, double* sigv,
               int* ng, double* xg, double* sigg,
               double* beta, double* gamma, int* N, int* P,
               double* av, double* ag, double* sigma, int* ierr)
{
    *ierr = mirBasis(*ndim, *nx, x, *nv, xv, sigv, *ng, xg, sigg,
                     *beta, *gamma, *N, *P, av, ag, sigma);
}


void mir_beta_gamma(int* nfunc, int* ndim,
                    int* nv, double* xv, double* fv, double* sigv,
                    int* ng, double* xg, double* fg, double* sigg,
                    int* N, int* P, double* safety,
                    double* beta, double* gamma, int* ierr)
{
    *ierr = mirBetaGamma(*nfunc, *ndim, *nv, xv, fv, sigv, *ng, xg, fg, sigg,
                         *N, *P, *safety, beta, gamma);
}

void mir_beta_gamma_(int* nfunc, int* ndim,
                    int* nv, double* xv, double* fv, double* sigv,
                    int* ng, double* xg, double* fg, double* sigg,
                    int* N, int* P, double* safety,
                    double* beta, double* gamma, int* ierr)
{
    *ierr = mirBetaGamma(*nfunc, *ndim, *nv, xv, fv, sigv, *ng, xg, fg, sigg,
                         *N, *P, *safety, beta, gamma);
}

void mir_beta_gamma__(int* nfunc, int* ndim,
                    int* nv, double* xv, double* fv, double* sigv,
                    int* ng, double* xg, double* fg, double* sigg,
                    int* N, int* P, double* safety,
                    double* beta, double* gamma, int* ierr)
{
    *ierr = mirBetaGamma(*nfunc, *ndim, *nv, xv, fv, sigv, *ng, xg, fg, sigg,
                         *N, *P, *safety, beta, gamma);
}

