# setup.py
# 
# Copyright (C) 2009 Qiqi Wang
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.

from distutils.core import setup, Extension
import numpy

ext = Extension('_mir', ['mir.c', 'pymir.c'],
                include_dirs=[numpy.get_include()],
                libraries=['gsl', 'gslcblas', 'm'])

setup(name='mir', version='0.9.4',
      description='A Multivariate Interpolation and Regression scheme',
      author='Qiqi Wang', author_email='qiqi@mit.edu',
      url='http://mirscheme.sourceforge.net',
      ext_modules=[ext], py_modules=['mir'])
