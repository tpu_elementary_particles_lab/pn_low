# mir.py
# 
# Copyright (C) 2009 Qiqi Wang
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.

import numpy

from _mir import _mirBasis, _mirBetaGamma


def binomialInv(l, n):
    "Calculate the largest m such that _binomial(m,n) <= l."
    m, b = n, 1
    while b <= l:
        m += 1
        b = b * m / (m-n)
    return m - 1


class Mir(object):
    """
    xv is the ``value data points'', i.e. the points
        where the function value is available and given
        by fv; sigv estimates the standard deviation of
        the error in the function values; the default of
        sigv is 0 (fv is exact).
    xg is the ``gradient data points'', i.e. points
        where the function gradient is available and given
        by fg; sigg estimates the standard devisiton of
        the error in the gradient values; the default of
        sigg is 0 (fg is exact).
    beta is the `magnitude' of the target function,
        can be automatically calculated.
    gamma is the `wave number' of the target function,
        can be automatically calculated.
        Combined with beta, it provides an estimate of
        the derivative growth: f^(k) = O(beta * gamma**k)
        Larger gamma yields more conservative, more robust
        and lower order approximation.
    N is the order of the Taylor expansion, can be
        automatically calculated.  Smaller N yields lower
        order approximation.  Numerical instability may
        occur when N is too large.
    p is the polynomial order.  The interpolant is
        forced to interpolate order p-1 polynomials
        exactly.  p=1 is the most robust, higher p makes
        a difference only when gamma is large, or when
        data is sparse and oscilatory if gamma is
        automatically calculated.
    Reference:
      * Q.Wang et al. A Rational Interpolation Scheme with
        Super-polynomial Rate of Convergence.
    """

    def __init__(self, xv, fv, sigv=None, xg=None, fg=None, sigg=None, \
                 beta=None, gamma=None, N=None, P=1, safetyFactor=1.0):
        """
        __init__(self, xv, fv, sigv=None, xg=None, fg=None, sigg=None,
                 beta=None, gamma=None, N=None, P=1, safetyFactor=1.0):
        Instantiation function, see class documentation
            for arguments.
        When beta and gamma must be both None or both given.
            When they are None, their values are automatically
            calculated.  The calculation of gamma may take a
            while if the number of datapoints is large.
        """
        
        # save value data points
        self.xv = numpy.array(xv)
        if self.xv.ndim == 1:
            self.xv = self.xv.reshape([self.xv.size, 1])

        self.fv = numpy.array(fv)
        if self.fv.ndim == 1:
            self.fv = self.fv.reshape([1, self.fv.size])

        if sigv is None:
            self.sigv = numpy.zeros(self.nv())
        else:
            self.sigv = numpy.array(sigv)

        # save gradient data points
        if xg is None:
            self.xg = numpy.zeros([0, self.ndim()])
        else:
            self.xg = numpy.array(xg)
            if self.xg.ndim == 1:
                self.xg = self.xg.reshape([self.xg.size, 1])

        if fg is None:
            self.fg = numpy.zeros([self.nfunc(), 0, self.ndim()])
        else:
            self.fg = numpy.array(fg)
            if self.fg.ndim < 3:
                self.fg = self.fg.reshape([self.nfunc(), self.ng(),
                                           self.ndim()])

        if sigg is None:
            self.sigg = numpy.zeros(self.ng())
        else:
            self.sigg = numpy.array(sigg)

        # check dimensions and agreement of nfunc, ndim, nv, ng
        assert self.xv.ndim == self.xg.ndim == 2
        assert self.fv.ndim == 2 and self.fg.ndim == 3
        assert self.sigv.ndim == self.sigg.ndim == 1
        assert self.fv.shape[0] == self.fg.shape[0]
        assert self.xv.shape[1] == self.xg.shape[1] == self.fg.shape[2]
        assert self.sigv.shape[0] == self.xv.shape[0] == self.fv.shape[1]
        assert self.sigg.shape[0] == self.xg.shape[0] == self.fg.shape[1]

        # check and save safety factor (default 1.0)
        assert safetyFactor > 0.0
        self.safetyFactor = float(safetyFactor)

        # check and automatically calculate N
        if N is None:
            n = min(100, self.nv() + self.ng() * self.ndim())
            self.N = binomialInv(n, self.ndim()) - self.ndim() + 1
        else:
            self.N = N
        assert self.N > 0

        # set polynomial order (default 1)
        assert int(P) == P
        self.P = int(P)

        # automatically calculate beta and gamma
        if beta is None:
            assert gamma is None
            self.beta, self.gamma = self.calcBetaGamma()
        else:
            self.beta, self.gamma = float(beta), float(gamma)


    def nfunc(self):
        return self.fv.shape[0]

    def ndim(self):
        return self.xv.shape[1]

    def nv(self):
        return self.xv.shape[0]

    def ng(self):
        return self.xg.shape[0]

    def calcBetaGamma(self):
        return _mirBetaGamma(self.xv, self.fv, self.sigv, self.xg, self.fg, \
                             self.sigg, self.N, self.P, self.safetyFactor)

    def basis(self, x, squeeze=True):
        x = numpy.array(x)
        if x.ndim == 0:
            x = x.reshape([1, 1])
        elif x.ndim == 1 and self.ndim() == 1:
            x = x.reshape([x.size, 1])
        elif x.ndim == 1 and self.ndim() > 1:
            x = x.reshape([1, x.size])

        assert x.ndim == 2 and x.shape[1] == self.ndim()

        return _mirBasis(x, self.xv, self.sigv, self.xg, self.sigg,\
                        self.beta, self.gamma, self.N, self.P)

    def evaluate(self, x, fv=None, fg=None, returnSigma=False):
        if fv is None:
            fv = self.fv
        if fg is None:
            fg = self.fg
        av, ag, sigma = self.basis(x, squeeze=False)

        fx = numpy.zeros(sigma.size)
        for i in range(fx.size):
            fx[i] = numpy.dot(av[i].flat, numpy.asarray(fv).flat)
        if fg is not None:
            for i in range(fx.size):
                fx[i] += numpy.dot(ag[i].flat, numpy.asarray(fg).flat)
        else:
            assert ag.size == 0

        if returnSigma:
            return fx, sigma
        else:
            return fx

