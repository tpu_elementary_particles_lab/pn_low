#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include<gsl/gsl_randist.h>
#include<gsl/gsl_rng.h>
#include<gsl/gsl_qrng.h>

#include"mir.h"

void testMultiIndex()
{
    int n, ndim, N, i;
    int* indices;

    ndim = 4;
    N = 4;
    n = multiIndex(ndim, N, NULL, 0);
    indices = malloc(sizeof(int) * n * ndim);
    multiIndex(ndim, N, indices, ndim);
    for (i = 0; i < n * ndim; ++i) {
        if (i % ndim == 0) {
            printf("\n");
        }
        printf("%d ", indices[i]);
    }
    printf("\n\n%d\n\n", n);
    free(indices);
}

void testFactorial()
{
    int kappa[5] = {0,1,2,3,4};
    printf("%f\n", factorial(kappa, 5));
}

void testInterp()
{
    double x, fx, sigma;
    double xv[2] = {-1.0, 1.0};
    double fv[2] = {-1.0, 1.0};
    double sigv[2] = {0.0, 0.0};

    for (x = -1.5; x < 1.55; x += 0.1) {
        mirEvaluate(1, 1, 1, &x, 2, xv, fv, sigv, 0, NULL, NULL, NULL,
                    1.0, 0.5, 2, 1, &fx, &sigma);
        printf("%f, %f\n", x, fx);
    }
}

void test1d()
{
    const int NV_MAX = 50, NX = 1000;

    int i, nv, N;
    double xv[NV_MAX], fv[NV_MAX], sigv[NV_MAX];
    double x[NX], fx[NX], sigma[NX];
    double beta, gamma;
    double fxe, l2err;

    for (i = 0; i < NX; ++i) {
        x[i] = -5.0 + 10.0 * i / (NX - 1);
    }

    for (nv = 3; nv <= NV_MAX; nv += 4) {
        for (i = 0; i < nv; ++i) {
            xv[i] = -5.0 + 10.0 * i / (nv - 1);
            fv[i] = 1.0 / (1.0 + xv[i] * xv[i]);
            sigv[i] = 0.0;
        }
        /* interpolate */
        printf("nv = %d\n", nv);
        N = (nv > 100) ? 100 : nv;
        mirBetaGamma(1, 1, nv, xv, fv, sigv, 0, NULL, NULL, NULL,
                     N, 1, 1.0, &beta, &gamma);
        printf("    beta = %f, gamma = %f, ", beta, gamma);
        mirEvaluate(1, 1, NX, x, nv, xv, fv, sigv, 0, NULL, NULL, NULL,
                    beta, gamma, N, 1, fx, sigma);
        /* calculate error */
        l2err = 0.0;
        for (i = 0; i < NX; ++i) {
            fxe = 1.0 / (1.0 + x[i] * x[i]);
            l2err += (fx[i] - fxe) * (fx[i] - fxe);
        }
        l2err = sqrt(l2err / NX);
        printf("L2 error = %e\n", l2err);
    }
}

void grad1d()
{
    const int NV_MAX = 50, NX = 1000;

    int i, nv, N;
    double xv[NV_MAX], fv[NV_MAX], sigv[NV_MAX];
    double xg[NV_MAX], fg[NV_MAX], sigg[NV_MAX];
    double x[NX], fx[NX], sigma[NX];
    double beta, gamma;
    double fxe, l2err;

    for (i = 0; i < NX; ++i) {
        x[i] = -5.0 + 10.0 * i / (NX - 1);
    }

    for (nv = 3; nv <= NV_MAX; nv += 4) {
        for (i = 0; i < nv; ++i) {
            xv[i] = xg[i] = -5.0 + 10.0 * i / (nv - 1);
            fv[i] = 1.0 / (1.0 + xv[i] * xv[i]);
            fg[i] = -2.0 * xv[i] * fv[i] * fv[i];
            sigv[i] = 0.0;
            sigg[i] = 0.0;
        }
        /* interpolate */
        printf("nv = %d\n", nv);
        N = (nv > 100) ? 100 : nv;
        mirBetaGamma(1, 1, nv, xv, fv, sigv, nv, xg, fg, sigg,
                     N, 1, 1.0, &beta, &gamma);
        printf("    beta = %f, gamma = %f, ", beta, gamma);
        mirEvaluate(1, 1, NX, x, nv, xv, fv, sigv, nv, xg, fg, sigg,
                    beta, gamma, N, 1, fx, sigma);
        /* calculate error */
        l2err = 0.0;
        for (i = 0; i < NX; ++i) {
            fxe = 1.0 / (1.0 + x[i] * x[i]);
            l2err += (fx[i] - fxe) * (fx[i] - fxe);
        }
        l2err = sqrt(l2err / NX);
        printf("L2 error = %e\n", l2err);
    }
}

void stagger1d()
{
    const int NV_MAX = 50, NX = 1000;

    int i, nv, N;
    double tmp;
    double xv[NV_MAX], fv[NV_MAX], sigv[NV_MAX];
    double xg[NV_MAX - 1], fg[NV_MAX - 1], sigg[NV_MAX - 1];
    double x[NX], fx[NX], sigma[NX];
    double beta, gamma;
    double fxe, l2err;

    for (i = 0; i < NX; ++i) {
        x[i] = -5.0 + 10.0 * i / (NX - 1);
    }

    for (nv = 3; nv <= NV_MAX; nv += 4) {
        for (i = 0; i < nv; ++i) {
            xv[i] = -5.0 + 10.0 * i / (nv - 1);
            fv[i] = 1.0 / (1.0 + xv[i] * xv[i]);
            sigv[i] = 0.0;
            if (i > 0) {
                xg[i - 1] = 0.8 * xv[i] + 0.2 * xv[i - 1];
                tmp = 1.0 / (1.0 + xg[i - 1] * xg[i - 1]);
                fg[i - 1] = -2.0 * xg[i - 1] * tmp * tmp;
                sigg[i - 1] = 0.0;
            }
        }
        /* interpolate */
        printf("nv = %d\n", nv);
        N = (nv > 100) ? 100 : nv;
        mirBetaGamma(1, 1, nv, xv, fv, sigv, nv - 1, xg, fg, sigg,
                     N, 1, 1.0, &beta, &gamma);
        printf("    beta = %f, gamma = %f, ", beta, gamma);
        mirEvaluate(1, 1, NX, x, nv, xv, fv, sigv, nv - 1, xg, fg, sigg,
                    beta, gamma, N, 1, fx, sigma);
        /* calculate error */
        l2err = 0.0;
        for (i = 0; i < NX; ++i) {
            fxe = 1.0 / (1.0 + x[i] * x[i]);
            l2err += (fx[i] - fxe) * (fx[i] - fxe);
        }
        l2err = sqrt(l2err / NX);
        printf("L2 error = %e\n", l2err);
    }
}

void regress1d()
{
    const int NV_MAX = 50, NX = 1000;
    const double STD = 0.1;

    int i, nv, N;
    double xv[NV_MAX], fv[NV_MAX], sigv[NV_MAX];
    double x[NX], fx[NX], sigma[NX];
    double beta, gamma;
    double fxe, l2err;

    gsl_rng * rng = gsl_rng_alloc(gsl_rng_ranlxd2);

    for (i = 0; i < NX; ++i) {
        x[i] = -5.0 + 10.0 * i / (NX - 1);
    }

    for (nv = 3; nv <= NV_MAX; nv += 4) {
        for (i = 0; i < nv; ++i) {
            xv[i] = -5.0 + 10.0 * i / (nv - 1);
            fv[i] = 1.0 / (1.0 + xv[i] * xv[i])
                  + gsl_ran_gaussian(rng, STD);
            sigv[i] = 0.0;
        }
        /* interpolate */
        printf("nv = %d\n", nv);
        N = (nv > 100) ? 100 : nv;
        mirBetaGamma(1, 1, nv, xv, fv, sigv, 0, NULL, NULL, NULL,
                     N, 1, 1.0, &beta, &gamma);
        printf("    beta = %f, gamma = %f, ", beta, gamma);
        mirEvaluate(1, 1, NX, x, nv, xv, fv, sigv, 0, NULL, NULL, NULL,
                     beta, gamma, N, 1, fx, sigma);
        /* calculate error */
        l2err = 0.0;
        for (i = 0; i < NX; ++i) {
            fxe = 1.0 / (1.0 + x[i] * x[i]);
            l2err += (fx[i] - fxe) * (fx[i] - fxe);
        }
        l2err = sqrt(l2err / NX);
        printf("L2 error = %e\n", l2err);
    }

    gsl_rng_free(rng);
}

void test2d()
{
    const int NV_MAX = 100, NX = 32, NX2 = NX * NX;
    const double SCALE = 5.0;

    int i, j, k, ij, nv, nv2, N;
    double xv[NV_MAX][2], fv[NV_MAX], sigv[NV_MAX];
    double x[NX2][2], fx[NX2], sigma[NX2];
    double beta, gamma;
    double fxe, l2err, lierr;

    FILE * f = fopen("test2d.txt", "w");

    /* interpolation points */
    for (i = 0; i < NX; ++i) {
        for (j = 0; j < NX; ++j) {
            ij = i * NX + j;
            x[ij][0] = SCALE * (-1.0 + i * 2.0 / (NX - 1));
            x[ij][1] = SCALE * (-1.0 + j * 2.0 / (NX - 1));
        }
    }

    for (nv = 3; nv * nv <= NV_MAX; nv += 2) {

        fprintf(f, "\nnv = %d\n", nv);
        nv2 = nv * nv;

        /* data points */
        for (i = 0; i < nv; ++i) {
            for (j = 0; j < nv; ++j) {
                ij = i * nv + j;
                xv[ij][0] = SCALE * (-1.0 + i * 2.0 / (nv - 1));
                xv[ij][1] = SCALE * (-1.0 + j * 2.0 / (nv - 1));
                fv[ij] = 1.0;
                for (k = 0; k < 2; ++k) {
                    fv[ij] += xv[ij][k] * xv[ij][k];
                }
                fv[ij] = 1.0 / fv[ij];
                sigv[ij] = 0.0;
                fprintf(f, "%5i (%5.2f,%5.2f) %5.2f\n",
                        ij, xv[ij][0], xv[ij][1], fv[ij]);
            }
        }

        /* interpolate */
        printf("nv = %d\n", nv2);
        N = (nv2 > 100) ? 100 : nv2;
        N = binomialInv(N, 2) - 1;
        mirBetaGamma(1, 2, nv2, (double*)xv, fv, sigv, 0, NULL, NULL, NULL,
                     N, 1, 1.0, &beta, &gamma);
        printf("    beta = %f, gamma = %f, ", beta, gamma);
        mirEvaluate(1, 2, NX2, (double*)x, nv2, (double*)xv, fv, sigv,
                    0, NULL, NULL, NULL, beta, gamma, N, 1, fx, sigma);
        /* calculate error */
        l2err = lierr = 0.0;
        for (i = 0; i < NX2; ++i) {
            fxe = 1.0;
            for (j = 0; j < 2; ++j) {
                fxe += x[i][j] * x[i][j];
            }
            fxe = 1.0 / fxe;
            l2err += (fx[i] - fxe) * (fx[i] - fxe);
            lierr = (fabs(fx[i] - fxe) > lierr) ? fabs(fx[i] - fxe) : lierr;
            fprintf(f, "%5i (%5.2f,%5.2f) %5.2f -- %5.2f: %5.2f\n",
                    i, x[i][0], x[i][1], fxe, fx[i], fabs(fx[i] - fxe));
        }
        l2err = sqrt(l2err / NX2);
        printf("Linf, L2 error =\n%e %e\n", lierr, l2err);
    }

    fclose(f);
}

void qrngcart2d()
{
    const int NV_MAX = 400, NX = 16, NX2 = NX * NX;
    const double SCALE = 5.0;

    int i, j, ij, nv, nv2, N;
    double xv[NV_MAX][2], fv[NV_MAX], sigv[NV_MAX];
    double x[NX2][2], fx[NX2], sigma[NX2];
    double beta, gamma;
    double fxe, l2err, lierr;

    FILE * f = fopen("test2d.txt", "w");

    gsl_qrng * qrng0 = gsl_qrng_alloc(gsl_qrng_niederreiter_2, 2);  
    gsl_qrng * qrng1 = gsl_qrng_alloc(gsl_qrng_niederreiter_2, 2);  

    /* interpolation points */
    for (i = 0; i < NX; ++i) {
        for (j = 0; j < NX; ++j) {
            ij = i * NX + j;
            x[ij][0] = SCALE * (-1.0 + i * 2.0 / (NX - 1));
            x[ij][1] = SCALE * (-1.0 + j * 2.0 / (NX - 1));
        }
    }

    for (nv = 3; nv * nv <= NV_MAX; nv += 2) {
        nv2 = nv * nv;
        gsl_qrng_memcpy(qrng1, qrng0);

        /* data points */
        for (i = 0; i < nv2; ++i) {
            gsl_qrng_get(qrng1, &xv[i][0]);
            for (j = 0; j < 2; ++j) {
                xv[i][j] -= 0.5;
                xv[i][j] *= 2.0 * SCALE;
            }
            fv[i] = 1.0;
            for (j = 0; j < 2; ++j) {
                fv[i] += xv[i][j] * xv[i][j];
            }
            fv[i] = 1.0 / fv[i];
            sigv[i] = 0.0;
        }

        /* interpolate */
        N = (nv2 > 100) ? 100 : nv2;
        N = binomialInv(N, 2) - 1;
        printf("nv = %d, N = %d\n", nv2, N);
        mirBetaGamma(1, 2, nv2, (double*)xv, fv, sigv, 0, NULL, NULL, NULL,
                     N, 1, 1.0, &beta, &gamma);
        printf("    beta = %f, gamma = %f, ", beta, gamma);
        mirEvaluate(1, 2, NX2, (double*)x, nv2, (double*)xv, fv, sigv,
                    0, NULL, NULL, NULL, beta, gamma, N, 1, fx, sigma);
        /* calculate error */
        l2err = lierr = 0.0;
        fprintf(f, "\nnv = %d\n", nv2);
        for (i = 0; i < NX2; ++i) {
            fxe = 1.0;
            for (j = 0; j < 2; ++j) {
                fxe += x[i][j] * x[i][j];
            }
            fxe = 1.0 / fxe;
            l2err += (fx[i] - fxe) * (fx[i] - fxe);
            lierr = (fabs(fx[i] - fxe) > lierr) ? fabs(fx[i] - fxe) : lierr;
            fprintf(f, "%5i (%5.2f,%5.2f) %5.2f -- %5.2f: %5.2f\n",
                    i, x[i][0], x[i][1], fxe, fx[i], fabs(fx[i] - fxe));
        }
        l2err = sqrt(l2err / NX2);
        printf("Linf, L2 error =\n%e %e\n", lierr, l2err);
    }

    fclose(f);

    gsl_qrng_free(qrng0);
    gsl_qrng_free(qrng1);
}

void qrng2d()
{
    const int NV_MAX = 500, NX = 100;
    const double SCALE = 5.0;

    int i, j, nv, nv2, N;
    double xv[NV_MAX][2], fv[NV_MAX], sigv[NV_MAX];
    double x[NX][2], fx[NX], sigma[NX];
    double beta, gamma;
    double fxe, l2err, lierr;

    FILE * f = fopen("test2d.txt", "w");

    gsl_qrng * qrng0 = gsl_qrng_alloc(gsl_qrng_niederreiter_2, 2);  
    gsl_qrng * qrng1 = gsl_qrng_alloc(gsl_qrng_niederreiter_2, 2);  

    for (nv = 3; nv * nv <= NV_MAX; nv += 2) {
        nv2 = nv * nv;
        gsl_qrng_memcpy(qrng1, qrng0);

        /* data points */
        for (i = 0; i < nv2; ++i) {
            gsl_qrng_get(qrng1, &xv[i][0]);
            for (j = 0; j < 2; ++j) {
                xv[i][j] -= 0.5;
                xv[i][j] *= 2.0 * SCALE;
            }
            fv[i] = 1.0;
            for (j = 0; j < 2; ++j) {
                fv[i] += xv[i][j] * xv[i][j];
            }
            fv[i] = 1.0 / fv[i];
            sigv[i] = 0.0;
        }

        /* interpolation points */
        for (i = 0; i < NX; ++i) {
            gsl_qrng_get(qrng1, &x[i][0]);
            for (j = 0; j < 2; ++j) {
                x[i][j] -= 0.5;
                x[i][j] *= 2.0 * SCALE;
            }
        }

        /* interpolate */
        N = (nv2 > 100) ? 100 : nv2;
        N = binomialInv(N, 2) - 1;
        printf("nv = %d, N = %d\n", nv2, N);
        mirBetaGamma(1, 2, nv2, (double*)xv, fv, sigv, 0, NULL, NULL, NULL,
                     N, 1, 1.0, &beta, &gamma);
        printf("    beta = %f, gamma = %f, ", beta, gamma);
        mirEvaluate(1, 2, NX, (double*)x, nv2, (double*)xv, fv, sigv,
                    0, NULL, NULL, NULL, beta, gamma, N, 1, fx, sigma);
        /* calculate error */
        l2err = lierr = 0.0;
        fprintf(f, "\nnv = %d\n", nv2);
        for (i = 0; i < NX; ++i) {
            fxe = 1.0;
            for (j = 0; j < 2; ++j) {
                fxe += x[i][j] * x[i][j];
            }
            fxe = 1.0 / fxe;
            lierr = (fabs(fx[i] - fxe) > lierr) ? fabs(fx[i] - fxe) : lierr;
            l2err += (fx[i] - fxe) * (fx[i] - fxe);
            fprintf(f, "%5i (%5.2f,%5.2f) %5.2f -- %5.2f: %5.2f\n",
                    i, x[i][0], x[i][1], fxe, fx[i], fabs(fx[i] - fxe));
        }
        l2err = sqrt(l2err / NX);
        printf("Linf, L2 error =\n%e %e\n", lierr, l2err);
    }

    fclose(f);

    gsl_qrng_free(qrng0);
    gsl_qrng_free(qrng1);
}

void qrng3d()
{
    const int NV_MAX = 1500, NX = 10;
    const double SCALE = 5.0;

    int i, j, nv, nv3, N;
    double xv[NV_MAX][3], fv[NV_MAX], sigv[NV_MAX];
    double x[NX][3], fx[NX], sigma[NX];
    double beta, gamma;
    double fxe, l2err, lierr;

    FILE * f = fopen("test3d.txt", "w");

    gsl_qrng * qrng0 = gsl_qrng_alloc(gsl_qrng_niederreiter_2, 3);  
    gsl_qrng * qrng1 = gsl_qrng_alloc(gsl_qrng_niederreiter_2, 3);  

    for (nv = 3; nv * nv * nv <= NV_MAX; nv += 1) {
        nv3 = nv * nv * nv;
        gsl_qrng_memcpy(qrng1, qrng0);

        /* data points */
        for (i = 0; i < nv3; ++i) {
            gsl_qrng_get(qrng1, &xv[i][0]);
            for (j = 0; j < 3; ++j) {
                xv[i][j] -= 0.5;
                xv[i][j] *= 2.0 * SCALE;
            }
            fv[i] = 1.0;
            for (j = 0; j < 3; ++j) {
                fv[i] += xv[i][j] * xv[i][j];
            }
            fv[i] = 1.0 / fv[i];
            sigv[i] = 0.0;
        }

        /* interpolation points */
        for (i = 0; i < NX; ++i) {
            gsl_qrng_get(qrng1, &x[i][0]);
            for (j = 0; j < 3; ++j) {
                x[i][j] -= 0.5;
                x[i][j] *= 2.0 * SCALE;
            }
        }

        /* interpolate */
        N = (nv3 > 100) ? 100 : nv3;
        N = binomialInv(N, 3) - 2;
        printf("nv = %d, N = %d\n", nv3, N);
        //mirBetaGamma(1, 3, nv3, (double*)xv, fv, sigv, 0, NULL, NULL, NULL,
        //             N, 1, 1.0, &beta, &gamma);
        beta = 0.2;
        gamma = 1.0;
        printf("    beta = %f, gamma = %f, ", beta, gamma);
        mirEvaluate(1, 3, NX, (double*)x, nv3, (double*)xv, fv, sigv,
                    0, NULL, NULL, NULL, beta, gamma, N, 1, fx, sigma);
        /* calculate error */
        l2err = lierr = 0.0;
        fprintf(f, "\nnv = %d\n", nv3);
        for (i = 0; i < NX; ++i) {
            fxe = 1.0;
            for (j = 0; j < 3; ++j) {
                fxe += x[i][j] * x[i][j];
            }
            fxe = 1.0 / fxe;
            lierr = (fabs(fx[i] - fxe) > lierr) ? fabs(fx[i] - fxe) : lierr;
            l2err += (fx[i] - fxe) * (fx[i] - fxe);
            fprintf(f, "%5i (%5.2f,%5.2f) %5.2f -- %5.2f: %5.2f\n",
                    i, x[i][0], x[i][1], fxe, fx[i], fabs(fx[i] - fxe));
        }
        l2err = sqrt(l2err / NX);
        printf("Linf, L2 error =\n%e %e\n", lierr, l2err);
    }

    fclose(f);

    gsl_qrng_free(qrng0);
    gsl_qrng_free(qrng1);
}

/*
void testReconstruct1d()
{
    const int nx_max = 60;
    int i, nx;
    double x[nx_max], fg[nx_max], f[nx_max], fr[nx_max], r[nx_max];
    double rMean, rNorm2, rNorm1, rNormI;

    for (nx = 4; nx <= nx_max; nx += 2) {
        for (i = 0; i < nx; ++ i) {
            x[i] = i / (double) (nx - 1) * 10 - 5;
            f[i] = 1.0 / (1.0 + x[i] * x[i]);
            fg[i] = -2.0 * x[i] * f[i] * f[i];
        }

        mirReconstruct(1, nx, x, fg, nx, 1, fr);
        
        rMean = 0.0;
        for (i = 0; i < nx; ++ i) {
            r[i] = fr[i] - f[i];
            rMean += r[i];
        }
        rMean /= nx;

        rNorm2 = rNorm1 = rNormI = 0.0;
        for (i = 0; i < nx; ++ i) {
            r[i] -= rMean;
            rNorm2 += r[i] * r[i];
            rNorm1 += fabs(r[i]);
            rNormI = (rNormI > fabs(r[i])) ? rNormI : fabs(r[i]);
        }
        rNorm2 = sqrt(rNorm2);
        printf("%3d %e, %e, %e\n", nx, rNorm2, rNorm1, rNormI);
    }
}

void testReconstruct2d()
{
    const int nx_max = 60;
    int i, nx;
    double x[nx_max], fg[nx_max], f[nx_max], fr[nx_max], r[nx_max];
    double rMean, rNorm2, rNorm1, rNormI;

    gsl_qrng * qrng0 = gsl_qrng_alloc(gsl_qrng_niederreiter_2, 2);  
    gsl_qrng * qrng1 = gsl_qrng_alloc(gsl_qrng_niederreiter_2, 2);  

    for (nx = 4; nx <= nx_max; nx += 2) {
        gsl_qrng_memcpy(qrng1, qrng0);
        for (i = 0; i < nx; ++ i) {
            x[i] = i / (double) (nx - 1) * 10 - 5;
            f[i] = 1.0 / (1.0 + x[i] * x[i]);
            fg[i] = -2.0 * x[i] * f[i] * f[i];
        }

        mirReconstruct(1, nx, x, fg, nx, 1, fr);
        
        rMean = 0.0;
        for (i = 0; i < nx; ++ i) {
            r[i] = fr[i] - f[i];
            rMean += r[i];
        }
        rMean /= nx;

        rNorm2 = rNorm1 = rNormI = 0.0;
        for (i = 0; i < nx; ++ i) {
            r[i] -= rMean;
            rNorm2 += r[i] * r[i];
            rNorm1 += fabs(r[i]);
            rNormI = (rNormI > fabs(r[i])) ? rNormI : fabs(r[i]);
        }
        printf("%3d %e, %e, %e\n", nx, rNorm2, rNorm1, rNormI);
    }
}
*/

# if 0
int main()
{
    test1d();
    grad1d();
    stagger1d();
    regress1d();
    test2d();
    qrngcart2d();
    qrng2d();
    qrng3d();
    /*
    testReconstruct1d();
    testReconstruct2d();
    */
    return 0;
}
# else
int wang_test_C()
{
    /*
    test1d();
    grad1d();
    stagger1d();
    regress1d();
    */
    test2d();
    /*
    qrngcart2d();
    qrng2d();
    qrng3d();
    */
    /*
    testReconstruct1d();
    testReconstruct2d();
    */
    return 0;
}
# endif
