# ifndef H_PN_LOW_RUN_MANAGER_H
# define H_PN_LOW_RUN_MANAGER_H

# include "app_config.h"
# include "app.hpp"

# ifdef WITH_GEANT4_MODEL

# include <G4RunManager.hh>
# include <G4StateManager.hh>
# include <G4UserEventAction.hh>
# include <G4VUserPrimaryGeneratorAction.hh>

namespace G4I {

class RunManager : public G4RunManager {
public:
    typedef G4RunManager Parent;
public:
    RunManager( const po::variables_map & vm );
    virtual ~RunManager();

    //
    // G4RunManager overriden
    //

    /// Main entry point of Geant4 kernel initialization.
    //virtual void Initialize();
    /// Main entry point of the event loop.
    //virtual void BeamOn(G4int n_event);
    /// Set the world volume to G4Navigator.
    //virtual void DefineWorldVolume(G4VPhysicalVolume * worldVol);
    /// Abort the run.
    //virtual void AbortRun();
protected:
    /// Geometry construction
    //virtual void InitializeGeometry();
    /// Physics processes construction.
    //virtual void InitializePhysics();
    /// Check the kernel conditions for the event loop.
    //virtual G4bool ConfirmBeamOnCondition();
    /// Prepare a run.
    virtual void RunInitialization() override;
    /// Manage an event loop.
    virtual void DoEventLoop(G4int n_event, const char* macroFile, G4int n_select) override;
    /// Generation of G4Event object.
    virtual G4Event* GenerateEvent(G4int i_event) override;
    /// Storage/analysis of an event.
    //virtual void AnalyzeEvent(G4Event* anEvent);
    /// Terminate a run.
    virtual void RunTermination() override;
};

# if 0  // Keep RIA's copy for references.

class ParRunManager : public G4RunManager {
protected:
    virtual void DoEventLoop(G4int n_event,
                             const char* macroFile=0,
                             G4int n_select=-1) override;

private:  // TOP-C callbacks
    TOPC_BUF GenerateEventInput();
    TOPC_BUF DoEvent( void * input_buf );
    TOPC_ACTION CheckEventResult( void * input_buf, void * ignore );

    static ParRunManager* myRunManager;
    static TOPC_BUF MyGenerateEventInput();
    static TOPC_BUF MyDoEvent( void * input_buf );
    static TOPC_ACTION MyCheckEventResult( void * input_buf, void * ignore );

    // Copies of local variables of G4RunManager::DoEventLoop()
    static G4StateManager* stateManager;
    static G4int n_event;
    static G4int n_select;
    static G4String msg;

    inline void ImportDoEventLoopLocals(
	    G4StateManager* stateManager, G4int n_event,
	    G4int n_select, G4String msg ) {
            ParRunManager::stateManager = stateManager;
            ParRunManager::n_event = n_event;
            ParRunManager::n_select = n_select;
            ParRunManager::msg = msg;
            ParRunManager::myRunManager = this;
        }

    // Copy of userEventAction:  see ParRunManager.cc for further comments
    G4UserEventAction * origUserEventAction;
    
    // RIA: this method replaces original one, forcing a slave 
    //      to use InsertPrimaries()
    G4Event* MyGenerateEvent(void *ibuf);
};

# endif

}  // namespace G4I

# endif  // WITH_GEANT4_MODEL

# endif  // H_PN_LOW_RUN_MANAGER_H

