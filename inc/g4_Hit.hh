# ifndef H_PN_LOW_G4_EVENT_HIT_H
# define H_PN_LOW_G4_EVENT_HIT_H


# include "app_config.h"
# include "app.hpp"

# ifdef WITH_GEANT4_MODEL

# include <G4VHit.hh>
# include <G4THitsCollection.hh>
# include <G4Allocator.hh>
# include <G4RotationMatrix.hh>
# include <G4ThreeVector.hh>

namespace G4I {

class Hit : public G4VHit {
# define for_all_hit_fields(m)         \
    m( G4double,         halflength )  \
    m( G4double,         absorbtion )  \
    m( G4double,         threshold  )  \
    /* ----------------------------*/  \
    m( G4double,         pde        )  \
    m( G4double,         Edep       )  \
    m( G4double,         LO         )  \
    m( G4double,         A1         )  \
    m( G4double,         A2         )  \
    m( G4double,         ToF        )  \
    /* ----------------------------*/  \
    m( G4ThreeVector,    Pos        )  \
    m( G4ThreeVector,    Vpos       )  \
    m( G4RotationMatrix, Vrot       )  \
    m( G4int,            blkN       )  \
    m( G4int,            Nprim      )  \
    m( G4bool,           Trig       )  \
    /* ... */
private:
    # define declare_field( type, name ) \
    type _ ## name;
    for_all_hit_fields(declare_field)
    # undef declare_field
public:
    /// Default ctr
    Hit();
    Hit( G4double l,
         G4double a,
         G4double thr,
         G4ThreeVector vp,
         const G4RotationMatrix *rr );
    /// 
    //Hit(G4double, G4double, G4double, G4ThreeVector, const G4RotationMatrix*);
    //Hit(G4ThreeVector, const G4RotationMatrix*);

    # define declare_getter_setter(type, name)      \
    const type & name() const { return _ ## name; } \
    void name( const type & o ) { _ ## name = o; }
    for_all_hit_fields(declare_getter_setter)
    # undef declare_getter_setter

    void add_Edep(G4double de) {_Edep += de;};
    void add_Pos(G4double de, G4ThreeVector pos, G4int n);
    void add_Pos(G4double de, G4ThreeVector pos, G4int n, G4double t=0.);
    void add_T(G4double t);
    void add_LO(G4double de, G4ThreeVector pos, G4ThreeVector delta);

    inline void* operator new(size_t);
    inline void  operator delete(void*);
};  // class Hit

extern G4Allocator<Hit> HitAllocator;

inline void* Hit::operator new(size_t) {
  void* aHit;
  aHit = (void*) HitAllocator.MallocSingle();
  return aHit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void Hit::operator delete(void* aHit) {
  HitAllocator.FreeSingle((Hit*) aHit);
}

}  // namespace G4I

# endif  // WITH_GEANT4_MODEL

# endif  // H_PN_LOW_G4_EVENT_HIT_H

