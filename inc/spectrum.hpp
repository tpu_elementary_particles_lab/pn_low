# ifndef H_PNLOW_SPECTRUM_H
# define H_PNLOW_SPECTRUM_H

# include "app_config.h"

# include <root/TFoamIntegrand.h>
# include <root/TFoam.h>
# include <gsl/gsl_spline.h>

# include "app.hpp"

namespace pnLow {

/**@brief A probability density function template abstraction.
 *
 * */
template<uint8_t D, typename VectorT >
class PDF {
public:
    typedef VectorT Vector;
protected:
    /// An interface function: returns density at given point {x1, x2 ... x_D}.
    virtual double _V_density( const Vector & ) const = 0;
public:
    double denisty( const Vector & x ) const { return _V_density(x); }
    virtual ~PDF(){}
};

template<>
class PDF<1, double> {
public:
    typedef double Vector;
protected:
    /// An interface function: returns density at given point {x1, x2 ... x_D}.
    virtual double _V_density( const Vector & ) const = 0;
public:
    double density( const Vector & x ) const { return _V_density(x); }
    virtual ~PDF(){}
};

class SmoothSpectrum : public PDF<1, double> {
protected:
    std::map<double, double> _originalData;
    /// Returns density (note: requires interpolation be provided!)
    virtual double _V_density( const Vector & ) const override;
private:
    gsl_interp_accel * _acc;
    gsl_spline       * _spline;
    double             _min, _max;  ///< argument range
    double             _scale;
protected:
    void _correct_range( double );
public:
    SmoothSpectrum();
    SmoothSpectrum( const std::map<double, double> & origSet );
    virtual ~SmoothSpectrum();

    /// Helps to keep it tight and clean.
    void clear_original_data() { _originalData.clear(); }

    /// Adds breakpoint (GSL B-spline terminology).
    void insert_point( double x, double f );

    /// Invokes approximation routines, -1 for error, 0 is ok.
    int approximate();

    /// Writes min/max range into given variables.
    double get_range( double & min, double & max ) const {
        return (max = _max) - (min = _min);
    }

    /// Renorms generated onto range.
    double denormalize( double v ) const {
        return v/_scale + _min;
    }
    bool is_good() const { return _acc && _spline; }
};

//
// Connection with ROOT's FOAM wrapper
//

template<uint8_t D>
class PDEBasedGenerator : public TFoamIntegrand {
public:
    struct Event { Double_t v[D]; };
    typedef PDF<D, double> ThisPDF;
private:
    ThisPDF & _PDF;
    TFoam   * _sim;
    bool _simInited;
protected:
    Double_t Density(int nDim, Double_t *Xarg) override {
        ((void) nDim);
        Event ev;
        memcpy( ev.v, Xarg, sizeof(Double_t)*D );
        return _PDF.density( ev.v[0] );
    }
    ThisPDF & pdf() { return _PDF; }
    TFoam & simulator() { return *_sim; }
public:
    PDEBasedGenerator( ThisPDF & pdfI,
                       const std::string tFoamName="FoamX",
                       int TFoamChatLevel=3 ) :
                            _PDF(pdfI), _sim(nullptr) {
        _sim = new TFoam( tFoamName.c_str() );
        _sim->SetkDim( D );
        _sim->SetRho( this );
        _sim->SetChat(TFoamChatLevel);
    }
    ~PDEBasedGenerator() {}

    /// Generates and writes event at given instance. Returns event's weight.
    virtual double generate( Event & ev ) const {
        assert( _sim );
        _sim->MakeEvent();
        _sim->GetMCvect( ev.v );
        return _sim->GetMCwt();
    }

    const ThisPDF & pdf() const { return _PDF; }
    const TFoam & simulator() const { return *_sim; }
};

}  // namespace pnLow

# endif  // H_PNLOW_SPECTRUM_H

