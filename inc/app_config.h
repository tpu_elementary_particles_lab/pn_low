# ifndef H_PN_LOW_APP_CONFIG_H
# define H_PN_LOW_APP_CONFIG_H

// A CMake-template file to reflect current build configuration.

//
// Build options
//

#define WITH_GEANT4_MODEL 1
#define WITH_GEANT4_UIVIS 2
#define ARENHOVEL_FIX_GEN 3
#define PN_LOW_EXPDAT_GEN 4

# define BUILD_C_FLAGS       -g -fno-omit-frame-pointer   -Wall -std=c99     -D_GNU_SOURCE -fexceptions -pthread -D_FILE_OFFSET_BITS=64 -rdynamic -DHAVE_INLINE -I
# define BUILD_CXX_FLAGS    -march=k8 -O2 -pipe -W -Wall -pedantic -Wno-non-virtual-dtor -Wno-long-long -Wwrite-strings -Wpointer-arith -Woverloaded-virtual -pipe -std=c++98 -g -fno-omit-frame-pointer -Wall -std=gnu++11 -D_GNU_SOURCE -fexceptions -pthread -D_FILE_OFFSET_BITS=64 -rdynamic -I
# define BUILDER_HOSTNAME   
# define CMAKE_SYSTEM       Linux-3.12.21-gentoo-r1
# define BUILD_TYPE         Debug

# define PN_LOW_DEUTERON_MASS_MEV   1875.612859
# define PN_LOW_PROTON_MASS_MEV     938.272046
# define PN_LOW_NEUTERON_MASS_MEV   939.56537
# define PN_LOW_PI0_MASS_MEV        134.9766

# endif   // H_PN_LOW_APP_CONFIG_H

