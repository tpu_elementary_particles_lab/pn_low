# ifndef H_PN_LOW_G4_EVENT_INFO_H
# define H_PN_LOW_G4_EVENT_INFO_H

# include "app_config.h"
# include "app.hpp"

# ifdef WITH_GEANT4_MODEL

# include <G4VUserEventInformation.hh>
# include <globals.hh>

namespace G4I {

class EventInfo : public G4VUserEventInformation {
public:
    EventInfo();
    ~EventInfo();
    void Print() const {};
    void SetEgamma(G4double e) {Egamma=e;};
    void SetXbeam(G4double val) {Xbeam=val;}
    void SetYbeam(G4double val) {Ybeam=val;}
    void SetProCM(G4double val) {ProCM=val;}
    void SetNreac(G4int val) {nreac=val;}
    void SetNp(G4int val) {np=val;}
    G4double GetEgamma(){return Egamma;}
    G4double GetProCM(){return ProCM;}
    G4double GetXbeam(){return Xbeam;}
    G4double GetYbeam(){return Ybeam;}
    G4int GetNreac(){return nreac;}
    G4int GetNp(){return np;}
private:
    G4double Egamma;
    G4double Xbeam, Ybeam;
    G4int nreac, np;
    G4double ProCM;
};  // class EventInfo

}  // namespace G4I

# endif  // WITH_GEANT4_MODEL

# endif  // H_PN_LOW_G4_EVENT_INFO_H

