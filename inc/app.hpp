# ifndef H_PN_LOW_APP_H
# define H_PN_LOW_APP_H

# include <unordered_map>
# include <unordered_set>
# include <boost/program_options.hpp>

namespace po = boost::program_options;

class Application {
private:
    static Application * _self;
public:
    typedef int (*TaskCallback)( const po::variables_map & vm, int argc, char * argv[] );
    typedef int (*TaskCleaner)();
protected:
    std::unordered_map< std::string, std::pair<std::string, TaskCallback> > _tasks;
    std::unordered_set<TaskCleaner> _tskCleaners;
    int8_t _verbosity;
    std::ostream & _logstream,
                 & _errstream;
public:
    Application();

    po::variables_map configure_command_line( int argc, char * argv[] );

    int run( const po::variables_map & vm,
             int argc, char * argv[] );

    static Application & object();
    static int quench( int runRC );
    /// Supposed to be invoked only by G4 routines.
    static void g4_abort();

    void add_task_callback( const std::string & name,
                            TaskCallback cllb,
                            const std::string & description );
    void add_task_cleaner( TaskCleaner );
    void list_callbacks( std::ostream & );

    uint8_t verbosity() const { return _verbosity; }

    void log1( const std::string &, bool prefix=true );
    void log2( const std::string &, bool prefix=true );
    void log3( const std::string &, bool prefix=true );
    void elog( const std::string &, bool prefix=true );
};

/// Produces string formatted like printf() result.
std::string fpr_format(const char *fmt, ...);

# define alog1( ... ) ( Application::object().log1( __VA_ARGS__ ) )
# define alog2( ... ) ( Application::object().log2( __VA_ARGS__ ) )
# define alog3( ... ) ( Application::object().log3( __VA_ARGS__ ) )
# define aelog( ... ) ( Application::object().elog( __VA_ARGS__ ) )

# endif  // H_PN_LOW_APP_H

