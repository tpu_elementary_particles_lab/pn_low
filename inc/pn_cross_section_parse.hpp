# ifndef H_PN_CROSS_SECTION_PARSER_H
# define H_PN_CROSS_SECTION_PARSER_H

# ifdef PN_LOW_EXPDAT_GEN

# include <map>
# include <root/TH2.h>
# include "wang_approx.tcc"

class TGraph2DErrors;

namespace pnLow {

struct CrossSectionDataPoint {
    double energy, angle;

    double value;
    struct Errors {
        double statistical,
               systematic;
    } errors;
};

int parse_cross_section_data( std::istream & is );

const std::map<double, std::map<double, CrossSectionDataPoint*> > &
get_energies_subsets();

const std::map<double, std::map<double, CrossSectionDataPoint*> > &
get_angular_subsets();

void clear_database();

/// Returns a ROOT's TGraph2DErrors version of crossection data parsed.
/// If nEnergy == nAngle == 0, the default binning grid will be used
/// as a referal. If nEnergy != 0 && nAngle != 0, provided binning
/// parameters will be used, obtaining grid points via
/// Wang's "mir" Taylor approximation procedure.
/// The further use of this object is implied to be an integration
/// function for event generators, where actual approximation is
/// a stright-forward Delaunay trinagulation (see
/// TGraph2DErrors::interpolate()).
/// Note: a Wang's approximation procedure when nEnergy == nAngle == 0
/// will be still applied if parser found any gaps in original grid
/// data set.
TGraph2DErrors * get_graph_repr(  size_t nEnergy=0,
                                  size_t nAngle=0,
                                  bool omitApproximation=false,
                                  double eMin=0, double eMax=0,
                                  double aMin=0, double aMax=0);

/// po::variables_map shortcut for get_graph_repr()
TGraph2DErrors * get_graph_repr_vm( const po::variables_map & );

void print_out_scatter_data( std::ostream & );

WangApproximant<2> &
get_pn_approximant_set();

}  // namespace pnLow

# endif  // PN_LOW_EXPDAT_GEN

# endif  // H_PN_CROSS_SECTION_PARSER_H

