# ifndef H_PN_LOW_PNPI0_KINEMATICS_H
# define H_PN_LOW_PNPI0_KINEMATICS_H

# include "app_config.h"

# ifdef ARENHOVEL_FIX_GEN

# include <root/TLorentzVector.h>

class TGenPhaseSpace;
class TLorentzVector;

namespace pnLow {

class PNPi0Kinematics {
public:
    enum ParticleType {
        cmFrame         = 0,
        forProton       = 1,
        forNeutron      = 2,
        forPion         = 3
    };
private:
    TLorentzVector _ps[4];
public:
    PNPi0Kinematics();
    PNPi0Kinematics( double E_gamma,
                     double T_p, double theta_P, double phi_P,
                     double T_n, double theta_N );
    virtual ~PNPi0Kinematics();

    const TLorentzVector & operator[](ParticleType pt) {
        return _ps[ (uint8_t) pt ];
    }



    // TODO: auxilliary routines for cross-section
    // calculation routines.

    /// Computes pion 4-momentum in CM frame
    static TLorentzVector pion_momentum_example(
            double T_p, double theta_P,
            double T_n, double theta_N );
};

}  // namespace pnLow

# endif  // ARENHOVEL_FIX_GEN

# endif  // H_PN_LOW_PNPI0_KINEMATICS_H

