# ifndef H_PN_LOW_G4_EVEN_ACTION_H
# define H_PN_LOW_G4_EVEN_ACTION_H

# include "app_config.h"
# include "app.hpp"

# ifdef WITH_GEANT4_MODEL

# include <G4UserEventAction.hh>
# include <globals.hh>

# include <root/TTree.h>
# include <root/TFile.h>

# include "g4_RunAction.hh"
# include "g4_DetectorConstruction.hh"

namespace G4I {

class EventAction : public G4UserEventAction {
private:
    G4int hcCollID;
    G4String drawFlag;          // XXX?
    RunAction * _run;           // pointer to the Run
    G4int base_code;
    TFile * outfile;
    TTree * TO;
    G4int nev;                  // event number
    G4int nr;                   // reaction type
    G4float Eg;                 // gamma energy
    G4float vx,vy,vz;           // vertex
    G4float ep,tp,fp;           // proton energy, theta, phi (all zeroes if no proton)
    G4int nhx;                  // number of HC x-bars hits
    G4float hxx[NX_BARS*2],
            hxy[NX_BARS*2],
            hxz[NX_BARS*2],
            hxa[NX_BARS*2];     // x,y,z and amplitude in each x-bar 
    G4int nhz;                  // number of HC z-bars hits
    G4float hzx[NZ_BARS*2],
            hzy[NZ_BARS*2],
            hzz[NZ_BARS*2],
            hza[NZ_BARS*2];     // x,y,z and amplitude in each y-bar 
    G4int nvc;                  // number of vx chamber hits
    G4float vcx[NVC_WRS*2],
            vcy[NVC_WRS*2];     // hit position in VC
    G4int nwa;                  // number of WA (theta-1) chamber hits
    G4float waz[NW1_WRS*2],
            way[NW1_WRS*2];     // hit position in WA
    G4int nwb;                  // number of WB (phi-1) chamber hits
    G4float wbx[NW2_WRS*2],
            wby[NW2_WRS*2];     // hit position in WB
    G4int nwc;                  // number of WC (theta-2) chamber hits
    G4float wcz[NW3_WRS*2],
            wcy[NW3_WRS*2];     // hit position in WC
    G4float ac[2];              // AntiCoin upper,lower
    G4float tof[2];             // TOF upper,lower
    G4float L[2];               // path length upper,lower    
public:
    EventAction(RunAction*);
    virtual ~EventAction();
public:
    virtual void BeginOfEventAction(const G4Event*) override;
    virtual void EndOfEventAction(const G4Event*) override;
};

}  // namespace G4I

# endif  // WITH_GEANT4_MODEL

# endif  // H_PN_LOW_G4_EVEN_ACTION_H

