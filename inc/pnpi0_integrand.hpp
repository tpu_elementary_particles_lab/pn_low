# ifndef H_PNPI0_GENERATOR_INTEGRAND_H
# define H_PNPI0_GENERATOR_INTEGRAND_H

# ifdef ARENHOVEL_FIX_GEN

# include <complex>
# include "root/TMath.h"
# include "root/TFoamIntegrand.h"
# include "root/TLorentzVector.h"
# include "wang_approx.tcc"

/*
 * The Fix-Arenhovel code helps us to figure out differential cross-section
 * for fixed kinematic parameters in form:
 *
 *  (E_gamma, pn1, pn2) -wed-> (Amplitude)
 *
 * Where whole kinematics should be agreed before invokation. Namely, we should
 * to define phase space coordinates {E_gamma, pn1, pn2, pPi0} in order to
 * obtain any probablity estimations. Thic can be reached with Kopilov's so
 * called «K-procedure» or with its equivalent «FOWL» implemented in CERNlib
 * (W505).
 * This code is further generalized and implemented in root TGenPhaseSpace
 * class utilizing GENBOD function of CERNlib (W515).
 * 
 * Since we determine this routine in such a way that E_gamma is an only
 * pre-defined parameter and others should be played of considering fixed
 * equivalent photon energy, actual generator instance should be:
 *
 *  (E_gamma) -> (pn1, pn2, pPi0)
 */

extern "C" {
  //***********************************************************
  //      jm=-1  ! -1 = >pi^-pp;     0 = >pi^0 np;    1 = >pi^+ nn;     Kanal
  //      lWW=2  !  0 = >IA;         1 = >IA+NN;      2 = >IA+NN+PiN;   FSI
  //***********************************************************
  void init_( int & jm,
              int & lWW,
              int & iND);
  void wed_(   int & jm,
            double * kgamma,
            double * qP,
            double * pN1,
            double * pN2,
            double & TKB,
            std::complex<double> TM[3][2][3][3],
            double & fk);
}  // extern "C"

   
namespace pnpi0 {

class FixIntegrand : public TFoamIntegrand {
public:
    FixIntegrand(double e_energy, double e_min, double e_max);
    virtual ~FixIntegrand();
    virtual Double_t Density( Int_t ndim, Double_t * ) override;
    void Init(int jm, int lWW, int iND);
    //Double_t n_virt_phot(Double_t w );
    //TLorentzVector* GetDecay(Int_t i);
    Double_t Get_Eg() {    return eg;}
    Double_t Get_FK() {    return fk;}
    Double_t Get_W0() {    return w0;}      ///< weight due to N_virt_photons
    Double_t Get_W1() {    return w1;}      ///< weigth due to Kopylov's star
    Double_t Get_Ampl(){   return ampl;}    ///< Tmatrix
    Double_t GetDensity(){ return dens;}    ///< return last calculated density

    /// Fix-Arenhovel wed_() routine wrapper.
    static double amplitude( const TLorentzVector & P,      // Overall boost.
                             const TLorentzVector & P_1,    // Nucl. #1 momentum
                             const TLorentzVector & P_2,    // Nucl. #2 momentum
                             double * fk_=nullptr,          // (?) factor
                             int8_t jm_=0                   // channel pi^{$jm}
                           );
protected:
    int jm;  ///< reaction channel
    int lWW; ///< FSI (final state interaction)
    int iND; ///< dibaryon
  
    double ampl2(TLorentzVector& k, TLorentzVector& p1, 
           TLorentzVector& p2, TLorentzVector& q,
           double& fk);

    //StarKop* sk;  //?

    TLorentzVector* P0;
    TLorentzVector* beam;
    TLorentzVector* target;
  
    double Check(double theta,double phi);
    //Dalitz_m* dm;
  
    double dens;
  
private:
    double mpi,     mN1,    mN2,
           m3[3],   eg_min, eg_max;

    double e0,
           lm, lm2, me2,
           fk, w0, w1,eg,ampl;
};

}  // namespace pnpi0

# endif  // ARENHOVEL_FIX_GEN

# endif  // H_PNPI0_GENERATOR_INTEGRAND_H

