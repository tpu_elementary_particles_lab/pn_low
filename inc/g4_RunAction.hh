# ifndef H_PN_LOW_G4_RUN_ACTION_H
# define H_PN_LOW_G4_RUN_ACTION_H

# include "app_config.h"
# include "app.hpp"

# ifdef WITH_GEANT4_MODEL

# include <globals.hh>
# include <G4UserRunAction.hh>
# include <root/TFile.h>

class G4Timer;
class G4Run;

namespace G4I {

class RunAction : public G4UserRunAction {
public:
    RunAction(G4String Fname);
    virtual ~RunAction();

public:
    virtual void BeginOfRunAction(const G4Run* aRun) override;
    virtual void EndOfRunAction(const G4Run* aRun) override;
    TFile* GetFile(){ return _outfile; }
private:
    G4Timer * _timer;
    TFile * _outfile;
};

}  // namespace G4I

# endif  // WITH_GEANT4_MODEL


# endif  // H_PN_LOW_G4_RUN_ACTION_H

