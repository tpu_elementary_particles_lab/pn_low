# ifndef H_SPECIAL_RELATIVISTIC_THEORY_H
# define H_SPECIAL_RELATIVISTIC_THEORY_H

# if 0

# include <root/TMatrixD.h>

/**@brief Utility classes implementing some shortcuts for SR.
 *
 * SR is a special relativistic theory. We need it here since we
 * deal with the relativistic kinematics and need to operate into
 * different inertial frames.
 */

namespace sr {

/// Abstract base holding 'rapidity' parameter that defines caching scheme.
class iCoordinateSystemBase {
private:
    double _r;  ///< rapidity
protected:
    virtual void _V_invalidate_caches() = 0;
    void r( const double & r ) {
        _V_invalidate_caches();
        _r = r;
    }
    double r() const { return _r; }
public:
    iCoordinateSystemBase() : _r(NAN){}
};

/// An inertial frame abstraction.
class InertialFrameUndirected : public iCoordinateSystemBase {
private:
    double _beta,
           _gamma,
           _eta,
           _velocity;
protected:
    virtual void _V_invalidate_caches() override;

    void beta(  double );
    void gamma( double );
    void eta(   double );
public:
    InertialFrameUndirected();

    double beta() const { return    _beta; }
    double gamma() const { return   _gamma; }
    double eta() const { return     _eta; }

    // See CLHEP/Units/SystemOfUnits.
    /// Returns velocity in basic CLHEP's units.
    double velocity_CLHEP() const;
    /// Sets the velocity of frame in basic CLHEP units.
    void velocity_CLHEP( double v );
};

/// Yet another vector (ordinary spatial this time).
class RealVector {
private:
    union {
        double _r[3];
        struct { double x, y, z; } _c;
    };
public:
    RealVector();
    RealVector(double, double, double);

    double norm() const;

    double x() const { return _r[0]; }
    double y() const { return _r[1]; }
    double z() const { return _r[2]; }

    void x( double );
    void y( double );
    void z( double );
    // ...
};

/// Meaningful coordinate system.
class InertialFrame : public InertialFrameUndirected {
private:
    RealVector _betaV;
    const InertialFrame * _base;

    /// Internal ctr -- for base system.
protected:
    InertialFrame( bool );
public:
    /// Produces a base (laboratory) coordinates frame.
    InertialFrame( );
    /// Produces a moving coordinates frame.
    InertialFrame( const RealVector & );
    /// Produces a moving coordinates frame.
    InertialFrame( const InertialFrame &, const RealVector & );

    virtual ~InertialFrame(){}

    /// Returns 'true' for base coordinate frame.
    bool is_base() const;

    /// Returns base coordinate system.
    static const InertialFrame & get_base();

    const RealVector & beta_vector() const { return _betaV; }
    virtual void beta_vector( const RealVector & );

    friend void base_frame_constructor() __attribute__(( __constructor__ ));

    void get_Lorentz_matrix( TMatrixD & L ) const;
};

}  // namespace sr

# endif

# endif  // H_SPECIAL_RELATIVISTIC_THEORY_H

