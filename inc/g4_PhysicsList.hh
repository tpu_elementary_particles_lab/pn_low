# ifndef H_PN_LOW_G4_PHYSICS_LIST_H
# define H_PN_LOW_G4_PHYSICS_LIST_H

# ifdef WITH_GEANT4_MODEL

# include <G4VUserPhysicsList.hh>
# include <globals.hh>

namespace G4I {

class PhysicsList: public G4VUserPhysicsList {
public:
    PhysicsList();
    ~PhysicsList();

protected:
    // mandatory method, invoked by RunManager
    // Instantiates particle types
    void ConstructParticle();
    // mandatory method, invoked by RunManager
    // Inits processes (transportation, EM, decay etc.)
    void ConstructProcess();
    // mandatory method, invoked by RunManager
    void SetCuts();

    void _construct_decay();
    void _construct_EM();
public:
    // sets the default cut value
    void cut_value( const G4double & v);  // XXX: 1mm typically
    // returns current defult cut value
    G4double cut_value() const;
};

}  // namespace G4I

# endif  // WITH_GEANT4_MODEL

# endif  // H_PN_LOW_G4_PHYSICS_LIST_H

