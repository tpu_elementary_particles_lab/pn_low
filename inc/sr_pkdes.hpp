# ifndef H_PARTICLE_KINEMATIC_DESCRIPTION_H
# define H_PARTICLE_KINEMATIC_DESCRIPTION_H

# if 0

# include "app_config.h"
# include "app.hpp"

# include "sr_cs.hpp"

//# include <root/TLorentzVector.h>

namespace sr {

/// Abstract moving particle kinematic description.
class MovingParticle {
protected:
    double _m;
    const InertialFrame & _frame;
    InertialFrame _ownFrame;
public:
    MovingParticle( double mass,
                    const RealVector & velocity,
                    const InertialFrame * frame=nullptr );

    /// Returns description of particle in other inertial frame.
    MovingParticle operator()( const InertialFrame & );

    /// Full energy.
    double E() const;

    /// Kinetic energy.
    double T() const;

    /// Own mass.
    double m() const { return _m; }

    TLorentzVector ROOT_momentum() const;
};

}  // namespace sr

# endif

# endif  // H_PARTICLE_KINEMATIC_DESCRIPTION_H



