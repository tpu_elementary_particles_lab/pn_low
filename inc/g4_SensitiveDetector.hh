# ifndef H_PN_LOW_SENSITIVE_DETECTOR_H
# define H_PN_LOW_SENSITIVE_DETECTOR_H

# include "app_config.h"
# include "app.hpp"

# ifdef WITH_GEANT4_MODEL

# include <G4THitsCollection.hh>
# include <G4VSensitiveDetector.hh>
# include <globals.hh>

# include "g4_Hit.hh"

class G4TouchableHistory;
class G4HCofThisEvent;
class G4Step;

namespace G4I {

class SensitiveDetector : public G4VSensitiveDetector {
public:
    typedef G4THitsCollection<G4I::Hit> Collection;
private:
    Collection           * _collection;
    G4int                * _ids;
public:
    SensitiveDetector();
    virtual ~SensitiveDetector();

    virtual void Initialize(G4HCofThisEvent*) override;
    virtual G4bool ProcessHits(G4Step*,G4TouchableHistory*) override;
    virtual void EndOfEvent(G4HCofThisEvent*) override;
};

}  // namespace G4I

# endif  // WITH_GEANT4_MODEL

# endif  // H_PN_LOW_SENSITIVE_DETECTOR_H

