# ifndef H_PN_LOW_WANG_APPROX_WRAPPER_H
# define H_PN_LOW_WANG_APPROX_WRAPPER_H

# include <cstdint>
# include <cstdlib>
# include <cassert>
# include <iostream>
# include <iomanip>
# include <vector>
# include <cmath>
# include <fstream>  // XXX

# include "mir.h"
# include "app.hpp"

# ifdef NDEBUG
// This macro determines whether or not supplementary debugging
// output will be provided by do_approx() method (see below).
// (DEV)
//# define NO_DBG_SUPP_OUT
# endif

/**@brief A rather simple C++-wrapper for Wang's m/v approximation method.
 *
 * For method details, source and authors, see
 * http://web.stanford.edu/group/uq/pdfs/journals/jcp_scattered_2010.pdf
 *
 * It is not possible to do approximation once and use obtained basis data
 * further for computing points of interest dynamically.
 *
 * Here, I have implemented a pretty simple C++ wrapper around original
 * ANSI C code in order to obtain regular grid in D-dimensional space.
 * */
template<uint8_t D>
class WangApproximant {
public:
    struct Vector {
        double x[D];
    };
    struct DataPoint {
        double x[D],
               v,
               error;
    };
protected:
    double xMin[D],
           xMax[D];
    std::vector<DataPoint> _dataSet;
    std::vector<DataPoint> _poi;  ///< points of interest
public:
    WangApproximant( size_t nDataPointsReserved=0,
                     size_t nPOIReserved=0 ) {
        if( nDataPointsReserved ) {
            reserve_data_set(nDataPointsReserved);
        }
        if( nPOIReserved ) {
            reserve_poi_set(nPOIReserved);
        }
        for( uint8_t d = 0; d < D; ++d ) {
            xMin[d] = xMax[d] = NAN;
        }
    }
    ~WangApproximant( ) {}

    const double * x_min() { return xMin; }
    const double * x_max() { return xMax; }

    void reserve_data_set( size_t n ) {
        _dataSet.reserve(n);
    }

    void reserve_poi_set( size_t n ) {
        _poi.reserve(n);
    }

    void push_data_point( const DataPoint & p ) {
        for( uint8_t d = 0; d < D; ++d ) {
            if( isnan( xMin[d] ) ) {
                xMax[d] = xMin[d] = p.x[d];
            } else if( xMin[d] > p.x[d] ) {
                xMin[d] = p.x[d];
            } else if( xMax[d] < p.x[d] ) {
                xMax[d] = p.x[d];
            }
        }
        _dataSet.push_back( p ); }

    void clear_sets() {
        _dataSet.erase();
        _poi.erase();
    }

    void push_poi( const Vector & p ) {
        DataPoint poi;
        memcpy( poi.x, p.x, sizeof(double)*D );
        _poi.push_back( poi );
    }

    int do_approx( uint8_t taylorOrder=5,
                   uint8_t polynomialExactness=2,
                   double safety=1.,
                   bool logarithmic=false) {
        uint8_t nfunc = 1;  // number of functions for MIR.
        double * poiX = (double *) malloc( sizeof(double)*_poi.size()*D ),        // in
               * poiF = (double *) malloc( sizeof(double)*_poi.size()*nfunc ),    // out
               * datX = (double *) malloc( sizeof(double)*_dataSet.size()*D ),    // in
               * datF = (double *) malloc( sizeof(double)*_dataSet.size() ),      // in
               * datE = (double *) malloc( sizeof(double)*_dataSet.size() ),      // in
               * estimatedError = (double *) malloc( sizeof(double)*_poi.size() ),// out
               beta, gamma;  // internal, heuristic

        double scales[D];
        if( Application::object().verbosity() ) {
            std::cout << "# Scales for Wang's approximant:" << std::endl;
        }
        for( uint8_t d = 0; d < D; ++d ) {
            scales[d] = 1./(xMax[d] - xMin[d]);
            if( Application::object().verbosity() ) {
            std::cout << "#\t" << (int) d
                      << std::setw(16) << xMin[d]
                      << std::setw(16) << xMax[d] << " : "
                      << std::setw(16) << scales[d]
                      << std::endl;
            }
        }

        // fill input arrays (and POI's coords)
        double * datXC = datX,
               * datFC = datF,
               * datEC = datE;

        # ifndef NO_DBG_SUPP_OUT
        std::ofstream ofle1("/tmp/normed_src.dat", std::ofstream::out);
        # endif

        for( auto it = _dataSet.begin(); _dataSet.end() != it; ++it ) {
            for( uint8_t d = 0; d < D; ++d ) {
                *(datXC++) = (it->x[d] - xMin[d])*scales[d];
                # ifndef NO_DBG_SUPP_OUT
                ofle1 << (it->x[d] - xMin[d])*scales[d] << " ";
                # endif
            }
            *(datFC++) = logarithmic ? log(it->v + 1.)
                                     : it->v;
            *(datEC++) = logarithmic ? fabs(log(it->v + 1. + it->error) - log(it->v + 1.))
                                     : it->error;
            # ifndef NO_DBG_SUPP_OUT
            ofle1 << *(datFC-1) << " " << *(datEC-1) << std::endl;
            # endif
        }

        # ifndef NO_DBG_SUPP_OUT
        ofle1.close();
        # endif

        assert( datFC == datF + _dataSet.size() );
        assert( datEC == datE + _dataSet.size() );
        assert( datXC == datX + _dataSet.size()*D );
        double * poiXC = poiX;

        for( auto it = _poi.begin(); _poi.end() != it; ++it ) {
            for( uint8_t d = 0; d < D; ++d, ++poiXC ) {
                *poiXC = (it->x[d] - xMin[d])*scales[d];
            }
        }

        // do approximation
        int rc = mirBetaGamma( nfunc, D,
                      _dataSet.size(), datX, datF, datE,
                      0, NULL, NULL, NULL, /* gradient data empty */
                      taylorOrder, polynomialExactness, safety,
                      &beta, &gamma );
        if( Application::object().verbosity() ) {
            std::cout << "# Wang's approximation method parameters:" << std::endl
                      << "#\ttaylorOrder = " << (int) taylorOrder << "," << std::endl
                      << "#\tpolynomialExactness = " << (int) polynomialExactness << "," << std::endl
                      << "#\tsafety = " << safety << "," << std::endl
                      << "#\tlogarithmic scaling: " << (logarithmic ? "enabled" : "disabled") << std::endl
                      << "# Heuristically obtained:" << std::endl
                      << "#\tbeta = " << beta << "," << std::endl
                      << "#\tgamma = " << gamma << std::endl;
        }
        if( rc == 0 ) {
            assert( _poi.size() );
            assert( _dataSet.size() );
            rc = mirEvaluate( nfunc, D,
                         _poi.size(), poiX,
                         _dataSet.size(), datX, datF, datE,
                         0, NULL, NULL, NULL, /* gradient data empty */
                         beta, gamma,
                         taylorOrder, polynomialExactness,
                         poiF, estimatedError );

            if( !rc ) {
                # ifndef NO_DBG_SUPP_OUT
                std::ofstream ofle3("/tmp/normed_sol.dat", std::ofstream::out);
                # endif

                // fill resulting arrays
                const double * aValC = poiF,
                             * aErrC = estimatedError;
                for( auto it = _poi.begin(); _poi.end() != it; ++it ) {
                    it->v     = logarithmic ? exp(*(aValC++)) - 1
                                            :     *(aValC++);
                    it->error = logarithmic ? exp(*(aErrC++) + *(aValC-1)) - it->v
                                            :     *(aErrC++);

                    # ifndef NO_DBG_SUPP_OUT
                    for( uint8_t d = 0; d < D; ++d, ++poiXC ) {
                        ofle3 << (it->x[d] - xMin[d])*scales[d] << " ";
                    }
                    ofle3 << *(aValC-1) << " " << (aValC-1) << std::endl;
                    # endif
                }

                # ifndef NO_DBG_SUPP_OUT
                ofle3.close();
                # endif
            } else {
                aelog( "Failure on Wang's approximant evaluation routine.\n" );
            }
        } else {
            aelog( "Failed to estimate optimal beta/gamma parameters for Wang's approximant.\n" );
        }

        if( Application::object().verbosity() ) {
            std::cout << "# Wang's approximation done for "
                      << _poi.size()
                      << " points" << std::endl;
        }

        // free heap
        free( poiX );
        free( poiF );
        free( datX );
        free( datF );
        free( datE );
        free( estimatedError );
        return rc;
    }

    void dump_poi_ascii( std::ostream & os ) {
        os << "#";
        {
            char bf[8];
            for( uint8_t d = 0; d < D; ++d ) {
                snprintf( bf, 8, "x_%d", (int) d );
                os << std::setw(16) << bf ;
            }
        }
        os << std::endl;
        os << std::scientific;
        for( auto it = _poi.begin(); _poi.end() != it; ++it ) {
            for( uint8_t d = 0; d < D; ++d ) {
                os << std::setw(16) << it->x[d];
            }
            os  << std::setw(16) << it->v
                << std::setw(16) << it->error
                << std::endl;
        }
        if( _poi.empty() ) {
            os << "# POI set is empty." << std::endl;
        }
    }

    const std::vector<DataPoint> & POIs() {
        return _poi;
    }

    const std::vector<DataPoint> & data_points() {
        return _dataSet;
    }
};  // class WangApproximant

# endif  // H_PN_LOW_WANG_APPROX_WRAPPER_H

