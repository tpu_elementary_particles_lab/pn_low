# ifndef H_PNL_DETECTOR_CONSTRUCTION_H
# define H_PNL_DETECTOR_CONSTRUCTION_H

# include "app_config.h"
# include "app.hpp"

# ifdef WITH_GEANT4_MODEL

# include <G4RotationMatrix.hh>
# include <G4VUserDetectorConstruction.hh>
# include <globals.hh>

// RIA's detector indeces; TODO: make it const-constexpr
# define NSENS       512
# define NX_BARS     19
# define NZ_BARS     17
# define NVC_WRS     42
# define NW1_WRS     18
# define NW2_WRS     18
# define NW3_WRS     27
# define NLQ_BRS     7
# define NCX_BRS     7

# define AC_IND      0                   /* 1 layers */
# define HCX_IND     2                   /* X bars */
# define HCZ_IND     HCX_IND+NX_BARS     /* Z bars */
# define VC_IND      HCZ_IND+NZ_BARS     /* Vertex Ch */
# define WC1_IND     VC_IND+NVC_WRS      /* Drift Chambers */
# define WC2_IND     WC1_IND+NW1_WRS     /* Drift Chambers */
# define WC3_IND     WC2_IND+NW2_WRS     /* Drift Chambers */
# define LQ_IND      WC3_IND+NW3_WRS     /* future use */
# define ECX_IND     LQ_IND+NLQ_BRS      /* X bars ?*/
# define ECZ_IND     ECX_IND+NCX_BRS     /* Z bars ?*/
# define ARM1_IND    0
# define ARM2_IND    256

// FWD G4
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
class G4Element;
class G4VisAttributes;
class G4Box;
class G4Ellipsoid;
class G4Tubs;
class G4SubtractionSolid;
class G4Colour;

namespace G4I {

/// Container class for task-meaningful volumes.
template<typename T>
struct G4VolAggregator {
    T                   * solid;
    G4LogicalVolume     * logv;
    G4VPhysicalVolume   * phys;

    G4VolAggregator() : solid(nullptr),
                        logv(nullptr),
                        phys(nullptr){}

    void clear() {
        // Note: we do not put it into a destructor since
        // Geant4 seems be not architectured with respect
        // of correct cleaning procedures or whatever leads
        // to obscure segmentation failts at application
        // quenching.
        if( solid ) delete solid;
        if( logv )  delete logv;
        if( phys )  delete phys;
    }
};

class DetectorConstructor : public G4VUserDetectorConstruction {
public:
    static const int armOneIndex;
    static const int armTwoIndex;
protected:
    //
    // Materials
    //

    # define for_each_element(m)                         \
        m( _elH , "Hydrogen",  "H",   1,   1.01*g/mole ) \
        m( _elC , "Carbon",    "C",   6,   12.01*g/mole) \
        m( _elN , "Nitrogen",  "N",   7,   14.01*g/mole) \
        m( _elO , "Oxygen",    "O",   8,   16.00*g/mole) \
        m( _elCr, "Chromium",  "Cr",  24,  52.0*g/mole ) \
        m( _elFe, "Iron",      "Fe",  26,  55.85*g/mole) \
        m( _elNi, "Nickel",    "Ni",  28,  58.69*g/mole) \
        m( _elCu, "Copper",    "Cu",  29,  63.55*g/mole) \
        m( _elZn, "Zinc",      "Zn",  30,  65.39*g/mole) \
        m( _elSi, "Silicon",   "Si",  14,  28.1*g/mole ) \
        /* ... */

    # define for_each_simple_material(m)                                              \
        m( _alumin,     "Aluminum",          13,    26.98*g/mole,   2.7*g/cm3)        \
        m( _copper,     "Copper",            29,    63.55*g/mole,   8.96*g/cm3)       \
        m( _gasAr,      "ArgonGas",          18,    39.95*g/mole,   1.98e-03*g/cm3)   \
        m( _titan,      "Titanium",          22,    47.87*g/mole,   4.54*g/cm3)       \
        m( _tungsten,   "Tungsten",          74,    183.84*g/mole,  19.3*g/cm3)       \
        m( _vacuum,     "Vacuum",            1,     1*g/mole,       1.0e-08*g/cm3)    \
        /* ... */

    # define for_each_composite_material(m)                      \
        m( _air,        "Air",               1.29e-03*g/cm3,  2) \
        m( _brass,      "Brass",             8.89*g/cm3,      2) \
        m( _gasArCO2,   "ArgonCarbonicGas",  1.98e-03*g/cm3,  2) \
        m( _gasCO2,     "CarbonicGas",       1.98e-03*g/cm3,  2) \
        m( _iron,       "Iron",              8.89*g/cm3,      1) \
        m( _mylar,      "Mylar",             1.39*g/cm3,      3) \
        m( _scintil,    "Scintillator",      1.032*g/cm3,     2) \
        m( _SiO2,       "SiO2",              2.2*g/cm3,       2) \
        m( _steel,      "Steel",             8.89*g/cm3,      3) \
        /* ... */

    # define declare_element( fieldName, strName, strSym, Am, molarMass ) \
        G4Element * fieldName;
    for_each_element( declare_element )
    # undef declare_element

    # define declare_simple_material( fieldName, strName, Am, molarMass, density ) \
        G4Material * fieldName;
    for_each_simple_material( declare_simple_material )
    # undef declare_simple_material

    # define declare_composite_material( fieldName, strName, density, num ) \
        G4Material * fieldName;
    for_each_composite_material( declare_composite_material )
    # undef declare_composite_material

    # define for_each_colour(m)                      \
        m( _clrAlfoil,      0.9,    0.9,    0.9)     \
        m( _clrAlum,        0.0,    0.8,    0.8)     \
        m( _clrBlackPaper,  0.2,    0.2,    0.2)     \
        m( _clrBrass,       0.75,   0.75,   0.1)     \
        m( _clrConvertor,   0.0,    0.5,    0.5)     \
        m( _clrCopper,      1.0,    0.0,    0.0)     \
        m( _clrGas,         0.93,   0.77,   0.57)    \
        m( _clrIron,        0.5,    0.5,    0.8)     \
        m( _clrMylar,       0.5,    0.2,    0.2)     \
        m( _clrPlastic,     1.0,    0.0,    1.0)     \
        m( _clrSteel,       0.,     0.0,    1.0)     \
        m( _clrStef,        0.742,  0.699,  0.121)   \
        m( _clrTitanfoil,   0.8,    0.8,    0.8)     \
        /* ... */

    # define for_each_visual_attr(m)        \
        m( _vaGas,        _clrGas)          \
        m( _vaFoil,       _clrAlfoil)       \
        m( _vaMylar,      _clrMylar)        \
        m( _vaStef,       _clrStef)         \
        m( _vaSteel,      _clrSteel)        \
        m( _vaIron,       _clrIron)         \
        m( _vaAlum,       _clrAlum)         \
        m( _vaPlastic,    _clrPlastic)      \
        m( _vaConvertor,  _clrConvertor)    \
        m( _vaTitanFoil,  _clrTitanfoil )   \
        /* ... */

    # define declare_colour(fieldName, R, G, B) \
        G4Colour * fieldName;
    for_each_colour( declare_colour )
    # undef declare_colour

    # define declare_visual_attributes_set( fieldName, clr ) \
        G4VisAttributes * fieldName;
    for_each_visual_attr( declare_visual_attributes_set )
    # undef declare_visual_attributes_set

public:
    DetectorConstructor();
    virtual ~DetectorConstructor();

private:
    G4RotationMatrix  // initialized by _init_rotation_matrices() invkd by ctr
        _rot10X,      _rot180X,     _rot180Y,     _rot180Z,
        _rot25X,      _rot270X,     _rot270Y,     _rot270Z,
        _rot45X,      _rot65X,      _rot90X,      _rot90Y,
        _rot90Z,      _rot99X,      _rot9X,       _rot90Y180X,
        _rot90Y180Z,  _rot90X180Z,  _rot45X180Z,  _rot270Y180X,
        RotateNull;

    G4double attLength[NSENS];      // Light attenuation length of scintillators.
    G4double discrThreshold[NSENS]; // Discriminator thresholds - for timing.

    static DetectorConstructor * _self;
protected:
    G4VolAggregator<G4Box> _world;
    struct {
        G4VolAggregator<G4SubtractionSolid> hull;
        G4VolAggregator<G4Tubs> inTube;
        // ... other target stuff
    } _target;
    struct {
        G4LogicalVolume * Theta1_gas,
                        * Phi1_gas,
                        * Theta2_gas;
    } _WC;  // wire chambers
    struct {
        G4LogicalVolume * AC_log, 
                        * RPC_log,
                        * xscint_log,
                        * zscint_log;
    } _scintillators;
    G4LogicalVolume * VCGas_log;
    struct {
        G4double layerX, layerY, layerZ;
        G4double sizeX, sizeY, sizeZ;
    } _sandwhichSizes;
    // TODO: ... stuff
protected:
    void _init_rotation_matrices();

    void _define_materials();
    void _delete_materials();
    void _construct_world( );
    void _delete_world( );
    void _define_colours( );
    void _delete_colours( );
    
    void _task_construct_target();
    void _task_construct_cell();
    void _task_construct_lowq();  // electron detector 'lowQ-9'
    void _task_construct_vertex_chamber(G4LogicalVolume *&);
    void _task_construct_tracking_chambers(G4LogicalVolume *&);
    void _task_construct_hadron_sandwich();
    void _task_construct_target_magnet();

    void _bind_sd();

    G4LogicalVolume * _task_construct_single_tracking_chamber(
            G4double Lwin,
            G4double Wwin,
            G4int ind,
            G4LogicalVolume*& forSD);
    // ... TODO: other stuff here

public:
    static DetectorConstructor * get() { return _self; }

    //// Light attenuation length of scintillators.
    G4double att_length(uint16_t n) const { return attLength[n]; }

    /// Discriminator thresholds - for timing.
    G4double discr_threshold(uint16_t n) const { return discrThreshold[n]; }

    // OVERRIDEN INTERFACE
    virtual G4VPhysicalVolume * Construct() override;
};


}  // namespace G4I

# endif

# endif  // H_PNL_DETECTOR_CONSTRUCTION_H

