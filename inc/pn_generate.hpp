# ifndef H_PN_EVENTS_GENERATOR_H
# define H_PN_EVENTS_GENERATOR_H

# include "app_config.h"

# ifdef PN_LOW_EXPDAT_GEN

# include "app.hpp"
# include "dalitz_yennie.hpp"
# include "pn_cross_section_parse.hpp"
# include <root/TLorentzVector.h>
# include <root/TLorentzRotation.h>

namespace pnLow {

class iPNGenerator {
public:
    struct Event {
        double  omega,      // Equivalent photon energy.
                thetaP;     // Theta of proton in C.M.S.
    };
protected:
    virtual double _V_generate( Event & ) const = 0;
public:
    double generate( Event & eve ) const {
        return _V_generate(eve); }

    virtual ~iPNGenerator(){}

    static int new_generator_instance( const po::variables_map & vm );
    static iPNGenerator & get_generator_instance();
    static void free_generator_instance();
};

/**
 *
 * Note: requires, either "pnCS.gridFile" file specified or that
 * 'pn-parse' task was already executed and
 * cross-section (p,n) database contains data that can be returned
 * by get_energies_subsets() query function.
 * */
class NaivePN : public iPNGenerator {
public:
    typedef iPNGenerator Parent;
    typedef typename Parent::Event Event;
    class FixedEGammaGenerator : public PDEBasedGenerator<1> {
    public:
        typedef PDEBasedGenerator<1> Parent;
        typedef typename Parent::Event Event;
    private:
        TRandom3 * _rndG;
        double _eGamma;
    public:
        FixedEGammaGenerator(
            double eGamma,
            std::map<double, double> &,
            bool noSimulate=false );
        ~FixedEGammaGenerator();
        bool distance( double eGamma ) const { return fabs( eGamma - _eGamma ); }
        double e_gamma() const { return _eGamma; }
        void dump_grid_f_to( std::ostream & ) const;
    };
private:
    DalitzYennieSpectrum                        _eGammaGen;
    std::unordered_set<FixedEGammaGenerator *>  _thetaPGen;
protected:
    FixedEGammaGenerator * _get_nearest_thetaGen( double eGamma ) const;
    virtual double _V_generate( Event & ) const override;
public:
    NaivePN( const po::variables_map & vm );
    ~NaivePN();

    template<typename CallableT>
    void for_all_theta_P_spectrums( CallableT & cllb ) const {
        for( auto it = _thetaPGen.begin(); _thetaPGen.end() != it; ++it ) {
            cllb( **it );
        }
    }
};


/**@brief Draft processor for (p,n) event calculation.
 *
 * Designed as a single-event kinematic processor: returns
 * kinematic variables calculated by single parameter: theta_{c.m.} of
 * proton in (p,n) reaction.
 * */
struct ReactionPN {
    double s, t;
    TLorentzRotation Lambda, Lambda_1;
    struct {
        struct {
            TLorentzVector gamma, deu;
        } inCMFrame, inLabFrame;
    } before;
    struct {
        struct {
            TLorentzVector p, n;
        } inCMFrame, inLabFrame;
    } after;
};

void reveal_pn_kinematics(
        double  gammaE,             // equivalent photon energy
        double  thetaP,             // proton angle in c.m.s.
        ReactionPN & kinematics     // a set of kinematic parameters
    );

}  // namespace pnLow

# endif  // PN_LOW_EXPDAT_GEN

# endif  // H_PN_EVENTS_GENERATOR_H

