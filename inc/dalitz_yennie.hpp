# ifndef H_DALITZ_YENNIE_SPECTRUM_H
# define H_DALITZ_YENNIE_SPECTRUM_H

# include "app_config.h"
# include <cstdint>
# include <root/TRandom3.h>
# include "spectrum.hpp"

namespace pnLow {

/**@brief An Equivalent Photon Approximation spectrum for low scattering angles.
 *
 * This PDF represents an equivalent photons method's spectrum approximation
 * originally proposed by Dalitz and Yennie (Phys. Rev., vol. 105, 1957, p 1598)
 * that is a development of original approach of Weizsacker‐Williams (EPA).
 *
 * Here, we imply a low electron scattering angle low-frequency approximation.
 * For detailed development, see V.M. budnev, Phys. Lett., vol. 15, 1975, p 182.
 * All calculations here are provided according to formula (6.19d) at p. 263.
 *
 * Note, that omega here has sense of equivalent photon energy and measured in
 * MeVs.
 * */
class EPA : public PDF<1, double> {
private:
    double _Ebase;
    double _eGammaMin,
           _scale;  ///< E_{\gamma} range -- determined by practical conditions.
protected:
    double _V_density( const Vector & omega ) const override;
public:
    EPA();
    EPA( double newEbase );
    double E_base() const;
    void E_base( double newE );
    void set_E_gamma_limits( double, double );
    void denorm_E_gamma( Vector & ev ) const;
    double E_gamma_min() const { return _eGammaMin; }
    double E_gamma_max() const { return _eGammaMin/(1. - 1./_scale); }
};


class DalitzYennieSpectrum : public PDEBasedGenerator<1> {
public:
    typedef PDEBasedGenerator<1> Parent;
    typedef typename Parent::Event Event;
private:
    TRandom3 * _rndG;
public:
    DalitzYennieSpectrum( const po::variables_map & vm );
    ~DalitzYennieSpectrum();

    EPA & get_EPA_density_function() {
        return static_cast<EPA&>(pdf()); }
    const EPA & get_EPA_density_function() const {
        return static_cast<const EPA&>(pdf()); }

    /// Generates and writes event at given instance. Returns event's weight.
    virtual double generate( Event & ev ) const {
        double weight = Parent::generate( ev );
        static_cast<const EPA&>(pdf()).denorm_E_gamma( ev.v[0] );
        return weight;
    }
};

}  // namespace pnLow

# endif  /* H_DALITZ_YENNIE_SPECTRUM_H */

