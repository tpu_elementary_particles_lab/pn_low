# ifndef H_PN_LOW_G4_PRIMARY_GENERATOR_ACTION_H
# define H_PN_LOW_G4_PRIMARY_GENERATOR_ACTION_H

# include "app_config.h"
# include "app.hpp"

# ifdef WITH_GEANT4_MODEL

# include <root/TVector3.h>
# include <root/TRotation.h>

# include <G4VUserPrimaryGeneratorAction.hh>

# include "pn_generate.hpp"

class G4ParticleGun;
class G4ParticleDefinition;
class G4Event;
class TFile;
class TTree;

namespace G4I {

class PrimaryGeneratorAction
        : public G4VUserPrimaryGeneratorAction {
private:
    G4ParticleGun * _pGun;
    TFile * _evFile;
    TTree * _evTree;

    G4ParticleDefinition * _pdP, 
                         * _pdN,
                         * _pdPi0;
    uint64_t _nEntry;
    pnLow::iPNGenerator::Event _pnEvent;
    double _phiRange;
    TRotation _primaryRotation;
public:
    /// Creates generator with given event file.
    PrimaryGeneratorAction( const std::string & evFile,
                            double phiRange );
    ~PrimaryGeneratorAction();
public:
    virtual void GeneratePrimaries(G4Event* anEvent) override;

    void generate_pn_event(G4Event* anEvent, const TVector3 &);
    //void generate_pnpi0_event(G4Event* anEvent, const TVector3 &);
};

}  // namespace G4I

# endif

# endif  // H_PN_LOW_G4_PRIMARY_GENERATOR_ACTION_H

